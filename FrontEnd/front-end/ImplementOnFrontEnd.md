```Typescript
// ==========Below need to go into the component===========

const [date, setDate] = useState(new Date());
const [hour, setHour] = useState(0);
const [minute, setMinute] = useState(0);

const clickHandler = async (date: any) => {
    // Set time in new date according to current states
    const defineTime = date.setHours(hour, minute, 0);
    // Convert UTC back in object
    const newDate = new Date(defineTime);
    // Update date state
    setDate(newDate);
};

  // Trigger each change from time picker
const timeChangeHandler = async (time: any) => {
    // Get value from minute
    const minute = time.minute();
    // Get value from hour
    const hour = time.hour();
    // Set time in new date according to current states
    const defineTime = date.setHours(hour, minute, 0);
    // Convert UTC back in object
    const newTime = new Date(defineTime);
    // Set new states
    setHour(hour);
    setMinute(minute);
    setDate(newTime);
}
// ==========Above need to go into the component===========


// ==========Below need to go into thunk===========
// =================When submitting data to BackEnd
// Upon Submission, submit the date object directly
    const obj = {
      date: newDate,
    };

// =================When fetching data from BackEnd
const result = await fetch("http://localhost:8080/soonExpireOffers");
const dataList = await result.json();
    if (result.status === 200) {
        const processedList = []
        // Loop data list from BackEnd
        for (let date of dataList) {
        // Get expiry_date from each data 
        const expiry_date: number = date.expiry_date
        // Create new date object (this is NodeJS)
        const newDate = new Date(expiry_date);
        // Change date value in each data set so it's shown as local time
        date.expiry_date = newDate
        // Push to processed dataset list
        processedList.push(date)
        }
        // Then dispatch to redux store and should re-render the state 
        setListDate(processedList)
    }
};
// ==========Above need to go into thunk===============

// ==========Below need to go into component===========
    <DayPicker onDayClick={clickHandler} />
    <TimePicker format={format} onChange={timeChangeHandler} />
// ==========Above need to go into the component=======
```