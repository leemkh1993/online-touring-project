import { Button } from "@material-ui/core";
import React from "react";
import { Link } from "react-router-dom";

const MyOrdersPage: React.FC = () => {
  return (
    <div>
      <h1>This is My Orders Page</h1>
      <Link to="videoStart" className="link">
        <Button href="#text-button" color="primary">
          Start Session
        </Button>
      </Link>
    </div>
  );
};

export default MyOrdersPage;
