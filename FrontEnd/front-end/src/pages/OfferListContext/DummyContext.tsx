import React, { createContext, useState } from 'react';
import { dummyProducts } from './dummy';
// import OfferListPage from "../store/OfferListPage";
import OfferListGrid from "../OfferListPage/OfferListGrid";

export const DummyContext = createContext({} as any);



const DummyContextProvider = () => {

    const [products] = useState(dummyProducts);
    // console.log(products)
    return ( 
        <DummyContext.Provider value={{products}} >
            <OfferListGrid/>
        </DummyContext.Provider>
     );
}
 
export default DummyContextProvider;