import React from "react";
import ServiceFlow from "../components/AboutPage/ServiceFlow";
import AboutPageText from "../components/AboutPage/AboutPageText";
import { Paper, makeStyles } from "@material-ui/core";
import DownloadAppBanner from "../components/HomePage/DownloadAppBanner";

const useStyles = makeStyles((theme) => ({
  pageContent: {
    margin: theme.spacing(8),
    padding: theme.spacing(3),
  },
}));

const AboutPage: React.FC = () => {
  // const [loginUser, setLoginUser] = useState("");
  const classes = useStyles();
  return (
    <React.Fragment>
      {/* <h1>This is About Page</h1> */}
      <Paper className={classes.pageContent}>
        <AboutPageText />
        <ServiceFlow />
        <DownloadAppBanner />
      </Paper>
    </React.Fragment>
  );
};

export default AboutPage;
