//package
// import React,{useState} from "react";
import React, { useState, useEffect } from "react";
import { IRootState } from "../../redux/store";
import { useSelector, useDispatch } from "react-redux";
import { getOneOfferThunk } from "../../redux/browseOffer/thunks";
import { getUserInfoThunk } from "../../redux/browseUser/thunks";
import useReactRouter from "use-react-router";

//internal srce component
import OfferIdCore from "./OfferIdCore";
import OfferIdAbtSharer from "./OfferIdAbtSharer";
import PaypalButton from "../../components/PaypalButton";

//external srce component
// import DayPicker from 'react-day-picker';
import DayPickerDummy from "./DayPickerV1";
import TimePicker from "react-time-picker";

//css
import gird_styles from "../../css/OfferPage/OfferId.module.css";
import element_styles from "../../css/OfferPage/OfferId.module.css";
import Button from "@material-ui/core/Button";
import "react-day-picker/lib/style.css";
import { getUserProfileThunk } from "../../redux/getPersonalUserRecords/thunk";

const OfferIdGrid: React.FC = () => {
  //offers info using thunk
  const dispatch = useDispatch();
  // const {match:{params:{id}}} = useReactRouter();
  // fetch sharer&offer info
  const params = useReactRouter().match.params as { id: string };
  const offerIdItems = useSelector(
    (state: IRootState) => state.browseOffer.offerIdItems
  );
  const userInfos = useSelector(
    (state: IRootState) => state.browseUser.userInfo
  );
  const userId = offerIdItems.map((offerIdItem: any) => {
    return offerIdItem.offer.guide_id;
  });
  // console.log(userId);
  // console.log(typeof userId);

  useEffect(() => {
    dispatch(getOneOfferThunk(parseInt(params.id)));
    dispatch(getUserInfoThunk(parseInt(userId[0])));
  }, [dispatch, params.id, userId[0]]);
  //dummy ver.
  // const testing = 0;
  // console.log(offerIdItems);

  //own user_id for payment using thunk
  const userProfileItems = useSelector(
    (state: IRootState) => state.getPersonalUserRecords.userProfile
  );
  useEffect(() => {
    dispatch(getUserProfileThunk());
  }, [dispatch]);
  // console.log(userProfileItems);
  const getID = userProfileItems.map((userProfileItem: any) => {
    const user_id = userProfileItem.user.id;
    return user_id;
  });
  // console.log(getID[0]);

  //paypal btn logic
  const [showPaypalBtn, setShowPaypalBtn] = useState(false);
  const showPaypalButtons = () => {
    setShowPaypalBtn(!showPaypalBtn);
    // console.log(showPaypalBtn);
    return showPaypalBtn;
  };

  //date-picker logic
  const [date, setDate] = useState(new Date());
  const [hour, setHour] = useState(0);
  const [minute, setMinute] = useState(0);
  //detect selected date before join tour
  const [selectedDate, setSelectedDate] = useState(false);

  const clickHandler = async (date: any) => {
    // Set time in new date according to current states
    const defineTime = date.setHours(hour, minute, 0);
    // Convert UTC back in object
    const newDate = new Date(defineTime);
    // Update date state
    setDate(newDate);
    setSelectedDate(true);
    // console.log(newDate);
  };
  // Trigger each change from time picker
  const timeChangeHandler = async (time: any) => {
    // Get value from minute
    const minute = time.substring(3, 5);
    // console.log(minute);
    // Get value from hour
    const hour = time.substring(0, 2);
    // console.log(hour);
    // Set time in new date according to current states
    const defineTime = date.setHours(hour, minute, 0);
    // Convert UTC back in object
    const newTime = new Date(defineTime);
    // Set new states
    setHour(hour);
    setMinute(minute);
    // console.log(newTime);
    setDate(newTime);
  };

  return (
    <div className={`${gird_styles.grid_wrapper} ${element_styles.flex_wrap}`}>
      <div className={gird_styles.core}>
        {offerIdItems.map((offerIdItem: any) => (
          <OfferIdCore key={offerIdItem.offer.offer_id} value={offerIdItem} />
        ))}
        {/* <OfferIdCore key={0} value={testing}/> */}
      </div>
      <div className={gird_styles.side}>
        <div
          className={`${gird_styles.date_picker} ${element_styles.min_height}`}
        >
          Calender
          {/* <DayPicker onDayClick={clickHandler}/> */}
          <DayPickerDummy
            selectedDate={selectedDate}
            dateClickedFn={clickHandler}
          />
          <TimePicker
            onChange={timeChangeHandler}
            value={date}
            clearIcon={null}
            className={element_styles.picker__wrapper}
          />
        </div>
        <br />
        <div className={gird_styles.sharer}>
          {offerIdItems.map((offerIdItem: any) => (
            // userInfos.map((userInfo:any)=>(
            <OfferIdAbtSharer
              key={offerIdItem.offer.guide_id}
              value={userInfos}
            />
            // ))
          ))}
        </div>
        <div className={gird_styles.payment}>
          {/* showPaypalBtn|| */}
          {/* {showPaypalBtn?
                        <PaypalButton /> : <Button className={element_styles.Button} onClick={showPaypalButtons}> Join this tour </Button>
                    } */}
          {selectedDate ? (
            showPaypalBtn ? (
              offerIdItems.map((offerIdItem: any) => (
                <PaypalButton
                  key={offerIdItem.offer.offer_id}
                  value={offerIdItem}
                  startDate={date}
                  own_user_id={getID[0]}
                />
              ))
            ) : (
              <Button
                className={element_styles.Button}
                onClick={showPaypalButtons}
              >
                {" "}
                Join this tour{" "}
              </Button>
            )
          ) : (
            <Button className={element_styles.Button} onClick={() => {}}>
              {" "}
              Select a day{" "}
            </Button>
          )}
        </div>
      </div>
    </div>
  );
};

export default OfferIdGrid;
