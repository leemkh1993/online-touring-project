import React from "react";
import Map from "../../components/OfferIDPage/GoogleMapList";
import normal_styles from "../../css/OfferPage/OfferId.module.css";
import { Link } from "react-router-dom";

const { REACT_APP_API_SERVER } = process.env;

interface IOfferIdProps {
  // key: number
  value: any;
}


const OfferIdCore = (props:IOfferIdProps)=>{
    // console.log(props.value);
    
    let sum = [0,0,0];
    props.value.audience_reviews? (sum=(props.value.audience_reviews.map((audience_review)=>{
        return audience_review.rating;
    }))):sum=[0,0,0];
    const toNumbers = arr => arr.map(Number);
    const SUM = toNumbers(sum).reduce((a:number,b:number)=>{
        return a+b
    },0);
    // console.log(SUM);
    return(
        <div className={normal_styles.padding}>
            <img  className="img-fluid" 
                    src={`${REACT_APP_API_SERVER}/cast_vacation_pic/${props.value.offer.offer_journey_pic}`} alt=""/>
            <h1 className={normal_styles.h1}>{props.value.offer.offer_name}</h1>
            <h3 className={normal_styles.h3}>"{props.value.offer.offer_description}"</h3>
            
            <p className={normal_styles.price_row}>
                <span className={normal_styles.price}>
                  Price: US$
                  <span className={normal_styles.pt} >
                  {props.value.offer.offer_cost} 
                  </span> 
                </span>
            </p>
            
            
            <p className={normal_styles.rating}>
                Rating:
                {
                   props.value.audience_reviews?
                  (<span className={normal_styles.pt} style={{color: parseInt((
                    SUM/(props.value.audience_reviews? (props.value.audience_reviews.length):1)
                    ).toFixed(1)) >= 3.5 ? "green" : "red"}}
                  >
                    {(SUM/props.value.audience_reviews.length).toFixed(1)} 
                  </span>)
                  :"No review yet"
                } 
                  / Reviewed nos.:
                {
                  props.value.audience_reviews?
                  (props.value.audience_reviews.length):"0"
                }
            </p>
            
                

            <h4 className={normal_styles.head_border}>About this Journey</h4>

      {props.value.destinations.map((destination: any) => (
        <div
          key={destination.latitude}
          className={normal_styles.content_container}
        >
          <img
            // className="img-fluid"
            style={{
              // display: "block",
              // maxWidth: "408px",
              // minWidth: "408px",
              // width:"408px",
              // height:"500px",
              // border: "1px",
              // objectFit:"contain",
            }}
            className={`${normal_styles.pic_box} img-fluid`}
            src={`${REACT_APP_API_SERVER}/cast_sites_pic/${destination.site_pic}`}
            alt=""
          />
          <div className={normal_styles.text_box}>
            <div>
              <div className={normal_styles.google_map}>
                {/* google map */}
                <Map 
                  lat={destination.latitude}
                  lng={destination.longitude}
                  address={`${destination.site_name},${destination.city},${destination.country}`}
                />
              </div>
              <p className={normal_styles.site_title}>
                {" "}
                
                <span className={normal_styles.site_name}>
                  {destination.site_name}
                </span>{" "}
                in {destination.city}
              </p>

              <p className={normal_styles.description}>
                "{destination.description}"
              </p>
            </div>
          </div>
        </div>
      ))}

      <h4 className={normal_styles.head_border}>
        Add-ons/What further brings you
      </h4>
      <div className={normal_styles.content_container2}>
        <div className={normal_styles.detail_text}>
          "{props.value.offer.offer_description_details.split("   ").map((para, key)=>{
            return <div key={key} className={normal_styles.detail_words}>{para}</div>
          })}
          <span className={normal_styles.end_speech}>"</span>

        </div>
      </div>

      <h4 className={normal_styles.head_border}>Reviews on Sharer</h4>
      <div className={normal_styles.center}>
        {
          props.value.audience_reviews?
            (props.value.audience_reviews.map((Review:any)=>(
              <div className={normal_styles.review_box}>
                <div className={normal_styles.review_box}>
                  <img  className={`img-fluid ${normal_styles.rating_pic}`} 
                            src={`${REACT_APP_API_SERVER}/cast_profile_pic/${Review.audience_profile_pic}`} alt=""/>
                  <div className={normal_styles.review_details_box}>
                    <div>
                      Participant: {Review.audience_first_name} {Review.audience_last_name}
                    </div>
                    <div className={normal_styles.comments}>
                      "{Review.comments}"
                    </div>
                    <div className={normal_styles.rating_pt}>
                      <div>{new Date(Review.created_at).toLocaleString()}</div>
                    </div>
                  </div>
                </div>
                <div className={normal_styles.ptt}>
                  <div>Rating: <span style={{color: parseInt(Review.rating) >= 3.5 ? "green" : "red"}} >{Review.rating === null? "0":Review.rating}</span></div>
                </div>
              </div>
            )))
            :" "
        }
      </div>
      <div>
        <h4 className={normal_styles.head_border}>
          tags
        </h4>
        <div className={normal_styles.hash_tag_box}>
          {props.value.hashtag.map((tag: string) => (
            <p className={normal_styles.tag_style}>{tag}</p>
          ))}
        </div>
        
        <Link to="/list">
          <p>Previous page</p>
        </Link>
        {/* <p>Follow/Contact</p> */}
      </div>
    </div>
  );
};

export default OfferIdCore;
