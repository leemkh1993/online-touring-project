import React from "react";
import styles from "../../css/OfferPage/OfferId.module.css";

const { REACT_APP_API_SERVER } = process.env;

interface IUserInfoProps {
  value: any;
}

const OfferIdAbtSharer = (props: IUserInfoProps) => {
  // console.log(props.value);
  return (
    <div className={styles.padding}>
      <div className={styles.padding}>About the Sharer</div>
      {/* <div className={styles.padding}>Profile Pic</div> */}
      <img
        className="img-fluid"
        style={{
          display: "block",
          margin: "0 auto 10px",
          maxHeight: "170px",
          minHeight: "170px",
          border: "1px",
        }}
        src={`${REACT_APP_API_SERVER}/cast_profile_pic/${props.value.profile_pic}`}
        alt=""
      />
      {/* <div>Sharer's say</div> */}
      {/* <div className={styles.about_me}>Hi I am Cleland Colette the Ethnologist in England. Happy share with u my knowledge to my country.</div> */}
      <div className={styles.about_me}>
        {" "}
        "Hi I am {props.value.first_name} the {props.value.profession} in{" "}
        {props.value.country}. {props.value.description}"
      </div>
    </div>
  );
};

export default OfferIdAbtSharer;
