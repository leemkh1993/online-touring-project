import React, { useEffect } from "react";
import styles from'../../css/OfferPage/OfferId.module.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import OfferIdGrid from "./OfferIdGrid";

const OfferListPage: React.FC = () => {

    //scroll to top
    useEffect(() => {
        window.scrollTo(0, 0)
      }, [])
      
    return (
        <div className={`${styles.ListContainer}`}>
            
            <OfferIdGrid/>
            
        </div>


    )
};

export default OfferListPage;
