import React from "react";
import  "./../../src/App.css";
import SearchTagBar from "../components/HomePage/searchBar/SearchTagBar";
import LastCallOffersCarousel from "../components/HomePage/LastCallCarousel/LastCallOffersCarousel";
import GridWrapUserCard from "../components/HomePage/UserCard/GridWrapUserCard";
import DownLoadApp from "../components/HomePage/DownloadAppBanner";
import ScrollUpButton from "react-scroll-up-button";
// import MultiCarouselPage from "../components/HomePage/ViewOffer/MultiCarouselPage";

// import DownloadAppBanner from "../components/DownloadReactApp/DownloadApp";
// import SingleUserCard from "../components/HomePage/SingleUserCard";
import GridWrapOffers from "../components/HomePage/ViewOffer/GridWrapOffers";
// import NewLayout from "../components/JitsiPage/NewLayout";

//old
// import ListedOfferButtonPush from "../components/ViewAllOfferPage/ListedOfferButtonPush";
//for test each offer
// import OfferssOnWaitingList from "../components/HomePage/OffersOnWaitingList";
// import SearchHashtagsInOneGo from "../components/HomePage/searchBar/SearchHashtagsInOneGo";

const HomePage: React.FC = () => {
  return (
    <>
    <section style={{background: "#ffffff !important"}}>
      <div  className="screen_left">
        {/* <h1>This is Home Page </h1> */}
        <SearchTagBar />
        {/* <MultiCarouselPage /> */}
        {/* <NewLayout /> */}
        {/* <SearchHashtagsInOneGo /> */}
        {/* will come back later to edit slides display*/}
        <LastCallOffersCarousel />
        {/* all the offers */}
        <GridWrapOffers />
        <GridWrapUserCard />
        <ScrollUpButton />
        {/* <DownloadAppBanner /> */}
        <DownLoadApp />
      </div>
      </section>
    </>
  );
};

export default HomePage;
