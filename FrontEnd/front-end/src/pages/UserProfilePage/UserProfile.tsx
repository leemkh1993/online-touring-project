import React from "react";
import styles from '../../css/UserProfile/UserProfile.module.css';
// const { REACT_APP_API_SERVER } = process.env;

interface IProfileProps1{
    key: number
    value:any
}

const UserProfile = (props:IProfileProps1)=>{
    // console.log(props.value.user.first_name);
    return(
        <div className={styles.user_box}>
            
            <div >
                <div>Username: </div>
                <div className={styles.enter_box}>{props.value.user.first_name}</div>
            </div>
            <div>
                <div>Email: </div>
                <div className={styles.enter_box}>{props.value.user.email}</div>
            </div>
            <div>
                <div>Introduction:</div>
                <div className={styles.enter_box}>{props.value.user.description}</div>
            </div>
            <div>
                <div>Living country:</div>
                <div className={styles.enter_box}>{props.value.user.country}</div>
            </div>
            <div>
                <div>Profession:</div>
                <div className={styles.enter_box}>{props.value.user.profession}</div>
            </div>
            {/* <div>
                Age: {props.value.user.range}
            </div> */}
            <div style={{
                    marginTop:"20px",
                    
                    }}>

            </div>

        </div>
    )
}

export default UserProfile;