import React from "react";
// import styles from "../../css/UserProfile/UserProfile.module.css";

import UserProfileTabs from "./UserProfileTabs";

const UserProfilePage: React.FC = () => {
  return (
    <div>
      <UserProfileTabs />
    </div>
  );
};

export default UserProfilePage;
