import React, { useState } from "react";
import {Link} from 'react-router-dom';
import CommentBox from "../../components/CommentBox";
import styles from '../../css/UserProfile/UserProfile.module.css';
import "../../css/OfferPage/cancel-hyperlink-style.css";
import moment from "moment";

const { REACT_APP_API_SERVER } = process.env;

interface IProfileProps3{
    key: number
    value:any
}


const UserMyOrder = (props:IProfileProps3)=>{

    const [showCommentBox, setShowCommentBox] = useState(Array(props.value.paymentRecords.payeeRecord.length).fill(false));
    const showBox = (index:number) => {
        const newBtn = showCommentBox.slice();
        newBtn[index] = true;
        setShowCommentBox(newBtn);
        // console.log(showCommentBox);
        return showCommentBox;
    };
    


    return(
        <div>
        
        {   props.value.paymentRecords.payeeRecord !== null?
            (props.value.paymentRecords.payeeRecord.map((paymentRecord:any,index:number)=>(
                <div className={styles.order_container2}>
                    <div className={styles.margin}>
                        <div className={`${styles.record_container} ${styles.offer_details}`}>
                            <div className={styles.status}>
                                <div className={styles.pay_items}>
                                    <div>Order Date </div>
                                    <div>{new Date(paymentRecord.updated_at).toLocaleDateString("en")}</div>
                                    {/* <div>{new Date(paymentRecord.order_start_date).toLocaleString()}</div> */}
                                </div>
                                <div className={styles.pay_items}>
                                    <div>Total</div>
                                    <div>US${paymentRecord.payment_amount}</div>
                                </div>
                            </div>
                            <div className={styles.pay_items}>
                                <div className={styles.pay_title}>Order No.:{paymentRecord.order_id}</div>
                                <div className={styles.status}>
                                    <div className={styles.pay_items}>
                                        <div>transaction status:</div>
                                        <div className={styles.completed}>Completed</div>
                                    </div>
                                    <div className={styles.pay_items}>
                                        <div>transaction time:</div>
                                        <div> {new Date(paymentRecord.updated_at).toLocaleTimeString('en',{ hour: 'numeric',hour12: true, minute: '2-digit',second:"2-digit" })} </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className={`${styles.offer_details} ${styles.offer_details_flex}`}>
                            <div className={styles.flex}>
                                <div className={styles.picture}> 
                                    <img style={{display: "block", margin: "0 auto 10px", maxHeight: "200px", minHeight:"150px",border:"1px"}} className="img-fluid" 
                                        src={`${REACT_APP_API_SERVER}/cast_vacation_pic/${paymentRecord.joined_tour_pic}`} alt=""/>
                                </div>
                                <div className={styles.offer_details}>
                                    <div>
                                        {`Sharer: ${paymentRecord.recipient_first_name} 
                                        ${paymentRecord.recipient_last_name}`}
                                    </div>
                                    <div className={styles.in_one_line}>
                                        <div>{"Selected offer :"}</div>
                                        <Link to={`/list/detail/${paymentRecord.offer_id}`}>
                                            <span className={styles.link}> {paymentRecord.joined_tour_name}</span>
                                        </Link>
                                    </div>
                                    <div>{`Request time : ${new Date(paymentRecord.start_date).toLocaleString('en',{ year: 'numeric',  month: 'numeric',  day: 'numeric',hour: 'numeric',hour12: true, minute: '2-digit' })}`}</div>
                                    {/* to ${new Date(paymentRecord.end_date).toLocaleTimeString()} */}
                                    {/* <div>{paymentRecord.joined_tour_rm_key}</div> */}
                                    <div>
                                        {/* {`Requested time : From ${new Date(paymentRecord.start_date).toLocaleString()} to 
                                        ${new Date(paymentRecord.end_date).toLocaleString()}`} */}
                                    </div>
                                </div>
                            </div>
              <div className={styles.button_part}>
              {/* 200000000 */}
                    {
                        (new Date(paymentRecord.start_date).getTime() - new Date().getTime()) >= -3600000 && 
                        (new Date(paymentRecord.start_date).getTime() - new Date().getTime()) <= 0 ? 
                        <Link to={
                            {
                                pathname: "/videoStart",
                                state: {roomKey:`${paymentRecord.joined_tour_rm_key}`,
                                        name:`${props.value.user.first_name} ${props.value.user.last_name}`}
                            }
                        }>
                            <div className={styles.button}>Enter into room</div>
                        </Link>
                        :
                        (
                            (new Date(paymentRecord.start_date).getTime() - new Date().getTime()) <= -3600000 ?
                            <div className={styles.button_closed}>Closed</div>
                            :
                            <div className={styles.button_wait}>{moment(new Date(paymentRecord.start_date).getTime()).fromNow()}</div>
                        )
                        
                    }
                
                <div className={styles.button} onClick={()=>showBox(index)}>
                    {showCommentBox[index]? <div><CommentBox user={props.value.user} audienceID={props.value.user.id} orderID={paymentRecord.order_id}/>Leave comment</div>:"Leave comment"}
                </div>
              </div>
            </div>
          </div>
        </div>
      ))):""
    }
    </div>
  );
};

export default UserMyOrder;
