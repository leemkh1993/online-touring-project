import React from "react";
import {Link} from "react-router-dom"
import styles from '../../css/UserProfile/UserProfile.module.css';
import "../../css/OfferPage/cancel-hyperlink-style.css";
import moment from "moment";

const { REACT_APP_API_SERVER } = process.env;

interface IProfileProps{
    key: number
    value:any
}

const UserAudienceOrders = (props:IProfileProps)=>{
    // console.log(new Date(props.value.offerRecords[0]).toLocaleString());
    return(
        <div>
        {
            props.value.orderRecords !== null?
        
            (
                props.value.orderRecords.map((orderRecord:any)=>(
                    <div className={styles.order_container}>
                        <div className={styles.margin}>
                            <div className={`${styles.record_container} ${styles.offer_details}`}>
                                <div className={styles.status}>
                                    <div className={styles.pay_items}>
                                        <div>Order Date </div>
                                        <div>{new Date(orderRecord.updated_at).toLocaleDateString("en")}</div>
                                        {/* <div>{new Date(orderRecord.order_start_date).toLocaleString()}</div> */}
                                    </div>
                                    <div className={styles.pay_items}>
                                        <div>Total</div>
                                        <div>US${orderRecord.cost}</div>
                                    </div>
                                </div>
                                <div className={styles.pay_items}>
                                    <div className={styles.pay_title}>Order No.:{orderRecord.order_id}</div>
                                    <div className={styles.status}>
                                        <div className={styles.pay_items}>
                                            <div>transaction status:</div>
                                            <div className={styles.completed}>Completed</div>
                                        </div>
                                        <div className={styles.pay_items}>
                                            <div>transaction time:</div>
                                            <div> {new Date(orderRecord.updated_at).toLocaleTimeString('en',{ hour: 'numeric',hour12: true, minute: '2-digit',second:"2-digit" })} </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div className={`${styles.offer_details} ${styles.offer_details_flex}`}>
                                <div className={styles.flex}>
                                    <div className={styles.profile_picture}>
                                        
                                        <img style={{display: "block", margin: "0 auto 10px", maxHeight: "100px", maxWidth:"100px",border:"1px",boxShadow: "1px 1px 5px 3px rgba(160, 134, 219, 0.295",marginTop:"13px"}} className="img-fluid" 
                                            src={`${REACT_APP_API_SERVER}/cast_profile_pic/${orderRecord.audience_profile_pic}`} alt=""/>
                                    </div>
                                    <div className={styles.offer_details}>
                                        <div>
                                            {`Audience: ${orderRecord.audience_first_name} 
                                            ${orderRecord.audience_last_name}`}
                                        </div>
                                        <div className={styles.in_one_line}>
                                            <div>{"Selected offer :"}</div>
                                            <Link to={`list/detail/${orderRecord.offer_id}`}>
                                                <span className={styles.link}> {orderRecord.name}</span>
                                            </Link>
                                        </div>
                                        {/* <div>{`Selected offer: ${orderRecord.name}`}</div> */}
                                        <div>
                                            {`Request time : ${new Date(orderRecord.order_start_date).toLocaleString('en',{ year: 'numeric',  month: 'numeric',  day: 'numeric',hour: 'numeric',hour12: true, minute: '2-digit' })}`}
                                            {/* to ${new Date(orderRecord.order_end_date).toLocaleTimeString()}` */}
                                        </div>
                                        {/* <div>{orderRecord.offer_room_key}</div> */}
                                    </div>
                                </div>
                                <div className={styles.button_part}>
                                    
                                    {
                                        (new Date(orderRecord.order_start_date).getTime() - new Date().getTime()) >= -3600000 && 
                                        (new Date(orderRecord.order_start_date).getTime() - new Date().getTime()) <= 3600000 ? 
                                        <Link to={{
                                            pathname: "/videoStart",
                                            state: {roomKey:`${orderRecord.offer_room_key}`,
                                            name:`${props.value.user.first_name} ${props.value.user.last_name}`}
                                        }}>
                                            <div className={styles.button}>Enter into room</div>
                                        </Link>
                                        :
                                        (
                                            (new Date(orderRecord.order_start_date).getTime() - new Date().getTime()) <= -3600000 ?
                                            <div className={styles.button_closed}>Closed</div>
                                            :
                                            <div className={styles.button_wait}>{moment(new Date(orderRecord.order_start_date).getTime()).fromNow()}</div>
                                        )
                                    }
                                    {/* <div className={styles.button}>Leave comment</div> */}
                                </div>
                            </div>
                        </div>
                    </div>
                    
        ))):""}
      </div>
    // Dummy ver. in case above not shown
    // <div className={styles.order_container}>
    //     <div className={`${styles.record_container} ${styles.offer_details}`}>
    //         <div className={styles.status}>
    //             <div className={styles.pay_items}>
    //                 <div>Order Date</div>
    //                 <div>3/9/2020</div>
    //             </div>
    //             <div className={styles.pay_items}>
    //                 <div>Total</div>
    //                 <div>US$100</div>
    //             </div>
    //         </div>
    //         <div className={styles.pay_items}>
    //             <div className={styles.pay_title}>Order No.:3786</div>
    //             <div className={styles.status}>
    //                 <div className={styles.pay_items}>
    //                     <div>transaction status:</div>
    //                     <div>Completed</div>
    //                 </div>
    //                 <div className={styles.pay_items}>
    //                     <div>transaction time:</div>
    //                     <div> XXXX:XXXX </div>
    //                 </div>
    //             </div>

    //         </div>
    //     </div>
    //     <div className={`${styles.offer_details} ${styles.offer_details_flex}`}>
    //         <div className={styles.flex}>
    //             <div className={styles.picture}> picture </div>
    //             <div className={styles.offer_details}>
    //                 <div>offers.name</div>
    //                 <div>offers.tourist</div>
    //                 <div>offers.offer_room_key</div>
    //             </div>
    //         </div>
    //         <div className={styles.button_part}>
    //             <div className={styles.button}>Enter into room</div>
    //             <div className={styles.button}>Leave comment</div>
    //         </div>
    //     </div>

    // </div>
  );
};

export default UserAudienceOrders;
