import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { IRootState } from "../../redux/store";
import { getUserProfileThunk } from "../../redux/getPersonalUserRecords/thunk";

import UserProfile from "./UserProfile";
import UserAudienceOrders from "./UserAudienceOrders";
import UserMyOrder from "./UserMyOrder";

import Tabs, { TabPane } from "rc-tabs";
import "../../../node_modules/rc-tabs/assets/index.css";
import styles from "../../css/UserProfile/UserProfile.module.css";
import "../../css/UserProfile/UserProfile_rc-tabs.css";

const { REACT_APP_API_SERVER } = process.env;

const UserProfileTabs: React.FC = () => {
  const dispatch = useDispatch();
  const userProfileItems = useSelector(
    (state: IRootState) => state.getPersonalUserRecords.userProfile
  );
  //insert email stored from localStorage to getUserProfileThunk, which in "/redux/getUserProfileThunk/thunk"
  useEffect(() => {
    dispatch(getUserProfileThunk());
  }, [dispatch]);
  //console.log to check if the info fetched is urs
  // console.log(userProfileItems);
  // console.log(localStorage.getItem("email"));

  // Tabs related error handling
  function callback(e) {
    return e;
  }

  return (
    <div>
      <div className={styles.container}>
        {userProfileItems.map((userProfileItem: any) => (
          <Tabs
            tabPosition="left"
            tabBarGutter={0}
            defaultActiveKey="2"
            onChange={callback}
          > 
            <TabPane tab={<img
                    className="img-fluid"
                    style={{
                    display: "block",
                    maxWidth: "300px",
                    minWidth: "300px",
                    border: "1px",
                    margin:"auto",
                    borderRadius:"4px"
                    }}
                    src={`${REACT_APP_API_SERVER}/cast_profile_pic/${userProfileItem.user.profile_pic}`}
                    alt=""
                  />} key="0">
                  <UserProfile key={0} value={userProfileItem} />
            </TabPane>
            <TabPane tab="Profile" key="1">
              <UserProfile key={1} value={userProfileItem} />
            </TabPane>
            <TabPane tab="Audience Order" key="2">
              <UserAudienceOrders key={2} value={userProfileItem} />
            </TabPane>
            <TabPane tab="My Order" key="3">
              <UserMyOrder key={3} value={userProfileItem} />
            </TabPane>
          </Tabs>
        ))}
      </div>
    </div>
  );
};

export default UserProfileTabs;
