import React from "react";
import { Result, Button } from "antd";
import { Link } from "react-router-dom";

const NoMatch: React.FC = () => {
  // useEffect(() => {
  //   console.log(goBackHomela);
  // });

  // function sendYouBackHome(event: any) {
  //   console.log(event.target);
  // }
  // const [goBackHomela, setGoBackHomela] = useState(false);
  // console.log(setGoBackHomela);
  return (
    <div>
      <h1>404 Page Not Found</h1>
      {/* <RHListingForm /> */}

      <Result
        // status="info"
        status="404"
        title="404"
        subTitle="Sorry, the page you visited does not exist."
        extra={
          <Link to="/" className="link">
            <Button onClick={()=>{}} type="primary">
              Back Home
            </Button>
          </Link>
        }
      />
    </div>
  );
};

export default NoMatch;
