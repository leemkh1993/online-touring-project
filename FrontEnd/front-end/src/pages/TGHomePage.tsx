import React from "react";
import TGSearchTagBar from "../components/TGHomePage/TGSearchTagBar";
import ListingOfferForm from "../components/TGHomePage/OfferForm/ListingOfferForm";
// import UploadImgFile from "../components/TGHomePage/UploadImgFile";
// import EmptyItemForm from "../components/TGHomePage/EmptyItemForm";
import InitMap from "../components/JitsiPage/InitMap";
import FormHeader from "../components/TGHomePage/OfferForm/FormHeader";
import ScrollUpButton from "react-scroll-up-button";
import "../css/TGHomePage/TGHomePage.scss";

const TGHomePage: React.FC = () => {
  return (
    <>
      <TGSearchTagBar />
      <div className="row">
        <div className="col-lg-12">
          <FormHeader />
          {/* <GoogleSearchBar /> */}
        </div>
        <div
          style={{ padding: "0!important", margin: "0" }}
          className="col-lg-5"
        >
          <ListingOfferForm />
        </div>
        <div
          style={{ padding: "0!important", margin: "0" }}
          className="col-lg-7"
        >
          <InitMap />
        </div>
        {/* Version 2 works fine */}
        {/* <Avatar /> */}
        {/* Version 1 cant refresh but icon is ok */}
        {/* <UploadImgFile /> */}

        {/* <EmptyItemForm /> */}
        {/* <ListingFormComponents /> */}
        {/* <RHListingForm /> */}
        <ScrollUpButton />
      </div>
    </>
    /* </div> */
  );
};

export default TGHomePage;
