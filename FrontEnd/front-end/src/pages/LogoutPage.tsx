import React from "react";
import { IRootState } from "../redux/store";
import { useSelector, useDispatch } from "react-redux";
import { logoutThunk } from "../redux/auth/thunks";

const LogoutPage: React.FC = () => {
  const dispatch = useDispatch();
  const isAuthenticated = useSelector(
    (state: IRootState) => state.auth.isAuthenticated
  );
  console.log(`isAuthenticated status: ${isAuthenticated}`);
    dispatch(logoutThunk());

  return (
    <div>
      <h1>This is Logout Page</h1>
      <p>Login status: logout success</p>
    </div>
  );
};

export default LogoutPage;
