import React from "react";
import UploadPhotoButton from "../components/UserProfilePage/UploadPhotoButton";
import UserRatingCard from "../components/HomePage/UserRatingCard";
import Grid from "@material-ui/core/Grid";

const UserProfilePage: React.FC = () => {
  return (
    <div>
      <h1>This is User Profile Page</h1>
      <UploadPhotoButton />

      {/* <Grid container> */}
      <Grid item xs={6} sm={4} lg={3}>
        <UserRatingCard />
      </Grid>
    </div>
  );
};

export default UserProfilePage;
