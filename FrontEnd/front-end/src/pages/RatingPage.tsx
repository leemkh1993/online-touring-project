import React from "react";
import "../../src/css/HomePage/UserRatingCard.scss";
import Grid from "@material-ui/core/Grid";
import UserRatingCard from "../components/HomePage/UserRatingCard";
const RatingPage: React.FC = () => {
  return (
    <div>
      <h1>This is Rating and Feedback Page </h1>
      <Grid item xs={4} md={6} lg={12}>
        <UserRatingCard />
      </Grid>
    </div>
  );
};

export default RatingPage;
