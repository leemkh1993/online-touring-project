import React from "react";
// import { Marker } from "react-google-maps";
// import GetLocalTime from "../components/JitsiPage/GetLocalTime";
// import SimpleMap from "../components/JitsiPage/SimpleMap";
// import GoogleMapReactMockUp from "../css/TGHomePage/GoogleMapReactMockUp.tsx.bak";
// import GoogleMap from "../components/JitsiPage/GoogleMap";

import RoomHeader from "./../components/JitsiPage/RoomHeader";
import JitsiFrame from "./../components/JitsiPage/JitsiFrame";
import Paper from "@material-ui/core/Paper";
import WeatherDisplay from "./../components/JitsiPage/WeatherDisplay";
import {useLocation} from "react-router-dom";

// import InitMap from "../components/JitsiPage/InitMap";
// import GoogleSearchBar from "../components/JitsiPage/GoogleSearchBar"
// import Marker from "../components/JitsiPage/Marker";

const JitsiPage: React.FC = () => {

  //30/9 Get room key from "Link" in order page by LC
  let data = useLocation<any>();
  // console.log(data.state.roomKey,data.state.name);
  const RoomKey=data.state.roomKey;
  const Name=data.state.name;


  return (
    <>
      {/* <h1>This is Jitsi Page</h1> */}
      <Paper>
        <RoomHeader />
        <div
          className="row"
          style={{
            backgroundColor: "#e3e3e3",
            margin: "auto 80px auto 150px",
          }}
        >
          <div className="col-md-8">
            {/* insert RoomKey&Name */}
            <JitsiFrame roomKey={RoomKey} name={Name}/>
          </div>
          <div className="col-md-4">
            <WeatherDisplay />
          </div>
        </div>
      </Paper>
    </>
  );
};

export default JitsiPage;

/* <GetLocalTime /> */

/* <h1>This is Jitsi Page</h1> */

/* <h1>This is Weather App</h1> */

/* <SimpleMap /> */

/* <Marker /> */
