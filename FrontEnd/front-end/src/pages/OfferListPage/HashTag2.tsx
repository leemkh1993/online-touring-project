

import React from "react";
import { Button } from "@material-ui/core";
import { Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import styles from "../../css/OfferPage/OfferListGrid.module.css";

import useReactRouter from "use-react-router";

const Hashtag2: React.FC = () => {
  const params = useReactRouter().match.params as { hashtag: string };
  const HashTag=params.hashtag;

  return (
    <div>
        <Button
              className={`eachButton ${styles.eachButton}`}
              variant="outlined"
              color="secondary"
              href={`#${HashTag.substring(3)}`}
        >
          {`#${HashTag.substring(3)}`}
        </Button>
        <Link to={`/list`}>
          <Button
                className={`eachButton ${styles.eachButton}`}
                variant="outlined"
                color="primary"
                href={`#All`}
          >
            {`#All`}
          </Button>
        </Link>
      </div>
  );
};

export default Hashtag2;