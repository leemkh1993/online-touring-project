//main page with grid logic

import React from "react";
import OfferListGridHashTag from "./OfferListGridHashTag";
import "bootstrap/dist/css/bootstrap.min.css";
import styles from "../../css/OfferPage/OfferListGrid.module.css";

import SearchBar from "./SearchBar";
import Hashtag2 from "./HashTag2";
import useReactRouter from "use-react-router";

const OfferListPage: React.FC = () => {
  const params = useReactRouter().match.params as { hashtag: string };
  const HashTag=params.hashtag;

  return (
    <div className={`${styles.ListContainer}`}>
      <h1 className={styles.h1}>Start your journey</h1>

      {/* <SearchTagBar/> */}
      <div className={styles.btn_grp}>
        <SearchBar/>
        <Hashtag2/>
      </div>



      <OfferListGridHashTag hashtagID={HashTag}/>
    </div>
  );
};

export default OfferListPage;
