import React from "react";
// import { Link } from 'react-router-dom';
import styles from "../../css/OfferPage/OfferListGrid.module.css";
import tourist from "../../css/OfferPage/OfferListGrid.module.css";

// image access
const { REACT_APP_API_SERVER } = process.env;

interface IOfferListProps {
  //Databse ver.

  //Dummy ver.
  key: number;
  value: any;
  // winner: string()=> void()
}

// const OfferListItem = (props: IOfferListProps) => {
//   const formatNumber = (number: number) => {
//     return new Intl.NumberFormat("en-US", {
//       style: "currency",
//       currency: "USD",
//     }).format(number);
//   };

//   return (
//     <div className={`card card-body ${styles.card}`}>
//       <img
//         style={{
//           display: "block",
//           margin: "0 auto 10px",
//           maxHeight: "200px",
//           minHeight: "150px",
//           border: "1px",
//         }}
//         className="img-fluid"
//         src={`${REACT_APP_API_SERVER}/cast_vacation_pic/${props.value.journey_pic}`}
//         alt=""
//       />
//       <p className={`text-left ${styles.trip_name}`}>{props.value.name}</p>
//       <p className={`text-left ${styles.trip_description}`}>
//         {props.value.description}
//       </p>
//       <p className={`text-right ${styles.price} ${styles.trip_description}`}>
//         US{formatNumber(props.value.cost)}
//       </p>
//       <div className={`${tourist.tourist_container}`}>
//         <div className={`${tourist.tourist_pic}`}>Tourist's pic</div>
//         <p className={` text-left ${tourist.tourist_words}`}>
//           " Available before {props.value.expiry_date} "{" "}
//         </p>
//       </div>
//     </div>
//   );
// };

// export default OfferListItem;


const OfferListItem = (props:IOfferListProps) => {

    const formatNumber = (number:number) => {
        return new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(number);
    }

    return ( 
        <div className={`card card-body ${styles.card}`}>
            <img style={{display: "block", margin: "0 auto 10px", maxHeight: "200px", minHeight:"150px",border:"1px"}} className="img-fluid" 
                    src={`${REACT_APP_API_SERVER}/cast_vacation_pic/${props.value.journey_pic}`} alt=""/>
            <p className={`text-left ${styles.trip_name}`}>{props.value.name}</p>
            <p className={`text-left ${styles.trip_description}`}>"{props.value.description}"</p>
            <p className={`text-right ${styles.price} ${styles.trip_description}`}>US{formatNumber(props.value.cost)}</p>
            <div className={`${tourist.tourist_container}`}>
                <div className={`${tourist.tourist_pic}`}>
                    <img style={{display: "block", margin: "0 auto 10px", maxHeight: "90px", maxWidth:"90px",border:"1px"}} className={`${styles.resize_profile_pic} img-fluid`}
                        src={`${REACT_APP_API_SERVER}/cast_profile_pic/${props.value.user_profile_pic}`} alt=""/>
                </div>
                <p className={` text-left ${tourist.tourist_words}`}>" Available before {new Date(props.value.expiry_date).toLocaleString('en',{ year: 'numeric',  month: 'numeric',  day: 'numeric',hour: 'numeric',hour12: true, minute: '2-digit' })} " </p>
            </div>
        </div>
     );
}
 
export default OfferListItem;
