//main page with grid logic

import React from "react";


import "bootstrap/dist/css/bootstrap.min.css";
import styles from "../../css/OfferPage/OfferListGrid.module.css";
import DummyContextProvider from "../OfferListContext/DummyContext";

import SearchBar from "./SearchBar";
import Hashtag1 from "./HashTag1";
import { Link } from "react-router-dom";


const OfferListPage: React.FC = () => {


  return (
    <div className={`${styles.ListContainer}`}>
      <h1 className={styles.h1}>Start your journey</h1>
      
      
      <div className={styles.btn_grp}>
        <SearchBar/>
        <Hashtag1/>
      </div>
      
      <DummyContextProvider />
      <Link to="/home" >
          <p className={styles.home_link}>Home Page</p>
        </Link>
    </div>
  );
};

export default OfferListPage;
