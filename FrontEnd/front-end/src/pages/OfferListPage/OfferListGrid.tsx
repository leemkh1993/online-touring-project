import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import OfferListItem from "./OfferListItem";
import styles from "../../css/OfferPage/OfferListGrid.module.css";
import "../../css/OfferPage/cancel-hyperlink-style.css";

//redux ver.
import { IRootState } from "../../redux/store";
import { useSelector, useDispatch } from "react-redux";
import { getOfferListItemsThunk } from "../../redux/browseOffer/thunks";
//dummy ver.
    // import {DummyContext} from '../OfferListContext/DummyContext';




const OfferListGrid:React.FC = ()=>{

    const dispatch = useDispatch();
    const offerListItems = useSelector((state: IRootState) => state.browseOffer.offerListItems);
    useEffect(() => {
        dispatch(getOfferListItemsThunk());
    }, [dispatch]);
    //dummy ver., use things from OfferListContext
        // const {products} = useContext(DummyContext);
        // console.log(products);


    
    return (
        <div className={styles.grid}>
            {
                offerListItems.map((offerListItem:any) => (
                    <Link key={offerListItem.id} to={`list/detail/${parseInt(offerListItem.id)}`}>
                        <OfferListItem key={offerListItem.id} value={offerListItem}/>
                    </Link>
                ))
            }
            
            {/* workable link for testing*/}
            {/* <Link to='list/detail/789789789'>
                    <OfferListItem key={0} value={""}/>
            </Link>                   */}
            
        </div>
        //dummy ver. data
            // <div className={styles.grid}>
            // {
            //     products.map((product:any) => (
            //         <OfferListItem key={product.id} value={product}/>
            //     ))
            // }
            // </div>
    )


}

export default OfferListGrid;
