import React, { useEffect} from "react";
import{useSelector,useDispatch} from "react-redux";


import { Link } from "react-router-dom";
import { Button} from "@material-ui/core";
import "bootstrap/dist/css/bootstrap.min.css";
import styles from "../../css/OfferPage/OfferListGrid.module.css";


import {IRootState} from "../../redux/store";
import {getHashtagThunk} from "../../redux/getHashTag/thunks"


const Hashtag1: React.FC = () => {

  const dispatch = useDispatch();
  const Hashtags = useSelector((state:IRootState)=>state.getHashtag.hashtag);
  useEffect(()=>{
    dispatch(getHashtagThunk());
  },[dispatch]);


  return (
    <div>
        <Button className="customMuiButtonRoot" disabled>
            | Popular Search Item |
        </Button>
        {
          Hashtags.slice(1,14).map((Hashtag:any)=>(
            <Link to={`/list/%23${Hashtag.substring(1)}`}>
              <Button
                    className={`eachButton ${styles.eachButton}`}
                    variant="outlined"
                    color="secondary"
                    href="#outlined-buttons"
              >
                    {Hashtag}
              </Button>
            </Link>
          ))
        }
          
      </div>
  );
};

export default Hashtag1;