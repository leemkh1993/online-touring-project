//main page with grid logic

import React, { useEffect, useState } from "react";
import{useSelector,useDispatch} from "react-redux";
import "bootstrap/dist/css/bootstrap.min.css";
import { Link } from "react-router-dom";
import Select from "react-select";
import makeAnimated from "react-select/animated";
import SendIcon from "@material-ui/icons/Send";
import styles from "../../css/OfferPage/OfferListGrid.module.css";

import {IRootState} from "../../redux/store";
import {getHashtagThunk} from "../../redux/getHashTag/thunks"
import IconButton from "@material-ui/core/IconButton";
// import Divider from "@material-ui/core/Divider";


const SearchBar: React.FC = () => {

  const dispatch = useDispatch();
  const Hashtags = useSelector((state:IRootState)=>state.getHashtag.hashtag);
  useEffect(()=>{
    dispatch(getHashtagThunk());
  },[dispatch]);

  //For search bar
  const options = Hashtags.map((Hashtag:any)=>{
    return { value: `${Hashtag.substring(1)}`, label: `${Hashtag.substring(1)}` };
  })
  // console.log(options);
  const [selectedOption, setSelectedOption] = useState("#island") as any;
  const animatedComponents = makeAnimated();



  return (

        <div className={styles.flex_bar}>
          <Select
                closeMenuOnSelect={false}
                components={animatedComponents}
                isMulti
                options={options}
                defaultValue={selectedOption}
                onChange={setSelectedOption}
                className={styles.input}
                classNamePrefix="select"
                placeholder="features, places or event here"
          />
          <Link to={`/list/%23${selectedOption=== null? "okok":selectedOption[0].value}`}>
                <IconButton
                  color="secondary"
                  className={styles.iconButton}
                  aria-label="Directions"
                >
                  <SendIcon />
                </IconButton>
          </Link>
        </div>   
  );
};

export default SearchBar;