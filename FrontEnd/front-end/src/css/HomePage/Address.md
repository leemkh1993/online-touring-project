Result from GoogleAPI: [{}]
[{
address*components: (3) [{…}, {…}, {…}],
formatted_address: "Brisbane QLD, Australia",
geometry: {bounds: *.Af, location: _.I, location_type: "APPROXIMATE", viewport: _.Af},
place_id: "ChIJM9KTrJpXkWsRQK_e81qjAgQ",
types: (3) ["colloquial_area", "locality", "political"],
**proto**: Object,
length: 1,
**proto**: Array(0),
}]
