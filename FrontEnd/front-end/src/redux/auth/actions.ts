export const LOGIN_PROCESSING = "@@auth/LOGIN_PROCESSING";
export const LOGIN_SUCCESS = "@@auth/LOGIN_SUCCESS";
export const LOGIN_FAIL = "@@auth/LOGIN_FAIL";
export const LOGOUT_SUCCESS = "@@auth/LOGOUT_SUCCESS";

interface ILoginProcessing {
  type: typeof LOGIN_PROCESSING;
}
interface ILoginSuccess {
  type: typeof LOGIN_SUCCESS;
  // email: string;
}

interface ILoginFail {
  type: typeof LOGIN_FAIL;
  message: string;
}

interface ILogoutSuccess {
  type: typeof LOGOUT_SUCCESS;
}

// Action Creator - Function that return Action
export const loginProcessing = (): ILoginProcessing => {
  return {
    type: LOGIN_PROCESSING,
  };
};
export const loginSuccess = (): ILoginSuccess => {
  // console.log("loginSuccess()");
  return {
    type: LOGIN_SUCCESS,
    // email,
  };
};

export const loginFail = (message: string): ILoginFail => {
  return {
    type: LOGIN_FAIL,
    message,
  };
};

export const logoutSuccess = (): ILogoutSuccess => {
  return {
    type: LOGOUT_SUCCESS,
  };
};

export type IAuthActions =
  | ILoginProcessing
  | ILoginSuccess
  | ILoginFail
  | ILogoutSuccess;
