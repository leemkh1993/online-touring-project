import { initAuthState, IAuthState } from "./state";
import { IAuthActions } from "./actions";
import {
  LOGIN_PROCESSING,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT_SUCCESS,
} from "./actions";

//Step 3 rootReducer in store.ts
export const authReducers = (
  state: IAuthState = initAuthState,
  action: IAuthActions
): IAuthState => {
  // console.log("authReducer()");

  switch (action.type) {
    case LOGIN_PROCESSING:
      return {
        ...state,
        isAuthenticated: false,
        isProcessing: true,
        message: "",
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
        // email: action.email,
        isProcessing: false,
      };
    case LOGIN_FAIL:
      return {
        ...state,
        isAuthenticated: false,
        isProcessing: false,
        message: action.message,
      };
    case LOGOUT_SUCCESS:
      return {
        ...state,
        isAuthenticated: false,
      };
    default:
      return state;
  }
};
