import { Dispatch } from "redux"; //import
import { IAuthActions } from "./actions";
import {
  loginProcessing,
  loginSuccess,
  loginFail,
  logoutSuccess,
} from "./actions";
import { push, CallHistoryMethodAction } from "connected-react-router";
import { IRootState } from "../store";

//Action creator return thunked function
export const loginThunk = (email: string, password: string) => {
  return async (dispatch: Dispatch<IAuthActions | CallHistoryMethodAction>) => {
    try {
      dispatch(loginProcessing());
      const res = await fetch(
        `${process.env.REACT_APP_BACKEND_DOMAIN}${process.env.REACT_APP_BACKEND_PORT}/login`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
          },
          body: JSON.stringify({ email, password }),
        }
      );
      const data = await res.json();
      if (res.status === 200) {
        localStorage.setItem("token", data.token);
        dispatch(loginSuccess());
        dispatch(push("/"));
      } else {
        dispatch(loginFail(data.message));
      }
    } catch (err) {
      console.log(err.message);
    }
  };
};

export const restoreLoginThunk = () => {
  return async (dispatch: Dispatch<IAuthActions | CallHistoryMethodAction>, getState: () => IRootState) => {
    // console.log("restoreLoginThunk()");
    try {
      const token = localStorage.getItem("token");
      if (token) {
        const res = await fetch(
          `${process.env.REACT_APP_BACKEND_DOMAIN}${process.env.REACT_APP_BACKEND_PORT}/restoreLogin`,
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        );
        if (res.status === 200) {
          dispatch(loginSuccess());
          dispatch(push(getState().router.location.pathname));
        } else {
          dispatch(loginFail(""));
        }
      } else {
        // Login Fail
        const action = loginFail("");
        dispatch(action);
      }
    } catch (err) {
      console.log(err.message);
      // err.message
    }
  };
};

export function logoutThunk() {
  return async (dispatch: Dispatch<IAuthActions | CallHistoryMethodAction>) => {
    dispatch(logoutSuccess());
    localStorage.removeItem("token");
    dispatch(push("/"));
  };
}
