//interface for IAuthState
export interface IAuthState {
  isAuthenticated: boolean | null;
  // username: string | null;
  isProcessing: boolean;
  message: string;
}

//interface for InitAuthState
export const initAuthState: IAuthState = {
  isAuthenticated: null,
  // username: null,
  isProcessing: false,
  message: "",
};
