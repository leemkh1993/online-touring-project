import { createStore, combineReducers, compose, applyMiddleware } from "redux";
import {
  RouterState,
  connectRouter,
  routerMiddleware,
  CallHistoryMethodAction,
} from "connected-react-router";
import { createBrowserHistory } from "history";
// import logger from "redux-logger";

import thunk, { ThunkDispatch as OldThunkDispatch } from "redux-thunk";

import { IAuthState } from "./auth/state";
import { IAuthActions } from "./auth/actions";
import { authReducers } from "./auth/reducers";
import { IRegisterState } from "./register/state";
import { IRegisterActions } from "./register/actions";
import { registerReducers } from "./register/reducers";

import {ICreateOfferFormState,toAddPlacesAddressState,} from "./submitOffer/state";
import { ISubmitOfferActions, IAddAddressActions } from "./submitOffer/actions";
import {submitOfferReducers,addAddressReducers} from "./submitOffer/reducers";
import { IOfferListState } from "./browseOffer/state";
import { IOfferListActions } from "./browseOffer/actions";
import { offerListReducers } from "./browseOffer/reducers";
import { IUserInfoState } from "./browseUser/state";
import { IUserInfoActions } from "./browseUser/actions";
import { userInfoReducers } from "./browseUser/reducers";
import { IPersonalUserRecordsState } from "./getPersonalUserRecords/state";
import { IGetPersonalUserRecordsActions } from "./getPersonalUserRecords/actions";
import { userProfileReducers } from "./getPersonalUserRecords/reducers";
import {IHashTagState} from "./getHashTag/state";
import {IHashtagActions} from "./getHashTag/actions";
import {hashtagReducers} from "./getHashTag/reducers";
import {ICommentState} from "./commentSuccess/state";
import {ICommentActions} from "./commentSuccess/actions";
import {commentReducers} from "./commentSuccess/reducers";


export const history = createBrowserHistory();

// Step 1: Interface for State
export interface IRootState {
  auth: IAuthState;
  browseOffer: IOfferListState;
  browseUser: IUserInfoState;
  register: IRegisterState;
  submitOffer: ICreateOfferFormState;
  addAddress: toAddPlacesAddressState;
  getPersonalUserRecords: IPersonalUserRecordsState;
  getHashtag: IHashTagState;
  addComment: ICommentState;
  router: RouterState;
}

// Step 2: Union Actions (InitialState)
type IRootAction =
  | IAuthActions
  | IRegisterActions
  | ISubmitOfferActions
  | IAddAddressActions
  | IOfferListActions
  | IUserInfoActions
  | IGetPersonalUserRecordsActions
  | CallHistoryMethodAction
  | IHashtagActions
  | ICommentActions;

// Step 2: InitialState
// const initialState: IRootState = {
//     isAuthenticated: null,
//     username: null,
// };

// Step 3: rootReducer (this is Function)
// const rootReducer = (state: IRootState = initialState): IRootState => {
//   return state;
// };

// rootReducer
const rootReducer = combineReducers<IRootState>({
  auth: authReducers,
  register: registerReducers,
  browseOffer: offerListReducers,
  browseUser: userInfoReducers,
  submitOffer: submitOfferReducers,
  addAddress: addAddressReducers,
  getPersonalUserRecords: userProfileReducers,
  getHashtag:hashtagReducers,
  addComment:commentReducers,
  router: connectRouter(history),
});

// step 4: middleware
declare global {
  /* tslint:disable:interface-name */
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
  }
}
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export type ThunkDispatch = OldThunkDispatch<IRootState, null, IRootAction>;

// Step 5: createStore()
export default createStore<IRootState, IRootAction, {}, {}>(
  rootReducer,
  composeEnhancers(
    //connect Middlewares
    applyMiddleware(thunk),
    // applyMiddleware(logger),
    applyMiddleware(routerMiddleware(history))
  )
);
