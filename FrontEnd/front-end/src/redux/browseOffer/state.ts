//shape of things got in action
export interface IOfferListItem {
  //to be continue.....................................
  id: number;
  name: string;
  cost:number;
  description: string;
  expiry_date: any;
  journey_pic: any;
  offer_room_key: string; //is room-key necessary
  label_id: number;
  is_completed: boolean;
  //to be continue.....................................
}

export interface IOfferIdItem {
  //to be continue.....................................
  value: any;
  //to be continue.....................................
}

//shape of initial state
export interface IOfferListState {
  isProcessing: boolean;
  offerListItems: Array<IOfferListItem>;
  offerIdItems: Array<IOfferIdItem>;
}
export const initOfferListState: IOfferListState = {
  isProcessing: false,
  offerListItems: [],
  offerIdItems: [],
};

//label mapping necessary?
