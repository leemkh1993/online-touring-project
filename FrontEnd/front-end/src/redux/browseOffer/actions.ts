//Action creator no need extra state?
//"add" logic not here
import { IOfferListItem, IOfferIdItem } from "./state";

export const SET_OFFER_LIST_ITEMS = "@@browseOffer/SET_OFFER_LIST_ITEMS";
export const SET_IS_PROCESSING = "@@browseOffer/SET_IS_PROCESSING";
export const GET_ONE_OFFER = "@@browseOffer/GET_ONE_OFFER";
export const GET_SOON_EXPIRE_OFFERS = "@@browseOffer/GET_SOON_EXPIRE_OFFERS";
export const SET_OFFER_LIST_ITEMS_FAILED =
  "@@browseOffer/SET_OFFER_LIST_ITEMS_FAILED";
export const GET_ONE_OFFER_FAILED = "@@browseOffer/GET_ONE_OFFER_FAILED";

interface ISetOfferListItems {
  // interface of action
  type: typeof SET_OFFER_LIST_ITEMS;
  offerListItems: Array<IOfferListItem>;
}
export const setOfferListItems = (
  offerListItems: Array<IOfferListItem>
): ISetOfferListItems => {
  return {
    type: SET_OFFER_LIST_ITEMS,
    offerListItems,
  };
};

interface ISetIsProcessing {
  type: typeof SET_IS_PROCESSING;
}
export const setIsProcessing = (): ISetIsProcessing => {
  return {
    type: SET_IS_PROCESSING,
  };
};

interface ISetOfferListItemsFailed {
  type: typeof SET_OFFER_LIST_ITEMS_FAILED;
}
export const setOfferListItemsFailed = (): ISetOfferListItemsFailed => {
  return {
    type: SET_OFFER_LIST_ITEMS_FAILED,
  };
};

interface IGetOneOffer {
  // interface of action, limit the function params.
  type: typeof GET_ONE_OFFER;
  offerIdItems: Array<IOfferIdItem>;
}
export const getOneOffer = (
  offerIdItems: Array<IOfferIdItem>
): IGetOneOffer => {
  return {
    type: GET_ONE_OFFER,
    offerIdItems,
  };
};

interface IGetSoonExpireOffer {
  type: typeof GET_SOON_EXPIRE_OFFERS;
  offerListItems: Array<IOfferListItem>;
}
export const getSoonExpireOffer = (
  offerListItems: Array<IOfferListItem>
): IGetSoonExpireOffer => {
  return {
    type: GET_SOON_EXPIRE_OFFERS,
    offerListItems,
  };
};

interface IGetOneOfferFailed {
  type: typeof SET_OFFER_LIST_ITEMS_FAILED;
}
export const getOneOfferFailed = (): IGetOneOfferFailed => {
  return {
    type: SET_OFFER_LIST_ITEMS_FAILED,
  };
};

//no post/delete method, no fail logic
export type IOfferListActions =
  | ISetOfferListItems
  | ISetIsProcessing
  | IGetOneOffer
    | IGetSoonExpireOffer
  | ISetOfferListItemsFailed
  | IGetOneOfferFailed;
