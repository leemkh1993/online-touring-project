import { initOfferListState, IOfferListState } from "./state";

import { IOfferListActions } from "./actions";
import {
  SET_OFFER_LIST_ITEMS,
  SET_IS_PROCESSING,
  GET_ONE_OFFER,
    GET_SOON_EXPIRE_OFFERS,
} from "./actions";
import produce from "immer";

export const offerListReducers = produce(
  (state: IOfferListState, action: IOfferListActions) => {
    switch (action.type) {
      case SET_IS_PROCESSING:
        state.isProcessing = true;
        return;
      case SET_OFFER_LIST_ITEMS:
        state.offerListItems = action.offerListItems;
        return;
      case GET_ONE_OFFER:
        state.offerIdItems = action.offerIdItems;
        return;
        case GET_SOON_EXPIRE_OFFERS:
          state.offerListItems = action.offerListItems;
          return;
      default:
        return state;
    }
  },
  initOfferListState
);
