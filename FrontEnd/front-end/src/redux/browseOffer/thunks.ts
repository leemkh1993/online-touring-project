import { Dispatch } from "redux";

// import { loginFail } from '../auth/actions';
import {
  IOfferListActions,
  setOfferListItems,
  getOneOffer,
  getSoonExpireOffer,
  setOfferListItemsFailed,
  getOneOfferFailed,
} from "./actions";
// import { IRootState } from '../store';

const { REACT_APP_API_SERVER } = process.env;

export const getOfferListItemsThunk = () => {
  return async (dispatch: Dispatch<IOfferListActions>) => {
    //not sure interface necessary

    const res = await fetch(`${REACT_APP_API_SERVER}/offer_list`);
    const result = await res.json();
    // console.log(result);
    if (res.status === 200 || result.isSuccess) {
      dispatch(setOfferListItems(result));
    } else {
      dispatch(setOfferListItemsFailed());
      // console.log("error");
    }
  };
};


export const getOfferListItemsByHashTagThunk = (hashtag:string) => {
  return async (dispatch: Dispatch<IOfferListActions>) => {
    //not sure interface necessary

    const res = await fetch(`${REACT_APP_API_SERVER}/offer_list/${hashtag}`);
    const result = await res.json();
    // console.log(result);
    if (res.status === 200 || result.isSuccess) {
      dispatch(setOfferListItems(result));
    } else {
      dispatch(setOfferListItemsFailed());
      // console.log("error");
    }
  };
};


export function getSoonExpireOffersThunk() {
  return async (dispatch: Dispatch<IOfferListActions>) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/offer/soonExpire`);
    const result = await res.json();
    // console.log(result);
    if (res.status === 200 || result.isSuccess) {
      dispatch(getSoonExpireOffer(result));
      // console.log(result);
    } else {
      // console.log("error occurs: cannot get offer");
    }
  };
}

export function getOneOfferThunk(offerId: number) {
  // console.log(offerId);
  return async (dispatch: Dispatch) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/offerInfo/${offerId}`);
    const result = await res.json();
    // console.log(result.offer[0].name);

    // console.log(result);
    if (res.status === 200 || result.isSuccess) {
      dispatch(getOneOffer(result));
    } else {
      dispatch(getOneOfferFailed());
      // console.log("error");
    }
  };
}
