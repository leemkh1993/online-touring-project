import { Dispatch } from 'redux';
import { getUserProfile,getUserProfileFailed} from './actions';
import {IGetPersonalUserRecordsActions} from './actions';
import { IAuthActions } from '../auth/actions';

const { REACT_APP_API_SERVER } = process.env;



export function getUserProfileThunk(){
    // console.log(userId);
    return async (dispatch:Dispatch<IGetPersonalUserRecordsActions|IAuthActions>)=>{
        const res = await fetch(`${REACT_APP_API_SERVER}/userInfo`,{
            method:"GET",
            headers:{
                "Content-Type":"application/json",
                "Authorization":`Bearer ${localStorage.getItem('token')}`
            },
        });
        const result = await res.json();
        // console.log(result.offer[0].name);
        // console.log(result);
        
        if(res.status === 200){
            dispatch(getUserProfile(result));
        }else{
            dispatch(getUserProfileFailed());
            // console.log("error");
        }
    }
}