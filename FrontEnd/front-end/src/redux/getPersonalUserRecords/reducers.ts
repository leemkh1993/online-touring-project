import { initPersonalUserRecordsState, IPersonalUserRecordsState} from './state';

import { IGetPersonalUserRecordsActions } from './actions';
import { GET_USER_PROFILE} from './actions';
import produce from 'immer';

export const userProfileReducers = produce((state: IPersonalUserRecordsState, action: IGetPersonalUserRecordsActions) => {
    switch (action.type) {
        case GET_USER_PROFILE:
            state.userProfile = action.userProfile;
            return;
        default:
            return state;
    }
}, initPersonalUserRecordsState);