import {IUserProfile} from "./state";

export const GET_USER_PROFILE = '@@getPersonalUserRecords/GET_USER_PROFILE';
export const GET_USER_PROFILE_FAILED = '@@getPersonalUserRecords/GET_USER_PROFILE_FAILED';

interface IGetUserProfile { // interface of action
    type: typeof GET_USER_PROFILE;
    userProfile: IUserProfile[];
}
export const getUserProfile = (userProfile: IUserProfile[]): IGetUserProfile => {
    return {
        type: GET_USER_PROFILE,
        userProfile,
    };
};


interface IGetUserProfileFailed { // interface of action
    type: typeof GET_USER_PROFILE_FAILED;
}
export const getUserProfileFailed = (): IGetUserProfileFailed => {
    return {
        type: GET_USER_PROFILE_FAILED,
    };
};

export type IGetPersonalUserRecordsActions = IGetUserProfile | IGetUserProfileFailed;