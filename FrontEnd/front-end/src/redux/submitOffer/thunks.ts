import { Dispatch } from "redux";
import { ISubmitOfferActions } from "./../submitOffer/actions";
import {
  submitOfferProcessing,
  submitOfferSuccess,
  submitOfferFail,
} from "./actions";
import { IAuthActions } from "./../auth/actions";

const { REACT_APP_API_SERVER } = process.env;

export const submitOfferThunk = (
  offerName: string,
  cost: number,
  expiryDate: any,
  offerDescription: string,// description: string | any,
  offerDescriptionDetails:string,
  destinations:any,
  hashtags: any
  // country: string,
  // siteName: string,
  
  // sitePic: string,
  // longitude: number,
  // latitude: number,
  // hashtags: string,
  
  // offerDate: string,
  // offerTime: string,
  
) => {
  return async function (
    dispatch: Dispatch<ISubmitOfferActions | IAuthActions>
  ) {
    // console.log("submitOfferThunk()");

    const token = localStorage.getItem("token");
    if (!token) {
      dispatch(submitOfferFail(""));
    }
    dispatch(submitOfferProcessing());
    const res = await fetch(`${REACT_APP_API_SERVER}/offer/createNewOffer`, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        offerName,
        cost,
        expiryDate,
        offerDescription,// description: string | any,
        offerDescriptionDetails,
        destinations,
        hashtags
        // country,
        // siteName,
        // description,
        // sitePic,
        // longitude,
        // latitude,
        // hashtags,
        // cost,
        // offerDate,
        // offerTime,
        // expiryDate,
      }),
    });

    // const result = await res.json()
    // console.log(result)
    // if (res.status === 401) {
    // dispatch(submitOfferFail(""));
    // }

    // if (res.status === 200) {
    //   const createNewOfferData = {
    //   offerName: offerName,
    //   siteName: siteName,
    //   description: description,
    //   sitePic: sitePic,
    //   longitude: longitude,
    //   latitude: latitude,
    //   hashtags: hashtags,
    //   expiryDate: expiryDate,
    //   };
    //   dispatch(submitOfferSuccess());
    // } else {
    //   dispatch(submitOfferFail(""));
    // }
    if (res.status === 200) {
      dispatch(submitOfferSuccess());
    } else {
      const action = submitOfferFail("");
      dispatch(submitOfferFail(action.message));
    }
  };
};

// export const addAddressSuccess
