// import { ITouristOfferFormSubmittionItems } from "./state";
//GET?
// export const GET_SUBMIT_OFFER_FORM_ITEMS =
//   "@@submitOffer/GET_SUBMITOFFER_FORM_ITEMS";

import { toAddPlacesAddress } from "./state";

export const SUBMITOFFER_PROCESSING = "@@submitOffer/SUBMITOFFER_PROCESSING";
export const SUBMITOFFER_SUCCESS = "@@submitOffer/SUBMITOFFER_SUCCESS";
export const SUBMITOFFER_FAIL = "@@submitOffer/SUBMITOFFER_FAIL";
export const ADD_ADDRESS_SUCCESS = "@@addAddress/ADD_ADDRESS_SUCCESS";
export const UPDATE_FILEURL_SUCCESS = "@@updateFileUrl/UPDATE_FILEURL_SUCCESS";

interface IAddAddressSuccess {
  type: typeof ADD_ADDRESS_SUCCESS;
  newitem: toAddPlacesAddress;
}

export const addAddressSuccess = (
  item: toAddPlacesAddress
): IAddAddressSuccess => {
  return {
    type: ADD_ADDRESS_SUCCESS,
    newitem: item,
  };
};

interface IUpdateFileUrlSuccess {
  type: typeof UPDATE_FILEURL_SUCCESS;
  newImgUrl: toAddPlacesAddress;
  idx: number;
}

export const updateFileUrlSuccess = (
  fileUrl: toAddPlacesAddress,
  idx: number
): IUpdateFileUrlSuccess => {
  return {
    type: UPDATE_FILEURL_SUCCESS,
    newImgUrl: fileUrl,
    idx: idx,
  };
};

interface ISubmitOfferProcessing {
  type: typeof SUBMITOFFER_PROCESSING;
  // message: string;
}
export const submitOfferProcessing = (): // message: string
ISubmitOfferProcessing => {
  return {
    type: SUBMITOFFER_PROCESSING,
    // message,
  };
};

interface ISubmitOfferSuccess {
  type: typeof SUBMITOFFER_SUCCESS;
  // message: string;
}
export const submitOfferSuccess = (): ISubmitOfferSuccess => {
  // console.log("submitOfferSuccess()");
  return {
    type: SUBMITOFFER_SUCCESS,
    // message,
  };
};

interface ISubmitOfferFail {
  type: typeof SUBMITOFFER_FAIL;
  message: string;
}
export const submitOfferFail = (message: string): ISubmitOfferFail => {
  return {
    type: SUBMITOFFER_FAIL,
    message,
  };
};

export type ISubmitOfferActions =
  | ISubmitOfferProcessing
  | ISubmitOfferSuccess
  | ISubmitOfferFail;

export type IAddAddressActions = IAddAddressSuccess | IUpdateFileUrlSuccess;
