//interface for IRegisterState
export interface ISubmitFormItems {
  offerName: string;
  country: string;
  siteName: string;
  description: string;
  sitePic: string;
  hashtag: string;
  cost: number;
  longitude: number;
  latitude: number;
  expiryDate: string;

  // value: any;
}

// export interface ISubmittionFrom {
//   list: [];
// }

// export const initISubmittionFromState: ISubmittionFrom = {
//   list: [],
// };

export interface ICreateOfferFormState {
  isAuthenticated: boolean | null;
  isProcessing: boolean;
  message: string;
}

export const initCreateOfferFormState: ICreateOfferFormState = {
  isAuthenticated: null,
  isProcessing: false,
  message: "",
};

export interface toAddPlacesAddress {
  coordinates: {
    lat: number;
    lng: number;
  };
  placeAddress: string;
  // fileUrl: string;
}

export interface toAddPlacesAddressState {
  list: toAddPlacesAddress[];
}

export const initToAddPlacesAddressState: toAddPlacesAddressState = {
  list: [],
};

export interface toAddTimePickerAndDatePicker {
  expiryDate: {
    time;
  };
}
