import {
  initCreateOfferFormState,
  ICreateOfferFormState,
  initToAddPlacesAddressState,
  toAddPlacesAddressState,
} from "./state";
import { ISubmitOfferActions, IAddAddressActions } from "./actions";
// import produce from "immer";
import {
  SUBMITOFFER_PROCESSING,
  SUBMITOFFER_SUCCESS,
  SUBMITOFFER_FAIL,
  ADD_ADDRESS_SUCCESS,
  UPDATE_FILEURL_SUCCESS,
} from "./actions";

export const submitOfferReducers = (
  state: ICreateOfferFormState = initCreateOfferFormState,
  action: ISubmitOfferActions
): ICreateOfferFormState => {
  // console.log("submitOfferReducers()");
  switch (action.type) {
    case SUBMITOFFER_PROCESSING:
      return {
        ...state,
        isAuthenticated: true,
        isProcessing: true,
        message: "",
      };
    case SUBMITOFFER_SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
        isProcessing: false,
        // message: action.message,
      };
    case SUBMITOFFER_FAIL:
      return {
        ...state,
        isAuthenticated: true,
        isProcessing: false,
        message: action.message,
      };
    default:
      return state;
  }
};

export const addAddressReducers = (
  state: toAddPlacesAddressState = initToAddPlacesAddressState,
  action: IAddAddressActions
): toAddPlacesAddressState => {
  // console.log("addPlacesAddressReducers()");
  switch (action.type) {
    case ADD_ADDRESS_SUCCESS:
      //state.list.push(action.newitem);
      let newlist = state.list.slice(0);
      newlist.push(action.newitem);
      state.list = newlist;
      return { ...state, list: newlist };
    default:
      return state;
  }
};

export const updateFileUrlReducers = (
  state: toAddPlacesAddressState = initToAddPlacesAddressState,
  action: IAddAddressActions
): toAddPlacesAddressState => {
  // console.log("updateFileUrlReducers()");
  switch (action.type) {
    case UPDATE_FILEURL_SUCCESS:
      //state.list.push(action.ImgUrl);
      let newFileUrlItem = state.list.slice(0); //clone obj
      // newFileUrlItem[action.idx].fileUrl = action.newImgUrl;
      state.list = newFileUrlItem;
      return { ...state, list: newFileUrlItem };
    default:
      return state;
  }
};
