import { initHashtagState,IHashTagState} from './state';

import { IHashtagActions } from './actions';
import { GET_HASHTAG} from './actions';
import produce from 'immer';

export const hashtagReducers = produce((state: IHashTagState, action: IHashtagActions) => {
    switch (action.type) {
        case GET_HASHTAG:
            state.hashtag = action.hashtag;
            return;
        default:
            return state;
    }
}, initHashtagState);