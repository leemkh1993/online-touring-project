import {IHashtag} from "./state";

export const GET_HASHTAG = '@@listHashtag/GET_HASHTAG';
export const GET_HASHTAG_FAILED = '@@listHashtag/GET_HASHTAG_FAILED';

interface IGetHashtag { // interface of action
    type: typeof GET_HASHTAG;
    hashtag: Array<IHashtag>;
}
export const getHashtag = (hashtag: Array<IHashtag>): IGetHashtag => {
    return {
        type: GET_HASHTAG,
        hashtag,
    };
};


interface IGetHashtagFailed { // interface of action
    type: typeof GET_HASHTAG_FAILED;
}
export const getHashtagFailed = (): IGetHashtagFailed => {
    return {
        type: GET_HASHTAG_FAILED,
    };
};

export type IHashtagActions = IGetHashtag | IGetHashtagFailed;