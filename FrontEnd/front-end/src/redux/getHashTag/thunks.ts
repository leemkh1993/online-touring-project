import { Dispatch } from 'redux';
import { getHashtag,getHashtagFailed} from './actions';


const { REACT_APP_API_SERVER } = process.env;



export function getHashtagThunk(){
    return async (dispatch:Dispatch)=>{
        const res = await fetch(`${REACT_APP_API_SERVER}/hashtag_list`);
        const result = await res.json();
        // console.log(result);
        if(res.status === 200 || result.isSuccess){
            dispatch(getHashtag(result));
        }else{
            dispatch(getHashtagFailed());
            // console.log("error");
        }
    }
}