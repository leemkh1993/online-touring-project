//for action
export interface IComment{
    audienceID:number,
    orderID:number
    comment:string,
    rating:string
}


//for reducer
export interface ICommentState{
    comment_insert: IComment[],
    isProcessing:Boolean
    
}
//for immer
export const InitCommentState:ICommentState = {
    comment_insert:[],
    isProcessing:false
}

