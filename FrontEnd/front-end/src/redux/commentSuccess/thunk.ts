import {Dispatch} from "redux";
// import {IPayment,IRecords} from "./state";
import {ICommentActions,addIsProcessing,addCommentSuccess,addCommentFail} from "./actions";
import { loginFail, IAuthActions } from "../auth/actions";

const {REACT_APP_API_SERVER} = process.env;


// Server, offerID, payeeID, recipientID, amount, paypalID, startDate, endDate
export const addCommentThunk = (
        audienceID:number,
        orderID:number,
        comment:string,
        rating:string,
    ) => {
    return async (dispatch: Dispatch<ICommentActions|IAuthActions>) => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
        }
        dispatch(addIsProcessing());
        const res = await fetch(`${REACT_APP_API_SERVER}/reviews/audience/createNewReview`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ audienceID, orderID, comment, rating }),
        });
        const result = await res.json();
        // console.log(result);
        if (res.status === 401) {
            dispatch(loginFail(''));
        }
        if (res.status === 200 || result) {
            const comment_insert = {
                audienceID: audienceID,//result.id?
                orderID:orderID,
                comment:comment,
                rating:rating
            };
            dispatch(addCommentSuccess(comment_insert));
        } else {
            dispatch(addCommentFail());
        }
    };
};