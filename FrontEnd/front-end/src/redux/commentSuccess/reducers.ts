import produce from 'immer';

import {InitCommentState, ICommentState, } from "./state";
import {ICommentActions} from "./actions";
import {ADD_IS_PROCESSING, ADD_COMMENT_SUCCESS, ADD_COMMENT_FAIL} from "./actions";



export const commentReducers = produce((state:ICommentState, action:ICommentActions) => {
    switch (action.type) {
        case ADD_IS_PROCESSING:
            state.isProcessing = true;
            return;
        case ADD_COMMENT_SUCCESS:
            state.comment_insert.unshift(action.comment_insert);
            state.isProcessing = false;
            return;
        case ADD_COMMENT_FAIL:
            state.isProcessing = false;
            return;
        default:
            return state;
    }
},InitCommentState)
