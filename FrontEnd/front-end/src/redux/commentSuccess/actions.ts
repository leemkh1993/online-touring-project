import {IComment} from "./state";

//for recognise
export const ADD_COMMENT_SUCCESS = '@@comment/ADD_COMMENT_SUCCESS';
export const ADD_COMMENT_FAIL = '@@comment/ADD_COMMENT_FAIL';
export const ADD_IS_PROCESSING = '@@comment/ADD_IS_PROCESSING';



interface IAddIsProcessing {
    type: typeof ADD_IS_PROCESSING;
}
export const addIsProcessing = (): IAddIsProcessing => {
    return {
        type: ADD_IS_PROCESSING,
    };
};


interface IAddCommentSuccess {
    type: typeof ADD_COMMENT_SUCCESS;
    comment_insert: IComment;
}
export const addCommentSuccess = (comment_insert:IComment):IAddCommentSuccess=>{
    return{
        type: ADD_COMMENT_SUCCESS,
        comment_insert
    }
}


interface IAddCommentFail {
    type: typeof ADD_COMMENT_FAIL;
}
export const addCommentFail = ():IAddCommentFail=>{
    return{
        type: ADD_COMMENT_FAIL
    };
};


// for reducer
export type ICommentActions = IAddIsProcessing | IAddCommentSuccess | IAddCommentFail ;
