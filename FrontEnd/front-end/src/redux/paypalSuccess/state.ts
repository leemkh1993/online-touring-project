//for action
export interface IPayment{
    offerID:number
    payeeID:number
    recipientID:number
    amount:number
    paypalID:string
    startDate:Date
    endDate:Date
        // user_paid_id:number
        // user_selected_date:string
    user_paid_status:boolean
}


export interface IRecords{
    value: any;
}


//for reducer
export interface IPaymentState{
    isProcessing: boolean
    status_record_update: IPayment[] //or Array<IPayment>
    status_records:IRecords[]
}
//for immer
export const InitPaymentState:IPaymentState = {
    isProcessing: false,
    status_record_update:[],
    status_records:[]
}




//alternative
    // export interface InitPaymentState{
    //     status_record: {
    //         user_paid_id:number
    //         user_selected_date:string
    //         user_paid_status:boolean
    //     }
    // }