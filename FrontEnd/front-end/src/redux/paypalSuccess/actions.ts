import {IPayment, IRecords} from "./state";

//for recognise
export const GET_PAYMENT_RECORDS = '@@payment/GET_PAYMENT_RECORDS';
export const SET_IS_PROCESSING = '@@payment/SET_IS_PROCESSING';
export const ADD_PAYMENT_RECORD_SUCCESS = '@@payment/ADD_PAYMENT_RECORD_SUCCESS';
export const ADD_PAYMENT_RECORD_FAIL = '@@payment/ADD_PAYMENT_RECORD_FAIL';



interface IGetPaymentRecords {
    type: typeof GET_PAYMENT_RECORDS;
    status_records: Array<IRecords>;//need edit
}
//for thunks
export const getPaymentRecords = (status_records: IRecords[]): IGetPaymentRecords => {
    return {
        type: GET_PAYMENT_RECORDS,
        status_records,
    };
};


interface ISetIsProcessing {
    type: typeof SET_IS_PROCESSING;
}
export const setIsProcessing = (): ISetIsProcessing => {
    return {
        type: SET_IS_PROCESSING,
    };
};


interface IAddPaymentRecordSuccess {
    type: typeof ADD_PAYMENT_RECORD_SUCCESS;
    status_record_update: IPayment;
}
export const addPaymentSuccess = (status_record_update:IPayment):IAddPaymentRecordSuccess=>{
    return{
        type: ADD_PAYMENT_RECORD_SUCCESS,
        status_record_update
    }
}


interface IAddPaymentRecordFail {
    type: typeof ADD_PAYMENT_RECORD_FAIL;
}
export const addPaymentRecordFail = ():IAddPaymentRecordFail=>{
    return{
        type: ADD_PAYMENT_RECORD_FAIL
    };
};


// for reducer
export type IPaymentRecordActions = IGetPaymentRecords | ISetIsProcessing | IAddPaymentRecordSuccess | IAddPaymentRecordFail ;
