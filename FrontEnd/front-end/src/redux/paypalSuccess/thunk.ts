import {Dispatch} from "redux";
// import {IPayment,IRecords} from "./state";
import {IPaymentRecordActions,getPaymentRecords,setIsProcessing,addPaymentSuccess,addPaymentRecordFail} from "./actions";
import { loginFail, IAuthActions } from "../auth/actions";

const {REACT_APP_API_SERVER} = process.env;

export const getPaymentRecordsThunk = () =>{
    return async (dispatch:Dispatch<IPaymentRecordActions|IAuthActions>)=>{
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
        }
        // 27/9 to be continue in backend
        const res = await fetch(`${REACT_APP_API_SERVER}/order/offer/info`,{
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });
        if (res.status === 401) {
            dispatch(loginFail(''));
        }
        if (res.status === 200) {
            const result = await res.json();
            dispatch(getPaymentRecords(result));
        }
    }
}



// Server, offerID, payeeID, recipientID, amount, paypalID, startDate, endDate
export const addPaymentRecordThunk = (
        offerID:number,
        payeeID:number,
        recipientID:number,
        amount:number,
        paypalID:string,
        startDate:Date,
        endDate:Date,
    ) => {
    return async (dispatch: Dispatch<IPaymentRecordActions|IAuthActions>) => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(loginFail(''));
        }
        dispatch(setIsProcessing());
        const res = await fetch(`${REACT_APP_API_SERVER}/order/offer/createNewOrder`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ offerID, payeeID, recipientID, amount, paypalID, startDate, endDate }),
        });
        const result = await res.json();
        // console.log(result);
        if (res.status === 401) {
            dispatch(loginFail(''));
        }
        if (res.status === 200 || result) {
            const status_record_update = {
                offerID: offerID,//result.id?
                payeeID: payeeID,
                recipientID: recipientID,
                amount:amount,
                paypalID: paypalID,
                startDate:startDate,
                endDate:endDate,
                user_paid_status:true
                // user_paid_id: user_paid_id,
                // user_selected_date:user_selected_date,
                // user_paid_status: user_paid_status,
            };
            dispatch(addPaymentSuccess(status_record_update));
        } else {
            dispatch(addPaymentRecordFail());
        }
    };
};