import produce from 'immer';

import {InitPaymentState, IPaymentState, } from "./state";
import {IPaymentRecordActions} from "./actions";
import {GET_PAYMENT_RECORDS, SET_IS_PROCESSING, ADD_PAYMENT_RECORD_SUCCESS, ADD_PAYMENT_RECORD_FAIL} from "./actions";



export const paymentReducers = produce((state:IPaymentState, action:IPaymentRecordActions) => {
    switch (action.type) {
        case GET_PAYMENT_RECORDS:
            state.status_records = action.status_records;
            return;
        case SET_IS_PROCESSING:
            state.isProcessing = true;
            return;
        case ADD_PAYMENT_RECORD_SUCCESS:
            state.status_record_update.unshift(action.status_record_update);
            state.isProcessing = false;
            return;
        case ADD_PAYMENT_RECORD_FAIL:
            state.isProcessing = false;
            return;
        default:
            return state;
    }
},InitPaymentState)
