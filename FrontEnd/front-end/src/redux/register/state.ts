export interface IRegisterForm {
  // email: string;
  // password: string;
  // firstName: string;
  // lastName: string;
  // age_range_id: number;
  // country_id: number;
  // city_id: number;
  // profession: string;
  // description: string;
  // profile_pic: string;
  firstName: string,
  lastName: string,
  email: string,
  password: string,
  profession: string,
  ageRange: string,
  languages: string,
  country: string,
  city: string


}

//interface for IRegisterState
export interface IRegisterState {
  isAuthenticated: boolean | null;
  isProcessing: boolean;
  message: string;
}

//interface for InitRegisterState
export const initRegisterState: IRegisterState = {
  isAuthenticated: null,
  isProcessing: false,
  message: "",
};
