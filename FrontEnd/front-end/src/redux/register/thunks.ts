import { Dispatch } from "redux"; //import
import { IRegisterActions } from "./actions";
import { registerProcessing, registerSuccess, registerFail } from "./actions";
import { push, CallHistoryMethodAction } from "connected-react-router";

export const createUserThunk = (
  // email: string,
  // password: string,
  // firstName: string,
  // lastName: string,
  // age_range_id: number,
  // country_id: number,
  // city_id: number,
  // profession: string,
  // description: string,
  // profile_pic: string
  firstName: string,
  lastName: string,
  email: string,
  password: string,
  profession: string,
  ageRange: string,
  languages: string,
  country: string,
  city: string
) => {
  return async (
    dispatch: Dispatch<IRegisterActions | CallHistoryMethodAction>
  ) => {
    try {
      dispatch(registerProcessing());
      const res = await fetch(`${process.env.REACT_APP_API_SERVER}/register`, {
        // const res = await fetch("http://localhost:8000/register", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          // firstName,
          // lastName,
          // email,
          // password,
          // profession,
          // age,
          // languages,
          // country,
          // city,
          // email: email,
          // password: password,
          // firstName: firstName,
          // lastName: lastName,
          // age_range_id: age_range_id,
          // country_id: country_id,
          // city_id: city_id,
          // profession: profession,
          // description: description,
          // profile_pic: profile_pic,
          firstName,
          lastName,
          email,
          password,
          profession,
          ageRange,
          languages,
          country,
          city,
        }),
      });
      const data = await res.json();
      // console.log(data);

      localStorage.setItem("token", data.token);

      if (res.status === 200) {
        // console.log(res.status);
        dispatch(registerSuccess());
        dispatch(push("/"));
      } else {
        // console.log(res.status);
        dispatch(registerFail(data.message));
      }
    } catch (err) {
      console.log(err.message);
    }
  };
};

export const restoreRegisterThunk = () => {
  return async (
    dispatch: Dispatch<IRegisterActions | CallHistoryMethodAction>
  ) => {
    // console.log("restoreRegisterThunk()");
    try {
      const token = localStorage.getItem("token");
      if (token) {
        const res = await fetch(
          `${process.env.REACT_APP_API_SERVER}/restoreLogin`,
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        );
        if (res.status === 200) {
          dispatch(registerSuccess());
          dispatch(push("/"));
        } else {
          dispatch(registerFail(""));
        }
      } else {
        // register Fail
        const action = registerFail("");
        dispatch(action);
      }
    } catch (err) {
      console.log(err.message);
    }
  };
};
