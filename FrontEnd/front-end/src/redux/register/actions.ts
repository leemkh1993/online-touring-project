export const REGISTER_PROCESSING = "@@register/REGISTER_PROCESSING";
export const REGISTER_SUCCESS = "@@register/REGISTER_SUCCESS";
export const REGISTER_FAIL = "@@register/REGISTER_FAIL";

interface IRegisterProcessing {
  type: typeof REGISTER_PROCESSING;
}
interface IRegisterSuccess {
  type: typeof REGISTER_SUCCESS;
  // username: string;
}

interface IRegisterFail {
  type: typeof REGISTER_FAIL;
  message: string;
}

// Action Creator - Function that return Action
export const registerProcessing = (): IRegisterProcessing => {
  return {
    type: REGISTER_PROCESSING,
  };
};
export const registerSuccess = (): IRegisterSuccess => {
  // console.log("registerSuccess()");
  return {
    type: REGISTER_SUCCESS,
    // username,
  };
};

export const registerFail = (message: string): IRegisterFail => {
  return {
    type: REGISTER_FAIL,
    message,
  };
};

export type IRegisterActions =
  | IRegisterProcessing
  | IRegisterSuccess
  | IRegisterFail;
