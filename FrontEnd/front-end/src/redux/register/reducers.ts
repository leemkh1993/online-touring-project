import { initRegisterState, IRegisterState } from "./state";
import { IRegisterActions } from "./actions";
import {
  REGISTER_PROCESSING,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
} from "./actions";

export const registerReducers = (
  state: IRegisterState = initRegisterState,
  action: IRegisterActions
): IRegisterState => {
  // console.log("registerReducer()");

  switch (action.type) {
    case REGISTER_PROCESSING:
      return {
        ...state,
        isAuthenticated: false,
        isProcessing: true,
        message: "",
      };
    case REGISTER_SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
        // username: action.username,
        isProcessing: false,
      };
    case REGISTER_FAIL:
      return {
        ...state,
        isAuthenticated: false,
        isProcessing: false,
        message: action.message,
      };
    default:
      return state;
  }
};
