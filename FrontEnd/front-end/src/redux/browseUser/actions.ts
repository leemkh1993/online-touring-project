import {IUserInfo} from "./state";

export const GET_USER_INFO = '@@browseUser/GET_USER_INFO';
export const GET_USER_INFO_FAILED = '@@browseUser/GET_USER_INFO_FAILED';

interface IGetUserInfo { // interface of action
    type: typeof GET_USER_INFO;
    userInfo: Array<IUserInfo>;
}
export const getUserInfo = (userInfo: Array<IUserInfo>): IGetUserInfo => {
    return {
        type: GET_USER_INFO,
        userInfo,
    };
};


interface IGetUserInfoFailed { // interface of action
    type: typeof GET_USER_INFO_FAILED;
}
export const getUserInfoFailed = (): IGetUserInfoFailed => {
    return {
        type: GET_USER_INFO_FAILED,
    };
};

export type IUserInfoActions = IGetUserInfo | IGetUserInfoFailed;