import { initUserInfoState,IUserInfoState} from './state';

import { IUserInfoActions } from './actions';
import { GET_USER_INFO} from './actions';
import produce from 'immer';

export const userInfoReducers = produce((state: IUserInfoState, action: IUserInfoActions) => {
    switch (action.type) {
        case GET_USER_INFO:
            state.userInfo = action.userInfo;
            return;
        default:
            return state;
    }
}, initUserInfoState);