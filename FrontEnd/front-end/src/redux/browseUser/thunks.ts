import { Dispatch } from 'redux';
import { getUserInfo,getUserInfoFailed} from './actions';
import { IAuthActions } from "../auth/actions";
import {
  getUserProfile,
  getUserProfileFailed,
  IGetPersonalUserRecordsActions,
} from "../getPersonalUserRecords/actions";

const { REACT_APP_API_SERVER } = process.env;



export function getUserInfoThunk(userId:number){
    // console.log(userId);
    return async (dispatch:Dispatch)=>{
        const res = await fetch(`${REACT_APP_API_SERVER}/userInfo/${userId}`);
        const result = await res.json();
        // console.log(result.offer[0].name);
        // console.log(result);
        if(res.status === 200 || result.isSuccess){
            dispatch(getUserInfo(result));
        }else{
            dispatch(getUserInfoFailed());
            // console.log("error");
        }
    }
}

export function getAllUserInfoThunk() {
    return async (
      dispatch: Dispatch<IGetPersonalUserRecordsActions | IAuthActions>
    ) => {
      const res = await fetch(`${REACT_APP_API_SERVER}/userInfo`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      const result = await res.json();
      // console.log(result);
  
      if (res.status === 200) {
        dispatch(getUserProfile(result));
      } else {
        dispatch(getUserProfileFailed());
        // console.log("error occurs!");
      }
    };
  }
