import React, { useEffect } from "react";
import "./App.css";
import NavBar from "./components/NavBar/NavBar";
import PrivateRoute from "./components/PrivateRoute";

import HomePage from "./pages/HomePage";
import TGHomePage from "./pages/TGHomePage";
import AboutPage from "./pages/AboutPage";
import MyOrdersPage from "./pages/MyOrdersPage";
import PaymentPage from "./pages/PaymentPage";
import JitsiPage from "./pages/JitsiPage";
// import RatingPage from "./pages/RatingPage";
// import DownloadReactAppPage from "./pages/DownloadReactAppPage";

import NoMatch from "./pages/NoMatch";

import LoginForm from "./components/LoginPage/LoginForm";
import RegisterForm from "./components/RegisterPage/RegisterForm";

import OfferListPage from "./pages/OfferListPage/OfferListPage";
import OfferIdPage from "./pages/OfferIdPage/OfferIdPage";
import UserProfilePage from "./pages/UserProfilePage/UserProfilePage";
import OfferListPageHashTag from "./pages/OfferListPage/OfferListPageHashTag";
// 2/10 map test
// import Map from "./components/OfferIDPage/GoogleMapList";

import SideNavBar from "./components/NavBar/SideNavBar";
import { useSelector, useDispatch } from "react-redux";
import { IRootState } from "./redux/store";
import { Route, Switch } from "react-router-dom";
import { restoreLoginThunk } from "./redux/auth/thunks";
// import LogoutPage from "./pages/LogoutPage";
// import { restoreRegisterThunk } from "./redux/register/thunks";
import LogoutPage from "./pages/LogoutPage";

// import RoleSwitchingButton from "./components/RoleSwitchingButton";
// import UserIcon from "./components/UserIcon";
function App() {
  const dispatch = useDispatch();
  const isAuthenticated = useSelector(
    (state: IRootState) => state.auth.isAuthenticated
  );

  useEffect(() => {
    if (isAuthenticated === null) {
      // restore login & register
      dispatch(restoreLoginThunk());
      // dispatch(restoreRegisterThunk());
    }
  }, [dispatch, isAuthenticated]);

  return (
    <div className="App">
      {/* <LogoutButtonMenu />  for testing */}
      {/* <RoleSwitchingButton />  for testing*/}
      {/* <UserIcon />  for testing*/}
      <SideNavBar />
      <NavBar />
      {/* {isAuthenticated === null && <h1>Is loading..</h1>} */}
      <Switch>
        <PrivateRoute path="/" exact={true} component={HomePage} />
        {/* for public asscess to home Page //for testing without server run */}
        <Route path="/home" exact={true} component={HomePage} />
        <Route
          path="/login"
          exact={true}
          component={() => (
            <>
              <LoginForm />
            </>
          )}
        />
        <Route path="/logout" exact={true} component={LogoutPage} />
        <Route
          path="/register"
          exact={true}
          component={() => (
            <>
              <RegisterForm />
            </>
          )}
        />
        {/* 10 Sep: logout should direct to HomePage once server is connected */}
        <Route path="/" exact={true} component={HomePage} />

        {/* <Route path="/logout" exact={true} component={HomePage} /> */}
        <Route path="/about" exact={true} component={AboutPage} />
        <Route path="/userProflie" exact={true} component={UserProfilePage} />
        <Route path="/myOrders" exact={true} component={MyOrdersPage} />
        <Route path="/paymentHistory" exact={true} component={PaymentPage} />
        <Route path="/tgHomepage" exact={true} component={TGHomePage} />
        {/* <Route
          path="/partnership"
          exact={true}
          component={DownloadReactAppPage}
        /> */}
        <Route path="/videoStart" exact={true} component={JitsiPage} />
        {/* <Route path="/feedback" exact={true} component={RatingPage} /> */}
        {/* <Route
          path="/reviews/audience/createNewReview"
          exact={true}
          component={RatingPage}
        /> */}
        <Route path="/list" exact={true} component={OfferListPage} />
        <Route
          path="/list/:hashtag"
          exact={true}
          component={OfferListPageHashTag}
        />
        <Route path="/list/detail/:id" component={OfferIdPage}></Route>
        <Route path="/profile" exact={true} component={UserProfilePage} />
        {/* <Route path="/profile/:id" exact={true} component={UserProfilePage} /> */}
        {/* For testing */}
        {/* <Route path="/map_test" exact={true} component={Map}/> */}
        <Route component={NoMatch} />
      </Switch>
    </div>
  );
}

export default App;
