import React from "react";
import Rater from "react-rater";
import "react-rater/lib/react-rater.css";

const Ratings: React.FC = () => {
  return <Rater total={5} rating={3} />;
};

export default Ratings;
