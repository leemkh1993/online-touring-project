import React from "react";
import { useState } from "react";
// import GoogleSearchBar from "./GoogleSearchBar";
import GoogleMapReact from "google-map-react";
// import Marker from "./Marker";

const InitMap: React.FC = (props: any) => {
  const getMapOptions = (maps: any) => {
    return {
      disableDefaultUI: true,
      mapTypeControl: true,
      streetViewControl: true,
      styles: [
        {
          featureType: "poi",
          elementType: "labels",
          stylers: [{ visibility: "on" }],
        },
      ],
    };
  };

  const [center, setCenter] = useState({ lat: -33.863276, lng: 151.207977 });
  const [zoom, setZoom] = useState(12);

  function handleClick(value) {
    // console.log(`you got click`);
    // console.log(GoogleMapReact);
    // console.log(value);
    setCenter(center);
    setCenter(value);
    setZoom(value);
  }

  // const [fetch, isFetch] = useState(true)

  // console.log(props);
  //  {!isProcessing && students && students.length > 0 && (
  return (
    <div>
      {/* <h1>This is init map</h1> */}
      <>
        {/* {props.map((site) => (
       <GoogleSearchBar
        coordinates={props.coordinates}
        address={props.address}
      /> 
      <h3>{site.coordinates}</h3>
      <h3>{site.address}</h3>
      ))}       */}
      </>
      <div
        style={{
          height: "120vh",
          width: "100%",
        }}
      >
        <GoogleMapReact
          bootstrapURLKeys={{
            key: "AIzaSyCcj4DsZ7eMaZQKZteIVol34Zav5EJkKU4",

            libraries: ["places", "geometry", "search"],
          }}
          defaultCenter={center}
          defaultZoom={zoom}
          hoverDistance={10}
          onClick={handleClick}
          yesIWantToUseGoogleMapApiInternals={true}
          options={getMapOptions}
        >
          {/* <Marker lat={11.0168} lng={76.9558} name="My Marker" color="blue" /> */}
        </GoogleMapReact>
      </div>
    </div>
  );
};

export default InitMap;
