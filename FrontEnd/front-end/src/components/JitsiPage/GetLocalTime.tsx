import React, { useState, useEffect } from "react";

const GetLocalTime: React.FC = () => {
  const now = new Date().toLocaleTimeString();

  const [time, setTime] = useState(now);
  const [time2, setTime2] = useState(time);

  // function updateTime() {
  const newTime = new Date().toLocaleTimeString();

  useEffect(() => {
    setTime(newTime);
    setTime2(newTime);
    // console.log("update local time");
    // console.log(time);
    // console.log(time2);
  }, [setTime, newTime, time, time2]);

  // setInterval(updateTime, 20000);

  return (
    <div>
      <h1>Local Time :{time}</h1>
      {/* <button onClick={updateTime}>Click me!</button> */}
    </div>
  );
};

export default GetLocalTime;
