import React, { useState } from "react";
import { Jutsu } from "react-jutsu";
import "./../../css/JitsiPage/JitsiFrame.scss";

interface IRoomKeyProps{
  roomKey:string
  name:string
}

const JitsiFrame = (props:IRoomKeyProps) => {
  
  const [call, setCall] = useState(false);
  // const [room, setRoom] = useState("");
  // const [name, setName] = useState("");
  // const [password, setPassword] = useState("");

  const handleClick = (event) => {
    event.preventDefault();
    // if (room && name) setCall(true);
    if (props.roomKey && props.name) setCall(true);
  };


  //test roomKey stuff by LC
  // console.log(props.roomKey, props.name);

  return (
    <div
    //   style={{
    //     display: "flex",
    //     alignContent: "center",
    //     justifyContent: "center",
    //   }}
    >
      <h2>Jutsu Demo</h2>
      <blockquote>
        View the <a href="https://github.com/this-fifo/jutsu">source</a> for
        Jutsu and check{" "}
        <a href="https://github.com/jitsi/jitsi-meet/blob/master/doc/api.md">
          jitsi-meet
        </a>{" "}
        for the Jitsi Meet API
      </blockquote>
      {call ? (
        <Jutsu
          style={{
            width: "auto",
            height: "800vh !important"
          }}
          // 30/9 modified to props.matters by LC
          roomName={props.roomKey}
          displayName={props.name}
          // roomName={room}
          password={""}
          // displayName={name}
          onMeetingEnd={() => console.log("Meeting has ended")}
          loadingComponent={<p>ʕ •ᴥ•ʔ jitsi is loading ...</p>}
        />
      ) : (
        <form>
          {/* <input
            id="room"
            type="text"
            placeholder="Room"
            value={room}
            onChange={(e) => setRoom(e.target.value)}
          />
          <input
            id="name"
            type="text"
            placeholder="Name"
            value={name}
            onChange={(e) => setName(e.target.value)}
          /> */}
          {/* <input
            id="password"
            type="text"
            placeholder="Password (optional)"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          /> */}
          <button className="buttonStyle" onClick={handleClick} type="submit">
            Start / Join
          </button>
        </form>
      )}
      <p>
        Made with{" "}
        <span role="img" aria-label="coffee">
          ☕
        </span>{" "}
      </p>
    </div>
  );
};

export default JitsiFrame;

// Jutsu {
// width: auto;
// height: 800px;
// }
