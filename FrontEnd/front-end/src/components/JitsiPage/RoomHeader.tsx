import React, { useState } from "react";
import Typography from "@material-ui/core/Typography";

// interface LocationValue{
//   location: string;
// }

const RoomHeader: React.FC = () => {
  const [location] = useState("kangaroo Island");

  // console.log(setLocation);
  return (
    <div
      style={{
        color: "#fff",
        backgroundColor: "rgba(0, 0, 0, 0.2)",
      }}
    >
      <Typography style={{ color: "transparent" }} component="h1" variant="h3">
        Room ID: 1
      </Typography>
      <Typography style={{ color: "transparent" }} component="h1" variant="h6">
        Location: {location}
      </Typography>
    </div>
  );
};
export default RoomHeader;
