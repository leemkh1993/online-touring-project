import React, { useState } from "react";
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from "react-places-autocomplete";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
// import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import { IconButton, Paper } from "@material-ui/core";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import UploadImg from "./../TGHomePage/OfferForm/UploadImg";
import { IRootState } from "../../redux/store";

import { useSelector, useDispatch } from "react-redux";

import { addAddressSuccess } from "./../../redux/submitOffer/actions";
import "./../../css/TGHomePage/AddedSiteList.scss";

// interface toAddPlacesAddress {
//   coordinates: {
//     lat: null;
//     lng: null;
//   };
//   placeAddress: string
// }

// interface toAddPlacesAddressList{
//   list: Array[]
// }

type Coordinate = {
  lat: number;
  lng: number;
};

const GoogleSearchBar: React.FC = () => {
  // <string[number]>
  const [cloneCoordinate, setCloneCoordinate] = useState<Coordinate>({
    lat: 10,
    lng: 20,
  }); //clone and store lat,lng
  const [siteNames, setSiteNames] = useState<string[]>([]); //clone and store new sitename

  const [updateCoordinate, setUpdateCoordinate] = useState(""); //clone fetch coordinate
  const [updateLocation, setUpdateLocation] = useState(""); // clone fetch address

  const [placeAddress, setPlaceAddress] = useState("");
  const [address, setAddress] = useState(""); //init address
  const [coordinates, setCoordinates] = useState<{ lat: number; lng: number }>({
    lat: 0,
    lng: 0,
  });

  let tempLat = 0;
  let templng = 0;
  // let tempCoordinate;
  // let tempFileUrl = "";

  const dispatch = useDispatch();
  const toAddPlacesAddressList = useSelector(
    (state: IRootState) => state.addAddress.list
  );
  console.log(toAddPlacesAddressList);
  //value input from search bar
  //() receive string from what they selected
  const handleSelect = async (value) => {
    //client input values
    const results = await geocodeByAddress(value); //result of client select the suggestion, shows by address
    const latLng = await getLatLng(results[0]); //passing the first result
    const placeId = await geocodeByAddress(value);

    console.log(typeof coordinates); //Object
    console.log(results); //[{..}]
    console.log(`JSON Result: ${results}`); //Object Object
    console.log(`geocodeByAddress: ${value}`); //geocodeByAddress: Australia Fair Shopping Centre, Marine Parade, Southport QLD, Australia
    console.log(value); //Australia Fair Shopping Centre, Marine Parade, Southport QLD, Australia
    console.log(`placeId:__${placeId}`); //Object Object
    console.log(placeId); //[{..}]
    // console.log(placeId.geometry); //undefine

    setCoordinates(latLng);
    setPlaceAddress(value);
    // setCoordinates(coordinates);

    // console.log(latLng); //{lat: -27.4697707, lng: 153.0251235}
    setUpdateCoordinate(latLng);

    console.log(coordinates); //{lat: null,lag: null}
    console.log(updateCoordinate); //""

    let newCoodinates = cloneCoordinate;
    console.log(`newCoodonates: ${newCoodinates.lat} ${newCoodinates.lng}`); //10,20
    setCloneCoordinate(newCoodinates);

    console.log(latLng); // {lat: 47.6062095, lng: -122.3320708}

    // const cloneNewCoordinate = Object.values(updateCoordinate);
    // console.log(cloneNewCoordinate);
    // console.log(toAddPlacesAddressList);
    // tempLat = updateCoordinate.lat;
    // templng = updateCoordinate.lng

    // tempCoordinate = coordinates;
    tempLat = latLng.lat;
    templng = latLng.lng;
  };

  //   console.log(address);
  // console.log(PlacesAutocomplete.key);

  function handleClick(event: any) {
    console.log(`clicked`);
    setUpdateLocation(placeAddress);
    console.log(typeof updateCoordinate); //Object

    const cloneNewCoordinate = Object.values(updateCoordinate);
    console.log(`confirm coordinate: ${coordinates.lat} ${coordinates.lng}`); //cloneNewCoordinate: 13.3736776 77.2074614
    console.log(cloneNewCoordinate);

    let newSiteName = siteNames; //clone new siteNames as in every new input
    newSiteName.push(placeAddress);
    setSiteNames(newSiteName);

    setCloneCoordinate({ lat: tempLat, lng: templng });
    console.log(`update coordinates values : ${cloneNewCoordinate}`); //[13.3736776, 77.2074614]

    console.log(
      `clone coordinates values : lat: ${cloneCoordinate.lat} lng: ${cloneCoordinate.lng}` //lat: 10, lng:20
    );
    console.log(cloneCoordinate); //{lat: 10, lng:20}

    dispatch(
      addAddressSuccess({
        coordinates: { lat: coordinates.lat, lng: coordinates.lng },
        placeAddress: newSiteName[0],
        // fileUrl: tempFileUrl,
      })
    );

    event.preventDefault();
    // setSiteNames("");
    // setCloneCoordinate("");
    // event.target.value = "";
  }

  const getFileUrl = (url: string, idx) => {
    //dispatch()
  };

  console.log(updateLocation); //input address e.g. DD Hills
  // console.log(toAddPlacesAddressList); //{new array list}
  console.log(coordinates.lat);
  console.log(coordinates.lng);

  return (
    <div>
      <PlacesAutocomplete
        value={address} // state value
        onChange={setAddress} //eveny time it changes when select the suggestion div and set newState
        onSelect={handleSelect} //async function (client select from UI) ==> this is get called when it is actually select one
      >
        {(
          { getInputProps, suggestions, getSuggestionItemProps, loading } //input props
        ) => (
          <div className="Add__SiteLocation__Container">
            {/* <h6 style={{ color: "#000" }}>Latitude is: {coordinates.lat}</h6> */}
            {/* <h6 style={{ color: "#000" }}>Longitude is: {coordinates.lng}</h6> */}
            <h6 className="noMargin">
              <div className="customDiv">
                {/* {toAddPlacesAddressList
                  ? JSON.stringify(toAddPlacesAddressList.length)
                  : ""} */}
                Please click (+) button if you confirm the site location: <br />
                {placeAddress}
                {/* placeAddress */}
              </div>
            </h6>

            <Paper className="AddSiteName__Container" component="form">
              <div>
                <input
                  className="inputValues"
                  style={{ width: "80%", padding: 10, borderRadius: "5px" }}
                  {...getInputProps({ placeholder: "Type address" })}
                />
                <IconButton
                  onClick={handleClick}
                  className="iconButton"
                  aria-label="AddCircleIcon"
                >
                  <AddCircleIcon />
                </IconButton>
              </div>
            </Paper>

            {siteNames && siteNames.length > 0 && (
              <List className="Add_SiteName_And_Photo_Container">
                {siteNames.map((
                  site: any, //sitenames are every new inputs from UI
                  idx: number
                ) => (
                  <>
                    <ListItem className="ListItem">
                      <div className="ListItem__siteName" />
                      <ListItemText
                        className="location__name"
                        primary={`${site}`}
                      />
                      <UploadImg fileSelect={getFileUrl} idx={idx} />
                    </ListItem>

                    {/* <ListItem className="ListItem">
                      <div className="ListItem__siteName" />
                      <ListItemText
                        className="location__name"
                        primary={`${site} lag:${coordinates.lat} lng:${coordinates.lng}`}
                      />
                    </ListItem> */}
                  </>
                ))}
              </List>
            )}

            <div>
              {loading ? <div>...loading</div> : null}

              {suggestions.map((suggestion) => {
                //list of suggestion shows from lib
                const style = {
                  backgroundColor: suggestion.active ? "#eeeeee" : "#fff", //dropdown menu
                };

                return (
                  // have to pass in the suggestion at first and assign the div to display the suggestion, get item from props which suggest from the get...
                  <div {...getSuggestionItemProps(suggestion, { style })}>
                    {suggestion.description}
                    {/* getSuggestionItemProps from lib which suggests for clients and that would be the additional rendering */}
                  </div>
                );
              })}
            </div>
          </div>
        )}
      </PlacesAutocomplete>
    </div>
  );
};

export default GoogleSearchBar;
