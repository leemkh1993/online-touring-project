import React from "react";
import "./../../css/JitsiPage/Marker.css";

const Marker = (props: any) => {
  const { color, name, id } = props;
  // console.log({ color, name, id });
  return (
    <div>
      <div
        key={id}
        className="pin bounce"
        style={{ backgroundColor: color, cursor: "pointer" }}
        title={name}
      />
      <div className="pulse" />
    </div>
  );
};

export default Marker;
//This is for self-define
