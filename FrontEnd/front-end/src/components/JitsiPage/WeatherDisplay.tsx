// import { CallHistoryMethodAction } from "connected-react-router";
import React, { useEffect, useState } from "react";
// import { IAuthActions, loginSuccess } from "../../redux/auth/actions";
// import { IRootState } from "../../redux/store";
import "./../../css/JitsiPage/WeatherDisplay.scss";

interface Weather {
  id: number;
  main: string;
  description: string;
}

interface Info {
  weather: Array<Weather>;
  [key: string]: any;
}
// const { REACT_APP_API_SERVER } = process.env;

const WeatherDisplay: React.FC = () => {
  // const [enterRoomCountry, setEnterRoomCountry] = useState(false);
  const [query, setQuery] = useState("Hong Kong");
  const [weather, setWeather] = useState<Info>();

  const api = {
    key: "0c2626867b41e8929315f6d79f641794",
    base: "https://api.openweathermap.org/data/2.5/",
  };

  useEffect(() => {
    async function enterRoomLocation(event: any) {
      // try {
      // setEnterRoomCountry(enterRoomCountry);
      // setQuery("Roma");

      const res = await fetch(
        `${api.base}weather?q=${query}&units=metric&appid=${api.key}`
      );
      if (res.status === 200) {
        // const result = await res.json();
        // console.log(result);
      }
    }
    // setQuery("");
    enterRoomLocation(query);
    // console.log(query);
    // setEnterRoomCountry(enterRoomCountry)
  }, [setQuery]);

  // console.log(setEnterRoomCountry);

  // const getUserId = () => {
  //   return async function (
  //     dispatch: Dispatch<IAuthActions | CallHistoryMethodAction>,
  //     getState: () => IRootState
  //   ) {
  //     const userId = localStorage.getItem("token");
  //     if (userId) {
  //       console.log(userId);
  //       const res = await fetch(`${REACT_APP_API_SERVER}/user/info`, {
  //         headers: {
  //           Authorization: `Bearer ${token}`,
  //         },
  //       });
  //       if (res.status === 200) {
  //         dispatch(loginSuccess());
  //       }
  //     }
  //   };
  // };

  async function search(event: any) {
    if (event.key === "Enter") {
      // console.log(event.key);
      const res = await fetch(
        `${api.base}weather?q=${query}&units=metric&appid=${api.key}`
      );
      if (res.status === 200) {
        const weather = await res.json();
        setWeather(weather);
        setQuery("");
        // console.log(weather);
        // console.log(weather?.weather);
        // console.log(weather.main.temp);
      }
    }
  }

  // const search = (event: any) => {
  //   if (event.key === "Enter") {
  //     console.log(event.key);
  //     fetch(`${api.base}weather?q=${query}&units=metric&appid=${api.key}`)
  //       .then((res) => res.json())
  //       .then((result) => {
  //         setWeather(result);
  //         setQuery("");
  //         console.log(result);
  //       });
  //   }
  // };

  const dateBuilder = (data: any) => {
    let months = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];
    let days = [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
    ];

    let day = days[data.getDay()];
    let date = data.getDate();
    let month = months[data.getMonth()];
    let year = data.getFullYear();

    return `${day} ${date} ${month} ${year}`;
  };

  //  weather.main;
  // weather.name;
  // weather.sys.country;
  // weather.main.temp;
  // weather.weather[0].description;

  return (
    // {enterRoomCountry ==  (
    <div
      className={
        typeof weather != "undefined"
          ? weather.main.temp > 12
            ? "app warm"
            : "app"
          : "app warm"
      }
    >
      <main>
        <div className="search-box">
          <input
            type="text"
            className="search-bar"
            placeholder="Your Location is ..."
            onChange={(e) => setQuery(e.target.value)}
            value={query}
            onKeyPress={search}
          />
        </div>
        {typeof weather != "undefined" ? (
          <div>
            <div className="location-box">
              <div className="location">
                {weather.name}, {weather.sys.country}
              </div>
              <div className="date">{dateBuilder(new Date())}</div>
            </div>
            <div className="weather-box">
              <div className="temp">{Math.round(weather.main.temp)}°c</div>
              <div className="weather">{weather.weather[0].description}</div>
            </div>
          </div>
        ) : (
          ""
        )}
      </main>
    </div>
  );
};

export default WeatherDisplay;
