import React from "react";

import "./../../css/NavBar/NavBar.scss";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
// import { Switch } from "antd";
import SwitchGotClicked from "./SwitchGotClicked";
import UserIcon from "./UserIcon";
import { IRootState } from "../../redux/store";
import dummyicon from "../../assets/dummyicon.png";
const NavBar: React.FC = () => {
  const isAuthenticated = useSelector(
    (state: IRootState) => state.auth.isAuthenticated
  );
  // const date = new Date();
  // const currentTime = date.getHours();

  // console.log(date);
  // console.log(currentTime);
  // let greeting;
  // if (currentTime < 12) {
  //   greeting = "Good Morning!";
  // } else if (currentTime < 18) {
  //   greeting = "Good Afternoon!";
  // } else {
  //   greeting = "Good Evening!";
  // }
  // console.log(greeting);
  return (
    <>
      <nav
        style={{ backgroundColor: "#fff !important", padding: 0}}
        className="navbar navbar-expand-sm navbar-light bg-white"
      >
        {/* <span>{greeting}</span> */}
        <div
          className="brandLogo"
          style={{
            display: "flex",
            flex: 1,
            marginLeft: 20,
            justifyContent: "flex-start",
            borderRadius: "10px",
          }}
        >
          <Link to="/" className="linkGroup">
            {/* Brand Logo */}
            <img className="dummyImg" style={{height: 80, width:"auto", paddingLeft:"10px", backgroundColor: "transparent"}} src={dummyicon} alt=""/>
          </Link>
        </div>
        <div className="unionisAuthButtons">
          {isAuthenticated ? (
            <div>
              <Link to="/tgHomePage" className="linkGroup">
                <SwitchGotClicked />
              </Link>
              <Link to="/list" className="linkGroup">
                List
              </Link>
              <Link to="/profile" className="linkGroup">
                {/* Profile */}
                <UserIcon />
              </Link>
            </div>
          ) : (
            <div className="unionNotisAuthButtons">
              <div>
                <Link to="/login" className="link">
                  Login
                </Link>
              </div>
              <div>
                <Link to="/register" className="link">
                  Register
                </Link>
              </div>
            </div>
          )}
        </div>
      </nav>
    </>
  );
};

export default NavBar;
