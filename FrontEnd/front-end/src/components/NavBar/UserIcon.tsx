import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { IRootState } from "../../redux/store";
import { getUserProfileThunk } from "../../redux/getPersonalUserRecords/thunk";
import Avatar from "antd/lib/avatar/avatar";
// import { Avatar } from "@rent_avail/avatar";
// import { Avatar } from "./Avatar";

// const { REACT_APP_API_SERVER } = process.env;

const UserIcon: React.FC = () => {
  const dispatch = useDispatch();

  const userProfileItems = useSelector(
    (state: IRootState) => state.getPersonalUserRecords.userProfile
  );

  useEffect(() => {
    dispatch(getUserProfileThunk());
  }, [dispatch]);

  // console.log(userProfileItems);
  // console.log(localStorage.getItem("email"));
  // <>
  //   <div className="nameContainer">
  //     <div className="initials">
  //       <h1 className="userInitials">C</h1>
  //     </div>
  //   </div>
  // </>
  return (
    <div>
      {userProfileItems.map((userProfileItem: any) => (
        <div>
          <Avatar style={{ backgroundColor: "#f56a00" }}>
            {userProfileItem.user.first_name.slice(0, 1)}
          </Avatar>
          {/* <Avatar key={userProfileItem.user.first_name} /> */}
          {/* <Avatar
            src={`${REACT_APP_API_SERVER}/cast_profile_pic/${userProfileItem.user.profile_pic}/48x48`}
          />
          <Avatar /> */}
        </div>
      ))}
    </div>
  );
};

export default UserIcon;
