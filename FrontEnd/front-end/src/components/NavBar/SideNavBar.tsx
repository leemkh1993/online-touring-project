import React from "react";
import { Button } from "reactstrap";
import { Link } from "react-router-dom";

import SideNav, { NavItem, NavIcon, NavText } from "@trendmicro/react-sidenav";
import HomeIcon from "@material-ui/icons/Home";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import PersonRoundedIcon from "@material-ui/icons/PersonRounded";
// import GroupAddIcon from "@material-ui/icons/GroupAdd";
import RemoveRedEyeOutlinedIcon from "@material-ui/icons/RemoveRedEyeOutlined";
// import VoiceChatOutlinedIcon from "@material-ui/icons/VoiceChatOutlined";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
// import ShoppingCartOutlinedIcon from "@material-ui/icons/ShoppingCartOutlined";
// import CreditCardOutlinedIcon from "@material-ui/icons/CreditCardOutlined";

import { useSelector } from "react-redux";
import { IRootState } from "../../redux/store";
import "./../../css//NavBar/SideNavBar.scss";
import "@trendmicro/react-sidenav/dist/react-sidenav.css";

const SideNavBar: React.FC = () => {
  const isAuthenticated = useSelector((state: IRootState) => {
    return state.auth.isAuthenticated;
  });
  // const [visible, setVisible] = useState(true);
  // function customizeSideNav(event: any) {
  //   setVisible(false);
  // }

  // function logoutHandler(event: any) {
  //   event.preventDefault();
  // }

  return (
    <div>
      {/* {visible && ( */}
      <SideNav className="sidenav">
        <SideNav.Toggle
        // onClick={customizeSideNav}
        />
        <SideNav.Nav defaultSelected="home">
          <NavItem eventKey="home">
            <NavIcon className="bg-light">
              <Link to="/home" className="link">
                <HomeIcon />
              </Link>
            </NavIcon>
            <NavText className="homeColor" style={{ color: "#000!important" }}>
              Home
            </NavText>
            <NavItem eventKey="signin">
              {!isAuthenticated && (
                <NavText>
                  <Link to="/login" className="link">
                    <Button
                      className="bg-dark addMargin"
                      color="Default"
                      type="submit"
                    >
                      <AccountCircleIcon />
                      <span style={{ color: "#fff" }}>Sign in</span>
                    </Button>
                  </Link>
                  <hr />
                </NavText>
              )}
            </NavItem>
          </NavItem>

          <NavItem eventKey="user">
            <NavIcon className="bg-light">
              <Link to="/userProflie" className="link">
                <PersonRoundedIcon />
              </Link>
            </NavIcon>
            <NavText>My profile</NavText>

            {/* <NavText>User Profile</NavText> */}

            {/* <NavItem eventKey="user/items">
              <NavText>
                <Link to="/userProflie" className="link">
                  <PersonRoundedIcon />
                </Link>
                {"  "} My profile
              </NavText>
            </NavItem>

            <NavItem eventKey="user/items">
              <NavText>
                <Link to="/myOrders" className="link">
                  <ShoppingCartOutlinedIcon />
                </Link>
                {"  "}My order
              </NavText>
            </NavItem>

            <NavItem eventKey="user/items">
              <NavText>
                <Link to="/paymentHistory" className="link ">
                  <CreditCardOutlinedIcon />
                </Link>
                {"  "}Payment
              </NavText>
            </NavItem> */}
          </NavItem>
          <NavItem eventKey="about">
            <NavIcon className="bg-light">
              <Link to="/about" className="link">
                <RemoveRedEyeOutlinedIcon />
              </Link>
            </NavIcon>
            <NavText>About</NavText>
          </NavItem>
          {/* <NavItem eventKey="video">
            <NavIcon className="bg-light">
              <Link to="/videoStart" className="link">
                <VoiceChatOutlinedIcon />
              </Link>
            </NavIcon>
            <NavText>360°c VR</NavText>
          </NavItem> */}

          {isAuthenticated && (
            <NavItem eventKey="/">
              <NavIcon className="bg-light">
                <Link to="/Logout" className="link">
                  <ExitToAppIcon />
                </Link>
              </NavIcon>
              <NavText>logout</NavText>
            </NavItem>
          )}
        </SideNav.Nav>
      </SideNav>
    </div>
  );
};

export default SideNavBar;
