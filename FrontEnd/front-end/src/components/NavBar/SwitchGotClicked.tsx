import React, { useState } from "react";
import { Switch } from "antd";
import { Link } from "react-router-dom";

const SwitchGotClicked: React.FC = () => {
  const [customize, setCustomize] = useState({ customize: true }); //{list offer: true}
  const [state, setState] = useState({ goBack: false });
  const [TGrole, setTGrole] = useState(false);

  function handleButtonChange(value: any) {
    // console.log(value); //this is value false
    setCustomize({ customize: value });
    // console.log("list offer got click:", customize); //{customize: true}
    setTGrole(customize.customize);
    // console.log("TGPage: ", TGrole);
  }
  function handleClicked(event: any) {
    // console.log(event); //this is value false
    setState(state);
    // console.log("switch to TGPage button status:", state); // {goBack: false}
  }

  return (
    <div>
      {!TGrole ? (
        <Switch
          // defaultChecked
          unCheckedChildren="Explore"
          checkedChildren="Inspire"
          checked={customize.customize} //{list offer : true}
          onChange={handleButtonChange}
          onClick={handleClicked}
        />
      ) : (
        <Link to="/" className="LinkGroup">
          <Switch
            unCheckedChildren="Explore"
            checkedChildren="Inspire"
            checked={state.goBack}
            onChange={handleButtonChange}
          />
        </Link>
      )}
    </div>
  );
};

export default SwitchGotClicked;
