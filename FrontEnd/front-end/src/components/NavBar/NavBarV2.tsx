import React from "react";
import "./../../css//NavBar/NavBar.scss";
import { NavLink, Link } from "react-router-dom";

//old
const NavBarV2: React.FC = () => {
  return (
    <>
      <nav className="navbar navbar-expand-sm navbar-light bg-light">
        <NavLink activeClassName="active" to="/" className="link">
          Home
        </NavLink>
        <Link to="/about" className="link">
          About Us
        </Link>
        <Link to="/touristProfiles" className="link">
          View Tourist
        </Link>
        <Link to="/partnership" className="link">
          Partner with us
        </Link>
      </nav>
    </>
  );
};

export default NavBarV2;
