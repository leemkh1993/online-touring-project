import React, { useState } from "react";
import { Button, Form, FormGroup, Label, Input, Alert } from "reactstrap";
import { useForm } from "react-hook-form";

// import FormControl from "@material-ui/core/FormControl";
// import PasswordField from "material-ui-password-field";
// import { PasswordField } from "material-ui-password-field/PasswordField";

import "./../../css/LoginPage/LoginForm.scss";
import { useSelector, useDispatch } from "react-redux";
import { IRootState } from "../../redux/store";
import { loginThunk } from "../../redux/auth/thunks";
// import { InputLabel } from "@material-ui/core";

interface ILoginFormInputs {
  email: string;
  password: string;
}

const LoginForm: React.FC = () => {
  const [loginUser, setLoginUser] = useState("");
  const [isGreetings, setGreetings] = useState("");
  // console.log(isGreetings);

  const dispatch = useDispatch();

  const errMessage = useSelector((state: IRootState) => state.auth.message);
  const isProcessing = useSelector(
    (state: IRootState) => state.auth.isProcessing
  );

  const { register, handleSubmit } = useForm<ILoginFormInputs>();

  const onSubmit = (data: ILoginFormInputs) => {
    // console.log("Login form submitted");
    // console.log(data);
    if (!isProcessing) {
      dispatch(loginThunk(data.email, data.password));
      // added for convien. by LC
      localStorage.setItem("email", data.email);
    }
  };

  function seeYourPassword(event: any) {
    // console.log(event.target.value);
  }

  function handleChange(event: any) {
    // console.log(event.target.value);
    setLoginUser(event.target.value);
  }

  function handleClick(event: any) {
    // console.log(event.target.value);
    setGreetings(loginUser);
    // event.preventDefault();
  }

  return (
    <>
      <section className="Login_container">
        {/* <img className="Login-img" src="https://cdn-cms.f-static.net/ready_uploads/media/7170/2000_5cda66c7dddd6.jpg" alt=""/> */}
        <div className="LoginForm">
          {/* {console.log("This is Login page...")} */}
          {/* <h1>This is Login Page</h1> */}
          {/* {!isAuthenticated && (
          <div>
            <p>You have a wrong input</p>
          </div>
        )} */}
          <h1 className="title">
            {" "}
            Login
            {/* <span>No Limits !</span>{" "} */}
          </h1>
          <i className="fas fa-arrow-alt-circle-down"></i>
          {isProcessing && (
            <h1 style={{color:"#fff"}}> 
              Welcome Backed {isGreetings}!
              {/* {console.log("Hi User!")} */}
            </h1>
          )}

          <Form onSubmit={handleSubmit(onSubmit)}>
            <FormGroup>
              <Label for="username" className="subtitle">
                Username
              </Label>
              <Input
                className="textinput"
                onChange={handleChange}
                type="text"
                name="email"
                value={loginUser}
                required
                placeholder=""
                innerRef={register}
              />
            </FormGroup>
            <FormGroup>
              <Label for="password" className="subtitle">
                Password
              </Label>
              <Input
                onChange={seeYourPassword}
                className="textinput"
                type="password"
                name="password"
                required
                placeholder=""
                innerRef={register}
              />
            </FormGroup>
            {errMessage ? <Alert color="danger">{errMessage}</Alert> : ""}
            <Button
              color="primary"
              onClick={handleClick}
              type="submit"
              className="LoginForm_submit"
            >
              Submit
            </Button>
            <hr />
          </Form>
        </div>
      </section>
    </>
  );
};

export default LoginForm;
