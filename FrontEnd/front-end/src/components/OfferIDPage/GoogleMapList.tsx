import React from 'react';
import GoogleMapReact from 'google-map-react';

import LocationPin from "./LocationPin";

import "../../css/OfferPage/map_ref.css";

const { REACT_APP_GOOGLE_MAP_API_KEY } = process.env;

// const location = {
//     address: '1600 Amphitheatre Parkway, Mountain View, california.',
//     lat: 37.42216,
//     lng: -122.08427,
// }
interface ILocateProps{
    address:string,
    lat:string,
    lng:string
}


const zoomLevel=14;

const Map = (props:ILocateProps) => {

    
    const location ={
        address: props.address,
        lng:parseFloat(props.lng) ,
        lat:parseFloat(props.lat) 
    }
    // console.log(location);
    return(
        <div className="map">

        
            <div className="google-map">
                <GoogleMapReact
                bootstrapURLKeys={{ key: `${REACT_APP_GOOGLE_MAP_API_KEY}` }}
                defaultCenter={location}
                defaultZoom={zoomLevel}
                >
                    <LocationPin
                        key={10}
                        lng={location.lng}
                        lat={location.lat}
                        text={props.address}
                    />
                </GoogleMapReact>
            </div>
        </div>
    )
    
};

export default Map;
