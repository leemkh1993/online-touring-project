import React, { useState } from "react";
import {addCommentThunk} from "../redux/commentSuccess/thunk";
import { Modal, ModalHeader, ModalBody, ModalFooter,Form, Label, Input } from 'reactstrap';
import {useForm} from 'react-hook-form';
import { useDispatch } from "react-redux";

import styles from "../css/UserProfile/UserProfile.module.css";

interface IUserInfoProps{
    user:any
    audienceID: number
    orderID: number

}

interface ICommentForm{
    comment:string,
    rating:string
}


const CommentBox= (props:IUserInfoProps)=>{

    const [isClose, setIsClose] = useState(true);

    const {register,handleSubmit} = useForm<ICommentForm>({
        defaultValues:{
            comment:"",
            rating:"",
        }
    });

    const dispatch = useDispatch();
    const onSubmit = (data:ICommentForm)=>{
        dispatch(addCommentThunk(
            props.audienceID, 
            props.orderID, 
            data.comment, 
            data.rating
        ));
        setIsClose(false);
    }

    return(
        <>
        <Modal isOpen={isClose}  >
            <ModalHeader closeButton>Please enter your comment</ModalHeader>
            <ModalBody>
                <Form onSubmit={handleSubmit(onSubmit)}>
                    <div className={styles.Label_box}>
                    <Label className={styles.colorB}>
                        
                        <input type='text' name="comment" ref={register} className={styles.comment_box}/>
                    </Label>
                    <Label className={styles.colorB}>
                        Your rating: 
                        <input type='text' name="rating" ref={register} className={styles.rating_box}/>
                        / 5
                    </Label>
                    </div>
                    <Input type='submit' value="Submit" />
                </Form>
            </ModalBody>
            <ModalFooter>
                {/* <Button color="primary" onClick={()=>{}}>Button</Button> */}
            </ModalFooter>
        </Modal>
        </>
    )
}

export default CommentBox;