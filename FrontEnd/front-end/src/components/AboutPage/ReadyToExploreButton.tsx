import React from "react";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";

const ReadyToExploreButton: React.FC = () => {
  return (
    <div>
      <Link to="/login" className="link">
        <Button
          className="explorebtn"
          color="primary"
          type="submit"
          variant="contained"
        >
          Ready to explore
        </Button>
      </Link>
    </div>
  );
};

export default ReadyToExploreButton;
