import React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import "./../../css/AboutPage/AboutPage.scss";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
      marginTop: theme.spacing(3),
    },
    backButton: {
      marginRight: theme.spacing(3),
      marginBottom: theme.spacing(2),
      backgroundColor: "#eee !important",
      border: "solid 1px currentColor !important",
    },
    instructions: {
      marginTop: theme.spacing(2),
      marginBottom: theme.spacing(2),
    },
    nextButton: {
      marginTop: theme.spacing(2),
      marginBottom: theme.spacing(4),
      border: "solid 1px currentColor !important",
    },
  })
);

function getSteps() {
  return [
    "Select your role in SourGrapes ",
    "You will get sign up and becomes either an traveller or earner or both. It's ready up to your choices!",
    "If you are the former, you are going to taste an ultimate travel experiences through live video chat which leads by local citizen; whereas an latter can make some incomes at their free times through each trip",
    "Once you have sign in, you can either start organising your trips or start earning at SourGrapes!",
    "We can't wait to have you on board too! Let us know if we could be of further assistance! See yaaaaaaaa!",
  ];
}

function getStepContent(stepIndex: number) {
  switch (stepIndex) {
    case 0:
      return "What roles can I choose?";
    case 1:
      return "What are the main differencies anyways?";
    case 2:
      return "This is the bit I really care about! What's next?";
    case 3:
      return "Sounds like a plan! I'm ready to begin the journey here!";
    case 4:
      return "See yaaaaaaaaaaa!";
    default:
      return "Unknown stepIndex";
  }
}

const ServiceFlow: React.FC = () => {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const steps = getSteps();

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  return (
    <>
      <div className="footer">
        <Typography variant="h2" color="textPrimary" component="p">
          Something you don't want to miss...
        </Typography>
      </div>
      <div className={classes.root}>
        <Stepper activeStep={activeStep} alternativeLabel>
          {steps.map((label) => (
            <Step key={label}>
              <StepLabel>{label}</StepLabel>
            </Step>
          ))}
        </Stepper>
        <div>
          {activeStep === steps.length ? (
            <div>
              <Typography className={classes.instructions}>
                Press "Reset" to can go over the flow again.
              </Typography>
              <Button className={classes.nextButton} onClick={handleReset}>
                Reset
              </Button>
            </div>
          ) : (
            <div>
              <Typography className={classes.instructions}>
                {getStepContent(activeStep)}
              </Typography>
              <div>
                <Button
                  disabled={activeStep === 0}
                  onClick={handleBack}
                  className={classes.backButton}
                >
                  Back
                </Button>
                <Button
                  variant="contained"
                  color="primary"
                  className={classes.nextButton}
                  onClick={handleNext}
                >
                  {activeStep === steps.length - 1 ? "OK" : "Next"}
                </Button>
              </div>
            </div>
          )}
          {/* <div style={{ display: "flex", alignItems: "center" }}> */}
          {/* <Typography className="customColor" variant="h2">
            Take a break, Taste the grapes
            <ReadyToExploreButton />
          </Typography> */}
          {/* </div> */}
        </div>
      </div>
    </>
  );
};

export default ServiceFlow;
