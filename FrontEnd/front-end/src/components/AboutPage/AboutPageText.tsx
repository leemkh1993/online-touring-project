import React from "react";

import "./../../css/AboutPage/AboutPage.scss";

const AboutPageText: React.FC = () => {
  // const [loginUser, setLoginUser] = useState("");
  return (
    <div className="header">
      <div className="wrapHeaderText">
        <h2>Stay coooooool & always stay tuned!</h2>
        <p>
          We will navigate you through the journey with the best person angles
          via video streaming and it's going to be 360 degree virtual travel, an
          unique travel experience for you! Save up your thousand dollars on airfares and travel free with SourGrapes!</p>
      </div>
      <hr />
    </div>
  );
};

export default AboutPageText;
