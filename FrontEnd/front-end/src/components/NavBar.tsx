import React from "react";
// import "../css/NavBar.scss";
import "./../../css//NavBar/NavBar.scss";

import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import RoleSwitchingButton from "./../components/HomePage/RoleSwitchingButton";

import UserIcon from "./../components/NavBar/UserIcon";
import { IRootState } from "../redux/store";

// import { useSelector, useDispatch } from "react-redux";
// const dispatch = useDispatch();

const NavBar: React.FC = () => {
  const isAuthenticated = useSelector(
    (state: IRootState) => state.auth.isAuthenticated
  );

  return (
    <>
      <nav className="navbar navbar-expand-sm navbar-light bg-light">
        <div className="brandlogo">
          <Link to="/" className="link">
            Brand Logo
          </Link>
        </div>

        <div className="unioninAuthButtons">
          <div>
            {isAuthenticated && (
              <Link to="tgHomepage" className="link">
                <RoleSwitchingButton />
              </Link>
            )}
          </div>
          <div>{isAuthenticated && <UserIcon />}</div>{" "}
        </div>

        {/* all are drawn to sideBar now */}
        {/* <Link to="/logout" className="link">
            Logout
          </Link> */}
        {/* <Link to="/userProflie" className="link">
          Proflie
        </Link>
        <Link to="/about" className="link">
          About Us
        </Link>
        <Link to="/partnership" className="link">
          Join us to earn
        </Link>
        <Link to="/myOrders" className="link">
          My orders
        </Link>
        <Link to="/paymentHistory" className="link">
          Payment
        </Link> */}
        <Link to="/list" className="link">
          List
        </Link>
        <Link to="/profile" className="link">
          Profile
        </Link>
        <div className="unionNotisAuthButtons">
          <div>
            {!isAuthenticated && (
              <Link to="/login" className="link">
                Login
              </Link>
            )}
          </div>
          <div>
            {!isAuthenticated && (
              <Link to="/register" className="link">
                Register
              </Link>
            )}
          </div>
        </div>
      </nav>
    </>
  );
};

export default NavBar;
