import React from "react";
import CopyrightIcon from "@material-ui/icons/Copyright";
import Typography from "@material-ui/core/Typography";
import "./../../css/HomePage/DownloadAppBanner.scss";
// import Button from "@material-ui/core/Button";
import MobileStoreButton from "./DownloadReactApp/MoblieStoreButton";

const iOSUrl = "https://itunes.apple.com";
// const androidUrl = "https://play.google.com";

const DownloadAppBanner: React.FC = () => {
  return (
    <>
      <div className="bannerDiv">
        <Typography
          style={{ transform: "scaleX(-1)" }}
          className="banner"
          variant="h1"
          color="textPrimary"
          component="p"
        >
          <p>Rate us if you love our product!</p>
        </Typography>
        <div
          style={{
            position: "relative",
            bottom: 300,
            left: 380,
            // display: "flex",
            // alignItems: "center",
            // justifyContent: "flex-start",
            // marginLeft: 1090,
            // marginRight: 450,
            // transform: "scaleX(-1)",
          }}
        >
          <MobileStoreButton
            store="ios"
            url={iOSUrl}
            linkProps={{ title: "iOS Store Button" }}
          />
        </div>
      </div>
      <p className="styleP">
        CopyRight <CopyrightIcon /> @2020
      </p>
    </>
  );
};
export default DownloadAppBanner;
