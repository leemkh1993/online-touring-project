import React, { useEffect } from "react";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";

// .MuiSwitch-colorPrimary.Mui-checked {
//   color: #3f51b5;
// }
// <style>
// .MuiSwitch-switchBase.Mui-checked {
//   transform: translateX(20px);
// }

const RoleSwitchingButton: React.FC = () => {
  const [state, setState] = React.useState({
    checked: false,
  });

  let checked = false;
  useEffect(() => {
    if (!checked) {
      setState(state);
      // console.log(checked);
    }
    return () => {
      // console.log(checked);
      setState({ checked: true });
    };
  }, [state]);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };
  return (
    <>
      <FormGroup row>
        <FormControlLabel
          control={
            <Switch
              // color="secondary"
              color="default"
              checked={state.checked}
              onChange={handleChange}
              onClick={(event: any) => HTMLFormControlsCollection}
              name="checked"
            />
          }
          label="list offer"
        />
      </FormGroup>
    </>
  );
};

export default RoleSwitchingButton;
