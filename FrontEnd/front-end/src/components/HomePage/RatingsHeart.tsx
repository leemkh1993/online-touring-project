import React from "react";
import Card from "@material-ui/core/Card";
import FavoriteIcon from "@material-ui/icons/Favorite";
import CardContent from "@material-ui/core/CardContent";

import "../../css/HomePage/UserRatingCard.scss";

const RatingHeart: React.FC = () => {
  return (
    <Card>
      <CardContent className="styleIcon">
        {/* <HeartShape /> */}
        {/* <FavoriteBorderSharpIcon /> */}
        <FavoriteIcon />
        <FavoriteIcon />
        <FavoriteIcon />
        <FavoriteIcon />
        <FavoriteIcon />
      </CardContent>
    </Card>
  );
};

export default RatingHeart;
