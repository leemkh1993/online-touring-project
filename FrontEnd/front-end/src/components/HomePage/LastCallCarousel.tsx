import React, { useEffect, useState } from "react";
import { Button } from "reactstrap";
import "./../../css/HomePage/LastCallCarousel.scss";

import HowToRegOutlinedIcon from "@material-ui/icons/HowToRegOutlined";
import LocationOnOutlinedIcon from "@material-ui/icons/LocationOnOutlined";
// import { IRootState } from "../../redux/store";
// import { useSelector } from "react-redux";
// import useReactRouter from "use-react-router";

// import { Route, Link, Switch } from "react-router-dom";
// import GoogleMap from "../../components/JitsiPage/GoogleMap";
//  useDispatch

const { REACT_APP_API_SERVER } = process.env;

const LastCallOfferCarousel: React.FC = () => {
  const [updateUI, setUpdateUI] = useState(false);
  const [name, setName] = useState(null);
  const [description, setDescription] = useState("");
  const [cost, setCost] = useState(null);
  const [journeyPicture, setJourneyPicture] = useState("");
  const [expiryDate, setExpiryDate] = useState("");

  // const params = useReactRouter().match.params as { id: string };
  // const offerIdItems = useSelector(
  //   (state: IRootState) => state.browseOffer.offerIdItems
  // );

  // const userInfos = useSelector(
  //   (state: IRootState) => state.browseUser.userInfo
  // );

  // const userId = offerIdItems.map((offerIdItem: any) => {
  //   return offerIdItem.offer[0].userId;
  // });
  // console.log(params)
  // console.log(userInfos)
  // console.log(userId);

  useEffect(() => {
    // dispatch(getSoonExpireOffersThunk(parseInt(params.id)));
    // getSoonExpireOffersThunk(parseInt(params.id));
    // console.log(params.id);

    // dispatch(getSoonExpireOffersThunk(parseInt(params.id)));
    // dispatch(getSoonExpireOffersThunk(parseInt(userId[0])));

    // function getSoonExpireOffersThunk(offerId: number) {
    // return async (dispatch: Dispatch) => {

    async function getSoonExpireOffer(value: any) {
      try {
        const res = await fetch(`${REACT_APP_API_SERVER}/offer/soonExpire`);
        // "http://localhost:8080/offer/soonExpire"

        const result = await res.json();

        // let API_userId = result[0].user_id;
        let API_name = result[0].name;
        let API_description = result[0].description;
        let API_cost = result[0].cost;
        let API_journeyPicture = result[0].journey_pic;
        let API_expiryDate = result[0].expiry_date;

        // console.log(result);
        setUpdateUI(result);
        // console.log(value);

        // console.log(result[0].id);
        // console.log(result[0].user_id);
        // console.log(result[0].name);
        // console.log(result[0].description);
        // console.log(result[0].cost);
        // console.log(result[0].journey_pic);
        // console.log(result[0].expiry_date);

        // setUserId(API_userId);
        setName(API_name);
        setDescription(API_description);
        setCost(API_cost);
        setJourneyPicture(API_journeyPicture);
        setExpiryDate(API_expiryDate);
      } catch {
        console.log("error occurs");
      }
    }
    getSoonExpireOffer("");
  }, [fetch]);

  return (
    <>
      {updateUI && (
        <div className="row">
          <div className="col-md-12">
            <h1>Last Call</h1>
            <div
              id="carouselExampleIndicators"
              className="carousel slide"
              data-ride="carousel"
            >
              <ol className="carousel-indicators">
                <li
                  data-target="#carouselExampleIndicators"
                  data-slide-to="0"
                  className="active"
                ></li>
                <li
                  data-target="#carouselExampleIndicators"
                  data-slide-to="1"
                ></li>
                <li
                  data-target="#carouselExampleIndicators"
                  data-slide-to="2"
                ></li>
                <li
                  data-target="#carouselExampleIndicators"
                  data-slide-to="3"
                ></li>
                <li
                  data-target="#carouselExampleIndicators"
                  data-slide-to="4"
                ></li>
                <li
                  data-target="#carouselExampleIndicators"
                  data-slide-to="5"
                ></li>
              </ol>
              <a
                className="carousel-control-prev"
                href="#carouselExampleIndicators"
                role="button"
                data-slide="prev"
              >
                <span
                  className="carousel-control-prev-icon"
                  aria-hidden="true"
                ></span>
                <span className="sr-only">Previous</span>
              </a>
              <a
                className="carousel-control-next"
                href="#carouselExampleIndicators"
                role="button"
                data-slide="next"
              >
                <span
                  className="carousel-control-next-icon"
                  aria-hidden="true"
                ></span>
                <span className="sr-only">Next</span>
              </a>
              <div className="carousel-inner">
                <div className="carousel-item col-lg-12 active">
                  <div className="overflow">
                    {/* {carouselItems.map((carouselItem: any) => ( */}
                    {/* src={`${REACT_APP_API_SERVER}/cast_profile_pic/${props.value.profile_pic}`}  */}
                    <img
                      src={`${REACT_APP_API_SERVER}/site_journey_pic/${journeyPicture}`}
                      // vacation_pic1.jpg
                      alt="offer1"
                      className="d-block w-100 image-fluid"
                    />
                    {/* )} */}
                  </div>
                  {/* {carouselItems.map((carouselItem: any) =>  (*/}
                  <div className="offerDiv">
                    <div>
                      {/* <h4>{`Offer ID:${offerIdItems.values}`}</h4> */}
                      <p>
                        <LocationOnOutlinedIcon />
                        {`Site name: ${name}`}
                      </p>
                      <p> Description ${description}</p>
                      <p>
                        Event date : ${`dd/mm`} Price: ${cost}
                      </p>
                      <p>Date of Expiry:{expiryDate}</p>

                      <Button color="primary" className="joinNow" type="submit">
                        Join Now
                        <HowToRegOutlinedIcon />
                      </Button>
                    </div>
                  </div>
                </div>

                <div className="carousel-item">
                  <div className="overflow">
                    <img
                      src="https://i.pinimg.com/564x/b5/07/7d/b5077d9082e44df6486312f61caf892e.jpg"
                      alt="offer2"
                      className="d-block w-100 image-fluid"
                    />
                  </div>
                  <div className="offerDiv">
                    <div>
                      <h4>Offer ID: 2</h4>
                      <p>
                        host & location
                        <LocationOnOutlinedIcon />
                      </p>
                      <p>event date : dd/mm </p>
                      <p>Expiry ()</p>
                      <HowToRegOutlinedIcon />
                      <Button color="primary" className="joinNow" type="submit">
                        Join Now
                      </Button>
                    </div>
                  </div>
                </div>

                <div className="carousel-item">
                  <div className="overflow">
                    <img
                      src="https://i.pinimg.com/564x/fa/b7/33/fab733498cd5fb8ace4fbdb6d9bf67a0.jpg"
                      alt="offer3"
                      className="d-block w-100 image-fluid"
                    />
                  </div>
                  <div className="offerDiv">
                    <div>
                      <h4>Offer ID: 3</h4>
                      <p>
                        host & location
                        <LocationOnOutlinedIcon />
                      </p>
                      <p>event date : dd/mm </p>
                      <p>Expiry ()</p>
                      <HowToRegOutlinedIcon />
                      <Button color="primary" className="joinNow" type="submit">
                        Join Now
                      </Button>
                    </div>
                  </div>
                </div>

                <div className="carousel-item">
                  <div className="overflow">
                    <img
                      src="https://i.pinimg.com/564x/85/46/2a/85462acd6c4a2b6afb291be356d554fb.jpg"
                      alt="offer4"
                      className="d-block w-100 image-fluid"
                    />
                  </div>
                  <div className="offerDiv">
                    <div>
                      <h4>Offer ID: 4</h4>
                      <p>
                        host & location
                        <LocationOnOutlinedIcon />
                      </p>
                      <p>event date : dd/mm </p>
                      <p>Expiry ()</p>
                      <HowToRegOutlinedIcon />
                      <Button color="primary" className="joinNow" type="submit">
                        Join Now
                      </Button>
                    </div>
                  </div>
                </div>

                <div className="carousel-item">
                  <div className="overflow">
                    <img
                      src="https://i.pinimg.com/564x/7b/6f/f0/7b6ff00beb5e7bcd285e588c0cccc155.jpg"
                      alt="offer5"
                      className="d-block w-100 image-fluid"
                    />
                  </div>
                  <div className="offerDiv">
                    <div>
                      <h4>Offer ID: 5</h4>
                      <p>
                        host & location
                        <LocationOnOutlinedIcon />
                      </p>
                      <p>event date : dd/mm </p>
                      <p>Expiry ()</p>
                      <HowToRegOutlinedIcon />
                      <Button color="primary" className="joinNow" type="submit">
                        Join Now
                      </Button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default LastCallOfferCarousel;
