import React from "react";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
// import UserRatingCard from "../UserCard/UserRatingCard";
// import RatingHeart from "./../RatingsHeart";
import "../../../css/HomePage/UserRatingCard.scss";
import Grid from "@material-ui/core/Grid";
// import { useDispatch, useSelector } from "react-redux";
// import { IRootState } from "../../../redux/store";
// import { getUserProfileThunk } from "../../../redux/getPersonalUserRecords/thunk";
import MDBCardContent from "./MDBCardContent";
// import CardContent from "./CardContent";

const GridWrapUserCard: React.FC = () => {
  // const dispatch = useDispatch();

  // const userProfileItems = useSelector(
  //   (state: IRootState) => state.getPersonalUserRecords.userProfile
  // );
  // useEffect(() => {
  //   dispatch(getUserProfileThunk());
  // }, [dispatch]);

  // const getID = userProfileItems.map((userProfileItem: any) => {
  //   const user_id = userProfileItem.user.id;
  //   return user_id;
  // });
  // console.log(userProfileItems);

  // console.log(userProfileItems);
  // console.log(localStorage.getItem("email"));

  // const sum = userProfileItems.audienceReviews.map((audienceReview) => {
  //   return audienceReview.rating;
  // });
  // const toNumbers = (arr) => arr.map(Number);
  // const SUM = toNumbers(sum).reduce((a: number, b: number) => {
  //   return a + b;
  // }, 0);

  // {userProfileItems.map((userProfileItem: any) => (
  // <UserRatingCard key={userProfileItem.id} value={userProfileItem} />;
  return (
    <div className="starUserContainer">
      <h1>Meet The Team</h1>
      <Grid
        container
        spacing={10}
        direction="row"
        justify="center"
        alignItems="center"
      >
        <Grid item xs={2} md={6} lg={12}>
          <MDBCardContent  />
          {/* <CardContent /> */}
        </Grid>
      </Grid>
    </div>
  );
};

export default GridWrapUserCard;
