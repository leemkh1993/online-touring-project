import React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import { Avatar, IconButton } from "@material-ui/core";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import { red } from "@material-ui/core/colors";
// import HeartShape from "./HeartShape";
// {require("../../assets/emma.jpg")}
import RatingHeart from "./../RatingsHeart";
// import "../../../css/HomePage/UserRatingCard.scss";
// const { REACT_APP_API_SERVER } = process.env;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      minWidth: 450,
      Width: "fit-content",
      backgroundColor: "rgba(0,0,0,0.2)",
    },
    // .makeStyles-userMediaCard-17 {
    // height: 0;
    // margin: 50px 130px !important;
    // padding: 95px 0px !important;
    // border-radius: 100%;

    // height: 0;
    // margin: 75px 140px!important;
    // padding: 80px 0px !important;
    // border- radius: 120 %;

    userMediaCard: {
      height: 0,
      borderRadius: "120%",
      margin: "50px 130px !important",
      padding: "95px 0 !important",
    },
    avatar: {
      backgroundColor: red[500],
    },
    wrapper: {
      display: "flex",
    },
  })
);

// const useStyles = makeStyles((theme: Theme) =>
//   createStyles({
//     root: {
//       minWidth: 120,
//     },
//     userMediaCard: {
//       height: 0,
//       borderRadius: "100%",
//       margin: "35px 145px !important",
//       padding: "90px 0 !important",
//     },
//     expand: {
//       transform: "rotate(0deg)",
//       marginLeft: "auto",
//       transition: theme.transitions.create("transform", {
//         duration: theme.transitions.duration.shortest,
//       }),
//     },
//     expandOpen: {
//       transform: "rotate(180deg)",
//     },
//     avatar: {
//       backgroundColor: red[500],
//     },
//   })
// );

interface IUserCardProps {
  key: number;
  value: any;
  id: number;
  user: string;
  rating: number;
  profile_pic: string;
}

// "user_id": 3,
// "email": "charlie@gmail.com",
// "range": "18-25",
// "profile_pic": "profile_pic.png",
// "first_name": "Charlie",
// "last_name": "Kong",
// "profession": "Programmer",
// "description": "Hello World",
// "country": "Hong Kong",
// "city": "Causeway Bay",
// "average_rating": 3.6

const SingleUserCard = (props: IUserCardProps) => {
  const classes = useStyles();

  return (
    <>
      <div className={classes.wrapper}>
        <Card className={classes.root}>
          <CardHeader
            key={`${props.value.id}`}
            avatar={
              <Avatar aria-label="recipe" className={classes.avatar}>
                I {props.value.username}
              </Avatar>
            }
            action={
              <IconButton aria-label="settings">
                <MoreVertIcon />
              </IconButton>
            }
            // title={`${props.value.user}`}
            // subheader={`${props.value.Language}`}
          />
          {/* .slice(0, 1 */}
          <CardMedia
            className={classes.userMediaCard}
            image={
              "http://localhost:8080/cast_profile_pic/profile_pic.png"
              // `${REACT_APP_API_SERVER}/cast_profile_pic/${props.value.profile_pic}`
            }
            title="TGProfilePhoto"
          />
          <p>rating: ${props.value.rating}</p>
          {/* streamerReviews */}
          <RatingHeart />
        </Card>
      </div>
    </>
  );
};

export default SingleUserCard;
