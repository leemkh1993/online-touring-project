import RatingHeart from "./MDBCardContent";

export const seeds = [
  {
    user_id: 3,
    profile_pic: "profile_pic.png",
    first_name: "Charlie",
    profession: "Programmer",
    description: "Hello World",
    average_rating: 3.6,
  },
  {
    user_id: 6,
    profile_pic: "dj_snake.png",
    first_name: "DJ",
    profession: "DJ",
    description: "Hello World",
    average_rating: 3.5,
  },
  {
    user_id: 5,
    profile_pic: "kraft_werk.jpg",
    first_name: "Kraft",
    profession: "Electric Music Pioneers",
    description: "Hello World",
    average_rating: 3.5,
  },
  {
    user_id: 7,
    profile_pic: "michael_jackson.jpeg",
    first_name: "Michael",
    profession: "King of Pop",
    description: "Hello World",
    average_rating: 3.4,
  },
];
