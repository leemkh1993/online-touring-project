import React from "react";
// { useEffect }
// import "mdbreact/dist/css/mdb-free.css";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import {
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBCol,
  MDBCardTitle,
  MDBCardText,
  // MDBAvatar,
  // MDBFlippingCard,
} from "mdbreact";

// import SingleUserCard from "../UserCard/SingleUserCard";
// import HeartShape from "./HeartShape";
import "../../../css/HomePage/UserRatingCard.scss";
// import { useDispatch, useSelector } from "react-redux";
// import { IRootState } from "../../../redux/store";
// import { getUserProfileThunk } from "../../../redux/getPersonalUserRecords/thunk";
// import Grid from "@material-ui/core/Grid";
import RatingHeart from "./../RatingsHeart";

interface IOfferIdProps {
  key: number;
  value: any;
}
const { REACT_APP_API_SERVER } = process.env;

const UserRatingCard = (props: IOfferIdProps) => {
  // const dispatch = useDispatch();
  // console.log(props.value);
  // const streamerUserProfiles = useSelector(
  //   (state: IRootState) => state.getPersonalUserRecords.userProfile
  // );

  // const sum = props.value.audience_reviews.map((audience_review) => {
  //   return audience_review.rating;
  // });
  // const toNumbers = (arr) => arr.map(Number);
  // const SUM = toNumbers(sum).reduce((a: number, b: number) => {
  //   return a + b;
  // }, 0);
  // console.log(SUM);

  // useEffect(() => {
  //   dispatch(getUserProfileThunk());
  // }, [dispatch]);

  // console.log(streamerUserProfiles);

  const sum = props.value.audienceReviews.map((audienceReview) => {
    return audienceReview.rating;
  });
  const toNumbers = (arr) => arr.map(Number);
  const SUM = toNumbers(sum).reduce((a: number, b: number) => {
    return a + b;
  }, 0);

  // console.log(localStorage.getItem("email"));

  return (
    <>
      <MDBCol row>
        <MDBCard 
        // style={{ width: "22rem" }}
        >
          <MDBCardImage
            className="img-fluid"
            src={`${REACT_APP_API_SERVER}/cast_profile_pic/${props.value.user.profile_pic}`}
            // "https://mdbootstrap.com/img/Photos/Others/images/43.jpg"
            waves
          />
          <MDBCardBody>
            <MDBCardTitle>
              {/* {props.value.audience_reviews.first_name} */}
              {props.value.user.profession}
            </MDBCardTitle>
            <MDBCardText>
              <p>
                rating:
                <span
                  // className={normal_styles.pt}
                  style={{
                    color:
                      parseInt(
                        (SUM / props.value.audienceReviews.rating).toFixed(1)
                      ) >= 3.5
                        ? "green"
                        : "red",
                  }}
                ></span>
              </p>
              {/* ${props.value.audienceReviews.rating}</p> */}
              {/* <p>cm: ${props.value.user.audienceReview.comments}</p> */}
              {/* streamerReviews */}
              <RatingHeart />
            </MDBCardText>
            {/* <MDBBtn href="#">MDBBtn</MDBBtn> */}
          </MDBCardBody>
        </MDBCard>
      </MDBCol>
    </>
  );
};

export default UserRatingCard;
