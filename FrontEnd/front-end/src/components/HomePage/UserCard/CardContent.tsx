import React from "react";
import { seeds } from "./DummySeed";
import "../../../css/HomePage/UserRatingCard.scss";

import HeartShape from "./../HeartShape";
const { REACT_APP_API_SERVER } = process.env;
// backgroundImage: url(`${REACT_APP_API_SERVER}/cast_profile_pic/${seed.profile_pic}`)
const CardContent = (props: any) => {
  return (
    <>
      <div className="row columns">{/* <h3>{seed.first_name}</h3> */}</div>
      <section className="cards-container">
        {seeds.map((seed: any) => (
          <div className="card">
            <div className="card-divider">
              <h4>{seed.profession}</h4>
            </div>
            <div className="card-section">
              <img
                className="img-fluid img-responsive"
                src={`${REACT_APP_API_SERVER}/cast_profile_pic/${seed.profile_pic}`}
              />
            </div>
            <div className="card-divider">
              <p>
                rating:
                {seed.average_rating}
                <HeartShape />
              </p>
            </div>
          </div>
        ))}
      </section>
    </>
  );
};

export default CardContent;
