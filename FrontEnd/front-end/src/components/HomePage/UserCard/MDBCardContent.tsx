import React from "react";
// import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import {
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBContainer,
  MDBRow,
  MDBCardTitle,
  MDBCardText,
} from "mdbreact";
import { seeds } from "./DummySeed";
import "../../../css/HomePage/UserRatingCard.scss";

// import Star from "./Star";
import Ratings from "../../JitsiPage/Review/Ratings";

// {require("../../assets/emma.jpg")}
// import RatingHeart from "../RatingsHeart";
// import "../../../css/HomePage/UserRatingCard.scss";

const { REACT_APP_API_SERVER } = process.env;

const MDBCardContent = (props: any) => {
  return (
    <>
      <MDBContainer style={{ margin: "auto 250px", padding: 0, display: "flex", justify: "center"}}>
      <MDBRow >
      {/* style={{ width: "28rem", borderWidth: 1 }} */}
        {seeds.map((seed: any) => (
         
          <MDBCard className="MDB-UserCard">
            <MDBCardImage
              className="img-responsive profile_img_resize"
              src={`${REACT_APP_API_SERVER}/cast_profile_pic/${seed.profile_pic}`}
              waves
            />
            <MDBCardBody>
              <MDBCardTitle>
                {seed.first_name}
                <br />
                Professional: {seed.profession}
              </MDBCardTitle>
              <MDBCardText>
                <p>
                 
                  rating:{seed.average_rating}
                  <br />
                  {/* <Star /> */}
                  <Ratings />
                </p>
              </MDBCardText>
            </MDBCardBody>
          </MDBCard>
        ))}
      </MDBRow>
      </MDBContainer>
    </>
  );
};

export default MDBCardContent;
