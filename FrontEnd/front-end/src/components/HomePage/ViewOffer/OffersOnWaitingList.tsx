import React from "react";
import { Button } from "reactstrap";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Collapse from "@material-ui/core/Collapse";
import Avatar from "@material-ui/core/Avatar";
// import TurnedInNotOutlinedIcon from "@material-ui/icons/TurnedInNotOutlined";
// import PersonRoundedIcon from "@material-ui/icons/PersonRounded";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import { red } from "@material-ui/core/colors";
import RoomRoundedIcon from "@material-ui/icons/RoomRounded";
// import LocationOnSharpIcon from "@material-ui/icons/LocationOnSharp";
import VisibilityOutlinedIcon from "@material-ui/icons/VisibilityOutlined";
// import FavoriteIcon from "@material-ui/icons/Favorite";
// import FavoriteBorderSharpIcon from "@material-ui/icons/FavoriteBorderSharp";
// import ShareIcon from "@material-ui/icons/Share";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
// import MoreVertIcon from "@material-ui/icons/MoreVert";
// import "../../css/UserRatingCard.css";
// import StarOutlineIcon from '@material-ui/icons/StarOutline';
// import StarHalfIcon from '@material-ui/icons/StarHalf';
// import StarIcon from "@material-ui/icons/Star";
// import Avatar from "@rent_avail/avatar";
import "../../../css/HomePage/OffersOnWaitingList.scss";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      // maxWidth: 345,
      minWidth: 120,
      // minHeight: 500,
    },

    media: {
      height: 0,
      paddingTop: "66%", // 16:9 66%
    },
    image: {
      position: "relative",
      height: 200,
      [theme.breakpoints.down("xs")]: {
        width: "100% !important", // Overrides inline-style
        height: 100,
      },
      "&:hover, &$focusVisible": {
        zIndex: 1,
        "& $imageBackdrop": {
          opacity: 0.15,
        },
        "& $imageMarked": {
          opacity: 0,
        },
        "& $imageTitle": {
          border: "4px solid currentColor",
        },
      },
    },
    focusVisible: {},
    imageButton: {
      position: "absolute",
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      color: theme.palette.common.white,
    },
    imageSrc: {
      position: "absolute",
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      backgroundSize: "cover",
      backgroundPosition: "center 30%",
    },
    imageBackdrop: {
      position: "absolute",
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      backgroundColor: theme.palette.common.black,
      opacity: 0.4,
      transition: theme.transitions.create("opacity"),
    },
    imageTitle: {
      position: "relative",
      padding: `${theme.spacing(2)}px ${theme.spacing(4)}px ${
        theme.spacing(1) + 6
      }px`,
    },
    imageMarked: {
      height: 3,
      width: 6,
      backgroundColor: theme.palette.common.white,
      position: "absolute",
      bottom: -2,
      left: "calc(50% - 9px)",
      transition: theme.transitions.create("opacity"),
    },
    expand: {
      transform: "rotate(0deg)",
      marginLeft: "auto",
      transition: theme.transitions.create("transform", {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: "rotate(180deg)",
    },
    avatar: {
      backgroundColor: red[500],
      //  "transparent",
    },
  })
);

interface IOfferListItems {
  key: number;
  value: any;
}
const { REACT_APP_API_SERVER } = process.env;

const OffersOnWaitingList = (props: IOfferListItems) => {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };
  // console.log(props.value);

  return (
    <div>
      <Card key={props.value.id} className={classes.root}>
        {/* <img src="https://www.flaticon.com/svg/static/icons/svg/854/854878.svg" /> */}
        <Link to="http://www.google.com">
          <CardHeader
            avatar={
              <Avatar
                // icon="http://maps.google.com/mapfiles/ms/icons/blue-dot.png"
                aria-label="usericon"
                className={classes.avatar}
              >
                <RoomRoundedIcon />
                {/* <PersonRoundedIcon /> */}
                <div
                  style={{
                    background: `url('${REACT_APP_API_SERVER}/cast_profile_pic/${props.value.profile_pic}') no-repeat center center`,
                  }}
                  className={props.value.name.slice(1)}
                />
              </Avatar>
            }
            // action={<IconButton aria-label="location"></IconButton>}
            title={props.value.name}
            subheader={props.value.location}
          />
        </Link>
        <div className="offerMediaCard">
          <CardMedia
            className={classes.media}
            image={`${REACT_APP_API_SERVER}/cast_vacation_pic/${props.value.journey_pic}`}
            title="vacationPicture"
          />
          <span className="decothisSpan"></span>
        </div>
        <CardContent>
          <Typography
            className="eventName"
            variant="subtitle2"
            color="textSecondary"
            component="p"
          >
            <span className="discover">Discover</span>
          </Typography>
        </CardContent>

        <CardActions disableSpacing>
          <IconButton
            className={clsx(classes.expand, {
              [classes.expandOpen]: expanded,
            })}
            onClick={handleExpandClick}
            aria-expanded={expanded}
            aria-label="show more"
          >
            <VisibilityOutlinedIcon />
            <ExpandMoreIcon />
          </IconButton>
        </CardActions>
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          <CardContent>
            <Typography paragraph>Event Name: {props.value.name}</Typography>
            <Typography paragraph>
              Event description: {props.value.description_details}
            </Typography>
            <Typography paragraph>
              Date of expiry: {props.value.expiry_date}
            </Typography>
            {/* className="checkout" */}
            <Button color="primary" type="submit">
              Check out
            </Button>
          </CardContent>
        </Collapse>
      </Card>
    </div>
  );
};

export default OffersOnWaitingList;
