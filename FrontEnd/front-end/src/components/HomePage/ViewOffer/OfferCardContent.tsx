// import { seeds } from "./DummySeed";
import React from "react";
import "../../../css/HomePage/OffersOnWaitingList.scss";
// import Card from "@material-ui/core/Card";
import {
  //   MDBCarousel,
  //   MDBCarouselInner,
  //   MDBCarouselItem,
  MDBContainer,
  //   MDBRow,
  //   MDBCol,
  MDBCard,
  MDBCardImage,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText,
  MDBBtn,
} from "mdbreact";
import "../../../css/HomePage/OffersOnWaitingList.scss";
import { Link } from "react-router-dom";

// import "../../../css/HomePage/UserRatingCard.scss";
// import HeartShape from "./../HeartShape";
const { REACT_APP_API_SERVER } = process.env;
// backgroundImage: url(`${REACT_APP_API_SERVER}/cast_profile_pic/${seed.profile_pic}`)

interface IOfferListItems {
  key: number;
  value: any;
}
const OfferCardContent = (props: IOfferListItems) => {
  //   const [expanded, setExpanded] = React.useState(false);

  //   const handleExpandClick = () => {
  //     setExpanded(!expanded);
  //   };
  // console.log(props.value);

  return (
    <>
      {/* style={{ margin: "0 65px" } */}
      <MDBContainer
        style={{
          margin: "0 20px",
          display: "flex",
          flexDirection: "column",
          flexWrap: "wrap",
          width: "700",
        }}
      >
        {/* <Card > */}
        {/* <MDBRow> */}
        {/* <MDBCol md="4" className={props.value.name.slice(1)}> */}
        <MDBCard
          // className={props.value.hashtags}
          id={props.value.id}
          style={{ maxWidth: 350 }}
          className="mb-2"
        >
          <MDBCardImage
            className="img-fluid w-100 MDBcardImg"
            src={`${REACT_APP_API_SERVER}/cast_vacation_pic/${props.value.journey_pic}`}
          />
          <MDBCardBody>
            <MDBCardTitle>{props.value.name}</MDBCardTitle>
            <MDBCardText>description: {props.value.description} </MDBCardText>
            {props.value.hashtags}
            <br />
            <Link to={`list/detail/${parseInt(props.value.id)}`}>
              <MDBBtn key={props.value.id} color="primary">
                Discover
              </MDBBtn>
            </Link>
          </MDBCardBody>
        </MDBCard>
      </MDBContainer>
    </>
  );
};
export default OfferCardContent;
