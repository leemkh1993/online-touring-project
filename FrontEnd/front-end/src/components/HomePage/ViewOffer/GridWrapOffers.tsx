import React, { useEffect } from "react";
// import { Button } from "reactstrap";
import { useDispatch, useSelector } from "react-redux";
import { IRootState } from "../../../redux/store";
import OfferCardContnet from "./OfferCardContent";
// import OffersOnWaitingList from "./OffersOnWaitingList";
import Grid from "@material-ui/core/Grid";
import { getOfferListItemsThunk } from "../../../redux/browseOffer/thunks";
import VisibilityOutlinedIcon from "@material-ui/icons/VisibilityOutlined";
import IconButton from "@material-ui/core/IconButton";
import { Link } from "react-router-dom";

const GridWrapOffers: React.FC = () => {
  const dispatch = useDispatch();
  // <div className="row">
  //  <div className="col-sm-4 col-md-4 col-lg-12">
  const offerListItems = useSelector(
    (state: IRootState) => state.browseOffer.offerListItems
  );
  useEffect(() => {
    dispatch(getOfferListItemsThunk());
  }, [dispatch]);

  return (
    <div className="InspirationContainer">
      <h1>
        Inspiration
        <IconButton>
          <Link to="/list" className="link">
            <VisibilityOutlinedIcon />
          </Link>
        </IconButton>
      </h1>
      <div className="rol"></div>
      <Grid
        container
        // spacing={1}
        direction="row"
        justify="center"
        alignItems="center"
      >
        {offerListItems.slice(13, 21).map((offerListItem: any) => (
          <Grid item xs={3} md={2} lg={3}>
            <OfferCardContnet key={offerListItem.id} value={offerListItem} />
          </Grid>
        ))}
      </Grid>
    </div>
  );
};
//  OffersOnWaitingList;
export default GridWrapOffers;
