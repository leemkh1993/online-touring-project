import React from "react";
import { Rate } from "antd";
import { HeartFilled } from "@ant-design/icons";
import "./../../css/HomePage/UserRatingCard.scss";
// import { HeartOutlined } from "@ant-design/icons";

const HeartShape: React.FC = () => {
  return (
    <div>
      <>
        <Rate className="ant-rate" character={<HeartFilled />} allowHalf />
        {/* <Rate character={<HeartOutlined />} allowHalf /> */}
      </>
    </div>
  );
};

export default HeartShape;
