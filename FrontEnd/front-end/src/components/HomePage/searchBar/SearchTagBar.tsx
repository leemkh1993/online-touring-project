import makeAnimated from "react-select/animated";
import React, { useEffect, useState } from "react";
//  { useState }
import Paper from "@material-ui/core/Paper";
// import InputBase from "@material-ui/core/InputBase";
import Divider from "@material-ui/core/Divider";
// import MenuIcon from "@material-ui/icons/Menu";
import { createStyles, makeStyles } from "@material-ui/core/styles";
import SearchIcon from "@material-ui/icons/Search";
import SendIcon from "@material-ui/icons/Send";
import IconButton from "@material-ui/core/IconButton";
import { Link } from "react-router-dom";
import { Button } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";

import Select from "react-select";
// import { hashtagOptions } from "./data";
import "./../../../css/HomePage/SearchTagBar.scss";
import { useDispatch, useSelector } from "react-redux";
import { IRootState } from "../../../redux/store";
import { getHashtagThunk } from "../../../redux/getHashTag/thunks";

const animatedComponents = makeAnimated();
const useStyles = makeStyles((theme) => {
  return createStyles({
    root: {
      padding: "3px 4px",
      display: "flex",
      alignItems: "center",
      margin: "10px 250px",
      width: "100%",
      color: "#fff !important",
      backgroundColor: "rgba(0, 0, 0, 0.7) !important",
    },
    input: {
      marginLeft: 8,
      flex: 1,
      backgroundColor: "rgba(0, 0, 0, 0.7) !important",
    },
    // "&:focus": {
    //   backgroundColor: "rgba(255,255,255,0.6)",
    // },
    iconButton: {
      padding: 10,
    },
    divider: {
      width: 1,
      height: 28,
      margin: 4,
    },
  });
});



const SearchTagBar: React.FC = () => {
  // const [hashtag, setHashtag] = useState("");


  // 1/10 hashtag search by LC
  const dispatch = useDispatch();
  const Hashtags = useSelector((state:IRootState)=>state.getHashtag.hashtag);
  useEffect(()=>{
    dispatch(getHashtagThunk());
  },[dispatch]);

  const options = Hashtags.map((Hashtag:any)=>{
    return { value: `${Hashtag.substring(1)}`, label: `${Hashtag.substring(1)}` };
  })
  const [selectedOption, setSelectedOption] = useState("#shopping") as any;

  const classes = useStyles();

  return (
    <React.Fragment>
      <div className="searchBarContainer">
        <Typography
          variant="h3"
          color="initial"
        >
          Explore the world from your living room
        </Typography>
        <Paper className={classes.root} elevation={1}>
          <Select
            closeMenuOnSelect={false}
            components={animatedComponents}
            isMulti
            options={options}
            defaultValue={selectedOption}
            onChange={setSelectedOption}
            className={classes.input}
            classNamePrefix="select"
            placeholder="Input destination, places, event name or features here..."
          />

          <IconButton className={classes.iconButton} aria-label="Search">
            <SearchIcon />
          </IconButton>
          <Divider className={classes.divider} />

          <Link to={`/list/%23${selectedOption === null ? "okok":selectedOption[0].value}`}>
            <IconButton
              color="secondary"
              className={classes.iconButton}
              aria-label="Directions"
            >
              <SendIcon />
            </IconButton>
          </Link>

        </Paper>

        <div className="buttonGroup">
          <Button className="customMuiButtonRoot" disabled>
            | Popular Search Item |
          </Button>
          {/* 1/10 Hashtag search by LC*/}
          {
            Hashtags.slice(0, 10).map((Hashtag:any)=>(
              <Link to={`/list/%23${Hashtag.substring(1)}`}>
                <Button
                      className={`eachButton`}
                      variant="outlined"
                      color="secondary"
                      href="#outlined-buttons"
                >
                      {Hashtag}
                </Button>
              </Link>
            ))
          }

        </div>
      </div>
      {/* </Paper> */}
    </React.Fragment>
  );
};

export default SearchTagBar;
