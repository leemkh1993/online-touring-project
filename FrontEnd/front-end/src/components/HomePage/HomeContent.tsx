import React from "react"; //This page will display after login is successful
import { IRootState } from "../../redux/store";
import { useSelector } from "react-redux";
// import {Link, Switch, Route } from "react-router-dom";

const HomeContent: React.FC = () => {
  const isAuthenticated = useSelector(
    (state: IRootState) => state.auth.isAuthenticated
  );

  return (
    <div>
      {/* <h1>This is Home Content</h1> */}
      {isAuthenticated === true && (
        <div>
          <p>Login Success, Welcome user!</p>
        </div>
      )}
      {isAuthenticated === false && (
        <div>
          <p>Login Status: Login Fail</p>
        </div>
      )}
    </div>
  );
};

export default HomeContent;
