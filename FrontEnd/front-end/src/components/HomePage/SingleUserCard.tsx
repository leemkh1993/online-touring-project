import React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import { Avatar, IconButton } from "@material-ui/core";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import { red } from "@material-ui/core/colors";
// import HeartShape from "./HeartShape";
// {require("../../assets/emma.jpg")}
import RatingHeart from "./RatingsHeart";
import "../../css/HomePage/UserRatingCard.scss";

const cardInfos = [
  {
    id: 1,
    username: "Tiffany",
    subheader: "Part time traveller",
    imgUrl:
      "https://i.pinimg.com/564x/87/f2/6f/87f26fabe52c57029fdbf1e8d6a13303.jpg",
  },
  {
    id: 2,
    username: "Jack",
    subheader: "Part time traveller",
    imgUrl: require("../../assets/emma.jpg"),
    // "https://i.pinimg.com/564x/b5/07/7d/b5077d9082e44df6486312f61caf892e.jpg",
  },
  {
    id: 3,
    username: "Emma",
    subheader: "Part time traveller",
    imgUrl: require("../../assets/emma2.jpg"),
  },
  {
    id: 4,
    username: "Shirley",
    subheader: "Part time traveller",
    imgUrl: require("../../assets/emma.jpg"),
  },
];
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      minWidth: 450,
      Width: "fit-content",
      backgroundColor: "rgba(0,0,0,0.2)",
    },
    // .makeStyles-userMediaCard-17 {
    // height: 0;
    // margin: 50px 130px !important;
    // padding: 95px 0px !important;
    // border-radius: 100%;

    // height: 0;
    // margin: 75px 140px!important;
    // padding: 80px 0px !important;
    // border- radius: 120 %;

    userMediaCard: {
      height: 0,
      borderRadius: "120%",
      margin: "50px 130px !important",
      padding: "90px 0 !important",
    },
    avatar: {
      backgroundColor: red[500],
    },
    wrapper: {
      display: "flex",
    },
  })
);

const SingleUserCard: React.FC = (props: any) => {
  const classes = useStyles();
  // console.log(cardInfos);

  return (
    <>
      <h1>Meet The Team</h1>
      <div className={classes.wrapper}>
        {cardInfos &&
          cardInfos.length > 0 &&
          cardInfos.map(
            (
              cardInfo: { username: string; subheader: string; imgUrl: string },
              idx: number
            ) => (
              <>
                <Card className={classes.root}>
                  <CardHeader
                    key={`${idx}`}
                    avatar={
                      <Avatar aria-label="recipe" className={classes.avatar}>
                        {`${cardInfo.username.slice(0, 1)}`}
                      </Avatar>
                    }
                    action={
                      <IconButton aria-label="settings">
                        <MoreVertIcon />
                      </IconButton>
                    }
                    title={`${cardInfo.username}`}
                    subheader={`${cardInfo.subheader}`}
                  />
                  <CardMedia
                    className={classes.userMediaCard}
                    image={`${cardInfo.imgUrl}`}
                    title="TGProfilePhoto"
                  />
                  <RatingHeart />
                </Card>
              </>
            )
          )}
      </div>
    </>
  );
};

export default SingleUserCard;
