import React, { useEffect } from "react";
import Typography from "@material-ui/core/Typography";
import Slider from "react-animated-slider";
import "react-animated-slider/build/horizontal.css";
// import "react-bootstrap-carousel/dist/react-bootstrap-carousel.css";
// import "bootstrap/dist/css/bootstrap.min.css";
import "./../../../css/HomePage/LastCallOffersCarousel.scss";

// import CarouselElements from "./CarouselElements";
//new
// import CarouselElementItems from "./CarouselElementItems";
import { IRootState } from "../../../redux/store";
import { useSelector, useDispatch } from "react-redux";
import { getSoonExpireOffersThunk } from "../../../redux/browseOffer/thunks";
import { getAllUserInfoThunk } from "../../../redux/browseUser/thunks";
import HowToRegOutlinedIcon from "@material-ui/icons/HowToRegOutlined";
// import LocationOnOutlinedIcon from "@material-ui/icons/LocationOnOutlined";
import { Button } from "reactstrap";

const { REACT_APP_API_SERVER } = process.env;

const LastCallOffersCarousel: React.FC = () => {
  const dispatch = useDispatch();

  const offerListItems = useSelector(
    (state: IRootState) => state.browseOffer.offerListItems
  );

  // const userInfos = useSelector(
  //   (state: IRootState) => state.browseUser.userInfo
  // );
  const userId = offerListItems.map((offerListItem: any) => {
    return offerListItem.user_id;
  });

  useEffect(() => {
    dispatch(getAllUserInfoThunk());
    // console.log(userInfos);
  }, [dispatch, userId]);

  useEffect(() => {
    dispatch(getSoonExpireOffersThunk());
  }, [dispatch]);

  // console.log(userId);

  const USDCurrency = (number: number) => {
    return new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
    }).format(number);
  };
  // console.log(offerListItems);
  // console.log(localStorage.getItem("email"));

  return (
    <>
      <Slider className="slider" autoplay={2000}>
        {offerListItems.slice(15).map((offerListItem, index) => (
          <div
            key={offerListItem.id}
            className="image-fluid offerContainer"
            style={{
              margin: "auto",
              background: `url('${REACT_APP_API_SERVER}/cast_vacation_pic/${offerListItem.journey_pic}') no-repeat center center`,
              backgroundSize: "cover",
              backgroundAttachment: "fixed",
            }}
          >
            {/* <h2>offerListItem.user_id</h2> */}
            <div className="contents">
              <Typography
                className="textColor"
                variant="h4"
                // color="textPrimary"
                component="p"
              >
                {offerListItem.name}
              </Typography>
              <p className="textColor">
                {/* <LocationOnOutlinedIcon /> */}
                Description: {offerListItem.description}
              </p>
              <p className="textColor">
                Price: $USD:{USDCurrency(offerListItem.cost)}
              </p>
              <p className="textColor">
                {" "}
                Date of Expiry:{new Date(offerListItem.expiry_date).toLocaleString()}
              </p>
              <Button
                // color="primary"
                className="joinNow textColor"
                type="submit"
              >
                Join Now
                <HowToRegOutlinedIcon />
              </Button>
            </div>
          </div>
        ))}
      </Slider>
    </>
  );
};

export default LastCallOffersCarousel;
