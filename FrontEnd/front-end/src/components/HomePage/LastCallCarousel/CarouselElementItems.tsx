import React from "react";
import "react-animated-slider/build/horizontal.css";

import HowToRegOutlinedIcon from "@material-ui/icons/HowToRegOutlined";
import LocationOnOutlinedIcon from "@material-ui/icons/LocationOnOutlined";
import { Button } from "reactstrap";
// import "./../../../css/HomePage/LastCallOffersCarousel.scss";
interface ICoarouselItemsProps {
  key: number;
  value: any;
}
//will come back
const { REACT_APP_API_SERVER } = process.env;

const CarouselElementItems = (props: ICoarouselItemsProps) => {
  const USDCurrency = (number: number) => {
    return new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
    }).format(number);
  };



  return (
    <div
      key={props.value.id}
      // value={props}
      style={{
        background: `url('${REACT_APP_API_SERVER}/cast_vacation_pic/${props.value.journey_pic}') no-repeat center center`,
      }}
    >
      {/* <div className="offerDiv"></div> */}
      <div className="center">
        <LocationOnOutlinedIcon />
        {props.value.user_id}
        <h1>{props.value.name}</h1>
        <p>{props.value.description}</p>
        <p>
          Event date : ${props.value.expiry_date}
          Price: $USD
          {USDCurrency(props.value.cost)}
        </p>
        <p>Date of Expiry:{props.value.expiry_date}</p>

        {/* {(SUM / props.value.audience_reviews.length).toFixed(1)} */}
        <Button color="primary" className="joinNow" type="submit">
          Join Now
          <HowToRegOutlinedIcon />
        </Button>
      </div>
    </div>
  );
};

export default CarouselElementItems;
