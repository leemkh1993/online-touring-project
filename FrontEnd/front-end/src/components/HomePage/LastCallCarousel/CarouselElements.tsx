import React from "react";
import HowToRegOutlinedIcon from "@material-ui/icons/HowToRegOutlined";
import LocationOnOutlinedIcon from "@material-ui/icons/LocationOnOutlined";
import { Button } from "reactstrap";
// import { IRootState } from "../../redux/store";
// import { useSelector, useDispatch } from "react-redux";
// import { getSoonExpireOffersThunk } from "./../../redux/browseOffer/thunks";

interface ICoarouselProps {
  key: number;
  value: any;
}

const { REACT_APP_API_SERVER } = process.env;
const CarouselElements = (props: ICoarouselProps) => {
  //   const dispatch = useDispatch();
  //   const offerListItems = useSelector(
  //     (state: IRootState) => state.browseOffer.offerListItems
  //   );

  //   useEffect(() => {
  //     dispatch(getSoonExpireOffersThunk());
  //   }, [dispatch]);
  const USDCurrency = (number: number) => {
    return new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
    }).format(number);
  };

  

  return (
    <>
      <div className="row">
        <div className="col-md-12">
          <h1>Last Call</h1>
          <div
            // key={pfferListItems.id}
            id="carouselExampleIndicators"
            className="carousel slide"
            data-ride="carousel"
          >
            <ol className="carousel-indicators">
              <li
                data-target="#carouselExampleIndicators"
                data-slide-to="0"
                className="active"
              ></li>
              <li
                data-target="#carouselExampleIndicators"
                data-slide-to="1"
              ></li>
              <li
                data-target="#carouselExampleIndicators"
                data-slide-to="2"
              ></li>
              <li
                data-target="#carouselExampleIndicators"
                data-slide-to="3"
              ></li>
              <li
                data-target="#carouselExampleIndicators"
                data-slide-to="4"
              ></li>
              <li
                data-target="#carouselExampleIndicators"
                data-slide-to="5"
              ></li>
            </ol>
            <a
              className="carousel-control-prev"
              href="#carouselExampleIndicators"
              role="button"
              data-slide="prev"
            >
              <span
                className="carousel-control-prev-icon"
                aria-hidden="true"
              ></span>
              <span className="sr-only">Previous</span>
            </a>
            <a
              className="carousel-control-next"
              href="#carouselExampleIndicators"
              role="button"
              data-slide="next"
            >
              <span
                className="carousel-control-next-icon"
                aria-hidden="true"
              ></span>
              <span className="sr-only">Next</span>
            </a>
            <div className="carousel-inner">
              <div className="carousel-item col-lg-12 active">
                <div className="overflow">
                  <img
                    src={`${REACT_APP_API_SERVER}/cast_vacation_pic/${props.value.journey_pic}`}
                    // vacation_pic1.jpg
                    alt=""
                    className="d-block w-100 image-fluid"
                  />
                </div>
                {/* {carouselItems.map((carouselItem: any) =>  (*/}
                <div className="offerDiv">
                  <div>
                    {/* <h4>{`Offer ID:${offerIdItems.values}`}</h4> */}
                    <p>
                      <LocationOnOutlinedIcon />
                      {`Site name: ${props.value.name}`}
                    </p>
                    <p> Description ${props.value.description}</p>
                    <p>
                      Event date : ${props.value.expiry_date}
                      Price: $USD
                      {USDCurrency(props.value.cost)}
                    </p>
                    <p>Date of Expiry:{props.value.expiry_date}</p>

                    <Button color="primary" className="joinNow" type="submit">
                      Join Now
                      <HowToRegOutlinedIcon />
                    </Button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default CarouselElements;
