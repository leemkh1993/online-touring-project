import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
// import { IRootState } from "../redux/store";
// import { getUserProfileThunk } from "../redux/getPersonalUserRecords/thunk";
import { addPaymentRecordThunk } from "../redux/paypalSuccess/thunk";

// ********************SandBox part**************************

// const CLIENT = {
//     sandbox:
//       "AVbH4HXauw6q1pgDLqXuChE49rZf25w9k2FMpcx7eEMU_tKHcZ_jWElkJlsiAePNWN9SgtZKcs9lwM64&currency=HKD",
//     production:
//       "production_key"
//   };

// const CLIENT_ID =
//     process.env.NODE_ENV === "production" ? CLIENT.production : CLIENT.sandbox;

// ********************SandBox part**************************

interface IPriceProps {
  // key: number
  value: any;
  startDate: Date;
  own_user_id: number;
}
// user_paid_status:boolean
// need provide below info in this page by props
// offerID: offerID,
// payeeID: payeeID, //need auth status
// recipientID: recipientID,
// amount:amount,
// paypalID: paypalID,
// startDate:startDate,
// endDate:endDate,
// user_paid_status:true

declare const window: any;
const PaypalButton = (props: IPriceProps) => {
  const dispatch = useDispatch();

  //insert email stored from localStorage to getUserProfileThunk, which in "/redux/getUserProfileThunk/thunk"

  // const [checkout, setCheckout] = React.useState(false);
  const [paid, setPaid] = React.useState(false);
  const [error] = React.useState(null);
  const paypalRef = React.useRef() as React.MutableRefObject<HTMLInputElement>;

  // check type and value if necessary
  // console.log(parseInt(props.value.offer.offer_cost));
  // console.log(typeof(props.value.offer.offer_cost));
  useEffect(() => {
    window.paypal
      .Buttons({
        createOrder: (data: any, actions: any) => {
          // console.log("createOrder");
          return actions.order.create({
            intent: "CAPTURE",
            purchase_units: [
              {
                description: "Your description",
                amount: {
                  currency_code: "USD",
                  value: parseInt(props.value.offer.offer_cost), //to be continue
                },
              },
            ],
          });
        },
        onApprove: async (data: any, actions: any) => {
          const order = await actions.order.capture();
          setPaid(true);
          //logic of inserting database
          // console.log(order)
          dispatch(
            addPaymentRecordThunk(
              props.value.offer.offer_id,
              props.own_user_id,
              props.value.offer.guide_id,
              props.value.offer.offer_cost,
              order.id,
              props.startDate,
              props.startDate
            )
          );
        },
        onError: (err: any) => {
          // setError(err),
          // console.error("error");
        },
      })
      .render(paypalRef.current);
  }, [dispatch]);

  // If the payment has been made
  if (paid) {
    return <div>Payment confirmed.! Please check details in my order page</div>;
  }

  // If any error occurs
  if (error) {
    return <div>Error Occurred in processing payment.! Please try again.</div>;
  }

  return (
    <div>
      <br />
      <div ref={paypalRef} />
    </div>
  );
};

export default PaypalButton;
