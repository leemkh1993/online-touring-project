import React from "react";
import { RouteProps, Redirect, Route } from "react-router-dom";
import { useSelector } from "react-redux";
import { IRootState } from "../redux/store";

// const props = { name: 'jason', age: 18 };
// const { name, ...others } = props;

const PrivateRoute: React.FC<RouteProps> = ({ component, ...rest }) => {
  //: RouteProps
  const isAuthenticated = useSelector(
    (state: IRootState) => state.auth.isAuthenticated
  );
  const Component = component as any; //React.FC
  if (Component === null) {
    return null;
  }

  let render: (props: any) => JSX.Element;
  if (isAuthenticated) {
    // to check whether User has permit to login
    render = (props: any) => <Component {...props} />; //props pass in React.FC then return JSX elements
  } else {
    render = (props: any) => (
      <Redirect
        to={{
          pathname: "/login",
          state: { from: props.location },
        }}
      />
    );
  }
  return <Route {...rest} render={render} />;
};

export default PrivateRoute;
