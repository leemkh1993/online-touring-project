import { Grid } from "@material-ui/core";
import React from "react";
import Card from "react-bootstrap/Card";
import Typography from "@material-ui/core/Typography";
// import Card from "@material-ui/core/Card";

const DownLoadApp: React.FC = () => {
  return (
    // <CssBaseline>
    <Grid item lg={12}>
      <h1>This is Download app Div</h1>
      <Card
        className="text-black bg-dark"
        style={{
          position: "relative",
          padding: "0 130px",
        }}
      >
        <Card.Img
          src="https://i.pinimg.com/564x/db/80/be/db80be3e33681117a4addaec287155e9.jpg"
          alt="Card image"
        />
        <Card.ImgOverlay>
          <Card>Card title</Card>
          <Typography
            variant="h2"
            color="textPrimary"
            component="p"
          ></Typography>
          <Card.Text>DOWN APP TO START EARNING</Card.Text>
          <Card.Text>Last updated 3 mins ago</Card.Text>
        </Card.ImgOverlay>
      </Card>
    </Grid>
    // </CssBaseline>
  );
};

export default DownLoadApp;
