import React from "react";
import "./../../../css/TGHomePage/AddedSiteList.scss";
import List from "@material-ui/core/List";
// import SiteNameConfirmedItems from "./SiteNameConfirmedItems";

const AddedSiteList: React.FC<{ SiteNameConfirmedItems: any }> = (props: {
  SiteNameConfirmedItems: any;
}) => {
  const { SiteNameConfirmedItems } = props;
  return (
    <>
      {/* {SiteNameConfirmedItems && SiteNameConfirmedItems.length > 0 && ( */}
      <List className="Added__siteName__List">
        {SiteNameConfirmedItems.map((siteNameConfirmedItem: any, idx) => (
          <SiteNameConfirmedItems
            key={`AddedSiteName_${idx}`}
            location={siteNameConfirmedItem.location}
            // onDelete={deleteSiteName}
          />
        ))}
      </List>
    </>
  );
};

export default AddedSiteList;
