import React from "react";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import "./../../../css/TGHomePage/SiteNameConfirmedItems.scss";

// interface ISiteNameConfirmedItemsProps {
// siteName: string;
// [key: string]: any;
// isConfirmed: boolean;
// }

const SiteNameConfirmationItems: React.FC = (props: any) => {
  // {
  // siteName,
  // isConfirmed,
  // }
  // ) => {
  // const textStyleName = isConfirmed ? "ListItem__deleted" : "";

  function handleClick(event: any) {
    event.preventDefault();
    // props.onDelete(props.id);
  }

  return (
    <ListItem className="ListItem">
      <div className="ListItem__siteName" />
      <ListItemText
        className="location__name"
        primary={props.location}
        // {"Marquette Park Pavilion"}
        // className={textStyleName}
      />

      <ListItemSecondaryAction>
        <IconButton edge="end" aria-label="edit">
          <EditIcon />
        </IconButton>
        <IconButton edge="end" aria-label="delete">
          <DeleteIcon onClick={handleClick} />
        </IconButton>
      </ListItemSecondaryAction>
    </ListItem>
  );
};

export default SiteNameConfirmationItems;
