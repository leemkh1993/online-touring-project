import "./../../../css/TGHomePage/AddSiteName.scss";
// import React, { useState, useEffect, useRef } from "react";
import React, { useState } from "react";
import Paper from "@material-ui/core/Paper";
import { IconButton } from "@material-ui/core";
import AddCircleIcon from "@material-ui/icons/AddCircle";
//import AddedSiteItems from "./AddedSiteList";
// import SiteNameConfirmedItems from "./SiteNameConfirmedItems";
// import InputBase from "@material-ui/core/InputBase";
import TextField from "@material-ui/core/TextField";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
// import DeleteIcon from "@material-ui/icons/Delete";

// import AddedSiteList from "./AddedSiteList";

// interface ISiteNameInputs {
//   id: number;
//   site_name: string;
// }
// interface ISite {
//   location: string;
//   [key: string]: any;
// }

function ToAddSiteName(props: any) {
  // const [selectedLabelIDStr, setSelectedLabelIDStr] = useState(Object.keys(labelMapping)[0]);
  const [siteName, setSiteName] = useState({ location: "" });
  const [siteNames, setSiteNames] = useState<string[]>([]);
  // const [updateLocation, setUpdateLocation] = useState("");
  // const [form, setValue] = useState({ location: "" });
  // const [processing, isProcessing] = useState(true);
  //const { SiteNameConfirmedItems } = props;
  // if (!processing) {
  // isProcessing(true);
  // dispatch(createTodoItemThunk(parseInt(selectedLabelIDStr), description));
  // }
  // };
  // console.log(updateLocation);

  function submitLocationHandler(event: any) {
    // props.onAdd(siteName);
    // if (event.target.value.length >= 2) {
    // console.log(event.target);
    // }
    // setUpdateLocation(siteName.location);

    // console.log(`clicked`);
    // console.log(siteName.location);

    // setUpdateLocation(siteName.location); //address
    // setSiteName({ location: "" });

    let newSiteName = siteNames.slice(0);
    newSiteName.push(siteName.location);
    setSiteNames(newSiteName);

    event.preventDefault();
    setSiteName({ location: "" });
    event.target.value = "";
    // isProcessing(false);
  }

  //OnChange
  function handleInput(event: any) {
    // const { name, value } = event.target;

    // setSiteName((prevSiteName) => {
    // return { ...prevSiteName, [name]: value };
    // });

    // let newSiteName = siteNames.slice(0);
    // newSiteName.push(event.target.value);
    // console.log(event.target.value);
    // setSiteNames(newSiteName);
    // console.log(newSiteName);
    setSiteName({ location: event.target.value });

    // setSiteName(...siteName, {[event.target.name] : event.target.value});
  }

  //on Form Submit
  // function printValues(event: any) {
  //   event.preventDefault();
  //   console.log(form.location);
  // }
  //onChange
  // function updateField(event: any) {
  //   setValue({ ...form, [event.target.name]: event.target.value });
  // }

  // function deleteSiteName(id) {
  //   setSiteNames((prevSiteNames) => {
  //     return prevSiteNames.filter((locationListItem, index) => {
  //       return index !== id;
  //     });
  //   });
  // }

  // function resetSiteName(event: any) {
  //   event.preventDefault();
  //   console.log(event.target);
  // }
  return (
    <>
      {/* <div> */}
      {/* <InputBase */}
      <Paper className="AddSiteName__Container" component="form">
        <TextField
          variant="outlined"
          fullWidth
          type="text"
          className="AddSiteName__input"
          placeholder="please add sitename here"
          onChange={handleInput}
          value={siteName.location}
          name="location"
        />
        <IconButton
          onClick={submitLocationHandler}
          // onClick={clickHandler}
          className="iconButton"
          aria-label="AddCircleIcon"
          // value={siteName}
        >
          <AddCircleIcon />
        </IconButton>
      </Paper>

      {/* <form onSubmit={printValues}>
        <label>
          Location:
          <input value={form.location} name="location" onChange={updateField} />
        </label>
        <button>Submit</button>
      </form> */}

      {/* {!processing && ( */}
      {/* <List className="Added__siteName__List">
        <ListItem className="ListItem">
          <div className="ListItem__siteName" />
          <ListItemText className="location__name" primary={updateLocation} />
          <ListItemSecondaryAction>
            <IconButton edge="end" aria-label="delete">
              <DeleteIcon />
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
      </List> */}
      {/* )} */}
      {/* {!processing && <AddedSiteList SiteNameConfirmedItems={siteNames} />} */}

      {/* {!processing &&
        SiteNameConfirmedItems &&
        SiteNameConfirmedItems.length > 0 && (
          <List className="Added__siteName__List">
            {siteNames.map((locationListItem, index) => (
              <SiteNameConfirmedItems
                key={index}
                id={index}
                location={locationListItem}
                onDelete={deleteSiteName}
                primary={updateLocation}
              />
            ))}
          </List>
        )}  */}
      {/* {siteNames.map((site) => (
        <div>{site}</div>
      ))} */}

      {siteNames && siteNames.length > 0 && (
        <List className="Added__siteName__List">
          {siteNames.map((site, index) => (
            <ListItem className="ListItem">
              <div className="ListItem__siteName" />
              <ListItemText className="location__name" primary={site} />
              <ListItemSecondaryAction>
                <IconButton edge="end" aria-label="delete">
                  {/* <DeleteIcon onDelete={deleteSiteName} /> */}
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
          ))}
        </List>
      )}
      {/* <AddedSiteList /> */}
    </>
  );
}
// onSubmit={resetSiteName}
export default ToAddSiteName;

/* /* {!isProcessing && <List className="">{siteName}</List>}   */

/* /* <SiteNameConfirmedItems value={props.siteName} />  */
