import React, { useState } from "react";

interface Item {
  //   [key: string]: any;
  //   key: number;
  value: any;
  // item: string;
  // id:number;
}

function ListOfThings() {
  // const [items, setItems] = useState({id: 0, name: "haha"})<Array>>[]
  const [items, setItems] = useState<Item>();
  const [itemName, setItemName] = useState("");

  const addItem = (event: any) => {
    event.preventDefault();
    // const { name, value } = event.target;
    // setItems((previous => {
    //     return { previous, [name]: value });
    // });
    //  setItems([
    //    ...items,
    //    {
    //      id: items.length,
    //      name: itemName,
    //    },
    //  ]);
    setItems(items);
    setItemName("");
  };

  return (
    <>
      <form onSubmit={addItem}>
        <label>
          <input
            name="item"
            type="text"
            value={itemName}
            onChange={(e) => setItemName(e.target.value)}
          />
        </label>
      </form>
      {typeof items != "undefined" && (
        <ul>
          {/* {items.map((item) => (
            <li key={item.id}>{item.name}</li>
          ))} */}
          {items}
        </ul>
      )}
    </>
  );
}

export default ListOfThings;
