import React, { useState } from "react";
// import { Link } from "react-scroll";
import { makeStyles, Theme } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
// import Select from "@material-ui/core/Select";
// import MenuItem from "@material-ui/core/MenuItem";
import Container from "@material-ui/core/Container";
import { useForm } from "react-hook-form";
// import UploadPhotoButton from "../UserProfilePage/UploadPhotoButton";
import { Space } from "antd";
import { useSelector, useDispatch } from "react-redux";
import { IRootState } from "../../../redux/store";
import { submitOfferThunk } from "../../../redux/submitOffer/thunks";
import { Alert, FormGroup,  Label } from "reactstrap";
import GoogleSearchBar from "../../JitsiPage/GoogleSearchBar";
// import DatePicker, { Controller } from "react-hook-form";
import ReactDatePicker from "../ReactDateTimePicker/ReactDatePicker";
import TimePicker from "react-time-picker";
import Grid from "@material-ui/core/Grid";
// import VacationImgUploadButton from "./VacationImgUploadButton";
import "../../../css/TGHomePage/Try.scss";

const useStyles = makeStyles((theme: Theme) => ({
  paper: {
    marginLeft: theme.spacing(7),
    marginTop: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  form: {
    width: "90%",
    // marginTop: theme.spacing(1),
  },
  submit: {
    marginTop: "25px",
  },
  checkOut: {
    // marginTop: theme.spacing(10),
    marginTop: "25px",
  },

  formControl: {
    minWidth: 150,
  },
  selectEmpty: {
    marginTop: theme.spacing(3),
  },
  text: {
    backgroundColor: "rgba(0, 0, 0, 0.8)",
    color: "#fff",
    borderRadius: "5px",
  },
}));

interface IOfferFormInputs {
  offerName: string;
  country: string;
  siteName: string;
  description: string;
  description_details:string;
  destinations: any;
  sitePic: string;
  longitude: number;
  latitude: number;
  hashtag: string;
  cost: number;
  expiryDate: string;
}

const FullFormContent: React.FC = () => {
  const { register, handleSubmit } = useForm<IOfferFormInputs>();

  const [hashtags, setHashTags] = useState("");
  const handleHashtagChange = (event:React.ChangeEvent<{value:any}>)=>{
    setHashTags(event.target.value as any);

  }

  const classes = useStyles();

  const dispatch = useDispatch();
  const errMessage = useSelector(
    (state: IRootState) => state.submitOffer.message
  );
  const isProcessing = useSelector(
    (state: IRootState) => state.submitOffer.isProcessing
  );
  // const respondMessage = useSelector((state: IRootState) => state.auth.message);
  const [submitMessage, setSubmitMessage] = useState(
    "submit successed, you can browse your offer at the main page"
  );
  const toAddPlacesAddressList = useSelector(
    (state: IRootState) => state.addAddress.list
  );
  // console.log(toAddPlacesAddressList);
  
  

  // //date-picker logic
  const [date, setDate] = useState(new Date());
  const [hour, setHour] = useState(0);
  const [minute, setMinute] = useState(0);
  //detect selected date before join tour
  const [selectedDate, setSelectedDate] = useState(false);

  const clickHandler = async (date: any) => {
    // Set time in new date according to current states
    const defineTime = date.setHours(hour, minute, 0);
    // Convert UTC back in object
    const newDate = new Date(defineTime);
    // Update date state
    setDate(newDate);
    setSelectedDate(true);
    // console.log(newDate);
  };
  // Trigger each change from time picker
  const timeChangeHandler = async (time: any) => {
    // Get value from minute
    const minute = time.substring(3, 5);
    // console.log(minute);
    // Get value from hour
    const hour = time.substring(0, 2);
    // console.log(hour);
    // Set time in new date according to current states
    const defineTime = date.setHours(hour, minute, 0);
    // Convert UTC back in object
    const newTime = new Date(defineTime);
    // Set new states
    setHour(hour);
    setMinute(minute);
    // console.log(newTime);
    setDate(newTime);
  };

  const onSubmit = (data: IOfferFormInputs) => {
    // console.log("This is submit form", data);
    // console.log(`form submitted!`);
    const array_submit = toAddPlacesAddressList.map((List:any)=>{
      // console.log(typeof(List.placeAddress))
        return {"siteName":List.placeAddress,
                // "description":"",
                "lat":List.coordinates.lat,
                "lng":List.coordinates.lng,
                "country":data.country, 
                // "city":""
              };
    })
  // console.log(array_submit);
  //  ITouristOfferFormSubmitedItems

    // e.preventDefault();
    // fetch("/", {
    //   method: "POST",
    //   body: JSON.stringify({ data }),
    //   headers: { "Content-Type": "application/json" },
    // })
    // .then((res) => res.json())
    // .then((json) => setUser(json.user));

    if (!isProcessing) {
      // const strData = JSON.stringify(data);
      // JSON.parse(strData)

      dispatch(
        submitOfferThunk(
          data.offerName,
          data.cost,
          date,
          data.description,
          data.description_details,
          array_submit,
          [hashtags],
          // toAddPlacesAddressList,
          // data.country,
          // data.siteName,
          // data.destinations,
          // data.sitePic,
          // data.longitude,
          // data.latitude,
          // data.hashtag,
          
          
          // data.expiryDate
        )
      );
      setSubmitMessage(submitMessage);
    }
  };

  // function handleCostChange(event: any) {
  //   setCostValue(event.target.value);
  //   console.log(event.target.value);
  // }

  // Fri Oct 23 2020 00:00:00 GMT+0800 (Hong Kong Standard Time)

  // const value = Object.values(toAddPlacesAddressList);
  // console.log(value);

  // const key = Object.keys(toAddPlacesAddressList);
  // console.log(key);

  return (
    <>
      {/* {toAddPlacesAddressList
        ? JSON.stringify(toAddPlacesAddressList.length)
        : ""} */}
      <Container
        // style={{ padding: "0!important " }}
        component="main"
        maxWidth="md"
      >
        {/* <div style={{ width: "100%", height: "auto" }}> */}
        <CssBaseline />
        <div className={classes.paper}>
          <form
            onSubmit={handleSubmit(onSubmit)}
            className={classes.form}
            noValidate
          >
            <div id="ScrollDown">
              <GoogleSearchBar />
            </div>

            {/* event title  */}
            <TextField
              variant="outlined"
              margin="normal"
              inputRef={register}
              required
              fullWidth
              id="offerName"
              label="event name"
              name="offerName"
              autoComplete="false"
              autoFocus
            />

            {/* event country name */}
            <TextField
              variant="outlined"
              margin="normal"
              inputRef={register}
              required
              fullWidth
              name="country"
              label="please input your current country location"
              type="text"
              id="country"
              autoComplete="false"
            />
            {/* event description */}
            <TextField
              variant="outlined"
              label="offer description: hint:the title shows on the main page "
              placeholder="e.g this is more than how you can imagine!"
              type="text"
              name="description"
              margin="normal"
              inputRef={register}
              required
              fullWidth
              multiline
              autoFocus
              id="description"
              autoComplete="false"
              // value={value}
              // onChange={handleChange}
            />
            <TextField
              variant="outlined"
              id="description"
              label="offer description"
              placeholder="please provide a short description of the offer such as the event theme and features etc."
              margin="normal"
              inputRef={register}
              required
              fullWidth
              multiline
              autoFocus
              autoComplete="false"
              type="text"
              name="description_details"
              rowsMax={4}
              // value={value}
              // onChange={handleChange}
            />
            {/* offer id & user id */}
            {/* <div style={{ width: "100%", height: "auto" }}> */}
            <Space style={{ display: "flex" }} direction="vertical" size={12}>
              {/* <GoogleSearchBar
              coordinates={props.coordinates}
              placeAddress={props.placeAddress}
              /> */}
              {/* event date & time */}
            </Space>
            <Grid
              container
              spacing={1}
              direction="row"
              justify="space-around"
              alignItems="center"
            >
              <Grid>
                <TextField
                  variant="outlined"
                  margin="normal"
                  style={{ margin: "20px auto" }}
                  inputRef={register}
                  required
                  fullWidth
                  name="cost"
                  id="quantity"
                  label="$cost"
                  type="number"
                  placeholder="$USD"
                  autoComplete="false"
                  // className="cost"
                  // onChange={handleCostChange}
                  className={classes.selectEmpty}
                  // value={costValue}
                />

                <FormGroup
                  // inputRef={(d) => console.log(d)}
                  // onChange={callback}
                  // className={classes.formControl}
                >
                  <Label
                    // onChange={handleHashtagChange}
                    className="labelHashtags"
                    for="hashtags"
                  >
                    hashtags:
                  </Label>
                  <select
                    className="hashtagsStyle"
                    name="hashtags"
                    onChange={handleHashtagChange}
                    value={hashtags}
                    ref={register({ required: true })}
                  >
                    <label htmlFor="hashtags">hashtags</label>
                    <option value="shopping">#shopping 🛍 🛒</option>
                    <option value="viewing">#viewing 🛤</option>
                    <option value="travell">#travell 📸</option>
                    <option value="beach">#beach👙</option>
                    <option value="nature">#nature 🌳</option>
                    <option value="landscape">#landscape 🗾</option>
                    <option value="religious">#religious 📿</option>
                    <option value="art gallery">#art gallery🖼️ </option>
                    <option value="weekendespace">weekendespace🏄🏼</option>
                    <option value="winery">#winery🥂</option>
                    <option value="sightseeing">#sightseeing 🏞</option>
                    <option value="onlylocalknows">onlylocalknows 👀</option>
                    <option value="night life ">#night life 🙈</option>
                    <option value="excitment">#excitment 🧗‍♂️</option>
                    <option value="wow">#wow🤹‍♀️</option>
                  </select>

                </FormGroup>
              </Grid>
            </Grid>

            <FormControl>
              <Grid
                container
                spacing={3}
                // direction="row"
                // justify="center"
                // alignItems="center"
              >
                <Grid
                  style={{
                    backgroundColor: "rgba(0, 0, 0, 0.95)",
                    borderRadius: 15,
                    color: "#fff",
                    marginTop: 15,
                  }}
                  item
                  xs={12}
                >
                  <h6 className={classes.text}>Event Date & Time:</h6>
                  <ReactDatePicker
                    selectedDate={selectedDate}
                    dateClickedFn={clickHandler}
                  />

                  <TimePicker
                    onChange={timeChangeHandler}
                    value={date}
                    name={date}
                    clearIcon={null}
                  />
                </Grid>
              </Grid>
            </FormControl>

            <Grid
              container
              spacing={1}
              direction="column"
              justify="space-between"
              alignItems="center"
              className={classes.selectEmpty}
            >
              <Button
                className={classes.submit}
                style={{ marginTop: "30px" }}
                type="submit"
                // fullWidth
                variant="contained"
                color="primary"
                value="submit"
                
              >
                Submit Form 📲
              </Button>
            </Grid>
            {errMessage ? (
              <Alert style={{ margin: 16 }} color="danger">
                {errMessage}
              </Alert>
            ) : (
              ""
              // <Alert style={{ margin: 16 }} color="success"></Alert>
            )}
          </form>
          {/* <pre>{JSON.stringify(submittedData, null, 2)}</pre> */}
          {/* atnd */}
          {/* <AntdButton onClick={success}>Submit</AntdButton> */}
        </div>
      </Container>
    </>
  );
};

export default FullFormContent;

//change structure of dataset
// date picker & time picker
// value format consist
