import React, { useState } from "react";
// import { useSelector } from "react-redux";
//  useDispatch
// import { IRootState } from "../../../redux/store";
// import { addAddressSuccess } from "./../../../redux/submitOffer/actions";

const UploadImg = (props: any) => {
  const [image, setImage] = useState({ preview: "", raw: "" });
  const [fileUrl, setfileUrl] = useState("");
  //const [updateImgUrl, setUpdateImgUrl] = useState("");
  const { fileSelect, idx } = props;

  // const dispatch = useDispatch();
  // const toAddPlacesAddressList = useSelector(
  //   (state: IRootState) => state.addAddress.list
  // );

  const handleChange = (e: any) => {
    if (e.target.files.length) {
      // console.log(e.target.files);
      setImage({
        preview: URL.createObjectURL(e.target.files[0]),
        raw: e.target.files[0],
      });
      // setfileUrl(e.value);
      // console.log(FileList);
      // console.log(image);
      setfileUrl(e.target.files[0].name);

      //clone imgUrl
      fileSelect(fileUrl, idx);
    }
  };

  // const handleUpload = async (e) => {
  //     e.preventDefault();
  //     console.log("You click the upload");
  //     return { image };
  // const formData = new FormData();
  // formData.append("image", image.raw);

  // //    fetch(`${process.env.REACT_APP_API_SERVER}/createOffer`), {
  // const token = localStorage.getItem("token");
  // if (!token) {
  //   alert(JSON.stringify(`please login to continue`));
  // }
  // await fetch(`${process.env.REACT_APP_API_SERVER}/`, {
  //   method: "POST",
  //   headers: {
  //     Authorization: `Bearer ${token}`,
  //     "Content-Type": "multipart/form-data",
  //   },
  //   body: formData,
  // });
  //   };

  // console.log(toAddPlacesAddressList);

  // console.log(fileUrl);

  // console.log(updateImgUrl)
  return (
    <div>
      <label htmlFor="upload-button">
        {/* toAddPlacesAddressList && */}
        {image.preview ? (
          // <img src={image.preview} alt="" width="auto" height="auto" />
          <div>{fileUrl})</div>
        ) : (
          <div>
            <span
              className="fa-stack fa-2x mt-1 mb-1"
              style={{
                display: "flex",
                textAlign: "center",
                color: "rgba(0,0,0,0.7)",
              }}
            >
              <i className="fas fa-circle fa-stack-2x" />
              {/* <UploadPhotoButton /> */}
              <i className="fas fa-camera fa-stack-1x" />
            </span>
            <div className="text-center">Upload photo</div>
          </div>
        )}
      </label>
      <input
        type="file"
        id="upload-button"
        style={{ display: "none" }}
        onChange={handleChange}
        // value={fileUrl}
      />
      <br />
    </div>
  );
};
export default UploadImg;

// <div className="container">
