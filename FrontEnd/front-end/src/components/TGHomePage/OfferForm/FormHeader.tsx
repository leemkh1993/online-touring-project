import React from "react";
import Typography from "@material-ui/core/Typography";

const FormHeader: React.FC = () => {
  return (
    <div
      style={{
        color: "#000",
        // backgroundColor: "rgba(0, 0, 0, 0.2)",
        width: "100%",
      }}
    >
      <Typography style={{ color: "#000" }} component="h1" variant="h3">
        Start listing Offer Here~!
      </Typography>
    </div>
  );
};
export default FormHeader;
