import React from "react";
import { Grid } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import { Paper, makeStyles } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
// import Avatar from "./Avatar";

const useStyles = makeStyles((theme) => ({
  pageContent: {
    margin: theme.spacing(5),
    padding: theme.spacing(5),
    backgroundColor: "transparent",
    border: "solid 2px #e3e3e3",
  },
  root: {
    margin: theme.spacing(0.5),
    // marginLeft: theme.spacing(2),
  },
  form: {
    width: "100%",
    marginTop: theme.spacing(1),
    height: 450,
  },
}));

const EmptyItemForm: React.FC = () => {
  const classes = useStyles();
  return (
    <Paper className={classes.pageContent}>
      <Grid item lg={12}>
        <form className={classes.form} noValidate>
          <Typography variant="h1" color="textPrimary" component="p">
            Start launching your offer
          </Typography>
          {/* <Avatar /> */}
          <Typography
            variant="body2"
            style={{
              height: 250,
              width: "fit-content",
              fontSize: 35,
              letterSpacing: "0 !important",
              marginTop: "25px",
              marginLeft: "auto",
              marginRight: "auto",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              border: "solid 1px #e3e3e3",
            }}
            color="textSecondary"
            component="p"
          >
            It looks like your offer List is empty now! You can start to launch
            the offer by clicking the below Button!
          </Typography>

          <div style={{ margin: 20 }}>
            <Button
              type="submit"
              // fullWidth
              variant="contained"
              color="primary"
              // className={classes.submit}
            >
              List Offer
            </Button>
          </div>
        </form>
      </Grid>
    </Paper>
  );
};

export default EmptyItemForm;
