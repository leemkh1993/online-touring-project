import React from "react";
import Styles from "./Styles";
import { Field } from "react-final-form";
// Form,
// import ReactDatePicker from "../ReactDateTimePicker/ReactDatePicker";
import TimePicker from "react-time-picker";

// import DatePicker from "react-date-picker";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import PhotoCamera from "@material-ui/icons/PhotoCamera";

const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

const onSubmit = async (values) => {
  await sleep(300);
  window.alert(JSON.stringify(values, 0, 2));
};

// export const render = { handleSubmit, form, submitting, pristine, values }

const MockUpForm: React.FC = () => (
  <>
    <Styles>
      <h1>🏁 Offer Submittion Form</h1>
      <a href="https://github.com/erikras/react-final-form#-react-final-form"></a>
      {/* <Form onSubmit={onSubmit} */}
      {/* <Form onSubmit={onSubmit} */}
      {/* <form onSubmit={handleSubmit}> */}
        <div>
          <label>Offer Name</label>
          <Field
            name="offerName"
            component="input"
            type="text"
            placeholder="Event title"
          />
        </div>
        <div>
          <label>Location</label>
          <Field
            name="country"
            component="input"
            type="text"
            placeholder="Please specific your country"
          />
        </div>
        <div>
          <label>Event description</label>
          <Field
            name="description"
            component="input"
            type="text"
            placeholder="Please provide a short event description here"
          />
        </div>
        {/* <label>Notes</label>
            <Field name="notes" component="textarea" placeholder="Notes" /> */}
        <div>
          <label>Charges Amount</label>
          <Field
            name="cost"
            component="input"
            type="text"
            placeholder="$USD / offer"
          />
        </div>
        {/* <div>
            <label>Prefer</label>
            <Field name="employed" component="input" type="checkbox" />
          </div> */}
        <div>
          <label>HashTags</label>
          <Field name="HashTags" component="select">
            <option />
            <option value="#winery">❤️ #Winery</option>
            <option value="#00ff00">💚 Green</option>
            <option value="#0000ff">💙 Blue</option>
          </Field>
        </div>

        {/* value="offerDate"  */}
        {/* value="offerTime" */}
        {/* <ReactDatePicker /> */}
        <TimePicker />
        <div>
          <label>Offer Expiry Date</label>
          <Field
            name="expirysdate"
            component="input"
            type="text"
            placeholder="mm/dd/yyyy"
          />
        </div>

        {/* <div>
            <label>Toppings</label>
            <Field name="toppings" component="select" multiple>
              <option value="chicken">🐓 Chicken</option>
              <option value="ham">🐷 Ham</option>
              <option value="mushrooms">🍄 Mushrooms</option>
              <option value="cheese">🧀 Cheese</option>
              <option value="tuna">🐟 Tuna</option>
              <option value="pineapple">🍍 Pineapple</option>
            </Field>
          </div> */}
        {/* <UploadImg /> */}
        <div style={{ display: "flex", flexWrap: "wrap" }}>
          <input
            accept="image/*"
            id="contained-button-file"
            name="site_pic"
            multiple
            type="file"
          />
          <label htmlFor="contained-button-file">
            <Button variant="contained" color="primary" component="span">
              Upload
            </Button>
          </label>
          <input accept="image/*" id="icon-button-file" type="file" />
          <label htmlFor="icon-button-file">
            <IconButton
              color="primary"
              aria-label="upload picture"
              component="span"
            >
              <PhotoCamera
              // value="sitephoto"
              />
            </IconButton>
          </label>
        </div>
        {/* <GoogleSearchBar /> */}

        {/* <div>
            <label>Sauces</label>
            <div>
              <label>
                <Field
                  name="sauces"
                  component="input"
                  type="checkbox"
                  value="ketchup"
                />{' '}
                Ketchup
              </label>
              <label>
                <Field
                  name="sauces"
                  component="input"
                  type="checkbox"
                  value="mustard"
                />{' '}
                Mustard
              </label>
              <label>
                <Field
                  name="sauces"
                  component="input"
                  type="checkbox"
                  value="mayonnaise"
                />{' '}
                Mayonnaise
              </label>
              <label>
                <Field
                  name="sauces"
                  component="input"
                  type="checkbox"
                  value="guacamole"
                />{' '}
                Guacamole 🥑
              </label>
            </div>
          </div> */}
        {/* <div>
            <label>Best Stooge</label>
            <div>
              <label>
                <Field
                  name="stooge"
                  component="input"
                  type="radio"
                  value="larry"
                />{' '}
                Larry
              </label>
              <label>
                <Field
                  name="stooge"
                  component="input"
                  type="radio"
                  value="moe"
                />{' '}
                Moe
              </label>
              <label>
                <Field
                  name="stooge"
                  component="input"
                  type="radio"
                  value="curly"
                />{' '}
                Curly
              </label>
            </div>
          </div> */}
        <div></div>
        <div className="buttons">
          <button type="submit">Submit</button>
          <button
            type="button"
            // onClick={form.reset}
            // disabled={submitting || pristine}
          >
            Reset
          </button>
        </div>
        {/* <pre>{JSON.stringify(values, 0, 2)}</pre> */}
      </form>
    )}
    />
    </Styles>
  </>
);

export default MockUpForm;
