import React, { useState } from "react";
import UploadPhotoButton from "./../../UserProfilePage/UploadPhotoButton";
// import { Result } from "antd";
//  <Result status="error"></Result>
// <Result status="success"></Result>
import { useSelector, useDispatch } from "react-redux";
import { IRootState } from "../../../redux/store";
import { addAddressSuccess } from "./../../../redux/submitOffer/actions";

const VacationImgUploadButton: React.FC = () => {
  const [image, setImage] = useState({ preview: "", raw: "" });
  const [fileUrl, setfileUrl] = useState("");

  const dispatch = useDispatch();
  const toAddPlacesAddressList = useSelector(
    (state: IRootState) => state.addAddress.list
  );

  dispatch(
    addAddressSuccess({
      coordinates: { lat: 0, lng: 0 },
      placeAddress: "",
      // fileUrl: "",
    })
  );

  const handleChange = (e: any) => {
    if (e.target.files.length) {
      // console.log(e.target.files);
      setImage({
        preview: URL.createObjectURL(e.target.files[0]),
        raw: e.target.files[0],
      });
      // setfileUrl(e.value);
      // console.log(FileList);
      // console.log(image);
      setfileUrl(e.target.files[0].name);
    }
  };

  //   const handleUpload = async (e) => {
  //     e.preventDefault();
  //     console.log("You click the upload");
  //     return { image };
  // const formData = new FormData();
  // formData.append("image", image.raw);

  // //    fetch(`${process.env.REACT_APP_API_SERVER}/createOffer`), {
  // const token = localStorage.getItem("token");
  // if (!token) {
  //   alert(JSON.stringify(`please login to continue`));
  // }
  // await fetch(`${process.env.REACT_APP_API_SERVER}/`, {
  //   method: "POST",
  //   headers: {
  //     Authorization: `Bearer ${token}`,
  //     "Content-Type": "multipart/form-data",
  //   },
  //   body: formData,
  // });
  //   };
  // console.log(toAddPlacesAddressList);
  return (
    <div
      style={{
        height: "auto",
        border: "solid 1px currentColor",
        borderRadius: "5px",
      }}
    >
      <label htmlFor="upload-button">
        {toAddPlacesAddressList && image.preview ? (
          <>
            <img
              className="img-responsive"
              src={image.preview}
              alt=""
              width="565px"
              height="800"
            />
            <p>{fileUrl})</p>
          </>
        ) : (
          <p>
            <span
              className="fa-stack fa-2x mt-1 mb-1"
              style={{
                display: "flex",
                textAlign: "center",
                color: "rgba(0,0,0,0.7)",
              }}
            >
              <UploadPhotoButton />
            </span>
            <div className="text-center">Upload photo</div>
          </p>
        )}
      </label>
      <input
        type="file"
        id="upload-button"
        style={{
          display: "none",
        }}
        onChange={handleChange}
        // value={fileUrl}
      />
      <br />
      {/* <button onClick={handleUpload}>Upload</button> */}
    </div>
  );
};
export default VacationImgUploadButton;

// <div className="container">
