import "./../../css/TGHomePage/TGSearchTagBar.scss";
import React, { useEffect, useState } from "react";
import Paper from "@material-ui/core/Paper";
import Divider from "@material-ui/core/Divider";
import { createStyles, makeStyles } from "@material-ui/core/styles";
import SearchIcon from "@material-ui/icons/Search";
import SendIcon from "@material-ui/icons/Send";
import IconButton from "@material-ui/core/IconButton";
import { Button } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import Select from "react-select";
import makeAnimated from "react-select/animated";
// import { hashtagOptions } from "../HomePage/SearchBar/data";
import { Link } from "react-scroll";
// import { hashtagOptions } from "../HomePage/searchBar/data";
import { useDispatch, useSelector} from "react-redux";
import { IRootState } from "../../redux/store";
import { getHashtagThunk } from "../../redux/getHashTag/thunks";
const animatedComponents = makeAnimated();

const useStyles = makeStyles((theme) => {
  return createStyles({
    root: {
      padding: "3px 4px",
      display: "flex",
      alignItems: "center",
      margin: "10px 250px !important",
      width: "100%",
      color: "#fff !important",
      backgroundColor: "rgba(0, 0, 0, 0.7) !important",
    },
    input: {
      marginLeft: 8,
      flex: 1,
    },
    iconButton: {
      padding: 10,
    },
    divider: {
      width: 1,
      height: 28,
      margin: 4,
    },
  });
});
const TGSearchTagBar: React.FC = () => {

  // 1/10 hashtag search by LC
  const dispatch = useDispatch();
  const Hashtags = useSelector((state:IRootState)=>state.getHashtag.hashtag);
  useEffect(()=>{
    dispatch(getHashtagThunk());
  },[dispatch]);

  const options = Hashtags.map((Hashtag:any)=>{
    return { value: `${Hashtag.substring(1)}`, label: `${Hashtag.substring(1)}` };
  })
  // console.log(options);
  const [selectedOption, setSelectedOption] = useState("#shopping") as any;

  const classes = useStyles();
  return (
    <React.Fragment>
      <div className="background-img"></div>
      <div className="TGsearchBarContainer">
        {/* "inherit" | "initial" | "secondary" | "secondary" | "textsecondary" | "textSecondary" | "error" | undefined' */}
        <Typography
          variant="h3"
          color="primary" 
      
          // component="p"
        >
          UPGRAGE YOUR WEEKENDS!

        </Typography>
        <Paper className={classes.root} elevation={1}>
          <Select
            closeMenuOnSelect={false}
            components={animatedComponents}
            // defaultValue={[hashtagOptions[0], hashtagOptions[8]]}
            isMulti
            options={options}
            defaultValue={selectedOption}
            onChange={setSelectedOption}
            className={classes.input}
            classNamePrefix="select"
            placeholder="Have a glance of what the others doing at the main page and start yours special one! Happy listing!"
          />

          <IconButton className={classes.iconButton} aria-label="Search">
            <SearchIcon />
          </IconButton>
          <Divider className={classes.divider} />
          <Link to={`/list/%23${selectedOption === null ? "okok":selectedOption[0].value}`}>
            <IconButton
              color="secondary"
              className={classes.iconButton}
              aria-label="Directions"
            >
              <SendIcon />
            </IconButton>
          </Link>
          {/* <IconButton
            color="secondary"
            className={classes.iconButton}
            aria-label="Directions"
          >
            <SendIcon />
          </IconButton> */}
        </Paper>
        {/* "hint: you can play around the #hashtag to see whtat's on the hit" */}
        <div className="buttonGroup">
          <Button className="customMuiButtonRoot" disabled>
            | Popular Search Item |
          </Button>
          {/* 1/10 Hashtag search by LC*/}
          {
            Hashtags.slice(0, 10).map((Hashtag:any)=>(
              <Link to={`/list/%23${Hashtag.substring(1)}`}>
                <Button
                      className={`eachButton`}
                      variant="outlined"
                      color="secondary"
                      href="#outlined-buttons"
                >
                      {Hashtag}
                </Button>
              </Link>
            ))
          }
          {/* <Button
            className="eachButton"
            variant="outlined"
            color="secondary"
            href="#outlined-buttons"
          >
            #Island
          </Button>
          <Button
            className="eachButton"
            variant="outlined"
            color="secondary"
            href="#outlined-buttons"
          >
            #village
          </Button>
          <Button
            className="eachButton"
            variant="outlined"
            color="secondary"
            href="#outlined-buttons"
          >
            #artgallery
          </Button>
          <Button
            className="eachButton"
            variant="outlined"
            color="secondary"
            href="#outlined-buttons"
          >
            #weekendespace
          </Button>
          <Button
            className="eachButton"
            variant="outlined"
            color="secondary"
            href="#outlined-buttons"
          >
            #winery
          </Button>
          <Button
            className="eachButton"
            variant="outlined"
            color="secondary"
            href="#outlined-buttons"
          >
            #sightseeing
          </Button>
          <Button
            className="eachButton"
            variant="outlined"
            color="secondary"
            href="#outlined-buttons"
          >
            #Onlylocalknows
          </Button>
          <Button
            className="eachButton"
            variant="outlined"
            color="secondary"
            href="#outlined-buttons"
          >
            #Nightlife
          </Button>
          <Button
            className="eachButton"
            variant="outlined"
            color="secondary"
            href="#outlined-buttons"
          >
            #luxuries
          </Button>
          <Button
            className="eachButton"
            variant="outlined"
            color="secondary"
            href="#outlined-buttons"
          >
            #wow
          </Button> */}
         
          <div
            style={{
              width: "auto",
              display: "flex",
              // paddingLeft: 300,
              // width: 
              justifyContent: "center",
              alignContent: "center",
              // position: "absolute",
              // margin: "0 auto",
              // top: "455px",
              // left: "700px",
            }}
          >

            <Link to="ScrollDown" smooth="true" duration="1000">
              <Button variant="contained" color="secondary">
                GET STARTED HERE
              </Button>
            </Link>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default TGSearchTagBar;
