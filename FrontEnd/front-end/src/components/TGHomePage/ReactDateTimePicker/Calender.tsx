import React, { useState } from "react";
import DayPicker from "react-day-picker";
import DayPickerDummy from "./DatePickerDummy";
import TimePicker from "react-time-picker";
//css
// import gird_styles from "../../../css/OfferPage/OfferId.module.css";
// import element_styles from "../../../css/OfferPage/OfferId.module.css";
// import "react-day-picker/lib/style.css";

//date-picker logic
const [date, setDate] = useState(new Date());
const [hour, setHour] = useState(0);
const [minute, setMinute] = useState(0);
//detect selected date before join tour
const [selectedDate, setSelectedDate] = useState(false);

const clickHandler = async (date: any) => {
  // Set time in new date according to current states
  const defineTime = date.setHours(hour, minute, 0);
  // Convert UTC back in object
  const newDate = new Date(defineTime);
  // Update date state
  setDate(newDate);
  setSelectedDate(true);
  // console.log(newDate);
};
// Trigger each change from time picker
const timeChangeHandler = async (time: any) => {
  // Get value from minute
  const minute = time.substring(3, 5);
  // console.log(minute);
  // Get value from hour
  const hour = time.substring(0, 2);
  // console.log(hour);
  // Set time in new date according to current states
  const defineTime = date.setHours(hour, minute, 0);
  // Convert UTC back in object
  const newTime = new Date(defineTime);
  // Set new states
  setHour(hour);
  setMinute(minute);
  // console.log(newTime);
  setDate(newTime);
};
// console.log(selectedDate);

const Calender: React.FC = () => {
  return (
    <div
    //   className={`${gird_styles.date_picker} ${element_styles.min_height}`}
    >
      Calender
      <DayPicker onDayClick={clickHandler} />
      <DayPickerDummy
        selectedDate={selectedDate}
        dateClickedFn={clickHandler}
      />
      <TimePicker
        onChange={timeChangeHandler}
        value={date}
        clearIcon={null}
        // className={element_styles.picker__wrapper}
      />
    </div>
  );
};

export default Calender;
