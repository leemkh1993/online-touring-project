// import React from "react";
// import DayPicker from "react-day-picker";
// import DayPickerInput from "react-day-picker/DayPickerInput";
// import "react-day-picker/lib/style.css";

// const ReactDatePicker: React.FC = () => {
//   return (
//     <div>
//       <h3>DayPicker</h3>
//       <DayPicker selectedDays={new Date()} />

//       <h3>DayPickerInput</h3>
//       <DayPickerInput placeholder="DD/MM/YYYY" format="DD/MM/YYYY" />
//     </div>
//   );
// };
// export default ReactDatePicker;
import React from "react";
import DayPicker from "react-day-picker";
import "react-day-picker/lib/style.css";

interface IState {
  selectedDay: any;
}

interface IProps {
  selectedDate: boolean;
  dateClickedFn: (date: any) => Promise<void>;
}

export default class DatePickerDummy extends React.Component<IProps, IState> {
  constructor(props) {
    super(props);
    this.handleDayClick = this.handleDayClick.bind(this);
    this.state = {
      selectedDay: null,
    };
  }

  handleDayClick(day: Date, { selected }) {
    this.props.dateClickedFn(day);
    // console.log(this.props.dateClickedFn(day));
    this.setState({
      selectedDay: selected ? undefined : day,
    });
    //back state to parent by fn.
  }

  render() {
    return (
      <div>
        <DayPicker
          selectedDays={this.state.selectedDay}
          onDayClick={
            this.handleDayClick as any
            // this.props.dateClickedFn as any
          }
        />
        <p>
          {this.state.selectedDay
            ? this.state.selectedDay.toLocaleDateString()
            : ""}
        </p>
      </div>
    );
  }
}
