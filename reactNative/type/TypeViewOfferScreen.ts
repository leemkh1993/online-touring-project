import {
  VIEW_OFFER_SCREEN_FAIL,
  VIEW_OFFER_SCREEN_FETCHING,
  VIEW_OFFER_SCREEN_LOADING,
  VIEW_OFFER_SCREEN_SUCCESS,
} from '../redux/OffersScreen/ViewOfferScreen/action';

export interface OfferDetailData {
  offer: any,
  destinations: any,
  hashtag: any,
  streamerReviews: any,
  audience_reviews: any
}

export interface ViewOfferScreenState {
  isLoading: boolean;
  isFetching: boolean;
  data: OfferDetailData | null;
  errMessage: string | null;
}

export interface ViewOrderProps {
  data: OfferDetailData;
}

export interface IViewOfferScreenLoading {
  type: typeof VIEW_OFFER_SCREEN_LOADING;
}

export interface IViewOfferScreenFetching {
  type: typeof VIEW_OFFER_SCREEN_FETCHING;
}

export interface IViewOfferScreenSuccess {
  type: typeof VIEW_OFFER_SCREEN_SUCCESS;
  data: OfferDetailData;
}

export interface IViewOfferScreenFail {
  type: typeof VIEW_OFFER_SCREEN_FAIL;
  errMessage: string;
}

export type ViewOfferScreenActions =
  | IViewOfferScreenLoading
  | IViewOfferScreenFetching
  | IViewOfferScreenSuccess
  | IViewOfferScreenFail;

export interface ViewOfferProps {
  data: OfferDetailData
}

export interface OfferDetailProps {
  data: any
}

export interface OfferDestinationsProps {
  data: any
}

export interface OfferAudienceReviewsProps {
  data: any
}

export interface OfferStreamerReviewsProps {
  data: any
}

export interface OfferHashtagsProps {
  data: any
}

export interface OfferStreamerProps {
  data: any 
}