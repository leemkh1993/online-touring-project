import {
  OFFER_SCREEN_FAIL,
  OFFER_SCREEN_FETCHING,
  OFFER_SCREEN_LOADING,
  OFFER_SCREEN_SUCCESS,
} from '../redux/OffersScreen/OfferScreen/action';

export interface OfferData {
  id: number;
  user_id: number;
  name: string;
  description: string;
  description_details: string;
  cost: string;
  journey_pic: string;
  offer_room_key: string;
  expiry_date: number | string;
  created_at: any;
  updated_at: any;
  email: string;
  password: null;
  user_profile_pic: string;
  first_name: string;
  last_name: string;
  age_range_id: number;
  profession: string;
  country_id: number;
  city_id: number;
  credit: string;
  range: string;
  country: string;
  city: string;
  hashtags: Array<string>;
  languages: Array<string>;
}

export interface OfferScreenState {
  isLoading: boolean;
  isFetching: boolean;
  data: OfferData[] | null;
  errMessage: string | null;
}

export interface IOfferScreenLoading {
  type: typeof OFFER_SCREEN_LOADING;
}

export interface IOfferScreenFetching {
  type: typeof OFFER_SCREEN_FETCHING;
}

export interface IOfferScreenSuccess {
  type: typeof OFFER_SCREEN_SUCCESS;
  data: OfferData[];
}

export interface IOfferScreenFail {
  type: typeof OFFER_SCREEN_FAIL;
  errMessage: string;
}

export type OfferScreenActions =
  | IOfferScreenLoading
  | IOfferScreenFetching
  | IOfferScreenSuccess
  | IOfferScreenFail;

export interface EachOfferProps {
  data: OfferData;
}

export interface OfferListProps {
  data: OfferData[];
}
