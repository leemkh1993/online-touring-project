import { PROFILE_SCREEN_FETCHING, PROFILE_SCREEN_FAIL, PROFILE_SCREEN_LOADING, PROFILE_SCREEN_SUCCESS } from "../redux/profile/action";

export interface ProfileData {
    user: any,
    Language: Array<string>,
    offerRecords: Array<any>,
    requestRecords: Array<any>,
    paymentRecords: {
        payeeRecord: Array<any>,
        recipientRecord: Array<any>
    },
    orderRecords: Array<any>,
    streamerReviews: Array<any>,
    audienceReviews: Array<any>
}

export interface ProfileScreenState {
    isLoading: boolean,
    isFetching: boolean,
    data: ProfileData | null,
    errMessage: string
}

export interface IProfile_Screen_Loading {
    type: typeof PROFILE_SCREEN_LOADING
}

export interface IProfile_Screen_Fetching {
    type: typeof PROFILE_SCREEN_FETCHING
}

export interface IProfile_Screen_Success {
    type: typeof PROFILE_SCREEN_SUCCESS,
    data: ProfileData
}

export interface IProfile_Screen_Fail {
    type: typeof PROFILE_SCREEN_FAIL,
    errMessage: string
}

export type ProfileScreenActions = IProfile_Screen_Loading | IProfile_Screen_Fetching | IProfile_Screen_Success | IProfile_Screen_Fail