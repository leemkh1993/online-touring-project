import {
  JITSI_SCREEN_FAIL,
  JITSI_SCREEN_FETCHING,
  JITSI_SCREEN_LOADING,
  JITSI_SCREEN_RESET,
  JITSI_SCREEN_SUCCESS,
} from '../redux/Jitsi/action';

export interface OrderData {
  order: Array<any>;
  offerData: Array<any>;
  destination: Array<any>;
  audiences: Array<any>;
}

export interface JitsiScreenState {
  isLoading: boolean;
  isFetching: boolean;
  data: OrderData | null;
  errMessage: string;
}

export interface IJitsi_Screen_Loading {
  type: typeof JITSI_SCREEN_LOADING;
}

export interface IJitsi_Screen_Fetching {
  type: typeof JITSI_SCREEN_FETCHING;
}

export interface IJitsi_Screen_Success {
  type: typeof JITSI_SCREEN_SUCCESS;
  data: OrderData;
}

export interface IJitsi_Screen_Fail {
  type: typeof JITSI_SCREEN_FAIL;
  errMessage: string;
}

export interface IJitsi_Screen_Reset {
  type: typeof JITSI_SCREEN_RESET;
}

export type JitsiScreenAction =
  | IJitsi_Screen_Loading
  | IJitsi_Screen_Fetching
  | IJitsi_Screen_Success
  | IJitsi_Screen_Fail
  | IJitsi_Screen_Reset;

export interface JitsiProps {
    data: OrderData
}

export interface VideoConferenceProps {
  data: any
}

export interface AudienceListProps {
  data: any
}

export interface MapProps {
  data: any
}

export interface DestinationsProps {
  data: any
}

export interface OrderDetailProps {
  data: any
}

export interface SubmitReviewProps {
  
}