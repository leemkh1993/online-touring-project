import { LOGIN_FAIL, LOGIN_LOADING, LOGIN_SUCCESS, LOGOUT_SUCCESS } from '../redux/auth/action';

export interface AuthState {
    isLoading: boolean,
    isLoggedIn: boolean | null,
    email: string,
    token: string
    errMessage: string
}

export interface ILoginLoading {
    type: typeof LOGIN_LOADING
};

export interface ILoginSuccess {
    type: typeof LOGIN_SUCCESS,
    email: string,
    token: string,
};

export interface ILoginFail {
    type: typeof LOGIN_FAIL,
    errMessage: string
};

export interface ILogoutSuccess {
    type: typeof LOGOUT_SUCCESS
};

export type AuthActions = ILoginLoading | ILoginSuccess | ILoginFail | ILogoutSuccess;

export interface LoginForm  {
    email: string,
    password: string 
}