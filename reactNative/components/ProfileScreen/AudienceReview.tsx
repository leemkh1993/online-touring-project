import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

interface AudienceReviewProps {
    data: any
}

const AudienceReview: React.FC<any> = (props: AudienceReviewProps) => {
    const data = props.data

    return (
        <View style={styles.mainContainer}>
            <Text>
                {JSON.stringify(data)}
            </Text>
        </View>
    );
};

const styles = StyleSheet.create({
    mainContainer: {
        marginVertical: 10
    }
});

export default AudienceReview