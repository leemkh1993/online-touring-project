import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

interface RecipientRecordProps {
    data: any
}

const RecipientRecord: React.FC<any> = (props: RecipientRecordProps) => {
    const data = props.data

    return (
        <View>
            <Text>
                {JSON.stringify(data)}
            </Text>
        </View>
    );
};

const styles = StyleSheet.create({

});

export default RecipientRecord