import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

interface PayeeRecordProps {
    data: any
}

const PayeeRecord: React.FC<any> = (props: PayeeRecordProps) => {
    const data = props.data

    return (
        <View>
            <Text>
                {JSON.stringify(data)}
            </Text>
        </View>
    );
};

const styles = StyleSheet.create({

});

export default PayeeRecord