import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

interface RequestRecordProps {
  data: any;
}

const RequestRecord: React.FC<any> = (props: RequestRecordProps) => {
  const data = props.data;

  return (
    <View>
      <Text>{JSON.stringify(data)}</Text>
    </View>
  );
};

const styles = StyleSheet.create({});

export default RequestRecord;
