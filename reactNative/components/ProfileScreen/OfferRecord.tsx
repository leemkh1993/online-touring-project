import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

interface OfferRecordProps {
    data: any
}

const OfferRecord: React.FC<any> = (props: OfferRecordProps) => {
    const data = props.data

    return (
        <View>
            <Text>
                {JSON.stringify(data)}
            </Text>
        </View>
    );
};

const styles = StyleSheet.create({

});

export default OfferRecord