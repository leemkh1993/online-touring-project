import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

interface OrderRecordProps {
    data: any
}

const OrderRecord: React.FC<any> = (props: OrderRecordProps) => {
    const data = props.data

    return (
        <View>
            <Text>
                {JSON.stringify(data)}
            </Text>
        </View>
    );
};

const styles = StyleSheet.create({

});

export default OrderRecord