import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

interface StreamerReviewProps {
    data: any
}

const StreamerReview: React.FC<any> = (props: StreamerReviewProps) => {
    const data = props.data

    return (
        <View>
            <Text>
                {JSON.stringify(data)}
            </Text>
        </View>
    );
};

const styles = StyleSheet.create({

});

export default StreamerReview