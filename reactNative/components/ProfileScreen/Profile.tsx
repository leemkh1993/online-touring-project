import React from 'react';
import {Dimensions, Image, StyleSheet} from 'react-native';
import {SafeAreaView, View, Text} from 'react-native';
import Config from 'react-native-config';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
import {useDispatch} from 'react-redux';
import {logoutThunk} from '../../redux/auth/thunk';
import {ProfileData} from '../../type/TypeProfileScreen';

const {height} = Dimensions.get('window');

interface ProfileProps {
  data: ProfileData;
}

const Profile: React.FC<any> = (props: ProfileProps) => {
  const data = props.data;
  const dispatch = useDispatch();
  
  const logoutHandler = () => {
    dispatch(logoutThunk());
  };

  return (
    <SafeAreaView>
      <ScrollView style={{backgroundColor: "#ffeacc"}}>
        <View style={styles.biggerContainer}>
          <View style={styles.profileMainContainer}>
            <View style={styles.imageContainer}>
              <Image
                style={styles.image}
                source={{
                  uri: `${Config.BACKEND_DOMAIN}${Config.BACKEND_PORT}/cast_profile_pic/${data.user.profile_pic}`,
                }}
              />
            </View>
            <View style={styles.userDetail}>
              <View style={styles.innerUserDetail}>
                <Text style={styles.label}>User ID: </Text>
                <Text style={styles.text}>{data.user.id}</Text>
              </View>
              <View style={styles.innerUserDetail}>
                <Text style={styles.label}>Email: </Text>
                <Text style={styles.text}>{data.user.email}</Text>
              </View>
              <View style={styles.innerUserDetail}>
                <Text style={styles.label}>First Name: </Text>
                <Text style={styles.text}>{data.user.first_name}</Text>
              </View>
              <View style={styles.innerUserDetail}>
                <Text style={styles.label}>Last Name: </Text>
                <Text style={styles.text}>{data.user.last_name}</Text>
              </View>
              <View style={styles.innerUserDetail}>
                <Text style={styles.label}>Age: </Text>
                <Text style={styles.text}>{data.user.range}</Text>
              </View>
              <View style={styles.innerUserDetail}>
                <Text style={styles.label}>Profession: </Text>
                <Text style={styles.text}>{data.user.profession}</Text>
              </View>
              <View style={styles.innerUserDes}>
                <Text style={styles.label}>Description: </Text>
                <Text style={styles.text1}>{data.user.description}</Text>
              </View>
              <View style={styles.innerUserDetail}>
                <Text style={styles.label}>Country: </Text>
                <Text style={styles.text}>{data.user.country}</Text>
              </View>
              <View style={styles.innerUserDetail}>
                <Text style={styles.label}>City: </Text>
                <Text style={styles.text}>{data.user.city}</Text>
              </View>
            </View>
          </View>
          <TouchableOpacity style={styles.logoutButton} onPress={logoutHandler}>
            <Text style={styles.label}>Logout</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  biggerContainer: {
    backgroundColor: "#ffeacc",
  },

  profileMainContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10,
    marginHorizontal: 10,
    padding: 20,
    borderRadius: 30,
    backgroundColor: '#ffd599',
    shadowColor: "grey",
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5
  },

  innerUserDetail: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    width: '100%',
    marginVertical: 5,
    borderRadius: 30,
    paddingHorizontal: 10,
    backgroundColor: '#ffbf66',
    shadowColor: "grey",
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5
  },

  innerUserDes: {
    justifyContent: 'space-between',
    alignItems: "center",
    flexDirection: 'row',
    width: '100%',
    marginVertical: 5,
    borderRadius: 30,
    paddingHorizontal: 10,
    backgroundColor: '#ffbf66',
    shadowColor: "grey",
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5
  },

  imageContainer: {
    height: height * 0.3,
    width: '100%',
    shadowColor: "grey",
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5
  },

  image: {
    height: '100%',
    width: '100%',
    borderRadius: 30,
  },

  userDetail: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },

  text: {
    margin: 5,
    textAlign: 'center',
    fontSize: 15,
  },

  text1: {
    margin: 5,
    textAlign: 'center',
    fontSize: 15,
    width: "60%"
  },

  label: {
    margin: 5,
    textAlign: 'center',
    fontSize: 15,
    fontWeight: "bold"
  },

  logoutButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffd599',
    height: 50,
    marginVertical: 10,
    marginHorizontal: 10,
    borderRadius: 20,
    shadowColor: "grey",
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5
  },
});

export default Profile;
