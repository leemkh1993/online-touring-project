import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {Dimensions, StyleSheet, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {ViewOfferProps} from '../../../type/TypeViewOfferScreen';
import OfferAudienceReviews from './offerAudienceReviews';
import OfferDestinations from './offerDestinations';
import OfferDetail from './offerDetail';
import OfferHashtags from './offerHashtags';
import OfferStreamer from './offerStreamer';
import OfferStreamerReviews from './offerStreamerReviews';

const {height, width} = Dimensions.get('window');

const ViewOffer: React.FC<any> = (props: ViewOfferProps) => {
  const navigation = useNavigation();
  const data = props.data;

  const bookingHandler = () => {
    navigation.navigate('Booking', {
      data: data,
    });
  };

  return (
    <View style={styles.viewOfferMainContainer}>
      <View style={styles.offerDetailsContainer}>
        <OfferDetail data={data.offer} />
      </View>
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.button} onPress={bookingHandler}>
          <Text style={styles.text}>Book This Offer Now!!!</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.hashtagsContainer}>
        <OfferHashtags data={data.hashtag} />
      </View>
      <View>
        <View style={styles.descriptionContainer}>
          <View style={styles.desMainContainer}>
            <View style={styles.desHeaderContainer}>
              <Text style={styles.desHeader}>Description:</Text>
            </View>
            <View style={styles.desBodyContainer}>
              <Text style={styles.text1}>{data.offer.offer_description}</Text>
            </View>
          </View>
          <View style={styles.desMainContainer}>
            <View style={styles.desHeaderContainer}>
              <Text style={styles.desHeader}>Details:</Text>
            </View>
            <View style={styles.desBodyContainer}>
              <Text style={styles.text1}>
                {data.offer.offer_description_details}
              </Text>
            </View>
          </View>
        </View>
      </View>
      <View style={styles.destinationsContainer}>
        <OfferDestinations data={data.destinations} />
      </View>
      <View style={styles.streamerContainer}>
        <View style={styles.biggerContainer}>
          <OfferStreamer data={data.offer} />
        </View>
      </View>
      <View style={styles.audienceReviewContainer}>
        <View style={styles.biggerContainer}>
          <OfferAudienceReviews data={data.audience_reviews} />
        </View>
      </View>
      <View style={styles.streamerReviewContainer}>
        <View style={styles.biggerContainer}>
          <OfferStreamerReviews data={data.streamerReviews} />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  desHeader: {
    fontSize: 25,
    fontWeight: 'bold',
  },

  desHeaderContainer: {
    marginVertical: 10,
  },

  desBodyContainer: {
    backgroundColor: '#ffbf66',
    width: '90%',
    borderRadius: 30,
    padding: 10,
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  desMainContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10,
    width: '100%',
  },

  biggerContainer: {},

  buttonContainer: {
    padding: 10,
    marginVertical: 5,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#c7c7c7',
    shadowOffset: {width: -5, height: -5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  button: {
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    width: width * 0.8,
    height: height * 0.08,
    backgroundColor: '#ffd599',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  viewOfferMainContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffeacc',
  },

  text: {
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
  },

  text1: {
    textAlign: "center",
  },

  descriptionContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: width * 0.95,
    borderRadius: 30,
    backgroundColor: '#ffd599',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
    marginVertical: 10,
    padding: 10,
  },

  offerDetailsContainer: {
    width: width * 0.95,
    marginVertical: 10,
    shadowColor: '#c7c7c7',
    shadowOffset: {width: -5, height: -5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  hashtagsContainer: {
    width: width * 0.9,
    marginVertical: 10,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#c7c7c7',
    shadowOffset: {width: -5, height: -5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  destinationsContainer: {
    marginVertical: 10,
    shadowColor: '#c7c7c7',
    shadowOffset: {width: -5, height: -5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  streamerContainer: {
    width: width * 0.95,
    marginVertical: 10,
    borderRadius: 30,
    backgroundColor: '#ffd599',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  audienceReviewContainer: {
    width: width * 0.95,
    marginVertical: 15,
    padding: 10,
    borderRadius: 30,
    backgroundColor: '#ffd599',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  streamerReviewContainer: {
    width: width * 0.95,
    marginVertical: 10,
    borderRadius: 30,
    padding: 10,
    backgroundColor: '#ffd599',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },
});

export default ViewOffer;
