import React from 'react';
import {View, Text, StyleSheet, Dimensions} from 'react-native';
import {OfferDestinationsProps} from '../../../type/TypeViewOfferScreen';
import Destinations from '../../JitsiScreen/Destinations';
import GMap from '../../JitsiScreen/Map';

const {height, width} = Dimensions.get('window');

const OfferDestinations: React.FC<any> = (props: OfferDestinationsProps) => {
  const data = props.data;

  return (
    <View style={styles.OfferDestinationsContainer}>
      <View style={styles.biggerContainer}>
        <View style={styles.OfferDestinations}>
          <Destinations data={data}></Destinations>
        </View>
      </View>
      <View style={styles.OfferMap}>
        <GMap data={data} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  biggerContainer: {
    shadowColor: '#c7c7c7',
  },

  anotherBiggerContainer: {

  },

  header: {
    marginBottom: 0,
    fontWeight: 'bold',
    fontSize: 25,
  },

  OfferDestinationsContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },

  innerOfferMap: {
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  OfferDestinations: {
    width: width * 0.95,
    marginVertical: 10,
    marginHorizontal: 10,
    backgroundColor: '#ffd599',
    alignItems: 'center',
    borderRadius: 30,
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  OfferMap: {
    backgroundColor: '#ffd599',
    justifyContent: 'center',
    alignItems: 'center',
    height: height*0.7,
    width: width*0.95,
    marginVertical: 10,
    marginHorizontal: 10,
    borderRadius: 30,
    shadowColor: "grey",
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5
  },
});

export default OfferDestinations;
