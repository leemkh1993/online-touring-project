import React from 'react';
import {View, Text, StyleSheet, Image, Dimensions} from 'react-native';
import Config from 'react-native-config';
import {OfferStreamerProps} from '../../../type/TypeViewOfferScreen';
import EmptyList from '../../MyOrderScreen/EmptyList';

const {height, width} = Dimensions.get('window');

const OfferStreamer: React.FC<any> = (props: OfferStreamerProps) => {
  const data = props.data;

  return (
    <View style={styles.innerOfferStreamer}>
      <View style={{width: "100%", justifyContent: "center", alignItems: "center"}}>
        <Text style={{fontSize: 25, marginBottom: 20, fontWeight: "bold"}}>Your Guide</Text>
      </View>
      <View style={styles.offerDetailContainer}>
        <View style={styles.innerUserDetail}>
          <Text style={styles.label}>First Name: </Text>
          <Text style={styles.text}>{data.guide_first_name}</Text>
        </View>
        <View style={styles.innerUserDetail}>
          <Text style={styles.label}>Last Name: </Text>
          <Text style={styles.text}>{data.guide_last_name}</Text>
        </View>
        <View style={styles.innerUserDetail}>
          <Text style={styles.label}>Country: </Text>
          <Text style={styles.text}>{data.guide_country}</Text>
        </View>
        <View style={styles.innerUserDetail}>
          <Text style={styles.label}>City: </Text>
          <Text style={styles.text}>{data.guide_city}</Text>
        </View>
        <View style={styles.innerUserDetail}>
          <Text style={styles.label}>Age: </Text>
          <Text style={styles.text}>{data.guide_age_range}</Text>
        </View>
        {data.langList[0] &&
          data.langList.map((data: string, idx: number) => (
            <View style={styles.innerUserDetail} key={idx}>
              <Text style={styles.label}>Language {idx + 1}: </Text>
              <Text style={styles.text}>{data}</Text>
            </View>
          ))}
      </View>
      <View style={styles.offerStreamerImageContainer}>
        <Image
          style={styles.image}
          source={{
            uri: `${Config.BACKEND_DOMAIN}${Config.BACKEND_PORT}/cast_profile_pic/${data.guide_profile_pic}`,
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  label: {
    textAlign: 'center',
    fontSize: 15,
    fontWeight: 'bold',
  },

  innerUserDetail: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    width: '100%',
    marginVertical: 5,
    borderRadius: 30,
  },

  innerOfferStreamer: {
    margin: 10,
    paddingVertical: 10,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    flexWrap: "wrap"
  },

  offerDetailContainer: {
    justifyContent: 'space-between',
    alignContent: 'center',
    width: '50%',
  },

  offerStreamerImageContainer: {
    left: 5,
    width: '50%',
    maxHeight: height * 0.2,
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },

  text: {
    textAlign: 'center',
    fontSize: 15,
    fontWeight: 'bold',
  },

  image: {
    borderRadius: 30,
    width: '95%',
    height: '100%',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },
});

export default OfferStreamer;
