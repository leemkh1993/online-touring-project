import moment from 'moment';
import React from 'react';
import {View, Text, StyleSheet, Image, Dimensions} from 'react-native';
import {OfferDetailProps} from '../../../type/TypeViewOfferScreen';
import Config from "react-native-config";

const {height, width} = Dimensions.get('window');

const OfferDetail: React.FC<any> = (props: OfferDetailProps) => {
  const data = props.data;

  const expiryDate = new Date(data.offer_expiry_date).getTime();
  const mo = moment(expiryDate).fromNow();

  return (
    <View style={styles.offerDetailContainer}>
      <View style={styles.innerOfferDetails}>
        <Text style={styles.text}>Offer ID: {data.offer_id}</Text>
        <Text style={styles.header}> {data.offer_name}</Text>
        <Text style={styles.subHeader}> Cost: {data.offer_cost}</Text>
        <Text style={styles.subHeader}>
          Expiring at: {data.offer_expiry_date}
        </Text>
        <Text style={styles.subHeader1}>
          Expiring {mo} !
        </Text>
      </View>
      <View style={styles.offerImageContainer}>
        <Image
          style={styles.image}
          source={{
            uri: `${Config.BACKEND_DOMAIN}${Config.BACKEND_PORT}/cast_vacation_pic/${data.offer_journey_pic}`,
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  offerDetailContainer: {
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    height: height*0.5,
    borderRadius: 30,
    backgroundColor: '#ffd599',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  innerOfferDetails: {
    marginTop: 20,
    width: '90%',
    justifyContent: "space-between"
    
  },

  offerImageContainer: {
    width: '90%',
    height: '45%',
    marginBottom: 10,
    shadowColor: "grey",
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 5,
    shadowRadius: 5
  },

  image: {
    width: '100%',
    height: '100%',
    borderRadius: 30,
  },

  header: {
    fontSize: 25,
    textAlign: 'center',
    fontWeight: "bold"
  },

  subHeader: {
    fontSize: 14,
    textAlign: "center",
    marginVertical: 5
  },

  subHeader1: {
    fontSize: 14,
    textAlign: "center",
    marginVertical: 5,
    fontWeight: "bold"
  },

  text: {
    textAlign: 'center',
    marginVertical: 5
  },
});

export default OfferDetail;
