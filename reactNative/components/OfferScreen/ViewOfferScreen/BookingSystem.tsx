import React, { Dispatch, SetStateAction } from 'react';
import {Dimensions, StyleSheet, Text, View} from 'react-native';
import {DatePicker, List, Provider} from '@ant-design/react-native';
import {useState} from 'react';
import enUS from 'antd-mobile/lib/locale-provider/en_US';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {IntegratedState} from '../../../redux/store';
import {useNavigation} from '@react-navigation/native';
import {BookingSystemActions} from '../../../redux/BookingSystem/types';
import {
  createOrderThunk,
  verifyExistingOrderThunk,
} from '../../../redux/BookingSystem/thunk';
import {
  bookingSystemReset,
  bookingSystemVerifyingFail,
} from '../../../redux/BookingSystem/action';
import { getAllOffersThunk } from '../../../redux/OffersScreen/OfferScreen/thunk';
import { getProfileDataThunk } from '../../../redux/profile/thunk';
import { getMyOrderScreenGuideDataThunk, getMyOrderScreenAudienceDataThunk } from '../../../redux/MyOrderScreen/thunk';

const {width, height} = Dimensions.get('window');

interface OfferBookingSystemProps {
  data: {
    offerID: number;
    cost: string;
  };
}

const BookingSystem: React.FC<any> = (props: OfferBookingSystemProps) => {
  const dispatch = useDispatch<BookingSystemActions | any>();
  const {offerID, cost} = props.data;
  const navigation = useNavigation();
  const userToken = useSelector((state: IntegratedState) => state.auth.token);
  const userEmail = useSelector((state: IntegratedState) => state.auth.email);
  const [startDate, setStartDate]: any = useState(undefined);
  const [endDate, setEndDate]: any = useState(undefined);
  const isLoading = useSelector(
    (state: IntegratedState) => state.bookingSystem.isLoading,
  );
  const isFetching = useSelector(
    (state: IntegratedState) => state.bookingSystem.isFetching,
  );
  const verifyExistingOrder = useSelector(
    (state: IntegratedState) => state.bookingSystem.verifyExistingOrder,
  );
  const orderExistedMessage = useSelector(
    (state: IntegratedState) => state.bookingSystem.orderExistedMessage,
  );
  const orderNotFound = useSelector(
    (state: IntegratedState) => state.bookingSystem.orderNotFound,
  );
  const orderData = useSelector(
    (state: IntegratedState) => state.bookingSystem.orderData,
  );
  const errMessage = useSelector(
    (state: IntegratedState) => state.bookingSystem.errMessage,
  );

  const startDateChangeHandler = (date: Date) => {
    setStartDate(date);
  };

  const endDateChangeHandler = (date: Date) => {
    setEndDate(date);
  };

  const paypalHandler = () => {
    if (!isLoading && !isFetching && startDate) {
      if (difference? difference >= 0: false) {
        dispatch(createOrderThunk(offerID, userToken, cost, startDate, endDate));
      } else {
        dispatch(bookingSystemVerifyingFail('We Cannot Travel Back In Time...Yet!'));
      }
    } else {
      dispatch(bookingSystemVerifyingFail('Start Date Needed!!!'));
    }
  };

  var startDateTime: number | null = null;
  var endDateTime: number | null = null;
  var difference: number | null= null;
  if (startDate) {
    startDateTime = startDate.getTime()
  }

  if (endDate) {
    endDateTime = endDate.getTime()
  }

  if (startDateTime && endDateTime) {
    difference = endDateTime - startDateTime
  }


  useEffect(() => {
    dispatch(verifyExistingOrderThunk(offerID, userToken));

    return () => {
      dispatch(bookingSystemReset());
    };
  }, []);

  useEffect(() => {
    dispatch(getProfileDataThunk(userEmail, userToken));
    dispatch(getMyOrderScreenGuideDataThunk(userToken));
    dispatch(getMyOrderScreenAudienceDataThunk(userToken));
    if (orderData) {
      navigation.navigate('ViewOrder', {
        order_id: orderData[0].id,
      });
    }
  }, [orderData]);

  return (
    <>
      {verifyExistingOrder && (
        <View style={styles.mainContainer}>
          <View style={styles.paypalButtonContainer}>
            <View style={styles.paypalButton}>
              <Text style={styles.text}>Loading...</Text>
            </View>
          </View>
        </View>
      )}
      {orderExistedMessage && (
        <View style={styles.mainContainer}>
          <View style={styles.paypalButtonContainer}>
            <View style={styles.paypalButton}>
              <Text style={styles.text}>See you on {orderExistedMessage}</Text>
            </View>
          </View>
        </View>
      )}
      {orderData && (
        <View style={styles.mainContainer}>
          <View style={styles.paypalButtonContainer}>
            <View style={styles.paypalButton}>
              <Text style={styles.text}>See you on {orderData[0].start_date}</Text>
            </View>
          </View>
        </View>
      )}
      {!orderData && orderNotFound && (
        <View style={styles.mainContainer}>
          <View style={styles.datePickerContainer}>
            <Provider>
              <View style={styles.datePicker}>
                <List>
                  <DatePicker
                    value={startDate}
                    mode="datetime"
                    defaultDate={undefined}
                    minDate={new Date()}
                    maxDate={new Date(2030, 12, 31, 0, 0)}
                    onChange={startDateChangeHandler}
                    format="YYYY-MM-DD HH:mm"
                    locale={enUS.DatePicker}>
                    <List.Item arrow="horizontal">Start Date</List.Item>
                  </DatePicker>
                </List>
              </View>
              <View style={styles.datePicker}>
                <List>
                  <DatePicker
                    value={endDate}
                    mode="datetime"
                    defaultDate={undefined}
                    minDate={new Date()}
                    maxDate={new Date(2030, 12, 31, 0, 0)}
                    onChange={endDateChangeHandler}
                    format="YYYY-MM-DD HH:mm"
                    locale={enUS.DatePicker}>
                    <List.Item arrow="horizontal">End Date</List.Item>
                  </DatePicker>
                </List>
              </View>
            </Provider>
          </View>
          <View style={styles.paypalButtonContainer}>
            <TouchableOpacity
              style={styles.paypalButton}
              onPress={paypalHandler}>
              {errMessage && !isLoading && <Text style={styles.text}>{errMessage}</Text>}
              {isLoading && <Text style={styles.text}>Loading...Please Wait!</Text>}
              {!isLoading && !errMessage && orderNotFound && (
                <Text style={styles.text}>Check Out With Paypal Here</Text>
              )}
              {orderData && <Text style={styles.text}>Payment Success!!! Redirecting...</Text>}
            </TouchableOpacity>
          </View>
        </View>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 15,
    fontWeight: "bold"
  },

  mainContainer: {
    width: '95%',
    shadowColor: "grey",
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
    backgroundColor: '#ffd599',
    padding: 15,
    borderRadius: 30
  },

  datePickerContainer: {
    width: '100%',
    marginBottom: 15,
  },

  datePicker: {
    width: '100%',
    marginVertical: 5,
  },

  paypalButtonContainer: {
    height: height * 0.1,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: "grey",
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5
  },

  paypalButton: {
    height: '100%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    backgroundColor: '#ffbf66',
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
});

export default BookingSystem;
