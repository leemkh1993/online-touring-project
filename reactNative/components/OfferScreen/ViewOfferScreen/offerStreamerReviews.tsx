import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import Config from 'react-native-config';
import {OfferStreamerReviewsProps} from '../../../type/TypeViewOfferScreen';
import EmptyList from '../../MyOrderScreen/EmptyList';

const OfferStreamerReviews: React.FC<any> = (
  props: OfferStreamerReviewsProps,
) => {
  const data = props.data;

  return (
    <View style={styles.offerStreamerReviewsContainer}>
      <View style={styles.anotherBiggerContainer}>
        <Text style={styles.header}>Streamer Reviews</Text>
      </View>
      {!data[0] && <EmptyList />}
      {data[0] &&
        data.map((data: any, idx: number) => (
          <View key={`${idx}`} style={styles.innerContainer}>
            <View style={styles.infoContainer}>
              <Text style={styles.text}>Rating: {data.rating}/5</Text>
              <Text style={styles.text}>{data.comments}</Text>
              <Text style={styles.text}>
                By: {data.audience_first_name} {data.audience_last_name}
              </Text>
            </View>
            <View style={styles.imageContainer}>
              <Image style={styles.image} source={{uri: `${Config.BACKEND_DOMAIN}${Config.BACKEND_PORT}/cast_profile_pic/${data.audience_profile_pic}`}} />
            </View>
          </View>
        ))}
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    fontSize: 25,
    fontWeight: "bold"
  },


  offerStreamerReviewsContainer: {
    width: '100%',
  },

  anotherBiggerContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10
  },

  infoContainer: {
    justifyContent:"center",
    alignItems: "center",
    width: '50%',
    padding: 5
  },

  imageContainer: {
    height: 150,
    width: '50%',
    backgroundColor: "white",
    borderRadius: 30,
  },

  image: {
    borderRadius: 30,
    height: '100%',
    width: '100%'
  },

  text: {
    marginVertical: 5,
    textAlign: "center",
    fontWeight: "bold",
  },

  innerContainer: {
    marginVertical: 10,
    width: '100%',
    flexDirection: "row"
  },
});

export default OfferStreamerReviews;
