import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {OfferHashtagsProps} from '../../../type/TypeViewOfferScreen';
import EmptyList from '../../MyOrderScreen/EmptyList';

const OfferHashtags: React.FC<any> = (props: OfferHashtagsProps) => {
  const data = props.data;

  return (
    <View style={styles.offerHashtagsContainer}>
      {!data[0] && <EmptyList />}
      {data[0] &&
        data.map((data: string, idx: number) => (
          <View key={`hashtag_${idx+1}`} style={styles.hashtagContainer}>
            <View style={styles.innerHashtagContainer}>
              <Text style={styles.text}>{data}</Text>
            </View>
          </View>
        ))}
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontWeight: "bold"
  },

  offerHashtagsContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flexWrap: 'wrap',
    flexDirection: 'row',
    borderRadius: 30,
    paddingVertical: 10,
    paddingHorizontal: 5,
    backgroundColor: '#ffd599',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  hashtagContainer: {
    borderRadius: 5,
    margin: 5,

  },

  innerHashtagContainer:{
    padding: 10,
    marginHorizontal: 10,
    marginVertical: 5,
    backgroundColor: '#ffbf66',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
    borderRadius: 30,
  }
});

export default OfferHashtags;
