import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {View, Text, StyleSheet, Dimensions, Image, Alert} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {EachOfferProps} from '../../type/TypeOfferScreen';
import moment from 'moment';
import Config from 'react-native-config';

const {height, width} = Dimensions.get('window');

const EachOffer: React.FC<any> = (props: EachOfferProps) => {
  const navigation = useNavigation();
  const data = props.data;

  const viewOfferHandler = () => {
    navigation.navigate('ViewOffer', {
      offer_id: data.id,
    });
  };

  const date = new Date(data.expiry_date).getTime();
  const mo = moment(date).fromNow();

  return (
    <View style={styles.biggerContainer}>
      <View style={styles.EachOfferMainContainer}>
        <View style={styles.biggerContainer}>
          <View style={styles.eachOfferInfoContainer}>
            <Text style={styles.header}>{data.name}</Text>
            <Text style={styles.subHeader}>Expiring In {mo} !!!</Text>
          </View>
        </View>
        <View style={styles.biggerContainer}>
          <View style={styles.eachOfferImageContainer}>
            <Image
              style={styles.eachOfferImage}
              source={{
                uri: `${Config.BACKEND_DOMAIN}${Config.BACKEND_PORT}/cast_vacation_pic/${data.journey_pic}`,
              }}
            />
          </View>
        </View>
        <View style={styles.eachOfferGuideInfoContainer}>
          <View style={styles.eachOfferHashtagsContainer}>
            {data.hashtags[0] &&
              data.hashtags.map((data: string, idx: number) => (
                <View key={`hashtag_${idx}`}>
                  <View style={styles.hashtagContainer}>
                    <View style={styles.innerButtonContainer}>
                      <Text style={styles.text}>{data}</Text>
                    </View>
                  </View>
                </View>
              ))}
          </View>
          <View style={styles.eachOfferProfilePicContainer}>
            <Image
              style={styles.eachOfferProfileImage}
              source={{
                uri: `${Config.BACKEND_DOMAIN}${Config.BACKEND_PORT}/cast_profile_pic/${data.user_profile_pic}`,
              }}
            />
          </View>
        </View>
        <View style={styles.buttonContainer}>
          <View style={styles.innerButtonContainer}>
            <TouchableOpacity style={styles.button} onPress={viewOfferHandler}>
              <Text style={styles.text}>View Offer Button</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  innerButtonContainer: {
    shadowColor: '#c7c7c7',
    shadowOffset: {width: -0.5, height: -0.5},
    shadowOpacity: 5,
    shadowRadius: 2,
  },

  biggerContainer: {
    shadowColor: '#c7c7c7',
    shadowOffset: {width: -5, height: -5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  EachOfferMainContainer: {
    height: height * 0.77,
    width: width * 0.9,
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 10,
    borderRadius: 40,
    backgroundColor: '#ffd599',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  eachOfferImageContainer: {
    height: height * 0.3,
    width: width * 0.8,
    bottom: -10,
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  eachOfferImage: {
    borderRadius: 40,
    width: '100%',
    height: '100%',
  },

  eachOfferInfoContainer: {
    width: '90%',
    justifyContent: 'center',
    alignItems: 'center',
    top: 15,
  },

  header: {
    textAlign: 'center',
    fontSize: 25,
    fontWeight: "bold"
  },

  subHeader: {
    top: 7,
    fontSize: 13,
  },

  eachOfferHashtagsContainer: {
    justifyContent: 'space-evenly',
    alignItems: 'center',
    flexDirection: 'column',
    height: '100%',
    borderRadius: 30,
  },

  eachOfferGuideInfoContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: "center",
    width: '100%',
    height: '25%',
    bottom: -10,
  },

  eachOfferGuideInfo: {
    marginLeft: 20,
    backgroundColor: 'white',
  },

  hashtagContainer: {
    backgroundColor: '#ffbf66',
    borderRadius: 10,
    margin: 5,
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  buttonContainer: {
    backgroundColor: '#ffca80',
    height: '13%',
    width: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
    borderBottomLeftRadius: 40,
    borderBottomRightRadius: 40,
  },

  eachOfferProfilePicContainer: {
    width: '50%',
    height: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
    shadowColor: 'grey',
    shadowOffset: {width: 6, height: 6},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  eachOfferProfileImage: {
    borderRadius: 90,
    width: '90%',
    height: '100%',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  text: {
    // backgroundColor: "white",
    borderRadius: 5,
    padding: 5,
    fontSize: 15,
    fontWeight: 'bold',
    textAlign: 'center',
    // marginVertical: 10,
    marginHorizontal: 1,
  },

  button: {
    justifyContent: 'center',
    alignItems: 'center',
    width: width * 0.7,
    height: '80%',
    backgroundColor: '#ffaa33',
    borderRadius: 30,
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },
});

export default EachOffer;
