import React, { useEffect } from 'react';
import {StyleSheet, View} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {SafeAreaView} from 'react-native-safe-area-context';
import {OfferData, OfferListProps} from '../../type/TypeOfferScreen';
import EmptyList from '../MyOrderScreen/EmptyList';
import EachOffer from './EachOffer';

const OfferList: React.FC<any> = (props: OfferListProps) => {
  let data: OfferData[] | null = props.data;

  useEffect(() => {
    return () => {
      data = null
    }
  })

  return (
    <ScrollView>
      <View style={styles.offerListContainer}>
        {!data[0] && <EmptyList />}
        {data[0] &&
          data.map((data: OfferData, idx: number) => (
            <EachOffer key={`offer_${idx + 1}`} data={data} />
          ))}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  offerListContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#ffeacc",
  },
});

export default OfferList;
