import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const EmptyList: React.FC = () => {
  return (
    <View style={styles.mainContainer}>
      <View style={styles.inner}>
        <View>
          <Text style={{fontSize: 30}}>This is An Empty List</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: 'blue',
    marginTop: 220,
  },

  inner: {
      height: 100
  },
});

export default EmptyList;
