import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {Dimensions, Image, StyleSheet, Text, View} from 'react-native';
import Config from 'react-native-config';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {OrderProps} from '../../type/TypeMyOrderScreen';
import moment from 'moment';

const {height, width} = Dimensions.get('window');

const Order: React.FC<any> = (props: OrderProps) => {
  const navigation = useNavigation();

  const viewOrderHandler = () => {
    navigation.navigate('ViewOrder', {
      order_id: props.data.order_id,
    });
  };

  const beginJourneyHandler = () => {
    navigation.navigate('JitsiScreen', {
      order_id: props.data.order_id,
    });
  };

  const time = new Date(props.data.order_start_date).getTime();
  const difference = time - new Date().getTime();
  const mo = moment(time).fromNow();

  return (
    <View style={styles.orderContainer}>
      <View style={styles.orderDetails}>
        <View style={styles.innerOrderDetails}>
          <Text style={styles.text}>Order ID: {props.data.order_id}</Text>
          <Text style={styles.text}>Start: {props.data.order_start_date}</Text>
          <Text style={styles.text}>End: {props.data.order_end_date}</Text>
        </View>
        <View style={styles.imageContainer}>
          <Image
            style={styles.image}
            source={{
              uri: `${Config.BACKEND_DOMAIN}${Config.BACKEND_PORT}/cast_vacation_pic/${props.data.journey_pic}`,
            }}></Image>
        </View>
      </View>
      <View style={styles.audienceDetails}>
        <View style={styles.innerAudienceDetails}>
          <Text style={styles.text}>
            First Name: {props.data.audience_first_name}
          </Text>
          <Text style={styles.text}>
            Last Name: {props.data.audience_last_name}
          </Text>
          <Text style={styles.text}>
            Payment Record ID: {props.data.payment_record_id}
          </Text>
          <Text style={styles.text}>
            Paypal Reference: {props.data.paypal_id}
          </Text>
        </View>
        <View style={styles.audienceImageContainer}>
          <Image
            style={styles.image}
            source={{
              uri: `${Config.BACKEND_DOMAIN}${Config.BACKEND_PORT}/cast_profile_pic/${props.data.audience_profile_pic}`,
            }}></Image>
        </View>
      </View>
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.button} onPress={viewOrderHandler}>
          <Text>View Order</Text>
        </TouchableOpacity>
        {difference >= 1800000 && (
          <TouchableOpacity style={styles.button}>
            <Text>Begin {mo}</Text>
          </TouchableOpacity>
        )}
        {difference <= 1800000 && (
          <TouchableOpacity style={styles.button} onPress={beginJourneyHandler}>
            <Text>Begin {mo}</Text>
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  orderContainer: {
    height: height * 0.6,
    width: width * 0.95,
    marginVertical: 15,
    justifyContent: 'space-between',
    borderRadius: 30,
    backgroundColor: '#ffd599',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
    alignItems: 'center',
  },

  orderDetailsContainer: {
    width: '90%',
    padding: 10,
    justifyContent: 'flex-start',
  },

  orderDetails: {
    flexDirection: 'row',
    marginBottom: 5,
    height: '40%',
    width: '90%',
    backgroundColor: '#ffbf66',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
    justifyContent: 'space-between',
    padding: 5,
    borderRadius: 30,
    marginTop: 20,
  },

  innerOrderDetails: {
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '50%',
  },

  imageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '45%',
    borderRadius: 50,
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  image: {
    borderRadius: 100,
    width: '90%',
    height: '70%',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  innerAudienceDetails: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '50%',
  },

  audienceImageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '45%',
    borderRadius: 50,
    maxHeight: height * 0.24,
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  text: {
    textAlign: 'center',
    marginVertical: 5,
    fontWeight: 'bold',
  },

  audienceDetails: {
    flexDirection: 'row-reverse',
    marginTop: 5,
    marginBottom: 10,
    height: '40%',
    width: '90%',
    backgroundColor: '#ffbf66',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
    borderRadius: 30,
    padding: 5,
    justifyContent: 'space-between',
  },

  word: {
    color: 'white',
  },

  buttonContainer: {
    borderTopRightRadius: 0,
    borderBottomRightRadius: 30,
    borderBottomLeftRadius: 30,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    height: '15%',
    backgroundColor: '#ffca80',
    width: '100%',
  },

  button: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    height: 40,
    width: 140,
    backgroundColor: '#ffaa33',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },
});

export default Order;
