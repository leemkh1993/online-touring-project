import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {MapProps} from '../../type/TypeJitsiScreen';
import MapView, {LatLng, Marker} from 'react-native-maps';

const GMap: React.FC<any> = (props: MapProps) => {
  let data = props.data;
  const markersList: LatLng[] = [];
  for (let destination of data) {
    const lat = Number(parseFloat(destination.latitude));
    const lng = Number(parseFloat(destination.longitude));
    const latLng = {
      latitude: lat,
      longitude: lng,
    };
    markersList.push(latLng);
  }

  let [theMap, setTheMap] = useState<MapView | null>(null);

  const mapReadyHandler = () => {
    theMap?.fitToCoordinates(markersList, {
      edgePadding: {
        top: 100,
        right: 100,
        bottom: 100,
        left: 100,
      },
      animated: true,
    });
  };

  useEffect((): any => {
    return () => {
      return (data = null);
    };
  }, []);

  return (
    <View style={styles.mapMainContainer}>
      <View style={styles.headerContainer}>
        <Text style={styles.header}>Click On Marker!</Text>
      </View>
      <View style={styles.mapContainer}>
        <MapView
          style={styles.map}
          ref={(ref) => setTheMap(ref)}
          provider={'google'}
          showsMyLocationButton
          showsCompass
          followsUserLocation
          showsUserLocation
          onMapReady={mapReadyHandler}>
          {data[0] &&
            data.map((data: any, idx: number) => {
              const latitude = Number(parseFloat(data.latitude));
              const longitude = Number(parseFloat(data.longitude));
              const latLng: LatLng = {
                latitude: latitude,
                longitude: longitude,
              };
              return (
                <Marker
                  key={idx}
                  coordinate={latLng}
                  title={data.site_name ? data.site_name : `Place ${idx}`}
                  description={
                    data.description ? data.description : `Surprise!!`
                  }
                />
              );
            })}
        </MapView>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  mapContainer: {
    height: '90%',
    width: '100%',
    shadowColor: "grey",
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5
  },

  mapMainContainer: {
    height: '100%',
    width: '100%',
    flexDirection: 'column',
    justifyContent: "space-between",
    padding: 20
  },

  headerContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '10%',
    marginBottom: 5
  },

  map: {
    height: '100%',
    width: '100%',
  },

  header: {
    fontWeight: 'bold',
    fontSize: 30,
  },
});

export default GMap;
