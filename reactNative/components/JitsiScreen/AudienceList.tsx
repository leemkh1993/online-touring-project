import React from 'react';
import {View, Text, StyleSheet, Dimensions, Image} from 'react-native';
import Config from 'react-native-config';
import {AudienceListProps} from '../../type/TypeJitsiScreen';

const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;

const AudienceList: React.FC<any> = (props: AudienceListProps) => {
  const audienceData = props.data;

  return (
    <>
      <Text style={styles.audienceHeader}>To Be Joining Us ...</Text>
      {audienceData[0] &&
        audienceData.map((data: any, idx: any) => (
          <View key={data.audience_id} style={styles.audienceContainer}>
            <View style={styles.audienceDetails}>
              <View style={styles.bigContainer}>
                <Text style={styles.text}>Audience: </Text>
                <Text style={styles.text}>{idx + 1}</Text>
              </View>
              <View style={styles.bigContainer}>
                <Text style={styles.text}>First Name: </Text>
                <Text style={styles.text}>{data.audience_first_name}</Text>
              </View>
              <View style={styles.bigContainer}>
                <Text style={styles.text}>Last Name: </Text>
                <Text style={styles.text}>{data.audience_last_name}</Text>
              </View>
              <View style={{width: "100%"}}>
                {data.Languages[0] &&
                  data.Languages.map((data: any, idx: any) => (
                    <View key={idx} style={styles.bigContainer}>
                      <Text  style={styles.text}>
                        Language {idx + 1}:
                      </Text>
                      <Text  style={styles.text}>
                        {data}
                      </Text>
                    </View>
                  ))}
              </View>
            </View>
            <View style={styles.imageContainer}>
              <Image
                style={styles.profilePic}
                source={{
                  uri: `${Config.BACKEND_DOMAIN}${Config.BACKEND_PORT}/cast_profile_pic/${data.profile_pic}`,
                }}></Image>
            </View>
          </View>
        ))}
    </>
  );
};

const styles = StyleSheet.create({
  audienceHeader: {
    marginTop: 30,
    fontWeight: 'bold',
    fontSize: 30,
  },

  bigContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "stretch",
    width: "100%"
  },

  audienceContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '90%',
    height: 150,
    marginVertical: 10,
    borderRadius: 30,
    backgroundColor: '#ffbf66',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  text: {
    textAlign: 'center',
    fontWeight: "bold"
  },

  audienceDetails: {
    height: screenHeight * 0.15,
    width: screenWidth * 0.42,
    justifyContent: 'space-between',
    alignItems: 'center',
    marginLeft: screenWidth * 0.05,
  },

  imageContainer: {
    width: screenWidth * 0.3,
    height: screenHeight * 0.15,
    marginRight: 20,
  },

  profilePic: {
    height: '100%',
    width: '100%',
    borderRadius: 30,
  },
});

export default AudienceList;
