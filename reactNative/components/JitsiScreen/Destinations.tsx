import React from 'react';
import {View, Text, StyleSheet, Image, Dimensions} from 'react-native';
import Config from 'react-native-config';
import {DestinationsProps} from '../../type/TypeJitsiScreen';

const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;

const Destinations: React.FC<any> = (props: DestinationsProps) => {
  const destinationData = props.data;

  return (
    <>
      <Text style={styles.header}>Destinations</Text>
      {destinationData[0] &&
        destinationData.map((data: any, idx: any) => (
          <View key={`destination_${idx}`} style={styles.biggerContainer}>
            <View
              style={styles.destinationContainer}>
              <View style={styles.destinationDetails}>
                <Text style={styles.destinationText}>Place {idx + 1}</Text>
                <Text style={styles.destinationText}>{data.country}</Text>
                <Text style={styles.destinationText}>{data.city}</Text>
                <Text style={styles.destinationText}>{data.site_name}</Text>
              </View>
              <View style={styles.destinationImageContainer}>
                <Image
                  style={styles.destinationImage}
                  source={{
                    uri: `${Config.BACKEND_DOMAIN}${Config.BACKEND_PORT}/cast_sites_pic/${data.site_pic}`,
                  }}></Image>
              </View>
            </View>
          </View>
        ))}
    </>
  );
};

const styles = StyleSheet.create({
  biggerContainer: {

  },

  header: {
    marginTop: 20,
    fontWeight: 'bold',
    fontSize: 30,
  },

  destinationContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '90%',
    padding: 10,
    marginVertical: 10,
    borderRadius: 30,
    backgroundColor: '#ffbf66',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  destinationDetails: {
    height: screenHeight * 0.15,
    width: screenWidth * 0.4,
    justifyContent: 'space-between',
    alignItems: 'center',
    marginLeft: screenWidth * 0.05,
  },

  destinationText: {
    textAlign: 'center',
    fontSize: 15,
    fontWeight: "bold"
  },

  destinationImageContainer: {
    backgroundColor: 'blue',
    width: screenWidth * 0.3,
    height: screenHeight * 0.15,
    marginRight: 20,
    borderRadius: 30,
    shadowColor: 'grey',
    shadowOffset: {width: 3, height: 3},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  destinationImage: {
    height: '100%',
    width: '100%',
    borderRadius: 10,
  },
});

export default Destinations;
