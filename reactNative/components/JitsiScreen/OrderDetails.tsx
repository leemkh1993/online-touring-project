import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {OrderDetailProps} from '../../type/TypeJitsiScreen';

const OrderDetails: React.FC<any> = (props: OrderDetailProps) => {
  const orderData = props.data.orderData;
  const offerData = props.data.offerData;

  return (
    <View style={styles.container}>
      <Text style={styles.orderDetailsTitle}>Order Summary</Text>
      <Text style={styles.orderID}>Order ID: {orderData.order_id}</Text>
      <Text style={styles.orderDate}>Start: {orderData.order_start_date}</Text>
      <Text style={styles.orderDate}>End: {orderData.order_end_date}</Text>
      <Text style={styles.eventName}>Event Name: {offerData.offer_name}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    alignItems: 'center',
  },

  orderDetailsTitle: {
    marginTop: 10,
    fontWeight: "bold",
    fontSize: 25
  },
  orderID: {
    marginVertical: 5,
    fontWeight: "bold",
    fontSize: 12
  },

  orderDate: {
    marginVertical: 5,
    fontWeight: "bold",
    fontSize: 12
  },

  eventName: {
    marginBottom: 10,
    fontWeight: "bold",
    fontSize: 12
  }
});

export default OrderDetails;
