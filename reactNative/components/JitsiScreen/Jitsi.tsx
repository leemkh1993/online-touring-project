import React, {useEffect, useRef, useState} from 'react';
import {View, Text, StyleSheet, Dimensions, Animated} from 'react-native';
import {JitsiProps} from '../../type/TypeJitsiScreen';
import VideoConference from './VideoConferenc';
import {TouchableOpacity} from 'react-native-gesture-handler';
import AudienceList from './AudienceList';
import GMap from './Map';
import Destinations from './Destinations';
import OrderDetails from './OrderDetails';
import {useDispatch, useSelector} from 'react-redux';
import {IntegratedState} from '../../redux/store';
import {videoConferenceReset} from '../../redux/Jitsi/VideoConference/action';

const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;

const Jitsi: React.FC<any> = (props: JitsiProps) => {
  const dispatch = useDispatch();
  const orderData: any = props.data.order;
  const offerData: any = props.data.offerData;
  const destinationData: any = props.data.destination;
  const audienceData: any = props.data.audiences;
  const [isReady, setIsReady] = useState(false);
  const videoEnded = useSelector(
    (state: IntegratedState) => state.videoConference.videoEnded,
  );

  const heightAnim = useRef(new Animated.Value(screenHeight * 0.2)).current;

  const changeHeight = () => {
    Animated.timing(heightAnim, {
      toValue: screenHeight * 0.7,
      duration: 1000,
      useNativeDriver: false,
    } as any).start();
  };

  const readyButton = () => {
    changeHeight();
    setIsReady(!isReady);
  };

  useEffect(() => {
    return () => {
      dispatch(videoConferenceReset());
    };
  }, []);

  return (
    <View style={styles.mainContainer}>
      <View style={styles.orderDetailContainer}>
        <OrderDetails data={{orderData: orderData, offerData: offerData}} />
      </View>
      {!videoEnded && (
        <Animated.View style={[styles.jitsiContainer, {height: heightAnim}]}>
          {!isReady && (
            <TouchableOpacity style={styles.readyButton} onPress={readyButton}>
              <Text style={styles.text}>Are you ready?</Text>
            </TouchableOpacity>
          )}
          {isReady && <VideoConference data={props.data} />}
        </Animated.View>
      )}
      {videoEnded && (
        <View style={[styles.jitsiContainer, {height: screenHeight*0.2}]}>
          <View style={styles.readyButton}>
            <Text style={styles.text}>
              Thank You For Coming!!
            </Text>
          </View>
        </View>
      )}
      <View style={styles.destinationContainer}>
        <Destinations data={destinationData} />
      </View>
      <View style={styles.mapContainer}>
        <GMap data={destinationData} />
      </View>
      <View style={styles.audienceContainer}>
        <AudienceList data={audienceData} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {},

  text: {
    margin: 5,
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
  },

  orderDetailContainer: {
    alignItems: 'center',
    width: screenWidth * 0.95,
    marginVertical: 10,
    marginHorizontal: 10,
    borderRadius: 30,
    backgroundColor: '#ffd599',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  jitsiContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: screenWidth * 0.95,
    backgroundColor: '#ffd599',
    borderRadius: 30,
    padding: 10,
    marginVertical: 10,
    marginHorizontal: 10,
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  readyButton: {
    paddingVertical: 30,
    paddingHorizontal: 80,
    backgroundColor: '#ffbf66',

    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
    borderRadius: 30,
  },

  destinationContainer: {
    width: screenWidth * 0.95,
    marginVertical: 10,
    marginHorizontal: 10,
    alignItems: 'center',
    borderRadius: 30,
    backgroundColor: '#ffd599',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  mapContainer: {
    height: screenHeight * 0.5,
    width: screenWidth * 0.95,
    marginVertical: 10,
    marginHorizontal: 10,
    backgroundColor: '#ffd599',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
    borderRadius: 30,
  },

  audienceContainer: {
    justifyContent: 'space-around',
    alignItems: 'center',
    width: screenWidth * 0.95,
    marginVertical: 10,
    marginHorizontal: 10,
    borderRadius: 30,
    backgroundColor: '#ffd599',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },
});

export default Jitsi;
