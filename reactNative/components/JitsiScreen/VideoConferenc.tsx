import React, {useEffect} from 'react';
import {StyleSheet, View} from 'react-native';
import {VideoConferenceProps} from '../../type/TypeJitsiScreen';
//@ts-ignore
import JitsiMeet, {JitsiMeetView} from 'react-native-jitsi-meet/index.ios';
import {useDispatch, useSelector} from 'react-redux';
import {IntegratedState} from '../../redux/store';
import { useNavigation } from '@react-navigation/native';
import { fulfilled } from '../../redux/Jitsi/thunk';
import { getMyOrderScreenGuideDataThunk, getMyOrderScreenAudienceDataThunk } from '../../redux/MyOrderScreen/thunk';
import { videoConferenceEnded } from '../../redux/Jitsi/VideoConference/action';

const VideoConference: React.FC<any> = (props: VideoConferenceProps) => {
  const dispatch = useDispatch()
  const navigation = useNavigation();
  const data = props.data;
  const profileData = useSelector(
    (state: IntegratedState) => state.profileScreen.data,
  );
  const profileEmail = useSelector((state: IntegratedState) => state.auth.email);
  const userToken = useSelector((state: IntegratedState) =>  state.auth.token);

  useEffect(() => {
    const roomKey = data.offerData.offer_room_key;
    const url = `https://meet.jit.si/${roomKey}`;
    const userInfo = {
      displayName: profileData?.user.first_name,
      email: profileData?.user.email,
    };
    JitsiMeet.call(url, userInfo);
  }, []);

  useEffect(() => {
    return () => {
      JitsiMeet.endCall();
    };
  });

  function onConferenceTerminated(nativeEvent: any) {
    /* Conference terminated event */
    dispatch(videoConferenceEnded())
    dispatch(fulfilled(data.order.order_id, userToken))
    dispatch(getMyOrderScreenGuideDataThunk(userToken));
    dispatch(getMyOrderScreenAudienceDataThunk(userToken));
    if (profileEmail != data.order.guide_email) {
      navigation.navigate("SubmitReview", {
        data: data
      })
    }
  }

  return (
    <View style={styles.VideoContainer}>
      <JitsiMeetView
        style={styles.Jitsi}
        onConferenceTerminated={onConferenceTerminated}
        onConferenceWillJoin={(event: any) => console.log("click")}
        onConferenceJoined={(event: any) => console.log("click2")} 
      />
    </View>
  );
};

const styles = StyleSheet.create({
  VideoContainer: {
    height: '95%',
    width: '95%',
  },
  Jitsi: {
    flex: 1,
    height: '100%',
    width: '100%',
  },
});

export default VideoConference;
