import React from 'react';
import {Dimensions, Image, StyleSheet, Text, View} from 'react-native';
import Config from 'react-native-config';
import {ViewOrderProps} from '../../redux/ViewOrder/types';
import Destinations from '../JitsiScreen/Destinations';
import GMap from '../JitsiScreen/Map';
import OrderDetails from '../JitsiScreen/OrderDetails';

const {height, width} = Dimensions.get('window');

const ViewOrder: React.FC<any> = (props: ViewOrderProps) => {
  const orderData = props.data;

  return (
    <View style={styles.viewOrderMainContainer}>
      <View style={styles.viewOrderOrderDataContainer}>
        <OrderDetails
          data={{
            orderData: orderData.order,
            offerData: orderData.offerData,
          }}></OrderDetails>
      </View>

      <View style={styles.viewOrderOfferDataContainer}>
        <View style={styles.viewOrderOfferDataImageContainer}>
          <Image
            style={styles.viewOrderOfferDataImage}
            source={{
              uri: `${Config.BACKEND_DOMAIN}${Config.BACKEND_PORT}/cast_vacation_pic/${orderData.offerData.offer_journey_pic}`,
            }}></Image>
        </View>
        <View style={styles.viewOrderOfferDataDetailContainer}>
          <View style={styles.innerPaymentDetail}>
            <Text style={styles.label}>Offer ID: </Text>
            <Text style={styles.text}>{orderData.offerData.offer_id}</Text>
          </View>
          <View style={styles.innerPaymentDetail}>
            <Text style={styles.label}>Name: </Text>
            <Text style={styles.text}>{orderData.offerData.offer_name}</Text>
          </View>
        </View>
      </View>
      
      <View style={styles.viewOrderPaymentRecordContainer}>
        <View style={styles.innerPaymentDetail}>
          <Text style={styles.label}>Payment Record ID: </Text>
          <Text style={styles.text}>{orderData.order.payment_record_id}</Text>
        </View>
        <View style={styles.innerPaymentDetail}>
          <Text style={styles.label}>Payment Amount: </Text>
          <Text style={styles.text}>
            {orderData.order.payment_record_amount}
          </Text>
        </View>
        <View style={styles.innerPaymentDetail}>
          <Text style={styles.label}>Paypal ID: </Text>
          <Text style={styles.text1}>{orderData.order.paypal_id}</Text>
        </View>
        <View style={styles.innerPaymentDetail}>
          <Text style={styles.label}>Recipient: </Text>
          <Text style={styles.text}>
            {orderData.order.guide_first_name} {orderData.order.guide_last_name}
          </Text>
        </View>
        <View style={styles.innerPaymentDetail}>
          <Text style={styles.label}>Payee: </Text>
          <Text style={styles.text}>
            {orderData.orderer.first_name} {orderData.orderer.last_name}
          </Text>
        </View>
      </View>

      <View style={streamerStyle.MainContainer}>
        <View style={streamerStyle.innerOfferStreamer}>
          <View
            style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: 25, marginBottom: 20, fontWeight: 'bold'}}>
              Your Guide
            </Text>
          </View>
          <View style={streamerStyle.offerDetailContainer}>
            <View style={streamerStyle.innerUserDetail}>
              <Text style={streamerStyle.label}>First Name: </Text>
              <Text style={streamerStyle.text}>
                {orderData.orderer.first_name}
              </Text>
            </View>
            <View style={streamerStyle.innerUserDetail}>
              <Text style={streamerStyle.label}>Last Name: </Text>
              <Text style={streamerStyle.text}>
                {orderData.orderer.last_name}
              </Text>
            </View>
            <View style={streamerStyle.innerUserDetail}>
              <Text style={streamerStyle.label}>Country: </Text>
              <Text style={streamerStyle.text}>
                {orderData.orderer.country ? orderData.orderer.country : ''}
              </Text>
            </View>
            <View style={streamerStyle.innerUserDetail}>
              <Text style={streamerStyle.label}>City: </Text>
              <Text style={streamerStyle.text}>
                {orderData.orderer.city ? orderData.orderer.city : ''}
              </Text>
            </View>
            {orderData.ordererLang[0] &&
              orderData.ordererLang.map((data: string, idx: number) => (
                <View style={streamerStyle.innerUserDetail} key={idx}>
                  <Text style={streamerStyle.label}>Language {idx + 1}: </Text>
                  <Text style={streamerStyle.text}>{data}</Text>
                </View>
              ))}
          </View>
          <View style={streamerStyle.offerStreamerImageContainer}>
            <Image
              style={streamerStyle.image}
              source={{
                uri: `${Config.BACKEND_DOMAIN}${Config.BACKEND_PORT}/cast_profile_pic/${orderData.orderer.profile_pic}`,
              }}
            />
          </View>
        </View>
      </View>

      <View style={styles.viewOrderDestinationContainer}>
        <View style={styles.destinations}>
          <Destinations data={orderData.destination} />
        </View>
        <View style={styles.map}>
          <GMap data={orderData.destination}></GMap>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  viewOrderMainContainer: {},

  viewOrderOrderDataContainer: {
    alignItems: 'center',
    width: width * 0.95,
    marginVertical: 10,
    marginHorizontal: 10,
    borderRadius: 30,
    backgroundColor: '#ffd599',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  viewOrderOfferDataContainer: {
    alignItems: 'center',
    marginHorizontal: 10,
    marginVertical: 10,
    borderRadius: 30,
    backgroundColor: '#ffd599',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  viewOrderOfferDataImageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    height: height * 0.3,
    width: '100%',
    shadowColor: "grey",
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5
  },

  viewOrderOfferDataImage: {
    height: '90%',
    width: '90%',
    borderRadius: 30,
  },

  viewOrderPaymentRecordContainer: {
    alignItems: 'center',
    backgroundColor: '#ffd599',
    padding: 10,
    marginHorizontal: 10,
    marginVertical: 10,
    borderRadius: 30,
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  viewOrderAudienceDataContainer: {
    flexDirection: 'row',
    marginHorizontal: 10,
    marginVertical: 10,
    height: height * 0.3,
    width: width * 0.95,
    borderRadius: 30,
    backgroundColor: '#ffd599',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  innerAudienceDetails: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '50%',
  },

  text: {
    textAlign: 'center',
    marginVertical: 5,
  },

  text1: {
    textAlign: 'center',
    marginVertical: 5,
    width: "68%"
  },

  audienceImageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '50%',
    backgroundColor: 'grey',
    borderRadius: 30,
  },

  image: {
    borderRadius: 100,
    width: '90%',
    height: '70%',
  },

  viewOrderDestinationContainer: {
    shadowColor: '#c7c7c7',
    shadowOffset: {width: -5, height: -5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  destinations: {
    width: width * 0.95,
    marginVertical: 10,
    marginHorizontal: 10,
    backgroundColor: '#ffd599',
    alignItems: 'center',
    borderRadius: 30,
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  map: {
    backgroundColor: '#ffd599',
    justifyContent: 'center',
    alignItems: 'center',
    height: height * 0.7,
    width: width * 0.95,
    marginVertical: 10,
    marginHorizontal: 10,
    borderRadius: 30,
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  label: {
    margin: 5,
    textAlign: 'center',
    fontSize: 15,
    fontWeight: 'bold',
  },

  innerPaymentDetail: {
    justifyContent: 'space-between',
    alignItems: "center",
    flexDirection: 'row',
    backgroundColor: '#ffbf66',
    width: '95%',
    marginVertical: 5,
    borderRadius: 30,
    paddingHorizontal: 10,
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },
  viewOrderOfferDataDetailContainer: {
    marginBottom: 20,
    width: "100%",
    alignItems: "center",
    justifyContent: "center"
  },
});

const streamerStyle = StyleSheet.create({
  label: {
    textAlign: 'center',
    fontSize: 15,
    fontWeight: 'bold',
  },

  innerUserDetail: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    width: '100%',
    marginVertical: 5,
    borderRadius: 30,
  },

  innerOfferStreamer: {
    margin: 10,
    paddingVertical: 10,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    flexWrap: 'wrap',
  },

  offerDetailContainer: {
    justifyContent: 'space-between',
    alignContent: 'center',
    width: '50%',
  },

  offerStreamerImageContainer: {
    left: 5,
    width: '50%',
    maxHeight: height * 0.2,
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },

  text: {
    textAlign: 'center',
    fontSize: 15,
    fontWeight: 'bold',
  },

  text1: {
    textAlign: 'center',
    fontSize: 15,
    fontWeight: 'bold',
  },

  image: {
    borderRadius: 30,
    width: '95%',
    height: '100%',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  MainContainer: {
    width: width * 0.95,
    marginVertical: 10,
    borderRadius: 30,
    backgroundColor: '#ffd599',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
    left: 9,
  },
});

export default ViewOrder;
