import React from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {useDispatch} from 'react-redux';
import {loginThunk} from '../../redux/auth/thunk';
import {Controller, useForm} from 'react-hook-form';
import {LoginForm} from '../../type/TypeLoginScreen';

const Form: React.FC = () => {
  const dispatch = useDispatch();

  const {control, handleSubmit, errors} = useForm<LoginForm>();

  const submitHandler = (data: LoginForm) => {
    dispatch(loginThunk(data.email, data.password));
  };

  return (
    <View style={{alignItems: "center"}}>
      <Text style={styles.labels}>Email</Text>
      <Controller
        name="email"
        control={control}
        defaultValue="marco@gmail.com"
        rules={{required: true}}
        render={({onChange, onBlur, value}) => (
          <TextInput
            style={styles.inputArea}
            placeholder="Type in your email"
            onBlur={onBlur}
            onChangeText={(value) => onChange(value)}
            value={value}
            autoCapitalize="none"
          />
        )}
      />
      {errors.email && <Text>Please Enter Your Email!</Text>}

      <Text style={styles.labels}>Password</Text>
      <Controller
        name="password"
        control={control}
        defaultValue="123"
        rules={{required: true}}
        render={({onChange, onBlur, value}) => (
          <TextInput
            style={styles.inputArea}
            placeholder="Type In Your Password"
            onBlur={onBlur}
            secureTextEntry={true}
            onChangeText={(value) => onChange(value)}
            value={value}
            autoCapitalize="none"
          />
        )}
      />
      {errors.password && <Text>Please Enter Your Password!</Text>}

      <TouchableOpacity
        style={styles.submitButton}
        onPress={handleSubmit(submitHandler)}>
        <Text style={styles.labels}>Login</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  inputArea: {
    marginHorizontal: 20,
    paddingHorizontal: 20,
    paddingVertical: 0,
    marginVertical: 20,
    height: 50,
    backgroundColor: 'white',
    borderRadius: 10,
    fontSize: 23,
    shadowColor: "grey",
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
    width: "90%",
  },

  labels: {
    marginHorizontal: 30,
    color: 'black',
    fontWeight: '900',
    fontSize: 20
  },

  submitButton: {
    marginTop: 10,
    backgroundColor: 'white',
    height: 50,
    width: "90%",
    marginHorizontal: 20,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: "grey",
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5
  },
});

export default Form;
