import React from 'react';
import {View, StyleSheet, Image} from 'react-native';
import Config from 'react-native-config';

const Header: React.FC = () => {
  return (
    <View style={styles.loginHeaderContainer}>
      <Image style={styles.image} source={{uri: `${Config.BACKEND_DOMAIN}${Config.BACKEND_PORT}/cast_profile_pic/logo.png`}}></Image>
    </View>
  );
};

const styles = StyleSheet.create({
  loginHeaderContainer: {
    justifyContent: 'center',
    alignItems: "center"
  },

  image: {
    width: 250,
    height: 250,
  },

  header: {
    marginHorizontal: 20,
    backgroundColor: 'white',
    color: 'black',
    textAlign: 'center',
    fontSize: 70,
  },
});

export default Header;
