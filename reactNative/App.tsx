import React from 'react';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import RootNavigator from './NavBar/RootNavigator';
import {Provider} from 'react-redux';
import Store from './redux/store';
import {
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  StatusBar,
  TouchableWithoutFeedback,
} from 'react-native';

const App: React.FC = () => {
  return (
    <>
      <NavigationContainer>
        <Provider store={Store}>
          <StatusBar
            barStyle="dark-content"
            networkActivityIndicatorVisible={true}
          />
          <KeyboardAvoidingView
            behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
            style={{flex: 1}}>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
              <RootNavigator />
            </TouchableWithoutFeedback>
          </KeyboardAvoidingView>
        </Provider>
      </NavigationContainer>
    </>
  );
};

export default App;
