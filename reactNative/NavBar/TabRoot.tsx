import React, {useEffect} from 'react';
import LoginScreen from '../screens/LoginScreens';
import {
  BottomTabBar,
  createBottomTabNavigator,
} from '@react-navigation/bottom-tabs';
import ProfileStackScreen from './Stacks/ProfileStackScreen';
import {useDispatch, useSelector} from 'react-redux';
import {IntegratedState} from '../redux/store';
import OfferStackScreen from './Stacks/OfferStackScreen';
import MyOrderStackScreen from './Stacks/OrderStackScreen';
import LoadingScreen from '../screens/LoadingScreen';
import {getProfileDataThunk} from '../redux/profile/thunk';
import {getAllOffersThunk} from '../redux/OffersScreen/OfferScreen/thunk';
import {getAllRequestThunk} from '../redux/RequestScreen/RequestScreen/thunk';
import {
  StyleSheet,
  Dimensions,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';

Icon.loadFont();

const offerIcon = <Icon name="globe" size={30} color="#bf0b0b"/>
const orderIcon = <Icon name="align-justify" size={30} color="#bf0b0b"/>
const profileIcon = <Icon name="user-circle" size={30} color="#bf0b0b"/>


const {height} = Dimensions.get('window');

const GradientTabBar = (props: any) => (
  <LinearGradient
    colors={['#ffeacc', "rgba(163, 0, 0, 0.5)"]}
    style={[
      StyleSheet.absoluteFill,
      {height: height * 0.1, position: 'relative'},
    ]}
  >
    <BottomTabBar {...props} style={{backgroundColor: 'transparent'}} />
  </LinearGradient>
);

const Tab = createBottomTabNavigator();

const TabRoot: React.FC = () => {
  const dispatch = useDispatch();
  const isLoggedIn = useSelector(
    (state: IntegratedState) => state.auth.isLoggedIn,
  );
  const isLoading = useSelector(
    (state: IntegratedState) => state.auth.isLoading,
  );
  const userEmail = useSelector((state: IntegratedState) => state.auth.email);
  const userToken = useSelector((state: IntegratedState) => state.auth.token);

  // Load initial data here
  useEffect(() => {
    dispatch(getProfileDataThunk(userEmail, userToken));
    dispatch(getAllOffersThunk());
    dispatch(getAllRequestThunk());
  }, [isLoggedIn]);

  return (
    <>
      {!isLoggedIn && !isLoading && <LoginScreen />}
      {isLoading && !isLoggedIn && <LoadingScreen />}
      {isLoggedIn && (
        <Tab.Navigator tabBar={(props) => <GradientTabBar {...props} />} tabBarOptions={{activeTintColor: "#bf0b0b", inactiveTintColor: "grey"}}>
          <Tab.Screen name="Offer" component={OfferStackScreen} options={{tabBarIcon: () => offerIcon}}/>
          {/* <Tab.Screen name="Requests" component={RequestStackScreen} /> */}
          <Tab.Screen name="My Order" component={MyOrderStackScreen} options={{tabBarIcon: () => orderIcon}}/>
          {/* <Tab.Screen name="Notification" component={NotificationStackScreen} /> */}
          <Tab.Screen name="Profile" component={ProfileStackScreen} options={{tabBarIcon: () => profileIcon}}/>
        </Tab.Navigator>
      )}
    </>
  );
};

export default TabRoot;
