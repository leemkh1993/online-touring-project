import React, { useEffect } from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import NotFoundScreen from '../screens/NotFoundScreen';
import TabRoot from './TabRoot';
import { useDispatch } from 'react-redux';
import { restoreLoginThunk } from '../redux/auth/thunk';
import OfferScreen from '../screens/OfferScreen';

const Stack = createStackNavigator();

const RootNavigator: React.FC = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(restoreLoginThunk())
  }, [])

  return (
    <Stack.Navigator mode="modal" screenOptions={{headerShown: false}}>
      <Stack.Screen name="Root" component={TabRoot} />
      <Stack.Screen name="OfferModal" component={OfferScreen} />
      <Stack.Screen name="NotFound" component={NotFoundScreen} />
    </Stack.Navigator>
  );
};

export default RootNavigator;
