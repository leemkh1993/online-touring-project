import React from 'react';
import {createStackNavigator, Header} from '@react-navigation/stack';
import ProfileScreen from '../../screens/ProfileScreen';
import {StyleSheet, View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const Stack = createStackNavigator();

const GradientHeader = (props: any) => (
  <View style={{backgroundColor: '#eee'}}>
    <LinearGradient
      colors={['rgba(163, 0, 0, 0.5)', '#ffeacc']}
      style={[
        StyleSheet.absoluteFill,
        {height: 80, justifyContent: 'center', position: 'relative'},
      ]}
      start={{x: 0.5, y: 0.5}}>
      <Header {...props} />
    </LinearGradient>
  </View>
);

const ProfileStackScreen: React.FC = () => {
  return (
    <Stack.Navigator initialRouteName="Profile">
      <Stack.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          title: 'About You',
          header: (props) => <GradientHeader {...props} />,
          headerStyle: styles.header,
        }}
      />
    </Stack.Navigator>
  );
};

const styles = StyleSheet.create({
  header: {
    backgroundColor: 'transparent',
  },
});

export default ProfileStackScreen;
