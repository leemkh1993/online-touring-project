import React from 'react';
import {createStackNavigator, Header} from '@react-navigation/stack';
import MyOrderScreen from '../../screens/MyOrderScreen';
import ViewOrderScreen from '../../screens/ViewOrderScreen';
import JitsiScreen from '../../screens/JitsiScreen';
import SubmitReviewScreen from '../../screens/SubmitReviewScreen';
import LinearGradient from 'react-native-linear-gradient';
import {View, StyleSheet} from 'react-native';

const Stack = createStackNavigator();

const GradientHeader = (props: any) => (
  <View style={{backgroundColor: '#eee'}}>
    <LinearGradient
      colors={['rgba(163, 0, 0, 0.5)', '#ffeacc']}
      style={[
        StyleSheet.absoluteFill,
        {height: 80, justifyContent: 'center', position: 'relative'},
      ]}
      start={{x: 0.5, y: 0.5}}>
      <Header {...props} />
    </LinearGradient>
  </View>
);

const MyOrderStackScreen: React.FC = () => {
  return (
    <Stack.Navigator initialRouteName="MyOrder">
      <Stack.Screen
        name="MyOrder"
        component={MyOrderScreen}
        options={{
          title: 'My Order',
          header: (props) => <GradientHeader {...props} />,
          headerStyle: styles.header,
        }}
      />
      <Stack.Screen
        name="ViewOrder"
        component={ViewOrderScreen}
        options={{
          title: 'View Order',
          header: (props) => <GradientHeader {...props} />,
          headerStyle: styles.header,
        }}
      />
      <Stack.Screen
        name="JitsiScreen"
        component={JitsiScreen}
        options={{
          title: "Let's Go!!!",
          header: (props) => <GradientHeader {...props} />,
          headerStyle: styles.header,
        }}
      />
      <Stack.Screen
        name="SubmitReview"
        component={SubmitReviewScreen}
        options={{
          title: 'Your Option Matters!',
          header: (props) => <GradientHeader {...props} />,
          headerStyle: styles.header,
        }}
      />
    </Stack.Navigator>
  );
};

const styles = StyleSheet.create({
  header: {
    backgroundColor: 'transparent',
  },
});

export default MyOrderStackScreen;
