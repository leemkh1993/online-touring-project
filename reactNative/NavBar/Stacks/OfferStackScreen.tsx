import React from 'react';
import {createStackNavigator, Header} from '@react-navigation/stack';
import OfferScreen from '../../screens/OfferScreen';
import ViewOfferScreen from '../../screens/ViewOfferScreen';
import OfferBookingScreen from '../../screens/BookingScreen';
import ViewOrderScreen from '../../screens/ViewOrderScreen';
import LinearGradient from 'react-native-linear-gradient';
import {View, StyleSheet} from 'react-native';

const Stack = createStackNavigator();

const GradientHeader = (props: any) => (
  <View style={{backgroundColor: '#eee'}}>
    <LinearGradient
      colors={['rgba(163, 0, 0, 0.5)', '#ffeacc']}
      style={[
        StyleSheet.absoluteFill,
        {height: 80, justifyContent: 'center', position: 'relative'},
      ]}
      start={{x: 0.5, y: 0.5}}>
      <Header {...props} />
    </LinearGradient>
  </View>
);

const OfferStackScreen: React.FC = () => {
  return (
    <Stack.Navigator initialRouteName="Offer">
      <Stack.Screen
        name="Offer"
        component={OfferScreen}
        options={{
          title: 'Offer List',
          header: (props) => <GradientHeader {...props} />,
          headerStyle: styles.header,
        }}
      />
      <Stack.Screen
        name="ViewOffer"
        component={ViewOfferScreen}
        options={{
          title: 'Check Me Out!',
          header: (props) => <GradientHeader {...props} />,
          headerStyle: styles.header,
        }}
      />
      <Stack.Screen
        name="Booking"
        component={OfferBookingScreen}
        options={{
          title: 'Last Step!!!',
          header: (props) => <GradientHeader {...props} />,
          headerStyle: styles.header,
        }}
      />
      <Stack.Screen
        name="ViewOrder"
        component={ViewOrderScreen}
        options={{
          title: 'View Order',
          header: (props) => <GradientHeader {...props} />,
          headerStyle: styles.header,
        }}
      />
    </Stack.Navigator>
  );
};

const styles = StyleSheet.create({
  header: {
    backgroundColor: 'transparent',
  },
});

export default OfferStackScreen;
