import {applyMiddleware, combineReducers, compose, createStore} from 'redux';
import {AuthState, AuthActions} from '../type/TypeLoginScreen';
import {authReducer} from './auth/reducer';
import thunk, {ThunkDispatch as OldThunkDispatch} from 'redux-thunk';
import {
  ProfileScreenActions,
  ProfileScreenState,
} from '../type/TypeProfileScreen';
import {profileScreenReducer} from './profile/reducer';
import {JitsiScreenAction, JitsiScreenState} from '../type/TypeJitsiScreen';
import {jitsiScreenReducer} from './Jitsi/reducer';
import {OfferScreenActions, OfferScreenState} from '../type/TypeOfferScreen';
import {OfferScreenReducer} from './OffersScreen/OfferScreen/reducer';
import {
  ViewOfferScreenActions,
  ViewOfferScreenState,
} from '../type/TypeViewOfferScreen';
import {ViewOfferScreenReducer} from './OffersScreen/ViewOfferScreen/reducer';
import { BookingSystemActions, BookingSystemState } from './BookingSystem/types';
import { bookingSystemReducer } from './BookingSystem/reducer';
import { ViewOrderActions, ViewOrderState } from './ViewOrder/types';
import { ViewOrderReducer } from './ViewOrder/reducer';
import { MyOrderScreenActions, MyOrderScreenState } from './MyOrderScreen/types';
import { myOrderScreenReducer } from './MyOrderScreen/reducer';
import { VideoConferenceState } from './Jitsi/VideoConference/state';
import { VideoConferenceAction } from './Jitsi/VideoConference/action';
import { videoConferenceReducer } from './Jitsi/VideoConference/reducer';

// Step 1: Integrated State
export interface IntegratedState {
  auth: AuthState;
  jitsiScreen: JitsiScreenState;
  profileScreen: ProfileScreenState;
  offerScreen: OfferScreenState;
  viewOfferScreen: ViewOfferScreenState;
  bookingSystem: BookingSystemState,
  viewOrder: ViewOrderState,
  myOrderScreen: MyOrderScreenState,
  videoConference: VideoConferenceState
}

// Step 2: Integrated Actions
type IntegratedAction =
  | AuthActions
  | ProfileScreenActions
  | JitsiScreenAction
  | OfferScreenActions
  | ViewOfferScreenActions
  | BookingSystemActions
  | ViewOrderActions
  | MyOrderScreenActions
  | VideoConferenceAction;

// Step 3: Integrated Reducer
const integratedReducer = combineReducers<IntegratedState>({
  auth: authReducer,
  jitsiScreen: jitsiScreenReducer,
  profileScreen: profileScreenReducer,
  offerScreen: OfferScreenReducer,
  viewOfferScreen: ViewOfferScreenReducer,
  bookingSystem: bookingSystemReducer,
  viewOrder: ViewOrderReducer,
  myOrderScreen: myOrderScreenReducer,
  videoConference: videoConferenceReducer
});

export type ThunkDispatch = OldThunkDispatch<
  IntegratedState,
  null,
  IntegratedAction
>;

// Step 4: Create Store
export default createStore<IntegratedState, IntegratedAction, {}, {}>(
  integratedReducer,
  compose(applyMiddleware(thunk)),
);
