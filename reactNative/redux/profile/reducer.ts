import {
  ProfileScreenActions,
  ProfileScreenState
} from '../../type/TypeProfileScreen';
import {
  PROFILE_SCREEN_FAIL,
  PROFILE_SCREEN_FETCHING,
  PROFILE_SCREEN_LOADING,
  PROFILE_SCREEN_SUCCESS,
} from './action';
import {initProfileScreenState} from './state';

export const profileScreenReducer = (
  state: ProfileScreenState = initProfileScreenState,
  action: ProfileScreenActions,
): ProfileScreenState => {
  switch (action.type) {
    case PROFILE_SCREEN_LOADING:
      return {
        ...state,
        isLoading: true,
      };
    case PROFILE_SCREEN_FETCHING:
      return {
        ...state,
        isLoading: true,
        isFetching: true,
      };
    case PROFILE_SCREEN_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isFetching: false,
        data: action.data,
      };
    case PROFILE_SCREEN_FAIL:
      return {
        ...state,
        isLoading: false,
        isFetching: false,
        errMessage: action.errMessage,
      };
    default:
      return state;
  }
};
