import {Dispatch} from 'react';
import Config from 'react-native-config';
import {ProfileData, ProfileScreenActions} from '../../type/TypeProfileScreen';
import {profileScreenFail, profileScreenFetching, profileScreenSuccess} from './action';

export const getProfileDataThunk = (email: string, token: string) => {
  return async (dispatch: Dispatch<ProfileScreenActions>) => {
    try {
      if (email) {
        dispatch(profileScreenFetching());
        const request = await fetch(`${Config.BACKEND_DOMAIN}${Config.BACKEND_PORT}/userInfo`, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
        });
        const result: ProfileData[] = await request.json();
        if (request.status === 200) {
            for (let offerRecord of result[0].offerRecords) {
              const expiryDate = offerRecord.expiry_date 
              const localTime = new Date(expiryDate).toString();
              offerRecord.expiry_date = localTime
            };
            for (let requestRecord of result[0].requestRecords) {
              const expiryDate = requestRecord.expiry_date 
              const localTime = new Date(expiryDate).toString();
              requestRecord.expiry_date = localTime
            };
            for (let orderRecord of result[0].orderRecords) {
              const { offer_expiry_date, order_start_date, order_end_date } = orderRecord
              orderRecord.offer_expiry_date = new Date(offer_expiry_date).toString();
              orderRecord.order_start_date = new Date(order_start_date).toString();
              orderRecord.order_end_date = new Date(order_end_date).toString();
            };
            dispatch(profileScreenSuccess(result[0]))
        } else {
            console.log("Fetch Failed")
            dispatch(profileScreenFail('Failed'));
        }
      } else {
        console.log('Need email');
        dispatch(profileScreenFail('Need email'));
      }
    } catch (err) {
      console.log(`getProfileDataThunk Error: ${err.message}`);
      dispatch(profileScreenFail(err.message));
    }
  };
};