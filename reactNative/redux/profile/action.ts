import {
  IProfile_Screen_Fail,
  IProfile_Screen_Fetching,
  IProfile_Screen_Loading,
  IProfile_Screen_Success,
  ProfileData,
} from '../../type/TypeProfileScreen';

export const PROFILE_SCREEN_LOADING = '@profile_screen/PROFILE_SCREEN_LOADING';
export const PROFILE_SCREEN_FETCHING ='@profile_screen/PROFILE_SCREEN_FETCHING';
export const PROFILE_SCREEN_SUCCESS = '@profile_screen/PROFILE_SCREEN_SUCCESS';
export const PROFILE_SCREEN_FAIL = '@profile_screen/PROFILE_SCREEN_FAIL';

export const profileScreenLoading = (): IProfile_Screen_Loading => {
  return {
    type: PROFILE_SCREEN_LOADING,
  };
};

export const profileScreenFetching = (): IProfile_Screen_Fetching => {
  return {
    type: PROFILE_SCREEN_FETCHING,
  };
};

export const profileScreenSuccess = (data: ProfileData): IProfile_Screen_Success => {
  return {
    type: PROFILE_SCREEN_SUCCESS,
    data: data,
  };
};

export const profileScreenFail = (errMessage: string): IProfile_Screen_Fail => {
  return {
    type: PROFILE_SCREEN_FAIL,
    errMessage: errMessage,
  };
};
