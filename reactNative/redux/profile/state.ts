export const initProfileScreenState = {
    isLoading: false,
    isFetching: false,
    data: null,
    errMessage: ""
}