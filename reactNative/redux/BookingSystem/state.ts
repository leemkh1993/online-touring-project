import { BookingSystemState } from "./types";

export const initBookingSystemState: BookingSystemState = {
    isLoading: false,
    isFetching: false,
    verifyExistingOrder: false,
    orderExistedMessage: null,
    orderNotFound: null,
    orderData: null,
    errMessage: null
}