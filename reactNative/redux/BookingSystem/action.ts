import {
  IBookingSystemFetching,
  IBookingSystemLoading,
  IBookingSystemPayment,
  IBookingSystemPaymentFail,
  IBookingSystemPaymentSuccess,
  IBookingSystemReset,
  IBookingSystemVerifying,
  IBookingSystemVerifyingFail,
  IBookingSystemVerifyingSuccess_Order_Found,
  IBookingSystemVerifyingSuccess_Order_Not_Found,
} from './types';

export const BOOKING_SYSTEM_LOADING = '@booking_system/BOOKING_SYSTEM_LOADING';
export const BOOKING_SYSTEM_FETCHING =
  '@booking_system/BOOKING_SYSTEM_FETCHING';
export const BOOKING_SYSTEM_VERIFYING =
  '@booking_system/BOOKING_SYSTEM_VERIFYING';
export const BOOKING_SYSTEM_VERIFYING_SUCCESS_ORDER_FOUND =
  '@booking_system/BOOKING_SYSTEM_VERIFYING_SUCCESS_ORDER_FOUND';
export const BOOKING_SYSTEM_VERIFYING_SUCCESS_ORDER_NOT_FOUND =
  '@booking_system/BOOKING_SYSTEM_VERIFYING_SUCCESS_ORDER_NOT_FOUND';
export const BOOKING_SYSTEM_VERIFYING_FAIL =
  '@booking_system/BOOKING_SYSTEM_VERIFYING_FAIL';
export const BOOKING_SYSTEM_PAYMENT = '@booking_system/BOOKING_SYSTEM_PAYMENT';
export const BOOKING_SYSTEM_PAYMENT_SUCCESS =
  '@booking_system/BOOKING_SYSTEM_PAYMENT_SUCCESS';
export const BOOKING_SYSTEM_PAYMENT_FAIL =
  '@booking_system/BOOKING_SYSTEM_PAYMENT_FAIL';
export const BOOKING_SYSTEM_RESET =
  '@booking_system/BOOKING_SYSTEM_RESET';


export const bookingSystemLoading = (): IBookingSystemLoading => {
  return {
    type: BOOKING_SYSTEM_LOADING,
  };
};

export const bookingSystemFetching = (): IBookingSystemFetching => {
  return {
    type: BOOKING_SYSTEM_FETCHING,
  };
};

export const bookingSystemVerifying = (): IBookingSystemVerifying => {
  return {
    type: BOOKING_SYSTEM_VERIFYING,
  };
};

export const bookingSystemVerifyingSuccessOrderFound = (
  data: string,
): IBookingSystemVerifyingSuccess_Order_Found => {
  return {
    type: BOOKING_SYSTEM_VERIFYING_SUCCESS_ORDER_FOUND,
    date: data,
  };
};

export const bookingSystemVerifyingSuccessOrderNotFound = (): IBookingSystemVerifyingSuccess_Order_Not_Found => {
    return {
      type: BOOKING_SYSTEM_VERIFYING_SUCCESS_ORDER_NOT_FOUND,
    };
  };

export const bookingSystemVerifyingFail = (
  errMessage: string,
): IBookingSystemVerifyingFail => {
  return {
    type: BOOKING_SYSTEM_VERIFYING_FAIL,
    errMessage: errMessage,
  };
};

export const bookingSystemPayment = (): IBookingSystemPayment => {
  return {
    type: BOOKING_SYSTEM_PAYMENT,
  };
};

export const bookingSystemPaymentSuccess = (
  data: any,
): IBookingSystemPaymentSuccess => {
  return {
    type: BOOKING_SYSTEM_PAYMENT_SUCCESS,
    data: data,
  };
};

export const bookingSystemPaymentFail = (
  errMessage: string,
): IBookingSystemPaymentFail => {
  return {
    type: BOOKING_SYSTEM_PAYMENT_FAIL,
    errMessage: errMessage
  };
};

export const bookingSystemReset = (): IBookingSystemReset => {
  return {
    type: BOOKING_SYSTEM_RESET,
  };
};
