import Config from 'react-native-config';
import {Dispatch} from 'redux';
import {
  bookingSystemPayment,
  bookingSystemPaymentFail,
  bookingSystemPaymentSuccess,
  bookingSystemVerifying,
  bookingSystemVerifyingFail,
  bookingSystemVerifyingSuccessOrderFound,
  bookingSystemVerifyingSuccessOrderNotFound,
} from './action';
import {BookingSystemActions} from './types';
const {requestOneTimePayment} = require('react-native-paypal/index');

export const verifyExistingOrderThunk = (offerID: number, token: string) => {
  return async (dispatch: Dispatch<BookingSystemActions>) => {
    try {
      dispatch(bookingSystemVerifying());
      const request = await fetch(
        `${Config.BACKEND_DOMAIN}${Config.BACKEND_PORT}/order/offer/verify/${offerID}`,
        {
          method: 'GET',
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      );
      if (request.status === 302) {
        const result = await request.json();
        const localTime = new Date(result).toLocaleString();
        dispatch(bookingSystemVerifyingSuccessOrderFound(localTime))
      } else {
        dispatch(bookingSystemVerifyingSuccessOrderNotFound());
      }
    } catch (err) {
      console.log(`verifyExistingOrderThunk Error: ${err.message}`);
      dispatch(bookingSystemVerifyingFail(err.message));
    }
  };
};

export const createOrderThunk = (
  offerID: number,
  token: string,
  cost: any,
  startDate: Date,
  endDate: Date | undefined,
) => {
  return async (dispatch: Dispatch<BookingSystemActions>) => {
    try {
      dispatch(bookingSystemPayment());
      const request = await fetch(
        `${Config.BACKEND_DOMAIN}${Config.BACKEND_PORT}/braintree/clientToken`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      );

      const {clientToken} = await request.json();
      const payment = await requestOneTimePayment(clientToken, {
        amount: cost,
        currency: 'USD',
        localeCode: 'en_GB',
        shippingAddressRequired: false,
        userAction: 'commit', // display 'Pay Now' on the PayPal review page
        intent: 'authorize',
      });
      const data = {
        offerID: offerID,
        amount: cost,
        nonce: payment.nonce as string,
        payerID: payment.payerId as string,
        startDate: startDate.toUTCString(),
        endDate: endDate ? endDate.toUTCString() : null,
      };
      const createTransaction = await fetch(
        `${Config.BACKEND_DOMAIN}${Config.BACKEND_PORT}/braintree/createTransaction`,
        {
          method: 'POST',
          headers: {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(data),
        },
      );
      const result = await createTransaction.json();
      console.log("booking system thunk:",result)
      if (createTransaction.status === 200) {
        const localTime = new Date(result[0].start_date).toLocaleString();
        result[0].start_date = localTime
        dispatch(bookingSystemPaymentSuccess(result));
      } else {
        console.log(`Transaction Failed`);
        dispatch(bookingSystemPaymentFail(`Transaction Failed: Please Try Again`));
      }
    } catch (err) {
      console.log(`createOrderThunk Error: ${err.message}`);
      dispatch(bookingSystemPaymentFail("Please Try Again"));
    }
  };
};
