import {
  BOOKING_SYSTEM_FETCHING,
  BOOKING_SYSTEM_LOADING,
  BOOKING_SYSTEM_PAYMENT,
  BOOKING_SYSTEM_PAYMENT_FAIL,
  BOOKING_SYSTEM_PAYMENT_SUCCESS,
  BOOKING_SYSTEM_RESET,
  BOOKING_SYSTEM_VERIFYING,
  BOOKING_SYSTEM_VERIFYING_FAIL,
  BOOKING_SYSTEM_VERIFYING_SUCCESS_ORDER_FOUND,
  BOOKING_SYSTEM_VERIFYING_SUCCESS_ORDER_NOT_FOUND,
} from './action';
import {initBookingSystemState} from './state';
import {BookingSystemActions, BookingSystemState} from './types';

export const bookingSystemReducer = (
  state: BookingSystemState = initBookingSystemState,
  action: BookingSystemActions,
): BookingSystemState => {
  switch (action.type) {
    case BOOKING_SYSTEM_LOADING: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case BOOKING_SYSTEM_FETCHING: {
      return {
        ...state,
        isLoading: true,
        isFetching: true,
      };
    }
    case BOOKING_SYSTEM_VERIFYING: {
      return {
        ...state,
        isLoading: true,
        isFetching: true,
        verifyExistingOrder: true,
      };
    }
    case BOOKING_SYSTEM_VERIFYING_SUCCESS_ORDER_FOUND: {
      return {
        ...state,
        isLoading: false,
        isFetching: false,
        verifyExistingOrder: false,
        orderExistedMessage: action.date,
      };
    }
    case BOOKING_SYSTEM_VERIFYING_SUCCESS_ORDER_NOT_FOUND: {
      return {
        ...state,
        isLoading: false,
        isFetching: false,
        verifyExistingOrder: false,
        orderNotFound: true,
      };
    }
    case BOOKING_SYSTEM_VERIFYING_FAIL: {
      return {
        ...state,
        isLoading: false,
        isFetching: false,
        errMessage: action.errMessage,
      };
    }
    case BOOKING_SYSTEM_PAYMENT: {
      return {
        ...state,
        isLoading: true,
        isFetching: true,
      };
    }
    case BOOKING_SYSTEM_PAYMENT_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        isFetching: false,
        orderData: action.data,
      };
    }
    case BOOKING_SYSTEM_PAYMENT_FAIL: {
      return {
        ...state,
        isLoading: false,
        isFetching: false,
        errMessage: action.errMessage,
      };
    }
    case BOOKING_SYSTEM_RESET: {
        return initBookingSystemState
    };
    default:
      return state;
  }
};
