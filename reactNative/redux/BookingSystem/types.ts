import {
  BOOKING_SYSTEM_LOADING,
  BOOKING_SYSTEM_FETCHING,
  BOOKING_SYSTEM_VERIFYING,
  BOOKING_SYSTEM_VERIFYING_FAIL,
  BOOKING_SYSTEM_PAYMENT,
  BOOKING_SYSTEM_PAYMENT_SUCCESS,
  BOOKING_SYSTEM_PAYMENT_FAIL,
  BOOKING_SYSTEM_VERIFYING_SUCCESS_ORDER_NOT_FOUND,
  BOOKING_SYSTEM_VERIFYING_SUCCESS_ORDER_FOUND, BOOKING_SYSTEM_RESET
} from './action';

export interface BookingSystemState {
  isLoading: boolean;
  isFetching: boolean;
  verifyExistingOrder: boolean;
  orderExistedMessage: string | null;
  orderNotFound: boolean | null;
  orderData: any | null;
  errMessage: string | null;
}

export interface IBookingSystemLoading {
  type: typeof BOOKING_SYSTEM_LOADING;
}

export interface IBookingSystemFetching {
  type: typeof BOOKING_SYSTEM_FETCHING;
}

export interface IBookingSystemVerifying {
  type: typeof BOOKING_SYSTEM_VERIFYING;
}

export interface IBookingSystemVerifyingSuccess_Order_Found {
  type: typeof BOOKING_SYSTEM_VERIFYING_SUCCESS_ORDER_FOUND;
  date: string;
}

export interface IBookingSystemVerifyingSuccess_Order_Not_Found {
  type: typeof BOOKING_SYSTEM_VERIFYING_SUCCESS_ORDER_NOT_FOUND;
}

export interface IBookingSystemVerifyingFail {
  type: typeof BOOKING_SYSTEM_VERIFYING_FAIL;
  errMessage: string;
}

export interface IBookingSystemPayment {
  type: typeof BOOKING_SYSTEM_PAYMENT;
}

export interface IBookingSystemPaymentSuccess {
  type: typeof BOOKING_SYSTEM_PAYMENT_SUCCESS;
  data: any;
}

export interface IBookingSystemPaymentFail {
  type: typeof BOOKING_SYSTEM_PAYMENT_FAIL;
  errMessage: string;
}

export interface IBookingSystemReset {
    type: typeof BOOKING_SYSTEM_RESET;
  }

export type BookingSystemActions =
  | IBookingSystemFetching
  | IBookingSystemLoading
  | IBookingSystemPayment
  | IBookingSystemVerifying
  | IBookingSystemVerifyingFail
  | IBookingSystemPaymentFail
  | IBookingSystemPaymentSuccess
  | IBookingSystemVerifyingSuccess_Order_Found
  | IBookingSystemVerifyingSuccess_Order_Not_Found
  | IBookingSystemReset;
