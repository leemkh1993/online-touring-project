import {
  OfferScreenActions,
  OfferScreenState,
} from '../../../type/TypeOfferScreen';
import {
  OFFER_SCREEN_FAIL,
  OFFER_SCREEN_FETCHING,
  OFFER_SCREEN_LOADING,
  OFFER_SCREEN_SUCCESS,
} from './action';
import {initOfferScreenState} from './state';

export const OfferScreenReducer = (
  state: OfferScreenState = initOfferScreenState,
  action: OfferScreenActions,
): OfferScreenState => {
  switch (action.type) {
    case OFFER_SCREEN_LOADING:
      return {
        ...state,
        isLoading: true,
      };
    case OFFER_SCREEN_FETCHING:
      return {
        ...state,
        isLoading: true,
        isFetching: true,
      };
    case OFFER_SCREEN_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isFetching: false,
        data: action.data,
      };
    case OFFER_SCREEN_FAIL:
      return {
        ...state,
        isLoading: false,
        isFetching: false,
        errMessage: action.errMessage,
      };
    default:
      return state;
  }
};
