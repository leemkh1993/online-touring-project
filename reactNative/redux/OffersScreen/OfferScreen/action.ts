import { IOfferScreenFail, IOfferScreenFetching, IOfferScreenLoading, IOfferScreenSuccess, OfferData } from "../../../type/TypeOfferScreen";

export const OFFER_SCREEN_LOADING = "@offer_screen/OFFER_SCREEN_LOADING";
export const OFFER_SCREEN_FETCHING = "@offer_screen/OFFER_SCREEN_FETCHING";
export const OFFER_SCREEN_SUCCESS = "@offer_screen/OFFER_SCREEN_SUCCESS";
export const OFFER_SCREEN_FAIL = "@offer_screen/OFFER_SCREEN_FAIL";

export const offerScreenLoading = (): IOfferScreenLoading => {
    return {
        type: OFFER_SCREEN_LOADING
    }
};

export const offerScreenFetching = (): IOfferScreenFetching => {
    return {
        type: OFFER_SCREEN_FETCHING
    }
};

export const offerScreenSuccess = (data: OfferData[]): IOfferScreenSuccess => {
    return {
        type: OFFER_SCREEN_SUCCESS,
        data: data
    }
};

export const offerScreenFail = (errMessage: string): IOfferScreenFail => {
    return {
        type: OFFER_SCREEN_FAIL,
        errMessage: errMessage
    }
};