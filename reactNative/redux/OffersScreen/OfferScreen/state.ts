import {OfferScreenState} from '../../../type/TypeOfferScreen';

export const initOfferScreenState: OfferScreenState = {
  isLoading: false,
  isFetching: false,
  data: null,
  errMessage: null,
};
