import {Dispatch} from 'react';
import Config from 'react-native-config';
import {OfferData, OfferScreenActions} from '../../../type/TypeOfferScreen';
import {offerScreenFail, offerScreenFetching, offerScreenSuccess} from './action';

export const getAllOffersThunk = () => {
  return async (dispatch: Dispatch<OfferScreenActions>) => {
    try {
      dispatch(offerScreenFetching())
      const request = await fetch(`${Config.BACKEND_DOMAIN}${Config.BACKEND_PORT}/offer_list`);
      const results: OfferData[] = await request.json();
      if (request.status === 200) {
        for (let result of results) {
            const expiryDate = result.expiry_date;
            const localDate = new Date(expiryDate).toString();
            result.expiry_date = localDate
        }
        dispatch(offerScreenSuccess(results))
      } else {
        console.log('Request Failed');
        dispatch(offerScreenFail('Request Failed'));
      }
    } catch (err) {
      console.log(`getAllOffersThunk Error: ${err.message}`);
      dispatch(offerScreenFail(err.message));
    }
  };
};
