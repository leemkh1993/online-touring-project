import {
  IViewOfferScreenFail,
  IViewOfferScreenFetching,
  IViewOfferScreenLoading,
  IViewOfferScreenSuccess,
  OfferDetailData,
} from '../../../type/TypeViewOfferScreen';

export const VIEW_OFFER_SCREEN_LOADING =
  '@view_offer_screen/VIEW_OFFER_SCREEN_LOADING';
export const VIEW_OFFER_SCREEN_FETCHING =
  '@view_offer_screen/VIEW_OFFER_SCREEN_FETCHING';
export const VIEW_OFFER_SCREEN_SUCCESS =
  '@view_offer_screen/VIEW_OFFER_SCREEN_SUCCESS';
export const VIEW_OFFER_SCREEN_FAIL =
  '@view_offer_screen/VIEW_OFFER_SCREEN_FAIL';

export const viewOfferScreenLoading = (): IViewOfferScreenLoading => {
  return {
    type: VIEW_OFFER_SCREEN_LOADING,
  };
};

export const viewOfferScreenFetching = (): IViewOfferScreenFetching => {
  return {
    type: VIEW_OFFER_SCREEN_FETCHING,
  };
};

export const viewOfferScreenSuccess = (
  data: OfferDetailData,
): IViewOfferScreenSuccess => {
  return {
    type: VIEW_OFFER_SCREEN_SUCCESS,
    data: data,
  };
};

export const viewOfferScreenFail = (
  errMessage: string,
): IViewOfferScreenFail => {
  return {
    type: VIEW_OFFER_SCREEN_FAIL,
    errMessage: errMessage,
  };
};
