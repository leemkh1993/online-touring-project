import {
  ViewOfferScreenActions,
  ViewOfferScreenState,
} from '../../../type/TypeViewOfferScreen';
import {VIEW_OFFER_SCREEN_FAIL, VIEW_OFFER_SCREEN_FETCHING, VIEW_OFFER_SCREEN_LOADING, VIEW_OFFER_SCREEN_SUCCESS} from './action';
import {initViewOfferScreenState} from './state';

export const ViewOfferScreenReducer = (
  state: ViewOfferScreenState = initViewOfferScreenState,
  action: ViewOfferScreenActions,
): ViewOfferScreenState => {
  switch (action.type) {
    case VIEW_OFFER_SCREEN_LOADING:
      return {
        ...state,
        isLoading: true,
      };
    case VIEW_OFFER_SCREEN_FETCHING:
      return {
        ...state,
        isLoading: true,
        isFetching: true
      };
    case VIEW_OFFER_SCREEN_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isFetching: false,
        data: action.data
      };

    case VIEW_OFFER_SCREEN_FAIL:
      return {
        ...state,
        isLoading: false,
        isFetching: false,
        errMessage: action.errMessage
      };
    default: 
      return state
  }
};
