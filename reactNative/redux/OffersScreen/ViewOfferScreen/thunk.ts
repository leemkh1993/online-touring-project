import Config from 'react-native-config';
import {Dispatch} from 'redux';
import {ViewOfferScreenActions} from '../../../type/TypeViewOfferScreen';
import {
  viewOfferScreenFail,
  viewOfferScreenFetching,
  viewOfferScreenSuccess,
} from './action';

export const getOfferDetailThunk = (offerID: number, token: string) => {
  return async (dispatch: Dispatch<ViewOfferScreenActions>) => {
    try {
      dispatch(viewOfferScreenFetching());
      const request = await fetch(
        `${Config.BACKEND_DOMAIN}${Config.BACKEND_PORT}/offer/Info/${offerID}`,
        {
          method: 'GET',
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      );
      const result = await request.json();
      if (request.status === 200) {
        const expiryDate = new Date(result[0].offer.offer_expiry_date).toString();
        result[0].offer.offer_expiry_date = expiryDate;
        dispatch(viewOfferScreenSuccess(result[0]));
      } else {
        console.log('Server Failed ');
        dispatch(viewOfferScreenFail('Failed'));
      }
    } catch (err) {
      console.log(`getOfferDetailThunk Error: ${err.message}`);
      dispatch(viewOfferScreenFail(err.message));
    }
  };
};
