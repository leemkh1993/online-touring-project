import { ViewOfferScreenState } from "../../../type/TypeViewOfferScreen";

export const initViewOfferScreenState: ViewOfferScreenState = {
  isLoading: false,
  isFetching: false,
  data: null,
  errMessage: null,
};
