import {
  VIEW_ORDER_FAIL,
  VIEW_ORDER_FETCHING,
  VIEW_ORDER_RESET,
  VIEW_ORDER_SUCCESS,
} from './action';
import {initViewOrderState} from './state';
import {ViewOrderActions, ViewOrderState} from './types';

export const ViewOrderReducer = (
  state: ViewOrderState = initViewOrderState,
  action: ViewOrderActions,
): ViewOrderState => {
  switch (action.type) {
    case VIEW_ORDER_FETCHING:
      return {
        ...state,
        isFetching: true,
      };
    case VIEW_ORDER_SUCCESS:
      return {
        ...state,
        isFetching: false,
        data: action.data,
      };
    case VIEW_ORDER_FAIL:
      return {
        ...state,
        isFetching: false,
        errMessage: action.errMessage,
      };
    case VIEW_ORDER_RESET:
      return initViewOrderState;
    default:
      return state;
  }
};
