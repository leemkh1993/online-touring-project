import { Config } from "react-native-config";
import { Dispatch } from "redux";
import { viewOrderScreenFail, viewOrderScreenFetching, viewOrderScreenSuccess } from "./action";
import { ViewOrderActions } from "./types";

export const viewOrderDetailThunk = (orderID: number, token: string) => {
  return async (dispatch: Dispatch<ViewOrderActions>) => {
    try {
      dispatch(viewOrderScreenFetching());
      const request = await fetch(
        `${Config.BACKEND_DOMAIN}${Config.BACKEND_PORT}/order/offer/info/${orderID}`,
        {
          method: 'GET',
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      );
      const result = await request.json();
      if (request.status === 200) {
        const {order_start_date, order_end_date} = result.order;
        result.order.order_start_date = new Date(order_start_date).toString();
        result.order.order_end_date = new Date(order_end_date).toString();
        const expiryDate = result.offerData.offer_expiry_date;
        result.offerData.offer_expiry_date = new Date(expiryDate).toString();
        dispatch(viewOrderScreenSuccess(result));
      } else {
        console.log('Server Failed ');
        dispatch(viewOrderScreenFail('Failed'));
      }
    } catch (err) {
      console.log(`getOrderDetailThunk Error: ${err.message}`);
      dispatch(viewOrderScreenFail(err.message));
    }
  };
};
