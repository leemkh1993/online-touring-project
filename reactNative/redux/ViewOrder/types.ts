import {OrderData} from '../../type/TypeJitsiScreen';
import {VIEW_ORDER_FAIL, VIEW_ORDER_FETCHING, VIEW_ORDER_RESET, VIEW_ORDER_SUCCESS} from './action';

export interface ViewOrderState {
  isFetching: boolean;
  data: OrderData | null;
  errMessage: string | null;
}

export interface IViewOrderFetching {
  type: typeof VIEW_ORDER_FETCHING;
}

export interface IViewOrderSuccess {
  type: typeof VIEW_ORDER_SUCCESS;
  data: OrderData | null;
}

export interface IViewOrderFail {
  type: typeof VIEW_ORDER_FAIL;
  errMessage: string | null;
}

export interface IViewOrderReset {
  type: typeof VIEW_ORDER_RESET;
}


export type ViewOrderActions =
  | IViewOrderFail
  | IViewOrderFetching
  | IViewOrderSuccess
  | IViewOrderReset;

export interface ViewOrderProps {
  data: any
}
