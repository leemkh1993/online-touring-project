import { OrderData } from "../../type/TypeJitsiScreen";
import { IViewOrderFetching, IViewOrderSuccess, IViewOrderFail, IViewOrderReset } from "./types";

export const VIEW_ORDER_FETCHING = "@view_order_screen/VIEW_ORDER_FETCHING";
export const VIEW_ORDER_SUCCESS = "@view_order_screen/VIEW_ORDER_SUCCESS";
export const VIEW_ORDER_FAIL = "@view_order_screen/VIEW_ORDER_FAIL";
export const VIEW_ORDER_RESET = "@view_order_screen/VIEW_ORDER_RESET";


export const viewOrderScreenFetching = (): IViewOrderFetching => {
    return {
        type: VIEW_ORDER_FETCHING
    }
};

export const viewOrderScreenSuccess = (data: OrderData): IViewOrderSuccess => {
    return {
        type: VIEW_ORDER_SUCCESS,
        data: data
    }
};

export const viewOrderScreenFail = (errMessage: string): IViewOrderFail => {
    return {
        type: VIEW_ORDER_FAIL,
        errMessage: errMessage
    }
};

export const viewOrderScreenReset = (): IViewOrderReset => {
    return {
        type: VIEW_ORDER_RESET
    }
}


