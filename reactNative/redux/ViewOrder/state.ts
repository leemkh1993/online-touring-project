import { ViewOrderState } from "./types";

export const initViewOrderState: ViewOrderState = {
    isFetching: false,
    data: null,
    errMessage: null
}