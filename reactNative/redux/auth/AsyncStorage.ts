import AsyncStorage from '@react-native-community/async-storage';

export async function storeData(key: string, token: string) {
  try {
    const storeData = await AsyncStorage.setItem(key, token);
    return storeData
  } catch (err) {
    console.log(`Async Storage f(storeData) Error: ${err.message}`);
    return undefined;
  }
}

export async function getData(key: string) {
  try {
    const getData = await AsyncStorage.getItem(key);
    return getData;
  } catch (err) {
    console.log(`Async Storage f(getData) Error: ${err.message}`);
    return undefined;
  }
}

export async function removeData(key: string) {
  try {
    const removeData = await AsyncStorage.removeItem(key);
    return removeData;
  } catch (err) {
    console.log(`Async Storage f(removeData) Error: ${err.message}`);
    return undefined;
  }
}
