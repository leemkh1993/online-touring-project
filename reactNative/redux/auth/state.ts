export const initAuthState = {
    isLoading: false,
    isLoggedIn: false,
    email: "",
    token: "",
    errMessage: ""
}