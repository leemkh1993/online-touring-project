import {
  ILoginLoading,
  ILoginSuccess,
  ILoginFail,
  ILogoutSuccess,
} from '../../type/TypeLoginScreen';

export const LOGIN_LOADING = '@@auth/LOGIN_LOADING';
export const LOGIN_SUCCESS = '@@auth/LOGIN_SUCCESS';
export const LOGIN_FAIL = '@@auth/LOGIN_FAIL';
export const LOGOUT_SUCCESS = '@@auth/LOGOUT_SUCCESS';

export const loginLoading = (): ILoginLoading => {
  return {
    type: LOGIN_LOADING,
  };
};

export const loginSuccess = (email: string, token: string): ILoginSuccess => {
  return {
    type: LOGIN_SUCCESS,
    email: email,
    token: token,
  };
};

export const loginFail = (errMessage: string): ILoginFail => {
  return {
    type: LOGIN_FAIL,
    errMessage: errMessage,
  };
};

export const logoutSuccess = (): ILogoutSuccess => {
  return {
    type: LOGOUT_SUCCESS,
  };
};
