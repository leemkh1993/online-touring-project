import Config from 'react-native-config';
import {Dispatch} from 'redux';
import {AuthActions} from '../../type/TypeLoginScreen';
import {loginFail, loginLoading, loginSuccess, logoutSuccess} from './action';
import {storeData, getData, removeData} from './AsyncStorage';

export const loginThunk = (email: string, password: string) => {
  return async (dispatch: Dispatch<AuthActions>) => {
    try {
      dispatch(loginLoading());
      const info = {email: email, password: password};      const request = await fetch(`${Config.BACKEND_DOMAIN}${Config.BACKEND_PORT}/login`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          "Accept": "application/json"
        },
        body: JSON.stringify(info),
      });
      
      const data = await request.json();
      if (request.status === 200) {
        const key = '@loginToken';
        await storeData(key, data.token);
        dispatch(loginSuccess(email, data.token));
      } else {
        dispatch(loginFail(data.message));
      }
    } catch(err) {
      console.log(`Failed to fetch ${err}`);
      dispatch(loginFail(err.message));
    }
  };
};

export const restoreLoginThunk = () => {
  return async (dispatch: Dispatch<AuthActions>) => {
    try {
      dispatch(loginLoading());
      const key = "@loginToken";
      const token = await getData(key);
      if (token) {
        const request = await fetch(`${Config.BACKEND_DOMAIN}${Config.BACKEND_PORT}/restoreLogin`, {
          headers:{
            Authorization: `Bearer ${token}`,
          }
        });
        const data = await request.json();
        if (request.status === 200) {
          dispatch(loginSuccess(data.email, token));
        } else {
          dispatch(logoutSuccess());
        }
      } else {
        dispatch(logoutSuccess());
      }
    } catch(err) {
      console.log(`Failed to restore login: ${err.message}`);
      dispatch(loginFail(err.message));
    }
  };
};

export const logoutThunk = () => {
  return async (dispatch: Dispatch<AuthActions>) => {
    try {
      const key = '@loginToken';
      await removeData(key);
      dispatch(logoutSuccess());
    } catch(err) {
      console.log(`Failed to logout: ${err.message}`);
    }
  };
};
