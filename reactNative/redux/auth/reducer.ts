import {initAuthState} from './state';
import { AuthActions, AuthState } from '../../type/TypeLoginScreen';
import {
  LOGIN_LOADING,
  LOGIN_SUCCESS,
  LOGOUT_SUCCESS,
  LOGIN_FAIL,
} from './action';

export const authReducer = (
  state: AuthState = initAuthState,
  action: AuthActions,
): AuthState => {
  switch (action.type) {
    case LOGIN_LOADING:
      return {
        ...state,
        isLoading: true
      };

    case LOGIN_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isLoggedIn: true,
        email: action.email,
        token: action.token
      };

    case LOGIN_FAIL:
      return {
        ...state,
        isLoading: false,
        isLoggedIn: false,
        errMessage: action.errMessage
      };

    case LOGOUT_SUCCESS:
      return {
        ...initAuthState
      };

    default:
      return state;
  }
};
