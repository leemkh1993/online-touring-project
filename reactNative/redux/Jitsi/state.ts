export const initJitsiScreen = {
    isLoading: false,
    isFetching: false,
    data: null,
    errMessage: ""
};