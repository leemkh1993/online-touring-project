export interface VideoConferenceState {
    videoEnded: boolean
}

export const initVideoConferenceState: VideoConferenceState = {
    videoEnded: false
}

