import {IVideoConferenceEnded, VideoConferenceAction, VIDEO_CONFERENCE_ENDED, VIDEO_CONFERENCE_RESET} from './action';
import {initVideoConferenceState, VideoConferenceState} from './state';

export const videoConferenceReducer = (
  state: VideoConferenceState = initVideoConferenceState,
  action: VideoConferenceAction,
): VideoConferenceState => {
  switch (action.type) {
    case VIDEO_CONFERENCE_ENDED:
      return {
        ...state,
        videoEnded: true,
      };
      case VIDEO_CONFERENCE_RESET:
      return initVideoConferenceState
    default:
      return state;
  }
};
