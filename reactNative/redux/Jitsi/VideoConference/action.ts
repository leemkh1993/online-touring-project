export const VIDEO_CONFERENCE_ENDED = "@video_conference/VIDEO_CONFERENCE_ENDED";
export const VIDEO_CONFERENCE_RESET = "@video_conference/VIDEO_CONFERENCE_RESET";


export interface IVideoConferenceEnded {
    type: typeof VIDEO_CONFERENCE_ENDED
}

export interface IVideoConferenceReset {
    type: typeof VIDEO_CONFERENCE_RESET
}

export const videoConferenceEnded = (): IVideoConferenceEnded => {
    return {
        type: VIDEO_CONFERENCE_ENDED
    }
}

export const videoConferenceReset = (): IVideoConferenceReset => {
    return {
        type: VIDEO_CONFERENCE_RESET
    }

}

export type VideoConferenceAction = IVideoConferenceEnded | IVideoConferenceReset