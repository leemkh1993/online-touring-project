import {JitsiScreenAction, JitsiScreenState} from '../../type/TypeJitsiScreen';
import {
  JITSI_SCREEN_FAIL,
  JITSI_SCREEN_FETCHING,
  JITSI_SCREEN_LOADING,
  JITSI_SCREEN_RESET,
  JITSI_SCREEN_SUCCESS,
} from './action';
import {initJitsiScreen} from './state';

export const jitsiScreenReducer = (
  state: JitsiScreenState = initJitsiScreen,
  action: JitsiScreenAction,
): JitsiScreenState => {
  switch (action.type) {
    case JITSI_SCREEN_LOADING:
      return {
        ...state,
        isLoading: true,
      };
    case JITSI_SCREEN_FETCHING:
      return {
        ...state,
        isLoading: true,
        isFetching: true,
      };
    case JITSI_SCREEN_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isFetching: false,
        data: action.data,
      };
    case JITSI_SCREEN_FAIL:
      return {
        ...state,
        isLoading: false,
        isFetching: false,
        errMessage: action.errMessage,
      };
    case JITSI_SCREEN_RESET: 
      return initJitsiScreen
    default:
        return state;
  }
};
