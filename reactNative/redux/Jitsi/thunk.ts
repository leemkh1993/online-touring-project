import {Dispatch} from 'react';
import Config from 'react-native-config';
import {JitsiScreenAction} from '../../type/TypeJitsiScreen';
import {
  jitsiScreenFail,
  jitsiScreenFetching,
  jitsiScreenSuccess,
} from './action';

export const getOrderDetailThunk = (orderID: number, token: string) => {
  return async (dispatch: Dispatch<JitsiScreenAction>) => {
    try {
      dispatch(jitsiScreenFetching());
      const request = await fetch(
        `${Config.BACKEND_DOMAIN}${Config.BACKEND_PORT}/order/offer/info/${orderID}`,
        {
          method: 'GET',
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      );
      const result = await request.json();
      if (request.status === 200) {
        const {order_start_date, order_end_date} = result.order;
        result.order.order_start_date = new Date(order_start_date).toString();
        result.order.order_end_date = new Date(order_end_date).toString();
        const expiryDate = result.offerData.offer_expiry_date;
        result.offerData.offer_expiry_date = new Date(expiryDate).toString();
        dispatch(jitsiScreenSuccess(result));
      } else {
        console.log('Server Failed ');
        dispatch(jitsiScreenFail('Failed'));
      }
    } catch (err) {
      console.log(`getOrderDetailThunk Error: ${err.message}`);
      dispatch(jitsiScreenFail(err.message));
    }
  };
};

export const fulfilled = (orderID: number, token: string) => {
  return async () => {
    await fetch(
      `${Config.BACKEND_DOMAIN}${Config.BACKEND_PORT}/order/offer/fulfilled/${orderID}`,
      {
        method: 'PUT',
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
  };
};

export const createReviews = async (
  orderID: number,
  guideID: number,
  token: string,
  offerReviewRating?: string,
  offerReviewComment?: string,
  guideReviewRating?: string,
  guideReviewComment?: string,
) => {
  try {
    if (orderID && guideID) {
      const offerRR = offerReviewRating? offerReviewRating: null;
      const offerRC = offerReviewComment? offerReviewComment: null;
      const guideRR = guideReviewRating? guideReviewRating: null;
      const guideRC = guideReviewComment? guideReviewComment: null;
      if (offerRR || offerRC || guideRC || guideRR) {
        const guideReview = {
          streamerID: guideID,
          orderID: orderID,
          rating: guideReviewRating,
          comment: guideReviewComment
        }
        console.log(guideReview)
        const offerReview = {
          orderID: orderID,
          rating: offerReviewRating,
          comment: offerReviewComment
        }
        console.log(offerReview)
        const guideRequest = await fetch(`${Config.BACKEND_DOMAIN}${Config.BACKEND_PORT}/reviews/streamer/createNewReview`, {
          method: "POST",
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json"
          },
          body: JSON.stringify(guideReview)
        })
        const guideResult = await guideRequest.json()
        const offerRequest = await fetch(`${Config.BACKEND_DOMAIN}${Config.BACKEND_PORT}/reviews/audience/createNewReview`, {
          method: "POST",
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json"
          },
          body: JSON.stringify(offerReview)
        }) 
        const offerResult = await offerRequest.json()
        console.log(guideRequest.status, offerRequest.status)
        console.log(guideResult, offerResult)
        if (guideRequest.status === 200 || offerRequest.status === 200) {
          return true
        } else {
          console.log("failed")
          return undefined
        }
      } else {
        console.log("no data")
        return undefined
      }
    } else {
      console.log(`essential data missing`)
      return undefined
    }
  } catch(err) {
    console.log(`createReviews Error: ${err.message}`)
  }
};
