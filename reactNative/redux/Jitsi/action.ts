import { IJitsi_Screen_Fail, IJitsi_Screen_Fetching, IJitsi_Screen_Loading, IJitsi_Screen_Reset, IJitsi_Screen_Success, OrderData } from "../../type/TypeJitsiScreen";

export const JITSI_SCREEN_LOADING = "@jitsi_screen/JITSI_SCREEN_LOADING";
export const JITSI_SCREEN_FETCHING = "@jitsi_screen/JITSI_SCREEN_FETCHING";
export const JITSI_SCREEN_SUCCESS = "@jitsi_screen/JITSI_SCREEN_SUCCESS";
export const JITSI_SCREEN_FAIL = "@jitsi_screen/JITSI_SCREEN_FAIL";
export const JITSI_SCREEN_RESET = "@jitsi_screen/JITSI_SCREEN_RESET";

export const jitsiScreenLoading = (): IJitsi_Screen_Loading=> {
    return {
        type: JITSI_SCREEN_LOADING
    }
}

export const jitsiScreenFetching = (): IJitsi_Screen_Fetching => {
    return {
        type: JITSI_SCREEN_FETCHING
    }
}

export const jitsiScreenSuccess = (data: OrderData): IJitsi_Screen_Success => {
    return {
        type: JITSI_SCREEN_SUCCESS,
        data: data
    }
}

export const jitsiScreenFail = (errMessage: string): IJitsi_Screen_Fail => {
    return {
        type: JITSI_SCREEN_FAIL,
        errMessage: errMessage
    }
}

export const jitsiScreenReset = (): IJitsi_Screen_Reset => {
    return {
        type: JITSI_SCREEN_RESET,
    }
}
