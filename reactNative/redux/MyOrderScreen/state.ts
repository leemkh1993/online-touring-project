import { MyOrderScreenState } from "./types";

export const initMyOrderScreen: MyOrderScreenState = {
    isFetching: false,
    data: null,
    audienceData: null,
    errMessage: null
}