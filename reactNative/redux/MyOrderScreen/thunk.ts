import Config from 'react-native-config';
import {Dispatch} from 'redux';
import {myOrderScreenAudienceDataSuccess, myOrderScreenFail, myOrderScreenFetching, myOrderScreenSuccess} from './action';
import {MyOrderScreenActions} from './types';

export const getMyOrderScreenGuideDataThunk = (token: string) => {
  return async (dispatch: Dispatch<MyOrderScreenActions>) => {
    try {
      dispatch(myOrderScreenFetching());
      const request = await fetch(
        `${Config.BACKEND_DOMAIN}${Config.BACKEND_PORT}/order/offer/myOrders`,
        {
          method: 'GET',
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
      );
      const results = await request.json();
      if (request.status === 200) {
        for (let result of results) {
          const {
            offer_expiry_date,
            order_start_date,
            order_end_date,
          } = result;
          result.offer_expiry_date = new Date(
            offer_expiry_date,
          ).toString();
          result.order_start_date = new Date(order_start_date).toString();
          result.order_end_date = new Date(order_end_date).toString();
        }
        const proceeded = results.filter((data: any) => !data.fulfilled)
        dispatch(myOrderScreenSuccess(proceeded));
      } else {
        console.log(`getMyOrderScreenGuideDataThunk Error`);
        dispatch(myOrderScreenFail(`getMyOrderScreenDataThunk Error`));
      }
    } catch (err) {
      console.log(`getOrderScreenThunk Error: ${err.message}`);
      dispatch(myOrderScreenFail(err.message));
    }
  };
};

export const getMyOrderScreenAudienceDataThunk = (token: string) => {
    return async (dispatch: Dispatch<MyOrderScreenActions>) => {
      try {
        dispatch(myOrderScreenFetching());
        const request = await fetch(
          `${Config.BACKEND_DOMAIN}${Config.BACKEND_PORT}/order/offer/myAudienceOrders`,
          {
            method: 'GET',
            headers: {
              Authorization: `Bearer ${token}`,
            },
          },
        );
        const results = await request.json();
        if (request.status === 200) {
          for (let result of results) {
            const {
              offer_expiry_date,
              order_start_date,
              order_end_date,
            } = result;
            result.offer_expiry_date = new Date(
              offer_expiry_date,
            ).toString();
            result.order_start_date = new Date(order_start_date).toString();
            result.order_end_date = new Date(order_end_date).toString();
          }
          const proceeded = results.filter((data: any) => !data.fulfilled)
          dispatch(myOrderScreenAudienceDataSuccess(proceeded));
        } else {
          console.log(`getMyOrderScreenDataThunk Error`);
          dispatch(myOrderScreenFail(`getMyOrderScreenDataThunk Error`));
        }
      } catch (err) {
        console.log(`getOrderScreenThunk Error: ${err.message}`);
        dispatch(myOrderScreenFail(err.message));
      }
    };
  };
  
