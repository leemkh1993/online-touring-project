import {
  IMyOrderScreenFetching,
  IMyOrderScreenSuccess,
  IMyOrderScreenFail,
  IMyOrderScreenAudienceDataSuccess,
} from './types';

export const MY_ORDER_SCREEN_FETCHING =
  '@my_order_screen/MY_ORDER_SCREEN_FETCHING';
export const MY_ORDER_SCREEN_SUCCESS =
  '@my_order_screen/MY_ORDER_SCREEN_SUCCESS';
export const MY_ORDER_SCREEN_AUDIENCE_DATA_SUCCESS =
  '@my_order_screen/MY_ORDER_SCREEN_AUDIENCE_DATA_SUCCESS';
export const MY_ORDER_SCREEN_FAIL = '@my_order_screen/MY_ORDER_SCREEN_FAIL';

export const myOrderScreenFetching = (): IMyOrderScreenFetching => {
  return {
    type: MY_ORDER_SCREEN_FETCHING,
  };
};

export const myOrderScreenSuccess = (data: any): IMyOrderScreenSuccess => {
  return {
    type: MY_ORDER_SCREEN_SUCCESS,
    data: data,
  };
};

export const myOrderScreenAudienceDataSuccess = (
  data: any,
): IMyOrderScreenAudienceDataSuccess => {
  return {
    type: MY_ORDER_SCREEN_AUDIENCE_DATA_SUCCESS,
    data: data,
  };
};

export const myOrderScreenFail = (errMessage: string): IMyOrderScreenFail => {
  return {
    type: MY_ORDER_SCREEN_FAIL,
    errMessage: errMessage,
  };
};
