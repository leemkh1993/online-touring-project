import {
    MY_ORDER_SCREEN_AUDIENCE_DATA_SUCCESS,
  MY_ORDER_SCREEN_FAIL,
  MY_ORDER_SCREEN_FETCHING,
  MY_ORDER_SCREEN_SUCCESS,
} from './action';

export interface MyOrderScreenState {
  isFetching: boolean;
  data: any | null;
  audienceData: any | null;
  errMessage: string | null;
}

export interface IMyOrderScreenFetching {
  type: typeof MY_ORDER_SCREEN_FETCHING;
}

export interface IMyOrderScreenSuccess {
  type: typeof MY_ORDER_SCREEN_SUCCESS;
  data: any;
}

export interface IMyOrderScreenAudienceDataSuccess {
  type: typeof MY_ORDER_SCREEN_AUDIENCE_DATA_SUCCESS;
  data: any;
}

export interface IMyOrderScreenFail {
  type: typeof MY_ORDER_SCREEN_FAIL;
  errMessage: string;
}

export type MyOrderScreenActions =
  | IMyOrderScreenFetching
  | IMyOrderScreenSuccess
  | IMyOrderScreenFail
  | IMyOrderScreenAudienceDataSuccess;
