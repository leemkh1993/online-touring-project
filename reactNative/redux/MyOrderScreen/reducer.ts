import { stat } from 'fs';
import { act } from 'react-test-renderer';
import {MY_ORDER_SCREEN_AUDIENCE_DATA_SUCCESS, MY_ORDER_SCREEN_FAIL, MY_ORDER_SCREEN_FETCHING, MY_ORDER_SCREEN_SUCCESS} from './action';
import {initMyOrderScreen} from './state';
import {MyOrderScreenActions, MyOrderScreenState} from './types';

export const myOrderScreenReducer = (
  state: MyOrderScreenState = initMyOrderScreen,
  action: MyOrderScreenActions,
): MyOrderScreenState => {
  switch (action.type) {
    case MY_ORDER_SCREEN_FETCHING:
      return {
        ...state,
        isFetching: true,
      };
    case MY_ORDER_SCREEN_SUCCESS:
      return {
        ...state,
        isFetching: false,
        data: action.data
      };
    case MY_ORDER_SCREEN_AUDIENCE_DATA_SUCCESS:
      return {
        ...state,
        isFetching: false,
        audienceData: action.data
      }
    case MY_ORDER_SCREEN_FAIL:
      return {
        ...state,
        isFetching: false,
        errMessage: action.errMessage
      };
    default:
      return state;
  }
};
