import {useRoute} from '@react-navigation/native';
import React from 'react';
import {Dimensions} from 'react-native';
import {View, StyleSheet} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import BookingSystem from '../components/OfferScreen/ViewOfferScreen/BookingSystem';
import OfferDestinations from '../components/OfferScreen/ViewOfferScreen/offerDestinations';
import OfferDetail from '../components/OfferScreen/ViewOfferScreen/offerDetail';

const {width} = Dimensions.get('window');

const OfferBookingScreen: React.FC<any> = () => {
  const route = useRoute();
  const {data}: any = route.params;

  return (
    <ScrollView style={{backgroundColor: "#ffeacc"}}>
      <View style={styles.bookingMainContainer}>
        <OfferDetail data={data.offer} />
      </View>
      <View style={styles.bookingContainer}>
        <BookingSystem data={{offerID: data.offer.offer_id, cost: data.offer.offer_cost}} />
      </View>
      <View style={styles.destinationsContainer}>
        <OfferDestinations data={data.destinations} />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  bookingMainContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    shadowColor: '#c7c7c7',
    shadowOffset: {width: -5, height: -5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  offerDetailsContainer: {
    width: width * 0.95,
    marginVertical: 10,

  },

  destinationsContainer: {
    marginVertical: 10,
    shadowColor: '#c7c7c7',
    shadowOffset: {width: -5, height: -5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },
  bookingContainer: {
    marginTop: 10,
    justifyContent: "center",
    alignItems: "center",
    shadowColor: '#c7c7c7',
    shadowOffset: {width: -5, height: -5},
    shadowOpacity: 5,
    shadowRadius: 5,
  }
});

export default OfferBookingScreen;
