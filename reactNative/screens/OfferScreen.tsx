import React from 'react';
import {ScrollView} from 'react-native-gesture-handler';
import {useSelector} from 'react-redux';
import OfferList from '../components/OfferScreen/OfferList';
import {IntegratedState} from '../redux/store';
import { SafeAreaLoadingScreen } from './LoadingScreen';

const OfferScreen: React.FC<any> = () => {
  const data = useSelector((state: IntegratedState) => state.offerScreen.data);
  const isLoading = useSelector(
    (state: IntegratedState) => state.offerScreen.isLoading,
  );
  const isFetching = useSelector(
    (state: IntegratedState) => state.offerScreen.isFetching,
  );

  return (
      <ScrollView style={{backgroundColor: "#ffeacc"}}>
        {isLoading && isFetching && <SafeAreaLoadingScreen />}
        {data && <OfferList data={data} />}
      </ScrollView>
  );
};

export default OfferScreen;
