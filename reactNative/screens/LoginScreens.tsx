import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  ImageBackground,
} from 'react-native';
import Config from 'react-native-config';
import Form from '../components/LoginScreen/Form';
import Header from '../components/LoginScreen/Header';

let {height} = Dimensions.get('window');

const LoginScreen: React.FC = () => {
  return (
    <View style={styles.loginFormBody}>
      <ImageBackground
        style={styles.image}
        source={{
          uri: `${Config.BACKEND_DOMAIN}${Config.BACKEND_PORT}/cast_profile_pic/login_background.jpg`,
        }}>
        <View style={styles.loginFormContainer}>
          <Header />
          <Form />
        </View>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  backgroundImgContainer: {
    position: 'relative',
  },

  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },

  loginFormBody: {
    flex: 1,
    flexDirection: "column",
  },

  loginFormContainer: {
    height: height,
    justifyContent: 'center',
  },
});

export default LoginScreen;
