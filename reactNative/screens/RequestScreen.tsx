import React from 'react';
import {ScrollView} from 'react-native-gesture-handler';
import {useSelector} from 'react-redux';
import RequestList from '../components/RequestScreen/RequestList';
import {IntegratedState} from '../redux/store';
import { SafeAreaLoadingScreen } from './LoadingScreen';

const RequestScreen: React.FC<any> = () => {
  const data = useSelector(
    (state: IntegratedState) => state.requestScreen.data,
  );
  const isLoading = useSelector(
    (state: IntegratedState) => state.requestScreen.isLoading,
  );
  const isFetching = useSelector(
    (state: IntegratedState) => state.requestScreen.isFetching,
  );

  return (
    <ScrollView>
      {isLoading && isFetching && <SafeAreaLoadingScreen />}
      {data && <RequestList data={data} />}
    </ScrollView>
  );
};

export default RequestScreen;
