import React, {useEffect} from 'react';
import {useState} from 'react';
import {Dimensions, SafeAreaView, StyleSheet, Text, View} from 'react-native';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
import {useDispatch, useSelector} from 'react-redux';
import EmptyList from '../components/MyOrderScreen/EmptyList';
import Order from '../components/MyOrderScreen/Order';
import {
  getMyOrderScreenAudienceDataThunk,
  getMyOrderScreenGuideDataThunk,
} from '../redux/MyOrderScreen/thunk';
import {IntegratedState} from '../redux/store';
import {SafeAreaLoadingScreen} from './LoadingScreen';

const {width, height} = Dimensions.get('window');

const MyOrderScreen: React.FC = () => {
  const dispatch = useDispatch();
  const [guide, setGuide] = useState(true);
  const token = useSelector((state: IntegratedState) => state.auth.token);
  const isFetching: boolean = useSelector(
    (state: IntegratedState) => state.myOrderScreen.isFetching,
  );
  const data: any[] = useSelector(
    (state: IntegratedState) => state.myOrderScreen.data,
  );

  const audienceData: any[] = useSelector(
    (state: IntegratedState) => state.myOrderScreen.audienceData,
  );

  const switchHandler = () => {
    setGuide(!guide);
  };

  useEffect(() => {
    dispatch(getMyOrderScreenGuideDataThunk(token));
    dispatch(getMyOrderScreenAudienceDataThunk(token));
  }, []);

  return (
    <SafeAreaView style={{backgroundColor: '#ffeacc'}}>
      <ScrollView style={{backgroundColor: '#ffeacc'}}>
        {isFetching && <SafeAreaLoadingScreen />}
        {!isFetching && (data || audienceData) && (
          <View style={styles.mainContainer}>
            <View style={styles.buttonContainer}>
              {guide && (
                <Text style={styles.header}>Current Mode: Tour Guide</Text>
              )}
              {!guide && (
                <Text style={styles.header}>Current Mode: Explorer</Text>
              )}
              <Text style={styles.header}>
                Room will open 30 minutes prior to start time!
              </Text>

              <TouchableOpacity style={styles.button} onPress={switchHandler}>
                {guide && (
                  <Text style={styles.subHeader}>
                    Press To Explore the World!
                  </Text>
                )}
                {!guide && (
                  <Text style={styles.subHeader}>
                    Press to Inspire the World!
                  </Text>
                )}
              </TouchableOpacity>
            </View>
            {guide && data && !data[0] && <EmptyList />}
            {guide &&
              data &&
              data[0] &&
              data.map((data: any) => (
                <Order key={data.order_id} data={data} />
              ))}
            {!guide && audienceData && !audienceData[0] && <EmptyList />}
            {!guide &&
              audienceData &&
              audienceData[0] &&
              audienceData.map((data: any) => (
                <Order key={data.order_id} data={data} />
              ))}
          </View>
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#ffeacc",

  },

  buttonContainer: {
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center',
    width: width * 0.95,
    borderRadius: 30,
    height: height * 0.2,
    backgroundColor: '#ffd599',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  header: {
    marginTop: 5,
    fontSize: 15,
  },

  subHeader: {
    fontSize: 20,
    fontWeight: 'bold',
  },

  button: {
    marginTop: 10,
    height: '65%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    paddingHorizontal: 20,
    backgroundColor: '#ffbf66',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },
});

export default MyOrderScreen;
