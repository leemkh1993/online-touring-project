import React from 'react';
import {useSelector} from 'react-redux';
import Profile from '../components/ProfileScreen/Profile';
import { IntegratedState } from '../redux/store';
import { SafeAreaLoadingScreen } from './LoadingScreen';

const ProfileScreen: React.FC = () => {
  const isFetching = useSelector((state: IntegratedState) => state.profileScreen.isFetching);
  const isLoading = useSelector((state: IntegratedState) => state.profileScreen.isLoading);
  const data = useSelector((state: IntegratedState) => state.profileScreen.data);

  return (
    <>
        {isLoading && isFetching && <SafeAreaLoadingScreen />}
        {data && <Profile data={data} />}
    </>
  );
};

export default ProfileScreen;
