import { useRoute } from '@react-navigation/native';
import React, { useEffect } from 'react';
import { ScrollView } from 'react-native-gesture-handler';
import { useDispatch, useSelector } from 'react-redux';
import ViewOffer from '../components/OfferScreen/ViewOfferScreen/ViewOffer';
import { getOfferDetailThunk } from '../redux/OffersScreen/ViewOfferScreen/thunk';
import { IntegratedState } from '../redux/store';
import { SafeAreaLoadingScreen } from './LoadingScreen';

const ViewOfferScreen: React.FC<any> = () => {
  const dispatch = useDispatch();
  const route = useRoute();
  const {offer_id}: any = route.params;
  const token = useSelector((state: IntegratedState) => state.auth.token);
  const isLoading = useSelector(
    (state: IntegratedState) => state.viewOfferScreen.isLoading,
  );
  const isFetching = useSelector(
    (state: IntegratedState) => state.viewOfferScreen.isFetching,
  );
  const data = useSelector((state: IntegratedState) => state.viewOfferScreen.data);

  useEffect(() => {
    dispatch(getOfferDetailThunk(offer_id, token));
  }, []);

  return (
      <ScrollView>
        {isLoading && isFetching && <SafeAreaLoadingScreen />}
        {data && <ViewOffer data={data} />}
      </ScrollView>
  );
};

export default ViewOfferScreen;
