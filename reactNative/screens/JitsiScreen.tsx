import {useRoute} from '@react-navigation/native';
import React, {useEffect} from 'react';
import {SafeAreaView, Text, View} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {useDispatch, useSelector} from 'react-redux';
import Jitsi from '../components/JitsiScreen/Jitsi';
import {getOrderDetailThunk} from '../redux/Jitsi/thunk';
import {IntegratedState} from '../redux/store';
import {SafeAreaLoadingScreen} from './LoadingScreen';

const JitsiScreen: React.FC = () => {
  const dispatch = useDispatch();
  const route = useRoute();
  const {order_id}: any = route.params;
  const token = useSelector((state: IntegratedState) => state.auth.token);
  const isLoading = useSelector(
    (state: IntegratedState) => state.jitsiScreen.isLoading,
  );
  const isFetching = useSelector(
    (state: IntegratedState) => state.jitsiScreen.isFetching,
  );
  const data = useSelector((state: IntegratedState) => state.jitsiScreen.data);

  useEffect(() => {
    dispatch(getOrderDetailThunk(order_id, token));
  }, []);

  return (
    <SafeAreaView>
      <ScrollView style={{backgroundColor: '#ffeacc'}}>
        {isLoading && isFetching && <SafeAreaLoadingScreen />}
        {data && <Jitsi data={data} />}
      </ScrollView>
    </SafeAreaView>
  );
};

export default JitsiScreen;
