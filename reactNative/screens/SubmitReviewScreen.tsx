import {useRoute} from '@react-navigation/native';
import React, {useRef, useState} from 'react';
import {Controller, useForm} from 'react-hook-form';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TextInput,
  Animated,
} from 'react-native';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
import {useSelector} from 'react-redux';
import {createReviews} from '../redux/Jitsi/thunk';
import {IntegratedState} from '../redux/store';

const {width, height} = Dimensions.get('window');

interface SubmitReviewForm {
  offerReviewRating: string;
  offerReviewComment: string;
  guideReviewRating: string;
  guideReviewComment: string;
}

const SubmitReviewScreen: React.FC<any> = () => {
  const heightAnim = useRef(new Animated.Value(height * 0.86)).current;
  const token = useSelector((state: IntegratedState) => state.auth.token);

  const changeHeight = () => {
    Animated.timing(heightAnim, {
      toValue: height * 0.77,
      duration: 1000,
      useNativeDriver: false,
    } as any).start();
  };

  const routes = useRoute();
  const {data}: any = routes.params;
  const {control, handleSubmit, errors} = useForm<SubmitReviewForm>();
  const [isLoading, setIsLoading] = useState(false);
  const [success, setSuccess] = useState(false);

  const submitHandler = async (formData: SubmitReviewForm) => {
    setIsLoading(true);
    const create = await createReviews(
      data.order.order_id,
      data.offerData.guide_id,
      token,
      formData.offerReviewRating,
      formData.offerReviewComment,
      formData.guideReviewRating,
      formData.offerReviewComment,
    );
    if (create) {
      changeHeight();
      setIsLoading(false);
      setSuccess(!success);
    }
  };

  return (
    <ScrollView style={{backgroundColor: '#ffeacc'}}>
      <Animated.View style={[styles.bigContainer, {height: heightAnim}]}>
        {!isLoading && success && (
          <View
            style={{
              height: '100%',
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View style={styles.buttonLook}>
              <Text style={styles.labels}>Submission Completed!!!</Text>
            </View>
          </View>
        )}
        {isLoading && (
          <View
            style={{
              height: '100%',
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View style={styles.buttonLook}>
              <Text style={styles.labels}>Loading...</Text>
            </View>
          </View>
        )}
        {!isLoading && !success && (
          <>
            <View style={styles.inputContainer}>
              <Text style={styles.labels}>
                How Is Your Experience with the Trip?
              </Text>
              <Text style={styles.labels}>Rate This Trip Out of 5!</Text>
              <Controller
                name="offerReviewRating"
                control={control}
                defaultValue={null}
                rules={{required: true, max: 5}}
                render={({onChange, onBlur, value}) => (
                  <TextInput
                    style={styles.inputArea}
                    placeholder="Rate this offer out of 5!"
                    onBlur={onBlur}
                    onChangeText={(value) => onChange(value)}
                    value={value}
                    autoCapitalize="none"
                  />
                )}
              />
              <Text style={styles.labels}>
                How Is Your Experience with the Trip?
              </Text>
              <Controller
                name="offerReviewComment"
                control={control}
                defaultValue={null}
                rules={{required: true}}
                render={({onChange, onBlur, value}) => (
                  <TextInput
                    style={styles.commentInputArea}
                    onBlur={onBlur}
                    onChangeText={(value) => onChange(value)}
                    value={value}
                    autoCapitalize="none"
                  />
                )}
              />
            </View>
            <View style={styles.inputContainer}>
              <Text style={styles.labels}>Do You Like This Guide?</Text>
              <Text style={styles.labels}>Rate This Guide Out of 5!</Text>
              <Controller
                name="guideReviewRating"
                control={control}
                defaultValue={null}
                rules={{required: true, max: 5}}
                render={({onChange, onBlur, value}) => (
                  <TextInput
                    style={styles.inputArea}
                    placeholder="Rate this offer out of 5!"
                    onBlur={onBlur}
                    onChangeText={(value) => onChange(value)}
                    value={value}
                    autoCapitalize="none"
                  />
                )}
              />
              <Text style={styles.labels}>What do you think of the Guide?</Text>
              <Controller
                name="guideReviewComment"
                control={control}
                defaultValue={null}
                rules={{required: true}}
                render={({onChange, onBlur, value}) => (
                  <TextInput
                    style={styles.commentInputArea}
                    onBlur={onBlur}
                    onChangeText={(value) => onChange(value)}
                    value={value}
                    autoCapitalize="none"
                  />
                )}
              />
            </View>
            <View>
              <TouchableOpacity
                style={styles.submitButton}
                onPress={handleSubmit(submitHandler)}>
                <Text style={styles.labels}>Submit</Text>
              </TouchableOpacity>
            </View>
          </>
        )}
      </Animated.View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    height: height * 0.9,
    backgroundColor: '#ffeacc',
    padding: 10,
  },

  labels: {
    color: 'black',
    fontWeight: '900',
    textAlign: 'center',
  },

  bigContainer: {
    width: width * 0.95,
    borderRadius: 30,
    backgroundColor: '#ffd599',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
    padding: 20,
    marginTop: 15,
    marginHorizontal: 10,
    marginBottom: 15,
  },

  inputContainer: {
    height: height * 0.34,
    backgroundColor: '#ffbf66',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
    marginVertical: 10,
    borderRadius: 30,
    padding: 15,
    justifyContent: 'space-between',
  },

  inputArea: {
    paddingHorizontal: 20,
    paddingVertical: 0,
    height: 30,
    backgroundColor: 'white',
    borderRadius: 10,
    fontSize: 15,
    color: 'black',
  },

  commentInputArea: {
    paddingHorizontal: 20,
    paddingVertical: 0,
    height: 130,
    backgroundColor: 'white',
    borderRadius: 10,
    fontSize: 15,
    color: 'black',
  },

  submitButton: {
    marginTop: 10,
    height: 50,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffbf66',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
  },

  buttonLook: {
    width: 280,
    height: 100,
    backgroundColor: '#ffbf66',
    shadowColor: 'grey',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 5,
    shadowRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
  },
});

export default SubmitReviewScreen;
