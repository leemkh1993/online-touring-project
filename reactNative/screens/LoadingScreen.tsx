import React from 'react';
import {Dimensions, StyleSheet, Text, View} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';

const screenHeight = Dimensions.get('window').height;

export const SafeAreaLoadingScreen: React.FC<any> = () => {
  return (
    <SafeAreaView style={{backgroundColor: '#ffeacc'}}>
      <View style={styles.safeAreaLoadingContainer}>
        <Text style={styles.label}>Loading...</Text>
      </View>
    </SafeAreaView>
  );
};

const LoadingScreen: React.FC = () => {
  return (
    <View style={styles.loadingContainer}>
      <Text style={styles.label}>Loading...</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  loadingContainer: {
    height: screenHeight,
    backgroundColor: '#ffeacc',
    justifyContent: 'center',
    alignItems: 'center',
  },

  safeAreaLoadingContainer: {
    height: screenHeight * 0.71,
    backgroundColor: '#ffeacc',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 50,
  },

  label: {
    fontSize: 25,
    fontWeight: 'bold',
  },
});

export default LoadingScreen;
