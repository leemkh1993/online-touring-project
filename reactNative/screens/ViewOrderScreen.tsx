import {useRoute} from '@react-navigation/native';
import React, {useEffect} from 'react';
import {SafeAreaView, ScrollView, Text, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import ViewOrder from '../components/ViewOrderScreen/ViewOrder';
import {IntegratedState} from '../redux/store';
import {viewOrderScreenReset} from '../redux/ViewOrder/action';
import {viewOrderDetailThunk} from '../redux/ViewOrder/thunk';
import {SafeAreaLoadingScreen} from './LoadingScreen';

const ViewOrderScreen: React.FC = () => {
  const dispatch = useDispatch();
  const route = useRoute();
  const {order_id}: any = route.params;
  const token = useSelector((state: IntegratedState) => state.auth.token);
  const isFetching = useSelector(
    (state: IntegratedState) => state.viewOrder.isFetching,
  );
  const data = useSelector((state: IntegratedState) => state.viewOrder.data);

  useEffect(() => {
    dispatch(viewOrderDetailThunk(order_id, token));

    return () => {
      dispatch(viewOrderScreenReset());
    };
  }, []);

  return (
    <SafeAreaView>
      <ScrollView style={{backgroundColor: '#ffeacc'}}>
        {isFetching && <SafeAreaLoadingScreen />}
        {data && <ViewOrder data={data} />}
      </ScrollView>
    </SafeAreaView>
  );
};

export default ViewOrderScreen;
