import express from "express";
import {
  userController,
  isLoggedIn,
  offerController,
  requestController,
  streamerReviewsController,
  audienceReviewsController,
  orderController,
  brainTreeController,
  hashTagController,
} from "./server";

export const routes = express.Router();

// ===============Home Page related routes===============
routes.post("/login", userController.login);
routes.post("/register", userController.createUser);
routes.get("/topStreamers", userController.topStreamer);
routes.get("/userInfo", isLoggedIn, userController.userInfoByEmail); //@BrowseOffer thunks
// 24/9 added for user intro purpose in offer page by LC     //@BrowseUser thunks
routes.get("/userInfo/:id", userController.userInfoById);
routes.get("/restoreLogin", isLoggedIn, userController.restoreLogin);

// ===============Offers related routes===============
routes.get("/offer/soonExpire", offerController.getSoonExpireTest);
routes.get("/offer/Info/:id", isLoggedIn, offerController.getOfferInfoByID); //@BrowseOffer thunks
routes.post("/offer/createNewOffer", isLoggedIn, offerController.createOffer);

// ===============Request related routes===============
routes.get("/request/getAllRequest", requestController.getAllRequest);
routes.get(
  "/request/Info/:id",
  isLoggedIn,
  requestController.getRequestInfoByID
);
routes.post(
  "/request/createNewRequest",
  isLoggedIn,
  requestController.createRequest
);

// ===============Order (Offer) related routes===============
routes.get("/order/offer/myOrders", isLoggedIn, orderController.getMyOrders);
routes.get(
  "/order/offer/myAudienceOrders",
  isLoggedIn,
  orderController.getMyAudienceOrders
);
routes.get(
  "/order/offer/info/:id",
  isLoggedIn,
  orderController.getOrderByOrderID
);
routes.post(
  "/order/offer/createNewOrder",
  isLoggedIn,
  orderController.createOrderByOfferID
);
routes.get("/order/offer/verify/:id", isLoggedIn, orderController.verifyOrderByUserIdANDOfferID);
routes.put("/order/offer/fulfilled/:id", isLoggedIn, orderController.fulfilled)

// ===============Order (Request) related routes===============
routes.get(
  "/order/request/info/:id",
  isLoggedIn,
  orderController.getOrderByOrderID
);
routes.post(
  "/order/request/createNewOrder",
  isLoggedIn,
  orderController.createOrderByRequestID
);

// Offers related routes
routes.get("/soonExpireOffers", offerController.getSoonExpireTest);
// 23/9 updated by LC
routes.get("/offer_list", offerController.listOffers);
// 30/9 updated by LC
routes.get("/offer_list/:hashtag", offerController.listOffersByHashTag);
routes.get("/offerInfo/:id", offerController.getOfferInfoByID);
routes.get("/hashtag_list", hashTagController.getAllHashtag);
routes.post("/createOffer", isLoggedIn, offerController.createOffer); //@SubmitOffer thunks

// ===============Streamer Reviews related routes===============
routes.post(
  "/reviews/streamer/createNewReview",
  isLoggedIn,
  streamerReviewsController.createStreamerReview
);

// ===============Audience Reviews related routes===============
routes.post(
  "/reviews/audience/createNewReview",
  isLoggedIn,
  audienceReviewsController.createAudienceReview
);

// ===============Brain Tree related routes===============
routes.get(
  "/braintree/clientToken",
  isLoggedIn,
  brainTreeController.getClientToken
);

routes.post(
  "/braintree/createTransaction",
  isLoggedIn,
  brainTreeController.createTransaction
);
