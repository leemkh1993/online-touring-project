import { Bearer } from "permit";
import { UserService } from "./Services/userService";
import { Request, Response, NextFunction } from "express";
import jwtSimple from "jwt-simple";
import jwt from "./jwt";

const permit = new Bearer({
  query: "access_token",
});

export function createIsLoggedIn(service: UserService) {
  return async function (req: Request, res: Response, next: NextFunction) {
    try {
      // Check if client has token stored in its localStore
      const token = permit.check(req);

      if (token) {
        // Reveal payload that was created when logging in
        const payload = jwtSimple.decode(token, jwt.jwtSecret);

        // Check if user existed in DB
        const getUser = await service.getUserInfoByID(payload.id);
        if (getUser) {
          // Remove password and add all other info to new object
          const { password, ...others } = getUser;
          req.user = others;
          return next();
        } else {
          return res.status(401).json({ UserNotFound: true });
        }
      } else {
        return res.status(401).json({ AccessDenied: true });
      }
    } catch (err) {
      console.log(err.message);
      res.status(500).json({ ServerError: true });
    }
  };
}
