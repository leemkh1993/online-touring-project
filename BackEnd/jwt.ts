export default {
    // For creating the token
    jwtSecret: "Iamasecretthatyoushouldneverrevealtoanyone",
    // Whether to use JWT token to maintain connection or logged in
    jwtSession: {
        session: false
    }
};