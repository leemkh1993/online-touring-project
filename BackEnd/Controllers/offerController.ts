import { OfferService } from "../Services/offerService";
import { Request, Response } from "express";
import { DestinationsService } from "../Services/destinationsService";
import { HashTagService } from "../Services/hashtagService";
import { StreamerReviewsService } from "../Services/streamerReviewsService";
import { AudienceReviewsService } from "../Services/audienceReviewsService";
import { CitiesService } from "../Services/citiesService";
import { CountriesService } from "../Services/countriesService";
import { v4 as uuidv4 } from "uuid";
import { SitesService } from "../Services/siteService";

export class OfferController {
  constructor(
    private offerService: OfferService,
    private destinationsService: DestinationsService,
    private hashtagService: HashTagService,
    private streamerReviewsService: StreamerReviewsService,
    private audienceReviewsService: AudienceReviewsService,
    private countriesService: CountriesService,
    private citiesService: CitiesService,
    private siteService: SitesService
  ) {}

  getSoonExpire = async (req: Request, res: Response) => {
    try {
      const getData = await this.offerService.getSoonExpire();
      res.status(200).json(getData);
    } catch (err) {
      console.log(`Offer Controller Error: ${err.message}`);
      res.status(500).json({ ServerError: true });
    }
  };

  getSoonExpireTest = async (req: Request, res: Response) => {
    try {
      const getData = await this.offerService.getSoonExpireTest();
      res.status(200).json(getData);
    } catch (err) {
      console.log(`Offer Controller Error: ${err.message}`);
      res.status(500).json({ ServerError: true });
    }
  };

  getOfferInfoByID = async (req: Request, res: Response) => {
    try {
      const offerID: number = parseInt(req.params.id);
      if (offerID) {
        const offerData = await this.offerService.getOfferByID(offerID);
        if (offerData) {
          const destinationData = await this.destinationsService.destinationsByOfferID(
            offerID
          );
          const hashtags = await this.hashtagService.getHashtagByOfferID(
            offerID
          );
          const streamerReviews = await this.streamerReviewsService.getStreamersReviewsByOfferID(
            offerID
          );
          const audienceReviews = await this.audienceReviewsService.getAudienceReviewsByOfferID(
            offerID
          );
          // 23/9 Error show react only receive array instead of obj., added [] to include the object by LC
          res.status(200).json([
            {
              offer: offerData,
              destinations: destinationData,
              hashtag: hashtags,
              streamerReviews: streamerReviews,
              audience_reviews: audienceReviews,
            },
          ]);
        } else {
          res.status(404).json({ OfferNotFound: true });
        }
      } else {
        res.status(400).json({ MissingID: true });
      }
    } catch (err) {
      console.log(`Offer Controller Error: ${err.message}`);
      res.status(500).json({ ServerError: true });
    }
  };

  createOffer = async (req: Request, res: Response) => {
    try {
      const userID = req.user?.id;
      if (userID) {
        const offerName = req.body.offerName;
        const offerRoomKey = uuidv4();
        const offerCost = Number(parseFloat(req.body.cost).toFixed(2));
        const expiryDate = req.body.expiryDate;
        const offerDes = req.body.offerDescription
          ? req.body.offerDescription
          : "Perhaps a little secrets would work too..";
        const offerDesDetails = req.body.offerDescriptionDetails
          ? req.body.offerDescriptionDetails
          : "Maybe a little more secrets...";
        const offerPic = req.file
          ? req.file.filename
          : "default_offer-request_pic.jpeg";
        const destinations = req.body.destinations;
        if (
          offerName &&
          offerCost &&
          expiryDate &&
          offerCost &&
          offerDes &&
          destinations
        ) {
          const createOffer: any = await this.offerService.createOffer(
            userID,
            offerName,
            offerRoomKey,
            offerCost,
            expiryDate,
            offerDes,
            offerPic,
            offerDesDetails
          );
          const hashtags: string[] = req.body.hashtags;
          const addHashtag: any = await this.hashtagService.createHashtag(
            hashtags
          );
          //@ts-ignore
          const linkHashtags = await this.hashtagService.linkHashtagToOffer(
            createOffer[0].id,
            addHashtag
          );
          // Add destinations
          for (let i = 0; i < 3; i++) {
            const siteName = destinations[i].siteName
              ? destinations[i].siteName
              : `Place ${i + 1}`;
            const siteDes = destinations[i].description
              ? destinations[i].description
              : "Come see this..";
            const siteLat = destinations[i].lat;
            const siteLng = destinations[i].lng;
            const sitePic = destinations[i].sitePic
              ? destinations[i].sitePic
              : "default_img.png";
            const siteCountry = destinations[i].country
              ? destinations[i].country
              : "Secret..";
            const siteCity = destinations[i].city
              ? destinations[i].city
              : "Secret..";
         
            if (siteCountry && siteCity) {
              const addCountry = await this.countriesService.createCountry(
                siteCountry
              );
              const addCity = await this.citiesService.createCity(
                addCountry,
                siteCity
              );
              const addSite: any = await this.siteService.createSites(
                siteName,
                siteDes,
                siteLat,
                siteLng,
                sitePic,
                addCountry,
                addCity
              );
              //@ts-ignore
              const addDestination = await this.destinationsService.createDestinations(
                addSite[0].id,
                createOffer[0].id
              );
            } else {
              const addSite: any = await this.siteService.createSites(
                siteName,
                siteDes,
                siteLat,
                siteLng,
                sitePic
              );
              //@ts-ignore
              const addDestination = await this.destinationsService.createDestinations(
                createOffer[0].id,
                addSite[0].id
              );
            }
          }
          const offerData = await this.offerService.getOfferByID(
            createOffer[0].id
          );
          const destinationData = await this.destinationsService.destinationsByOfferID(
            createOffer[0].id
          );
          const hashtagsData = await this.hashtagService.getHashtagByOfferID(
            createOffer[0].id
          );
          res.status(200).json({
            offer: offerData,
            destinations: destinationData,
            hashtag: hashtagsData,
          });
        } else {
          res.status(400).json({ MissingEssentialData: true });
        }
      } else {
        res.status(400).json({ MissingID: true });
      }
    } catch (err) {
      console.log(`Offer Controller Error: ${err.message}`);
      res.status(500).json({ ServerError: true });
    }
  };

  // 30/9 updated by LC
  listOffers = async (req: Request, res: Response) => {
    try {
      const getData = await this.offerService.getAllOffersV2();
      res.status(200).json(getData);
    } catch (err) {
      console.log(`Offer Controller Error: ${err.message}`);
      res.status(500).json({ ServerError: true });
    }
  };

  listOffersByHashTag = async (req: Request, res: Response) => {
    try {
      const HashTag: string = decodeURI(req.params.hashtag);
      const getData = await this.offerService.getAllOffersV3ByHashTag(HashTag);
      res.status(200).json(getData);
    } catch (err) {
      console.log(`Offer Controller Error: ${err.message}`);
      res.status(500).json({ ServerError: true });
    }
  };
}
