import { UserService } from "../Services/userService";
import { Request, Response } from "express";
import { hashPW, checkPW } from "../hash";
import jwtSimple from "jwt-simple";
import jwt from "../jwt";
import { StreamerReviewsService } from "../Services/streamerReviewsService";
import { AudienceReviewsService } from "../Services/audienceReviewsService";
import { PaymentRecordsService } from "../Services/paymentRecordsService";
import { OfferService } from "../Services/offerService";
import { RequestService } from "../Services/requestService";
import { OrderService } from "../Services/orderService";
import { LanguageService } from "../Services/languagesService";
import { Bearer } from "permit";
import { CountriesService } from "../Services/countriesService";
import { CitiesService } from "../Services/citiesService";
import { AgeRangeService } from "../Services/ageRangeService";

export class UserController {
  constructor(
    private userService: UserService,
    private steamerReviewsService: StreamerReviewsService,
    private audienceReviewsService: AudienceReviewsService,
    private paymentRecordService: PaymentRecordsService,
    private offerService: OfferService,
    private requestService: RequestService,
    private orderService: OrderService,
    private langService: LanguageService,
    private countryService: CountriesService,
    private citiesService: CitiesService,
    private ageRangeService: AgeRangeService
  
  ) {}

  userInfoByEmail = async (req: Request, res: Response) => {
    try {
      //30/9 changed to check token as discussed, by LC
      const permit = new Bearer({
        query: "access_token",
      });
      const token = permit.check(req);

      if (token) {
        //@ts-ignore
        const payload = jwtSimple.decode(token, jwt.jwtSecret);
        const userEmail = req.user?.email;
        if (userEmail) {
          const userData = await this.userService.getUserInfoByEmail(userEmail);
          if (userData) {
            const streamerReviews = await this.steamerReviewsService.getStreamersReviewsByUserID(
              userData.id
            );
            const audienceReviews = await this.audienceReviewsService.getAudienceReviewsByUserID(
              userData.id
            );
            const paymentRecords = await this.paymentRecordService.getPaymentRecordsByID(
              userData.id
            );
            const offerRecords = await this.offerService.getOffersByUserID(
              userData.id
            );
            const requestRecords = await this.requestService.getRequestsByUserID(
              userData.id
            );
            const orderRecords = await this.orderService.getOrdersByUserID(
              userData.id
            );
            const langList = await this.langService.getUserLanguagesByID(
              userData.id
            );
            res.status(200).json([
              {
                user: userData,
                Language: langList,
                offerRecords: offerRecords,
                requestRecords: requestRecords,
                paymentRecords: paymentRecords,
                orderRecords: orderRecords,
                streamerReviews: streamerReviews,
                audienceReviews: audienceReviews,
              },
            ]);
          } else {
            res.status(404).json({ UserNotFound: true });
          }
        } else {
          res.status(400).json({ MissingID: true });
        }
      }
    } catch (err) {
      console.log(err.message);
      res.status(500).json({ ServerError: true });
    }
  };

  createUser = async (req: Request, res: Response) => {
    try {
      const {
        email,
        password,
        firstName,
        lastName,
        ageRange,
        country,
        city,
        languages,
        profession = " ",
        description = " ",
        profile_pic = "profile_pic1.png",
      } = req.body;
      if (email && password) {
        const getUser = await this.userService.getUserInfoByEmail(email);
        if (!getUser) {
          const getCountryID = await this.countryService.getCountryByCountryName(country);
          const countryID = getCountryID? getCountryID.id: await this.countryService.createCountry(country);
          const getCityID = await this.citiesService.getCityByCityName(city)
          const cityID =  getCityID? getCityID.id: await this.citiesService.createCity(countryID, city)
          const ageRangeID = await this.ageRangeService.getIdByValueOrCreate(ageRange)
          const hashedPW = await hashPW(password);
          const createUser = await this.userService.createUser(
            email,
            hashedPW,
            firstName,
            lastName,
            ageRangeID,
            countryID,
            cityID,
            profession,
            description,
            profile_pic
          );
          const payload = {
            id: createUser[0],
            email: email,
            password: password,
            first_name: firstName,
            last_name: lastName,
            age_range_id: ageRange,
            country_id: country,
            city_id: city,
            profession: profession,
            description: description,
            profile_pic: profile_pic,
            credit: 0.0,
          };
          const getLangID = await this.langService.getIdByValueOrCreate(languages);
          //@ts-ignore
          const linkUser = await this.langService.linkLangIdToUser(createUser[0], getLangID);
          const token = jwtSimple.encode(payload, jwt.jwtSecret);
          res.status(200).json({ token: token });
        } else {
          res.status(403).json({ UsernameExisted: true });
        }
      } else {
        res.status(400).json({ ClientError: true });
      }
    } catch (err) {
      console.log(err.message);
      res.status(500).json({ ServerError: true });
    }
  };

  login = async (req: Request, res: Response) => {
    try {
      const { email, password } = req.body;
      if (email && password) {
        const getUser = await this.userService.getUserInfoByEmail(email);
        if (getUser) {
          const check = await checkPW(password, getUser.password);
          if (check) {
            const payload = {
              id: getUser.id,
              email: getUser.email,
              first_name: getUser.first_name,
              last_name: getUser.last_name,
              age_range: getUser.range,
              country: getUser.country,
              city: getUser.city,
              profession: getUser.profession,
              description: getUser.description,
              profile_pic: getUser.profile_pic,
              credit: getUser.credit,
            };
            const token = jwtSimple.encode(payload, jwt.jwtSecret);
            res.status(200).json({ token: token });
          } else {
            res.status(401).json({ WrongInputInfo: true });
          }
        } else {
          res.status(404).json({ WrongInputInfo: true });
        }
      } else {
        res.status(400).json({ ClientError: true });
      }
    } catch (err) {
      console.log(err.message);
      res.status(500).json({ ServerError: true });
    }
  };

  restoreLogin = async (req: Request, res: Response) => {
    try {
      const getUser = req.user;
      res.status(200).json({ email: getUser?.email });
    } catch (err) {
      console.log(err.message);
      res.status(500).json({ ServerError: true });
    }
  };

  topStreamer = async (req: Request, res: Response) => {
    try {
      const list = await this.steamerReviewsService.getTopStreamers();
      res.status(200).json({ data: list });
    } catch (err) {
      console.log(err.message);
      res.status(500).json({ ServerError: true });
    }
  };

  //24/9 for fetching basic intro of user in offer page by LC
  userInfoById = async (req: Request, res: Response) => {
    try {
      const offerID: number = parseInt(req.params.id);
      if (offerID) {
        const data = await this.userService.getUserInfoByID(offerID);
        res.status(200).json(data);
      }
    } catch (err) {
      console.log(err.message);
      res.status(500).json({ ServerError: true });
    }
  };
}
