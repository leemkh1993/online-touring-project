import { Request, Response } from "express";
import { RequestService } from "../Services/requestService";
import { DestinationsService } from "../Services/destinationsService";
import { HashTagService } from "../Services/hashtagService";
import { CountriesService } from "../Services/countriesService";
import { CitiesService } from "../Services/citiesService";
import { v4 as uuidv4 } from "uuid";
import { SitesService } from "../Services/siteService";

export class RequestController {
  constructor(
    private requestService: RequestService,
    private hashtagService: HashTagService,
    private destinationsService: DestinationsService,
    private countriesService: CountriesService,
    private citiesService: CitiesService,
    private siteService: SitesService
  ) {}

  getAllRequest = async (req: Request, res: Response) => {
    try {
      const results = await this.requestService.getAllRequest();
      res.status(200).json(results)
    } catch (err) {
      console.log(`Request Controller Error: ${err.message}`);
      res.status(500).json({ ServerError: true });
    }
  };

  getRequestInfoByID = async (req: Request, res: Response) => {
    try {
      const requestID: number = parseInt(req.params.id);
      if (requestID) {
        const requestData: any = await this.requestService.getRequestByID(requestID);
        if (requestData) {
          const destinationData = await this.destinationsService.destinationsByRequestID(
            requestID
          );
          const hashtags = await this.hashtagService.getHashtagByRequestID(
            requestID
          );
          res.status(200).json({
            request: requestData,
            destinations: destinationData,
            hashtag: hashtags,
          });
        } else {
          res.status(404).json({ OfferNotFound: true });
        }
      } else {
        res.status(400).json({ MissingID: true });
      }
    } catch (err) {
      console.log(`Request Controller Error: ${err.message}`);
      res.status(500).json({ ServerError: true });
    }
  };

  createRequest = async (req: Request, res: Response) => {
    try {
      const userID = req.user?.id;
      if (userID) {
        const requestName = req.body.requestName;
        const requestRoomKey = uuidv4();
        const requestReward = Number(parseFloat(req.body.reward).toFixed(2));
        const expiryDate = req.body.expiryDate;
        const requestDes = req.body.requestDescription;
        const requestDesDetails = req.body.requestDescriptionDetails
          ? req.body.requestDescriptionDetails
          : "";
        const requestPic = req.file
          ? req.file.filename
          : "default_offer:request_pic.png";
        const destinations = req.body.destinations;
        if (
          requestName &&
          requestReward &&
          expiryDate &&
          requestDes &&
          destinations
        ) {
          const createRequest: any = await this.requestService.createRequest(
            userID,
            requestName,
            requestRoomKey,
            requestReward,
            expiryDate,
            requestDes,
            requestDesDetails,
            requestPic
          );
          const hashtags: string[] = req.body.hashtags;
          const addHashtag: any = await this.hashtagService.createHashtag(
            hashtags
          );
          //@ts-ignore
          const linkHashtags = await this.hashtagService.linkHashtagToRequest(
            createRequest.id,
            addHashtag
          );
          // Add destinations
          for (let destination of destinations) {
            const siteName = destination.siteName ? destination.siteName : "";
            const siteDes = destination.description
              ? destination.description
              : "";
            const siteLat = destination.lat;
            const siteLng = destination.lng;
            const sitePic = destination.sitePic
              ? destination.sitePic
              : "default_offer:request_pic.png";
            const siteCountry = destination.country ? destination.country : "";
            const siteCity = destination.city ? destination.city : "";
            if (siteCountry && siteCity) {
              const addCountry = await this.countriesService.createCountry(
                siteCountry
              );
              const addCity = await this.citiesService.createCity(
                addCountry,
                siteCity
              );
              const addSite: any = await this.siteService.createSites(
                siteName,
                siteDes,
                siteLat,
                siteLng,
                sitePic,
                addCountry,
                addCity
              );
              //@ts-ignore
              const addDestination = await this.destinationsService.createDestinationsByRequestID(
                addSite[0].id,
                createRequest.id
              );
            } else {
              const addSite: any = await this.siteService.createSites(
                siteName,
                siteDes,
                siteLat,
                siteLng,
                sitePic
              );
              //@ts-ignore
              const addDestination = await this.destinationsService.createDestinationsByRequestID(
                addSite[0].id,
                createRequest.id
              );
            }
          }
          const requestData = await this.requestService.getRequestByID(
            createRequest.id
          );
          const destinationData = await this.destinationsService.destinationsByRequestID(
            createRequest.id
          );
          const hashtagsData = await this.hashtagService.getHashtagByRequestID(
            createRequest.id
          );
          res.status(200).json({
            request: requestData,
            destinations: destinationData,
            hashtag: hashtagsData,
          });
        } else {
          res.status(400).json({ MissingEssentialData: true });
        }
      } else {
        res.status(400).json({ MissingID: true });
      }
    } catch (err) {
      console.log(`Request Controller Error: ${err.message}`);
      res.status(500).json({ ServerError: true });
    }
  };
}
