import { Response, Request } from "express";
import { AudienceReviewsService } from "../Services/audienceReviewsService";

export class AudienceReviewsController {
  constructor(private audienceReviewService: AudienceReviewsService) {}

  createAudienceReview = async (req: Request, res: Response) => {
    try {
      const userID = req.user?.id;
      if (userID) {
        const audienceID = userID;
        const orderID = parseInt(req.body.orderID);
        if (audienceID && orderID) {
          const comment = req.body.comment ? req.body.comment : null;
          const rating = req.body.rating
            ? Number(parseFloat(req.body.rating).toFixed(1))
            : null;
          if (comment || rating) {
            const createReview = await this.audienceReviewService.createAudienceReviews(
              audienceID,
              orderID,
              comment,
              rating
            );
            res
              .status(200)
              .json({ created: true, streamerReview: createReview });
          } else {
            res.status(400).json({ NeedMoreInfo: true });
          }
        } else {
          res.status(400).json({ NeedIDs: true });
        }
      } else {
        res.status(401).json({ NotLoggedIn: true });
      }
    } catch (err) {
      console.log(`Audience Review Controller Error: ${err.message}`);
      res.status(500).json({ ServerError: true });
    }
  };
}
