import { HashTagService } from "../Services/hashtagService";
import { Request, Response } from "express";

export class HashtagController {
  constructor(private hashtagService: HashTagService) {}

  getAllHashtag = async (req:Request, res:Response)=>{
    try{
      const getData = await this.hashtagService.getAllHashtag();
      res.status(200).json(getData);
    } catch(err){
      console.log(`Hashtag Controller Error: ${err.message}`);
      res.status(500).json({ServerError:true});
    }
  }
}
