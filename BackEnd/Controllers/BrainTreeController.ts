import { Request, Response } from "express";
import { brainTreeGateway } from "../server";
import { OfferService } from "../Services/offerService";
import { OrderService } from "../Services/orderService";
import { PaymentRecordsService } from "../Services/paymentRecordsService";

export class BrainTreeController {
  constructor(
    private paymentRecordService: PaymentRecordsService,
    private orderService: OrderService,
    private offerService: OfferService
  ) {}

  getClientToken = async (req: Request, res: Response) => {
    try {
      const userID = req.user?.id.toString();
      const brainTreeToken = await brainTreeGateway.clientToken.generate({
        customerId: userID,
      });
      res.status(200).json(brainTreeToken);
    } catch (err) {
      console.log(`BrainTreeController Error: ${err.message}`);
      res.status(500).json({ ServerError: true });
    }
  };

  createTransaction = async (req: Request, res: Response) => {
    try {
      const { offerID, amount, nonce, startDate } = req.body;
      const endDate = req.body.endDate? req.body.endDate: null;
      if (offerID && amount && nonce) {
        const transaction: any = await brainTreeGateway.transaction.sale({
          amount: amount,
          paymentMethodNonce: nonce,
        });
        if (transaction.success) {
          const paypalID = transaction.transaction.paypal.paymentId;
          const getOffer = await this.offerService.getOfferByID(offerID);
          const parseAmount = parseFloat(amount)
          const createPaymentRecord: any = await this.paymentRecordService.createPaymentRecord(
            req.user?.id,
            getOffer.guide_id,
            paypalID,
            parseAmount
          );
          
          const createOrder: any = await this.orderService.createOrderByOfferID(offerID, createPaymentRecord[0].id, req.user?.id, startDate, endDate );
          res.status(200).json(createOrder);
        } else {
          res.status(401).json({ TransactionFailed: true });
        }
      } else {
        res.status(400).json({ EssentialDataMissing: true });
      }
    } catch (err) {
      console.log(`BrainTreeController Error: ${err.message}`);
      res.status(500).json({ ServerError: true });
    }
  };
}
