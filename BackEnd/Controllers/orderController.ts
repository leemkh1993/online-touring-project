import { Request, Response } from "express";
import { OrderService } from "../Services/orderService";
import { OfferService } from "../Services/offerService";
import { RequestService } from "../Services/requestService";
import { DestinationsService } from "../Services/destinationsService";
import { HashTagService } from "../Services/hashtagService";
import { PaymentRecordsService } from "../Services/paymentRecordsService";
import { UserService } from "../Services/userService";
import { LanguageService } from "../Services/languagesService";

export class OrderController {
  constructor(
    private orderService: OrderService,
    private offerService: OfferService,
    private requestService: RequestService,
    private destinationService: DestinationsService,
    private hashtagService: HashTagService,
    private paymentRecordService: PaymentRecordsService,
    private userService: UserService,
    private langService: LanguageService
  ) {}

  getOrderByOrderID = async (req: Request, res: Response) => {
    try {
      const orderID: number = parseInt(req.params.id);
      if (orderID) {
        const orderData = await this.orderService.getOrderByOrderID(orderID);
        const ordererData = await this.userService.getUserInfoByID(
          orderData.audience_id
        );
        const { password, ...other } = ordererData;
        const ordererLang = await this.langService.getUserLanguagesByID(
          ordererData.id
        );
        const offerData = await this.offerService.getOfferByID(
          orderData.offer_id
        );
        const destinationData = await this.destinationService.destinationsByOfferID(
          orderData.offer_id
        );
        const audiencesData = await this.orderService.getAudiencesOfSameEvent(
          orderData.offer_id,
          orderData.order_start_date
        );
        res.status(200).json({
          order: orderData,
          orderer: other,
          ordererLang: ordererLang,
          offerData: offerData,
          destination: destinationData,
          audiences: audiencesData,
        });
      } else {
        res.status(400).json({ OfferIDNotFound: true });
      }
    } catch (err) {
      console.log(err.message);
      res.status(500).json({ ServerError: true });
    }
  };

  getOrderByRequestID = async (req: Request, res: Response) => {
    try {
      const orderID: number = parseInt(req.params.id);
      if (orderID) {
        const orderData = await this.orderService.getOrderByOrderID(orderID);
        const requestData = await this.requestService.getRequestByID(
          orderData.request_id
        );
        const destinationData = await this.destinationService.destinationsByOfferID(
          orderData.request_id
        );
        res.status(200).json({
          order: orderData,
          requestData: requestData,
          destination: destinationData,
        });
      } else {
        res.status(400).json({ OfferIDNotFound: true });
      }
    } catch (err) {
      console.log(err.message);
      res.status(500).json({ ServerError: true });
    }
  };

  createOrderByOfferID = async (req: Request, res: Response) => {
    try {
      const {
        offerID,
        payeeID,
        recipientID,
        amount,
        paypalID,
        startDate,
      } = req.body;
      const endDate = req.body.endDate ? req.body.endDate : null;
      if (
        offerID &&
        payeeID &&
        recipientID &&
        amount &&
        paypalID &&
        startDate
      ) {
        const createPaymentRecord: any = await this.paymentRecordService.createPaymentRecord(
          payeeID,
          recipientID,
          paypalID,
          amount
        );
        const createOrder: any = await this.orderService.createOrderByOfferID(
          offerID,
          createPaymentRecord[0].id,
          req.user?.id,
          startDate,
          endDate
        );
        const orderDetail = await this.orderService.getOrderByOrderID(
          createOrder[0].id
        );
        const destinationData = await this.destinationService.destinationsByOfferID(
          createOrder[0].offer_id
        );
        const hashtags = await this.hashtagService.getHashtagByOfferID(
          createOrder[0].offer_id
        );
        res.status(200).json({
          order: orderDetail,
          destinations: destinationData,
          hashtags: hashtags,
        });
      } else {
        res.status(400).json({ EssentialDataMissing: true });
      }
    } catch (err) {
      console.log(err.message);
      res.status(500).json({ ServerError: true });
    }
  };

  createOrderByRequestID = async (req: Request, res: Response) => {
    try {
      const {
        requestID,
        payeeID,
        recipientID,
        amount,
        paypalID,
        startDate,
        endDate,
      } = req.body;
      if (
        requestID &&
        payeeID &&
        recipientID &&
        amount &&
        paypalID &&
        startDate &&
        endDate
      ) {
        const createPaymentRecord: any = await this.paymentRecordService.createPaymentRecord(
          payeeID,
          recipientID,
          paypalID,
          amount
        );
        const createOrder: any = await this.orderService.createOrderByRequestID(
          requestID,
          createPaymentRecord[0].id,
          req.user?.id,
          startDate,
          endDate
        );
        const orderDetail: any = await this.orderService.getOrderByOrderIDRequestID(
          createOrder[0].id,
          createOrder[0].request_id
        );
        const destinationData = await this.destinationService.destinationsByRequestID(
          createOrder[0].request_id
        );
        const hashtags = await this.hashtagService.getHashtagByRequestID(
          createOrder[0].request_id
        );
        res.status(200).json({
          order: orderDetail,
          destinations: destinationData,
          hashtags: hashtags,
        });
      } else {
        res.status(400).json({ EssentialDataMissing: true });
      }
    } catch (err) {
      console.log(err.message);
      res.status(500).json({ ServerError: true });
    }
  };

  getMyOrders = async (req: Request, res: Response) => {
    try {
      const userID = req.user?.id;
      const orders = await this.orderService.getOrdersByUserID(userID);
      res.status(200).json(orders);
    } catch (err) {
      console.log(`Order Controller Error: ${err.message}`);
      res.status(500).json({ ServerError: true });
    }
  };

  getMyAudienceOrders = async (req: Request, res: Response) => {
    try {
      const userID = req.user?.id;
      const orders = await this.orderService.getAudienceOrdersByUserID(userID);
      res.status(200).json(orders);
    } catch (err) {
      console.log(`Order Controller Error: ${err.message}`);
      res.status(500).json({ ServerError: true });
    }
  };

  verifyOrderByUserIdANDOfferID = async (req: Request, res: Response) => {
    try {
      const userID = req.user?.id;
      const offerID = parseInt(req.params.id);
      const getOrder: number = await this.orderService.verify(userID, offerID);
      if (getOrder) {
        res.status(302).json(getOrder);
      } else {
        res.status(204).json({ OrderNotFound: true });
      }
    } catch (err) {
      console.log(`Offer Controller Error: ${err.message}`);
      res.status(500).json({ ServerError: true });
    }
  };

  fulfilled = async (req: Request, res: Response) => {
    try {
      const orderID = parseInt(req.params.id);
      const getOrder = await this.orderService.fulfilled(orderID);
      if (getOrder) {
        res.status(200).json({ Done: true });
      } else {
        res.status(400).json({ Declined: true });
      }
    } catch (err) {
      console.log(`Offer Controller Error: ${err.message}`);
      res.status(500).json({ ServerError: true });
    }
  };
}
