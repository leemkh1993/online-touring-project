import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
  async function hasTable(tableName: string) {
    const check = await knex.schema.hasTable(tableName);
    return check;
  }

  hasTable("age_range")
  .then(async (boolean) => {
    if (!boolean) {
      await knex.schema.createTable("age_range", (table) => {
        table.increments();
        table.string("range").notNullable().unique();
        table.timestamps(false, true);
      });
    }
  })
  .catch((err) => {
    console.log(err.message);
  });

  hasTable("languages")
  .then(async (boolean) => {
    if (!boolean) {
      await knex.schema.createTable("languages", (table) => {
        table.increments();
        table.string("language_name").notNullable();
        table.timestamps(false, true);
      });
    }
  })
  .catch((err) => {
    console.log(err.message);
  });

  hasTable("countries")
    .then(async (boolean) => {
      if (!boolean) {
        await knex.schema.createTable("countries", (table) => {
          table.increments();
          table.string("country").notNullable();
          table.timestamps(false, true);
        });
      }
    })
    .catch((err) => {
      console.log(err.message);
    });

  hasTable("cities")
    .then(async (boolean) => {
      if (!boolean) {
        await knex.schema.createTable("cities", (table) => {
          table.increments();
          table.integer("country_id").notNullable();
          table.foreign("country_id").references("countries.id")
          table.string("city").notNullable();
          table.timestamps(false, true);
        });
      }
    })
    .catch((err) => {
      console.log(err.message);
    });

  hasTable("users")
    .then(async (boolean) => {
      if (!boolean) {
        await knex.schema.createTable("users", (table) => {
          table.increments();
          table.string("email").notNullable().unique();
          table.string("password").notNullable();
          table.string("profile_pic");
          table.string("first_name").notNullable();
          table.string("last_name").notNullable();
          table.integer("age_range_id");
          table.foreign("age_range_id").references("age_range.id");
          table.string("profession");
          table.text("description");
          table.integer("country_id").notNullable();
          table.foreign("country_id").references("countries.id");
          table.integer("city_id").notNullable();
          table.foreign("city_id").references("cities.id");
          table.decimal("credit", 10, 2).unsigned();
          table.timestamps(false, true);
        });
      }
    })
    .catch((err) => {
      console.log(err.message);
    });

    hasTable("user_languages")
    .then(async (boolean) => {
      if (!boolean) {
        await knex.schema.createTable("user_languages", (table) => {
          table.increments();
          table.integer("user_id").notNullable().unsigned();
          table.foreign("user_id").references("users.id");
          table.integer("language_id").notNullable().unsigned();
          table.foreign("language_id").references("languages.id");
          table.timestamps(false, true);
        });
      }
    })
    .catch((err) => {
      console.log(err.message);
    });

    hasTable("hashtags")
    .then(async (boolean) => {
      if (!boolean) {
        await knex.schema.createTable("hashtags", (table) => {
          table.increments();
          table.string("hashtag").notNullable().unique()
          table.timestamps(false, true);
        });
      }
    })
    .catch((err) => {
      console.log(err.message);
    });

    hasTable("offers")
    .then(async (boolean) => {
      if (!boolean) {
        await knex.schema.createTable("offers", (table) => {
          table.increments();
          table.integer("user_id").notNullable();
          table.foreign("user_id").references("users.id");
          table.string("name").notNullable();
          table.text("description");
          table.text("description_details");
          table.decimal("cost", 10, 2);
          table.string("journey_pic");
          table.uuid("offer_room_key").notNullable();
          table.timestamp("expiry_date", true);
          table.timestamps(false, true);
        })
      }
    })
    .catch ((err) => {
      console.log(err.message)
    });

    hasTable("requests")
    .then(async (boolean) => {
      if (!boolean) {
        await knex.schema.createTable("requests", (table) => {
          table.increments();
          table.integer("user_id").notNullable();
          table.foreign("user_id").references("users.id");
          table.string("name").notNullable();
          table.text("description");
          table.text("description_details");
          table.decimal("reward", 10, 2);
          table.string("journey_pic");
          table.uuid("request_room_key").notNullable();
          table.timestamp("expiry_date", true);
          table.boolean("fulfilled");
          table.timestamps(false, true);
        })
      }
    })
    .catch ((err) => {
      console.log(err.message)
    });

    hasTable("hashtags_offers")
    .then(async (boolean) => {
      if (!boolean) {
        await knex.schema.createTable("hashtags_offers", (table) => {
          table.increments();
          table.integer("hashtag_id").notNullable();
          table.foreign("hashtag_id").references("hashtags.id")
          table.integer("offer_id");
          table.foreign("offer_id").references("offers.id");
          table.integer('request_id');
          table.foreign("request_id").references("requests.id");
          table.timestamps(false, true);
        })
      }
    })
    .catch ((err) => {
      console.log(err.message)
    });

    hasTable("payment_records")
    .then(async (boolean) => {
      if (!boolean) {
        await knex.schema.createTable("payment_records", (table) => {
          table.increments();
          table.string("paypal_id").notNullable();
          table.integer("payee_id").notNullable();
          table.foreign("payee_id").references("users.id")
          table.integer("recipient_id").notNullable();
          table.foreign("recipient_id").references("users.id")
          table.decimal("amount", 10, 2);
          table.timestamps(false, true);
        })
      }
    })
    .catch ((err) => {
      console.log(err.message)
    });

    hasTable("orders")
    .then(async (boolean) => {
      if (!boolean) {
        await knex.schema.createTable("orders", (table) => {
          table.increments();
          table.integer("request_id");
          table.foreign("request_id").references("requests.id");
          table.integer("offer_id");
          table.foreign("offer_id").references("offers.id");
          table.integer("payment_record_id").notNullable();
          table.foreign("payment_record_id").references("payment_records.id")
          table.integer("audience_id").notNullable();
          table.foreign("audience_id").references("users.id");
          table.timestamp("start_date", true).notNullable();
          table.timestamp("end_date", true);
          table.boolean("fulfilled");
          table.timestamps(false, true);
        })
      }
    })
    .catch ((err) => {
      console.log(err.message)
    });

    hasTable("streamer_reviews")
    .then(async (boolean) => {
      if (!boolean) {
        await knex.schema.createTable("streamer_reviews", (table) => {
          table.increments();
          table.integer("user_id").notNullable();
          table.foreign("user_id").references("users.id");
          table.integer("order_id").notNullable();
          table.foreign("order_id").references("orders.id");
          table.text("comments");
          table.decimal("rating", 2, 1)
          table.timestamps(false, true);
        })
      }
    })
    .catch ((err) => {
      console.log(err.message)
    });

    hasTable("audience_reviews")
    .then(async (boolean) => {
      if (!boolean) {
        await knex.schema.createTable("audience_reviews", (table) => {
          table.increments();
          table.integer("user_id").notNullable();
          table.foreign("user_id").references("users.id");
          table.integer("order_id").notNullable();
          table.foreign("order_id").references("orders.id");
          table.text("comments");
          table.decimal("rating", 2, 1)
          table.timestamps(false, true);
        })
      }
    })
    .catch ((err) => {
      console.log(err.message)
    });

    hasTable("sites")
    .then(async (boolean) => {
      if (!boolean) {
        await knex.schema.createTable("sites", (table) => {
          table.increments();
          table.integer("country_id").notNullable();
          table.foreign("country_id").references("countries.id");
          table.integer("city_id").notNullable();
          table.foreign("city_id").references("cities.id");
          table.string("site_name").notNullable();
          table.text("description");
          table.string("site_pic");
          table.decimal("longitude", 10, 6).notNullable();
          table.decimal("latitude", 10, 6).notNullable();
          table.timestamps(false, true);
        })
      }
    })
    .catch ((err) => {
      console.log(err.message)
    });

    hasTable("destinations")
    .then(async (boolean) => {
      if (!boolean) {
        await knex.schema.createTable("destinations", (table) => {
          table.increments();
          table.integer("offer_id");
          table.foreign("offer_id").references("offers.id");
          table.integer("request_id");
          table.foreign("request_id").references("requests.id");
          table.integer("site_id").notNullable();
          table.foreign("site_id").references("sites.id");
          table.timestamps(false, true);
        })
      }
    })
    .catch ((err) => {
      console.log(err.message)
    });
}

export async function down(knex: Knex): Promise<void> {
  await knex.raw("drop table age_range cascade");
  await knex.raw("drop table languages cascade");
  await knex.raw("drop table countries cascade");
  await knex.raw("drop table cities cascade");
  await knex.raw("drop table users cascade");
  await knex.raw("drop table user_languages cascade");
  await knex.raw("drop table hashtags cascade");
  await knex.raw("drop table offers cascade");
  await knex.raw("drop table requests cascade");
  await knex.raw("drop table hashtags_offers cascade");
  await knex.raw("drop table orders cascade");
  await knex.raw("drop table payment_records cascade");
  await knex.raw("drop table streamer_reviews cascade");
  await knex.raw("drop table audience_reviews cascade");
  await knex.raw("drop table sites cascade");
  await knex.raw("drop table destinations cascade");
}

