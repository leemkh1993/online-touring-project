import Knex from "knex";

export class DestinationsService {
  constructor(private knex: Knex) {}

  destinationsByOfferID = async (offerID: number) => {
    try {
      const result = await this.knex("destinations")
        .select(
          "countries.country",
          "cities.city",
          "sites.site_name",
          "sites.description",
          "sites.site_pic",
          "sites.longitude",
          "sites.latitude"
        )
        .innerJoin("sites", "destinations.site_id", "sites.id")
        .innerJoin("countries", "sites.country_id", "countries.id")
        .innerJoin("cities", "sites.city_id", "cities.id")
        .where("offer_id", offerID);
      return result;
    } catch (err) {
      console.log(`Destinations Service Error: ${err.message}`);
      return undefined;
    }
  };

  destinationsByRequestID = async (requestID: number) => {
    try {
      const result = await this.knex("destinations")
        .select(
          "countries.country",
          "cities.city",
          "sites.site_name",
          "sites.description",
          "sites.site_pic",
          "sites.longitude",
          "sites.latitude"
        )
        .innerJoin("sites", "destinations.site_id", "sites.id")
        .innerJoin("countries", "sites.country_id", "countries.id")
        .innerJoin("cities", "sites.city_id", "cities.id")
        .where("request_id", requestID);
      return result;
    } catch (err) {
      console.log(`Destinations Service Error: ${err.message}`);
      return undefined;
    }
  };

  createDestinations = async (siteID: number, offerID: number) => {
    try {
      const createData = await this.knex("destinations")
        .insert([{ offer_id: offerID, site_id: siteID }])
        .returning("id");
      return createData;
    } catch (err) {
      console.log(`Destinations Service Error: ${err.message}`);
      return undefined;
    }
  };

  createDestinationsByRequestID = async (siteID: number, requestID: number) => {
    try {
      const createData = await this.knex("destinations")
        .insert([{ request_id: requestID, site_id: siteID }])
        .returning("id");
      return createData;
    } catch (err) {
      console.log(`Destinations Service Error: ${err.message}`);
      return undefined;
    }
  };
}
