import Knex from "knex";

export class CountriesService {
  constructor(private knex: Knex) {}

  getCountryByCountryName = async (countryName: string) => {
    try {
      const result = await this.knex("countries")
        .select("*")
        .where("country", countryName);
      return result[0];
    } catch (err) {
      console.log(`Countries Service Error: ${err.message}`);
      return undefined;
    }
  };

  createCountry = async (countryName: string) => {
    try {
      const result = await this.knex("countries").insert([
        { country: countryName }
      ]).returning("id")
      return result[0];
    } catch (err) {
      console.log(`Countries Service Error: ${err.message}`);
      return undefined;
    }
  };
}
