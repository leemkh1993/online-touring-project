import Knex from "knex";

export class StreamerReviewsService {
  constructor(private knex: Knex) {}

  getStreamersReviewsByOfferID = async (offerID: number) => {
    try {
      const result = await this.knex("streamer_reviews").select(
        "offers.id as offer_id",
        "streamer_reviews.rating",
        "streamer_reviews.comments",
        "users.first_name as audience_first_name",
        "users.last_name as audience_last_name",
        "users.profile_pic as audience_profile_pic",
        "streamer_reviews.created_at"
      )
      .innerJoin("orders", "streamer_reviews.order_id", "orders.id")
      .innerJoin("offers", "orders.offer_id", "offers.id")
      .innerJoin("users", "orders.audience_id", "users.id")
      .where("offers.id", offerID);
      const plusOffsetEndD =
      result[0].created_at.getTime() -
      result[0].created_at.getTimezoneOffset() * 60000;
    result[0].created_at = plusOffsetEndD;
      return result;
    } catch (err) {
      console.log(`Streamer Reviews Service Error: ${err.message}`);
      return undefined;
    }
  };

  getStreamersReviewsByUserID = async (userID: number) => {
    try {
      const result = await this.knex("streamer_reviews").select("*").where("user_id", userID);
      return result;
    } catch (err) {
      console.log(`Streamer Reviews Service Error: ${err.message}`);
      return undefined;
    }
  };

  getTopStreamers = async () => {
    try {
      const results = await this.knex("streamer_reviews")
        .select(
          "user_id",
          "users.email",
          "age_range.range",
          "users.profile_pic",
          "users.first_name",
          "users.last_name",
          "users.profession",
          "users.description",
          "countries.country",
          "cities.city"
        )
        .avg("rating as average_rating")
        .innerJoin("users", "streamer_reviews.user_id", "users.id")
        .innerJoin("age_range", "users.age_range_id", "age_range.id")
        .innerJoin("countries", "users.country_id", "countries.id")
        .innerJoin("cities", "users.city_id", "cities.id")
        .groupBy(
          "user_id",
          "users.email",
          "age_range.range",
          "users.profile_pic",
          "users.first_name",
          "users.last_name",
          "users.profession",
          "users.description",
          "countries.country",
          "cities.city"
        )
        .orderBy("average_rating", "desc")
        .limit(5);
        for (let result of results) {
          const avgRating = parseFloat(result.average_rating);
          const round = Number(avgRating.toFixed(1));
          result.average_rating = round;
        }
      return results;
    } catch (err) {
      console.log(`Streamer Reviews Service Error: ${err.message}`);
      return undefined;
    }
  };

  createStreamerReviews = async (streamerID: number, orderID: number, comments?: string | null, rating?: number | null) => {
    try {
        if (comments && rating) {
            const add = await this.knex("streamer_reviews").insert([
                { user_id: streamerID, order_id: orderID, comments: comments, rating: rating }
            ]).returning("*");
            return add
        } else if (comments && !rating) {
            const add = await this.knex("streamer_reviews").insert([
                { user_id: streamerID, order_id: orderID, comments: comments }
            ]).returning("*");
            return add
        } else if (!comments && rating) {
            const add = await this.knex("streamer_reviews").insert([
                { user_id: streamerID, order_id: orderID, rating: rating }
            ]).returning("*");
            return add
        } else {
            return undefined
        };
    } catch(err) {
        console.log(`streamer Reviews Service Error: ${err.message}`);
        return undefined;
    };
};
}
