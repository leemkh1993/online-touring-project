import Knex from "knex";
import { LanguageService } from "./languagesService";

export class OrderService {
  constructor(private knex: Knex, private langService: LanguageService) {}

  getAudiencesOfSameEvent = async (offerID: number, time: number) => {
    try {
      const date = new Date(time);
      const UTCString = date.toUTCString();
      const results = await this.knex("orders")
        .select(
          "orders.id as order_id",
          "users.id as audience_id",
          "users.first_name as audience_first_name",
          "users.last_name as audience_last_name",
          "users.description",
          "users.profession",
          "users.profile_pic"
        )
        .innerJoin("users", "orders.audience_id", "users.id")
        .where("orders.start_date", UTCString)
        .where("orders.offer_id", offerID)
        .where("fulfilled", false);
      for (let result of results) {
        const userID = result.audience_id;
        const langList = await this.langService.getUserLanguagesByID(userID);
        result.Languages = langList;
      }
      return results;
    } catch (err) {
      console.log(`Order Service Error: ${err.message}`);
      return undefined;
    }
  };

  getOrderByOrderID = async (orderID: number) => {
    try {
      const results = await this.knex("orders")
        .select(
          "orders.id as order_id",
          "orders.offer_id",
          "orders.audience_id",
          "orders.start_date as order_start_date ",
          "orders.end_date as order_end_date ",
          "offers.description as offer_description",
          "offers.description_details as offer_description_details",
          "payment_records.id as payment_record_id",
          "payment_records.amount as payment_record_amount",
          "payment_records.paypal_id",
          "users.email as guide_email",
          "users.first_name as guide_first_name",
          "users.last_name as guide_last_name",
          "users.profile_pic as guide_profile_pic"
        )
        .innerJoin("offers", "orders.offer_id", "offers.id")
        .innerJoin(
          "payment_records",
          "orders.payment_record_id",
          "payment_records.id"
        )
        .innerJoin("users", "offers.user_id", "users.id")
        .where("orders.id", orderID);
      const plusOffsetStartD =
        results[0].order_start_date.getTime() -
        results[0].order_start_date.getTimezoneOffset() * 60000;
      results[0].order_start_date = plusOffsetStartD;
      const plusOffsetEndD =
        results[0].order_end_date.getTime() -
        results[0].order_end_date.getTimezoneOffset() * 60000;
      results[0].order_end_date = plusOffsetEndD;
      return results[0];
    } catch (err) {
      console.log(`Order Service Error: ${err.message}`);
      return undefined;
    }
  };

  getOrderByOrderIDRequestID = async (orderID: number, requestID: number) => {
    try {
      const results = await this.knex("orders")
        .select(
          "orders.id as order_id",
          "orders.request_id",
          "orders.start_date as order_start_date",
          "orders.end_date as order_end_date",
          "requests.description as requests.description",
          "requests.description_details as requests.description_details",
          "payment_records.id as payment_record_id",
          "payment_records.amount as payment_record_amount",
          "payment_records.paypal_id",
          "users.email as guide_email",
          "users.first_name as guide_first_name",
          "users.last_name as guide_last_name",
          "users.profile_pic as guide_profile_pic"
        )
        .innerJoin("requests", "orders.request_id", "requests.id")
        .innerJoin(
          "payment_records",
          "orders.payment_record_id",
          "payment_records.id"
        )
        .innerJoin("users", "requests.user_id", "users.id")
        .where("orders.id", orderID)
        .where("orders.request_id", requestID);
      console.log(`order ${results}`);
      const plusOffsetStartD =
        results[0].order_start_date.getTime() -
        results[0].order_start_date.getTimezoneOffset() * 60000;
      results[0].order_start_date = plusOffsetStartD;
      const plusOffsetEndD =
        results[0].order_end_date.getTime() -
        results[0].order_end_date.getTimezoneOffset() * 60000;
      results[0].order_end_date = plusOffsetEndD;
      return results[0];
    } catch (err) {
      console.log(`Order Service Error: ${err.message}`);
      return undefined;
    }
  };
  // 29/9 added timestamp by LC
  getOrdersByUserID = async (userID: number) => {
    try {
      const results = await this.knex("orders")
        .select(
          "offers.id as offer_id",
          "offers.name",
          "offers.description",
          "offers.cost",
          "offers.journey_pic",
          "offers.offer_room_key",
          "offers.expiry_date as offer_expiry_date",
          "offers.description as offer_description",
          "offers.description_details as offer_description_details",
          "payment_records.id as payment_record_id",
          "payment_records.amount as payment_amount",
          "payment_records.paypal_id",
          "users.email as audience_email",
          "users.first_name as audience_first_name",
          "users.last_name as audience_last_name",
          "users.profile_pic as audience_profile_pic",
          "orders.id as order_id",
          "orders.start_date as order_start_date",
          "orders.end_date as order_end_date",
          "orders.fulfilled",
          "orders.updated_at"
        )
        .innerJoin("offers", "orders.offer_id", "offers.id")
        .innerJoin(
          "payment_records",
          "orders.payment_record_id",
          "payment_records.id"
        )
        .innerJoin("users", "orders.audience_id", "users.id")
        .where("offers.user_id", userID);
      for (let result of results) {
        const plusOffsetExpiryD =
          result.offer_expiry_date.getTime() -
          result.offer_expiry_date.getTimezoneOffset() * 60000;
        result.offer_expiry_date = plusOffsetExpiryD;
        const plusOffsetStartD =
          result.order_start_date.getTime() -
          result.order_start_date.getTimezoneOffset() * 60000;
        result.order_start_date = plusOffsetStartD;
        if (result.order_end_date) {
          const plusOffsetEndD =
            result.order_end_date.getTime() -
            result.order_end_date.getTimezoneOffset() * 60000;
          result.order_end_date = plusOffsetEndD;
        }
      }
      return results;
    } catch (err) {
      console.log(`Order Service Error: ${err.message}`);
      return undefined;
    }
  };

  getAudienceOrdersByUserID = async (userID: number) => {
    try {
      const results = await this.knex("orders")
        .select(
          "offers.id as offer_id",
          "offers.name",
          "offers.description",
          "offers.cost",
          "offers.journey_pic",
          "offers.offer_room_key",
          "offers.expiry_date as offer_expiry_date",
          "offers.description as offer_description",
          "offers.description_details as offer_description_details",
          "payment_records.id as payment_record_id",
          "payment_records.amount as payment_amount",
          "payment_records.paypal_id",
          "users.email as audience_email",
          "users.first_name as audience_first_name",
          "users.last_name as audience_last_name",
          "users.profile_pic as audience_profile_pic",
          "orders.id as order_id",
          "orders.start_date as order_start_date",
          "orders.end_date as order_end_date",
          "orders.fulfilled",
          "orders.updated_at"
        )
        .innerJoin("offers", "orders.offer_id", "offers.id")
        .innerJoin(
          "payment_records",
          "orders.payment_record_id",
          "payment_records.id"
        )
        .innerJoin("users", "orders.audience_id", "users.id")
        .where("orders.audience_id", userID);
      for (let result of results) {
        const plusOffsetExpiryD =
          result.offer_expiry_date.getTime() -
          result.offer_expiry_date.getTimezoneOffset() * 60000;
        result.offer_expiry_date = plusOffsetExpiryD;
        const plusOffsetStartD =
          result.order_start_date.getTime() -
          result.order_start_date.getTimezoneOffset() * 60000;
        result.order_start_date = plusOffsetStartD;
        if (result.order_end_date) {
          const plusOffsetEndD =
            result.order_end_date.getTime() -
            result.order_end_date.getTimezoneOffset() * 60000;
          result.order_end_date = plusOffsetEndD;
        }
      }
      return results;
    } catch (err) {
      console.log(`Order Service Error: ${err.message}`);
      return undefined;
    }
  };


  createOrderByOfferID = async (
    offerID: number,
    paymentRecordID: number,
    audienceID: number,
    startDate: string,
    endDate: string | null
  ) => {
    try {
      const addData = await this.knex("orders")
        .insert([
          {
            offer_id: offerID,
            payment_record_id: paymentRecordID,
            audience_id: audienceID,
            start_date: startDate,
            end_date: endDate,
            fulfilled: false,
          },
        ])
        .returning("*");
      const plusOffsetStartD =
        addData[0].start_date.getTime() -
        addData[0].start_date.getTimezoneOffset() * 60000;
      addData[0].start_date = plusOffsetStartD;
      if (addData[0].end_date) {
        const plusOffsetEndD =
          addData[0].end_date.getTime() -
          addData[0].end_date.getTimezoneOffset() * 60000;
        addData[0].end_date = plusOffsetEndD;
      }
      return addData;
    } catch (err) {
      console.log(`Order Service Error: ${err.message}`);
      return undefined;
    }
  };

  createOrderByRequestID = async (
    requestID: number,
    paymentRecordID: number,
    audienceID: number,
    startDate: string,
    endDate?: string | null
  ) => {
    try {
      const addData = await this.knex("orders")
        .insert([
          {
            request_id: requestID,
            payment_record_id: paymentRecordID,
            audience_id: audienceID,
            start_date: startDate,
            end_date: endDate ? endDate : null,
            fulfilled: false,
          },
        ])
        .returning("*");
      return addData;
    } catch (err) {
      console.log(`Order Service Error: ${err.message}`);
      return undefined;
    }
  };

  verify = async (userID: number, offerID: number) => {
    try {
      const getData = await this.knex("orders")
        .select("*")
        .where("offer_id", offerID)
        .where("audience_id", userID)
        .where("fulfilled", false);
      if (getData[0]) {
        const plusOffsetStartD =
        getData[0].start_date.getTime() -
        getData[0].start_date.getTimezoneOffset() * 60000;
      getData[0].start_date = plusOffsetStartD; 
      return getData[0].start_date
      } else {
        return false
      }
    } catch (err) {
      console.log(`Order Service Error: ${err.message}`);
      return undefined;
    }
  };

  fulfilled = async (orderID: number) => {
    try {
      const getData = await this.knex("orders")
        .where("id", orderID)
        .update({ fulfilled: true });
      return getData
    } catch (err) {
      console.log(`Order Service Error: ${err.message}`);
      return undefined;
    }

  }
}
