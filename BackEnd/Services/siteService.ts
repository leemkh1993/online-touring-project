import Knex from "knex";

export class SitesService {
  constructor(private knex: Knex) {}

  createSites = async (
    siteName: string,
    siteDes: string,
    siteLat: number,
    siteLng: number,
    sitePic: string,
    countryID?: number,
    cityID?: number
  ) => {
    try {
      if (countryID && cityID) {
        const result = await this.knex("sites").insert([
          {
            country_id: countryID,
            city_id: cityID,
            site_name: siteName,
            description: siteDes,
            site_pic: sitePic,
            longitude: siteLng,
            latitude: siteLat,
          },
        ]).returning("*");
        return result;
      } else {
        const result = await this.knex("sites").insert([
          {
            site_name: siteName,
            description: siteDes,
            site_pic: sitePic,
            longitude: siteLng,
            latitude: siteLat,
          },
        ]).returning("*");
        return result;
      }
    } catch (err) {
      console.log(`Site Service Error: ${err.message}`);
      return undefined;
    }
  };
}
