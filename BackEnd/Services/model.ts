export {};

declare global {
  namespace Express {
    interface Request {
      user: {
        id: number;
        email: string;
        first_name: string;
        last_name: string;
        range: string;
        country: string;
        city: string;
        profession: string;
        description: string;
        profile_pic: string;
      };
    }
  }
}
