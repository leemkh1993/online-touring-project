import Knex from "knex";

export class CitiesService {
  constructor(private knex: Knex) {}

  getCityByCityName = async (cityName: string) => {
    try {
      const result = await this.knex("cities")
        .select("*")
        .where("city", cityName);
      return result[0];
    } catch (err) {
      console.log(`Cities Service Error: ${err.message}`);
      return undefined;
    }
  };

  createCity = async (countryID: number, cityName: string) => {
    try {
      const result = await this.knex("cities").insert([
        { country_id: countryID, city: cityName }
      ]).returning("id");
      return result[0];
    } catch (err) {
      console.log(`Cities Service Error: ${err.message}`);
      return undefined;
    }
  };
}
