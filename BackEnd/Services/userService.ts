import Knex from "knex";
import "./model";

export class UserService {
  constructor(private knex: Knex) {}

  getUserInfoByID = async (userID: number) => {
    const getUserByID = await this.knex("users").select(
      "users.id",
      "users.email",
      "users.password",
      "users.profile_pic",
      "users.first_name",
      "users.last_name",
      "age_range.range",
      "users.profession",
      "users.description",
      "countries.country",
      "cities.city",
      "users.credit"
      )
      .innerJoin(
        "countries",
        "users.country_id",
        "countries.id"
      )
      .innerJoin(
        "cities",
        "users.city_id",
        "cities.id"
      )
      .innerJoin(
        "age_range",
        "users.age_range_id",
        "age_range.id"
      )
      .where(
        "users.id",
        userID
      )
      .first();
    return getUserByID
  };

  getUserInfoByEmail = async (email: string) => {
    const getUserByEmail = await this.knex("users").select(
      "users.id",
      "users.email",
      "users.password",
      "users.profile_pic",
      "users.first_name",
      "users.last_name",
      "age_range.range",
      "users.profession",
      "users.description",
      "countries.country",
      "cities.city",
      "users.credit"
      )
      .innerJoin(
        "countries",
        "users.country_id",
        "countries.id"
      )
      .innerJoin(
        "cities",
        "users.city_id",
        "cities.id"
      )
      .innerJoin(
        "age_range",
        "users.age_range_id",
        "age_range.id"
      )
      .where(
        "users.email",
        email
      )
      .first();
    return getUserByEmail
  };

  createUser = async (
    email: string,
    password: string,
    firstName: string,
    lastName: string,
    age_range_id: number,
    country_id: number,
    city_id: number,
    profession: string,
    description: string,
    profile_pic: string
  ) => {
    return this.knex("users")
      .insert({
        email: email,
        password: password,
        first_name: firstName,
        last_name: lastName,
        age_range_id: age_range_id,
        country_id: country_id,
        city_id: city_id,
        profession: profession,
        description: description,
        profile_pic: profile_pic,
        credit: 0.00
      })
      .returning("id");
  };
}
