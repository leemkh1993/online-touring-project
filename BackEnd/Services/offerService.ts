import Knex from "knex";

export class OfferService {
  constructor(private knex: Knex) {}

  getOffersByUserID = async (userID: number) => {
    try {
      const results = await this.knex("offers")
        .select("*")
        .where("user_id", userID);
      if (results) {
        for (let result of results) {
          const plusOffset =
            result.expiry_date.getTime() -
            result.expiry_date.getTimezoneOffset() * 60000;
          result.expiry_date = plusOffset;
        }
      }
      return results;
    } catch (err) {
      console.log(err.message);
      return undefined;
    }
  };

  getSoonExpire = async () => {
    try {
      const results = await this.knex.raw(
        "select * from offers where expiry_date > now() at time zone 'utc' and age(expiry_date, now() at time zone 'utc') < make_time(6, 00, 00) order by expiry_date ASC limit 5;"
      );
      for (let result of results) {
        const plusOffset =
          result.expiry_date.getTime() -
          result.expiry_date.getTimezoneOffset() * 60000;
        result.expiry_date = plusOffset;
      }
      return results;
    } catch (err) {
      console.log(err.message);
      return undefined;
    }
  };

  getSoonExpireTest = async () => {
    try {
      const results = await this.knex("offers")
        .select("*")
        .orderBy("expiry_date", "asc")
        .limit(5);
      for (let result of results) {
        const plusOffset =
          result.expiry_date.getTime() -
          result.expiry_date.getTimezoneOffset() * 60000;
        result.expiry_date = plusOffset;
      }
      return results;
    } catch (err) {
      console.log(err.message);
      return undefined;
    }
  };

  // 30/9 updated by LC
  getAllOffersV2 = async () => {
    try {
      const results = await this.knex("offers")
        .select(
          "offers.id",
          "offers.journey_pic",
          "offers.name",
          "offers.cost",
          "offers.expiry_date",
          "offers.journey_pic",
          "offers.description",
          "offers.description_details",
          "offers.updated_at",
          "offers.user_id",
          "users.profile_pic as user_profile_pic",
          "users.profession as user_profession"
        )
        .innerJoin("users", "offers.user_id", "users.id")
        .orderBy("expiry_date", "asc");
      for (let result of results) {
        const plusOffset =
          result.expiry_date.getTime() -
          result.expiry_date.getTimezoneOffset() * 60000;
        result.expiry_date = plusOffset;
        const getHashtags = await this.knex("hashtags_offers")
          .select("hashtag")
          .innerJoin("hashtags", "hashtags_offers.hashtag_id", "hashtags.id")
          .where("hashtags_offers.offer_id", result.id);
        const hashtagList = []
        for (let hashtag of getHashtags) {
          hashtagList.push(hashtag.hashtag)
        }
        result.hashtags = hashtagList
      }
      return results;
    } catch (err) {
      console.log(err.message);
      return undefined;
    }
  };

  getAllOffersV3ByHashTag = async (hashtag: string) => {
    try {
      const results = await this.knex("hashtags_offers")
        .select(
          "hashtags.id",
          "hashtags.hashtag",
          "offers.id as offer_id",
          "offers.journey_pic",
          "offers.name",
          "offers.cost",
          "offers.expiry_date",
          "offers.journey_pic",
          "offers.description",
          "offers.description_details",
          "offers.updated_at",
          "offers.user_id",
          "users.profile_pic as user_profile_pic",
          "users.profession as user_profession"
        )
        .innerJoin("offers", "hashtags_offers.offer_id", "offers.id")
        .innerJoin("hashtags", "hashtags_offers.hashtag_id", "hashtags.id")
        .innerJoin("users", "offers.user_id", "users.id")
        .where("hashtags.hashtag", hashtag)
        .orderBy("expiry_date", "asc");
      for (let result of results) {
        const plusOffset =
          result.expiry_date.getTime() -
          result.expiry_date.getTimezoneOffset() * 60000;
        result.expiry_date = plusOffset;
        const profilePic = await this.knex("users")
          .select("profile_pic")
          .where("id", result.user_id)
          .first();
        result.profile_pic = profilePic.profile_pic;
        const hashtags = await this.knex("hashtags_offers")
          .select("hashtag")
          .innerJoin("hashtags", "hashtags_offers.hashtag_id", "hashtags.id")
          .where("hashtags_offers.offer_id", result.id);
        const list = [];
        for (let hashtag of hashtags) {
          list.push(hashtag.hashtag);
        }
        result.hashtags = list;
      }
      return results;
    } catch (err) {
      console.log(err.message);
      return undefined;
    }
  };

  getOfferByID = async (offerID: number) => {
    try {
      const results = await this.knex("offers")
        .select(
          "offers.id as offer_id",
          "offers.name as offer_name",
          "offers.description as offer_description",
          "offers.description_details as offer_description_details",
          "offers.cost as offer_cost",
          "offers.journey_pic as offer_journey_pic",
          "offers.offer_room_key",
          "offers.expiry_date as offer_expiry_date",
          "users.id as guide_id",
          "users.email as guide_email",
          "users.first_name as guide_first_name",
          "users.last_name as guide_last_name",
          "users.profile_pic as guide_profile_pic",
          "countries.country as guide_country",
          "cities.city as guide_city",
          "age_range.range as guide_age_range"
        )
        .innerJoin("users", "offers.user_id", "users.id")
        .innerJoin("countries", "users.country_id", "countries.id")
        .innerJoin("cities", "users.city_id", "cities.id")
        .innerJoin("age_range", "users.age_range_id", "age_range.id")
        .where("offers.id", offerID)
        .first();
      const langList = [];
      const lang = await this.knex("user_languages")
        .select("language_name")
        .innerJoin("languages", "user_languages.language_id", "languages.id")
        .where("user_languages.user_id", results.guide_id);
      for (let lan of lang) {
        langList.push(lan.language_name);
      }
      results.langList = langList;
      const plusOffset =
        results.offer_expiry_date.getTime() -
        results.offer_expiry_date.getTimezoneOffset() * 60000;
      results.offer_expiry_date = plusOffset;
      return results;
    } catch (err) {
      console.log(err.message);
      return undefined;
    }
  };

  createOffer = async (
    userID: number,
    name: string,
    offerRoomKey: string,
    cost: number,
    expiryDate: string,
    description: string,
    pic: string,
    desDetails: string
  ) => {
    try {
      const add = await this.knex("offers")
        .insert([
          {
            user_id: userID,
            name: name,
            description: description,
            cost: cost,
            journey_pic: pic,
            offer_room_key: offerRoomKey,
            expiry_date: expiryDate,
            description_details: desDetails,
          },
        ])
        .returning("*");
      return add;
    } catch (err) {
      console.log(err.message);
      return undefined;
    }
  };
}
