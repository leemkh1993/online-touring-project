import Knex from "knex";

export class AgeRangeService {
  constructor(private knex: Knex) {}

  getIdByValueOrCreate = async (range: string) => {
    try {
      const getID = await this.knex("age_range")
        .select("id")
        .where("range", range)
        .first();
      if (getID) {
        return getID.id;
      } else {
        const createID: { id: number }[] = await this.knex("age_range")
          .insert({ range: range })
          .returning("id");
        return createID[0];
      }
    } catch (err) {
      console.log(`ageRangeService Error: ${err.message}`);
      return undefined;
    }
  };
}
