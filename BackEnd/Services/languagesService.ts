import Knex from "knex";

export class LanguageService {
  constructor(private knex: Knex) {}

  getUserLanguagesByID = async (userID: number) => {
    try {
      const results = await this.knex("user_languages")
        .select("languages.language_name")
        .innerJoin("languages", "user_languages.language_id", "languages.id")
        .where("user_languages.user_id", userID);
      const langList = [];
      for (let result of results) {
        langList.push(result.language_name);
      }
      return langList;
    } catch (err) {
      console.log(`Languages Service Error: ${err.message}`);
      return undefined;
    }
  };

  getIdByValueOrCreate = async (langName: string) => {
    try {
      const getID = await this.knex("languages")
        .select("id")
        .where("language_name", langName)
        .first();
      if (getID) {
        return getID.id;
      } else {
        const createID: { id: number }[] = await this.knex("languages")
          .insert({ language_name: langName })
          .returning("id");
        return createID[0];
      }
    } catch (err) {
      console.log(`ageRangeService Error: ${err.message}`);
      return undefined;
    }
  };

  linkLangIdToUser = async (userID: number, langID: number) => {
    try {
      const getData = await this.knex("user_languages").select("*").where("user_id", userID).where("language_id", langID).first();
      if (getData) {
        return getData.id
      } else {
        const createData = await this.knex("user_languages")
          .insert([
            { user_id: userID, language_id: langID }
          ]).returning("id")
        return createData[0]
      }
    } catch (err) {
      console.log(`ageRangeService Error: ${err.message}`);
      return undefined;
    }
  }
}
