import Knex from "knex";

export class HashTagService {
  constructor(private knex: Knex) {}

  getHashtagByOfferID = async (requestID: number) => {
    try {
      const results = await this.knex("hashtags_offers")
        .select("hashtags.hashtag")
        .innerJoin("hashtags", "hashtags_offers.hashtag_id", "hashtags.id")
        .where("hashtags_offers.offer_id", requestID);
      const hashtagList: string[] = []
      for (let result of results) {
        hashtagList.push(result.hashtag)
      }
      return hashtagList;
    } catch (err) {
      console.log(`HashTag Service Error: ${err.message}`);
      return undefined;
    }
  };

  getHashtagByRequestID = async (requestID: number) => {
    try {
        const results = await this.knex("hashtags_offers")
          .select("hashtags.hashtag")
          .innerJoin("hashtags", "hashtags_offers.hashtag_id", "hashtags.id")
          .where("hashtags_offers.request_id", requestID);
        const hashtagList: string[] = []
        for (let result of results) {
          hashtagList.push(result.hashtag)
        }
        return hashtagList;
      } catch (err) {
        console.log(`HashTag Service Error: ${err.message}`);
        return undefined;
      }
  };

  getAllHashtag = async () => {
    try {
        const results = await this.knex("hashtags")
          .select("*");
        const hashtagList: string[] = []
        for (let result of results) {
          hashtagList.push(result.hashtag)
        }
        return hashtagList;
      } catch (err) {
        console.log(`HashTag Service Error: ${err.message}`);
        return undefined;
      }
  };

  createHashtag = async (hashtags: string[]) => {
    try {
        if (hashtags[0]) {
            const listID: number[] = []
            for (let hashtag of hashtags ) {
              const check = await this.knex("hashtags").select("*").where("hashtag", hashtag);
              if (!check[0]) {
                const add = await this.knex("hashtags").insert([
                    { hashtag: hashtag }
                ]).returning("id");
                listID.push(add[0]);
              } else {
                listID.push(check[0].id);
              }
            }
            return listID;
        } else {
            return []
        }
    } catch(err) {
        console.log(`HashTag Service Error: ${err.message}`);
        return undefined;
    }
  };

  linkHashtagToOffer = async (offerID: number, hashtagIDList: number[]) => {
    try {
        if (hashtagIDList[0]) {
            const listID: number[] = []
            for (let ID of hashtagIDList) {
                const add = await this.knex("hashtags_offers").insert([
                    { hashtag_id: ID, offer_id: offerID }
                ]).returning("id");
                listID.push(add[0]);
            }
            return listID;
        } else {
            return [];
        }
    } catch(err) {
        console.log(`HashTag Service Error: ${err.message}`);
        return undefined;
    }
  }
 
  linkHashtagToRequest = async (requestID: number, hashtagIDList: number[]) => {
    try {
        if (hashtagIDList[0]) {
            const listID: number[] = []
            for (let ID of hashtagIDList) {
                const add = await this.knex("hashtags_offers").insert([
                    { hashtag_id: ID, request_id: requestID }
                ]).returning("id");
                listID.push(add[0]);
            }
            return listID;
        } else {
            return [];
        }
    } catch(err) {
        console.log(`HashTag Service Error: ${err.message}`);
        return undefined;
    }
  }
}
