import Knex from "knex";

export class AudienceReviewsService {
  constructor(private knex: Knex) {}

  getAudienceReviewsByOfferID = async (offerID: number) => {
    try {
      const result = await this.knex("audience_reviews")
        .select(
          "offers.id as offer_id",
          "audience_reviews.rating",
          "audience_reviews.comments",
          "users.first_name as audience_first_name",
          "users.last_name as audience_last_name",
          "users.profile_pic as audience_profile_pic",
          "audience_reviews.created_at"
        )
        .innerJoin("orders", "audience_reviews.order_id", "orders.id")
        .innerJoin("offers", "orders.offer_id", "offers.id")
        .innerJoin("users", "orders.audience_id", "users.id")
        .where("offers.id", offerID);
      const plusOffsetEndD =
        result[0].created_at.getTime() -
        result[0].created_at.getTimezoneOffset() * 60000;
      result[0].created_at = plusOffsetEndD;
      return result;
    } catch (err) {
      console.log(`Audience Reviews Service Error: ${err.message}`);
      return undefined;
    }
  };

  getAudienceReviewsByUserID = async (userID: number) => {
    try {
      const result = await this.knex("audience_reviews")
        .select("*")
        .where("user_id", userID);
      return result;
    } catch (err) {
      console.log(`Audience Reviews Service Error: ${err.message}`);
      return undefined;
    }
  };

  createAudienceReviews = async (
    audienceID: number,
    orderID: number,
    comments?: string | null,
    rating?: number | null
  ) => {
    try {
      // removed .first() by LC since error >>>> Audience Reviews Service Error: Cannot chain .first() on "insert" query!
      if (comments && rating) {
        const add = await this.knex("audience_reviews")
          .insert([
            {
              user_id: audienceID,
              order_id: orderID,
              comments: comments,
              rating: rating,
            },
          ])
          .returning("*");
        return add;
      } else if (comments && !rating) {
        const add = await this.knex("audience_reviews")
          .insert([
            { user_id: audienceID, order_id: orderID, comments: comments },
          ])
          .returning("*");
        return add;
      } else if (!comments && rating) {
        const add = await this.knex("audience_reviews")
          .insert([{ user_id: audienceID, order_id: orderID, rating: rating }])
          .returning("*");
        return add;
      } else {
        return undefined;
      }
    } catch (err) {
      console.log(`Audience Reviews Service Error: ${err.message}`);
      return undefined;
    }
  };
}
