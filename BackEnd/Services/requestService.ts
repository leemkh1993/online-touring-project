import Knex from "knex";

export class RequestService {
  constructor(private knex: Knex) {}

  getAllRequest = async () => {
    try {
      const results = await this.knex("requests")
        .select("*")
        .where("requests.fulfilled", false);
      if (results[0]) {
        for (let result of results) {
          const plusOffset =
            result.expiry_date.getTime() -
            result.expiry_date.getTimezoneOffset() * 60000;
          result.expiry_date = plusOffset;
        }
      }
      return results
    } catch (err) {
      console.log(`Request Service Error: ${err.message}`);
      return undefined;
    }
  };

  getRequestsByUserID = async (userID: number) => {
    try {
      const results = await this.knex("requests")
        .select("*")
        .where("user_id", userID);
      if (results[0]) {
        for (let result of results) {
          const plusOffset =
            result.expiry_date.getTime() -
            result.expiry_date.getTimezoneOffset() * 60000;
          result.expiry_date = plusOffset;
        }
      }
      return results;
    } catch (err) {
      console.log(`Request Service Error: ${err.message}`);
      return undefined;
    }
  };

  getRequestByID = async (requestID: number) => {
    try {
      const results = await this.knex("requests")
        .select("*")
        .innerJoin("users", "requests.user_id", "users.id")
        .where("requests.id", requestID);
      if (results[0]) {
        for (let result of results) {
          const plusOffset =
            result.expiry_date.getTime() -
            result.expiry_date.getTimezoneOffset() * 60000;
          result.expiry_date = plusOffset;
        }
      }
      //@ts-ignore
      const {password, ...others} = results[0]
      console.log(others)
      return others;
    } catch (err) {
      console.log(`Request Service Error: ${err.message}`);
      return undefined;
    }
  };

  createRequest = async (
    userID: number,
    name: string,
    requestRoomKey: string,
    reward: number,
    expiryDate: string,
    description: string,
    desDetails: string,
    pic: string
  ) => {
    try {
      const add = await this.knex("requests")
        .insert([
          {
            user_id: userID,
            name: name,
            description: description,
            reward: reward,
            journey_pic: pic,
            description_details: desDetails,
            request_room_key: requestRoomKey,
            expiry_date: expiryDate,
            fulfilled: false,
          },
        ])
        .returning("*");
      return add[0];
    } catch (err) {
      console.log(err.message);
      return undefined;
    }
  };
}
