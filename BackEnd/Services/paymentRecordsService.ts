import Knex from "knex";

export class PaymentRecordsService {
  constructor(private knex: Knex) {}
  // 29/9 added additional info by LC
  getPaymentRecordsByID = async (userID: number) => {
    try {
      const payeeRecords = await this.knex("payment_records")
        .select(
          "payment_records.id as payment_record_id",
          "payment_records.paypal_id",
          "payment_records.updated_at",
          "orders.id as order_id",
          "orders.start_date as start_date",
          "orders.end_date as end_date",
          "users.email as recipient_email",
          "users.first_name as recipient_first_name",
          "users.last_name as recipient_last_name",
          "payment_records.amount as payment_amount",
          "orders.offer_id as offer_id",
          "offers.name as joined_tour_name",
          "offers.journey_pic as joined_tour_pic",
          "offers.offer_room_key as joined_tour_rm_key",
        )
        .innerJoin("users", "payment_records.recipient_id", "users.id")
        .innerJoin("orders", "payment_records.id", "orders.payment_record_id")
        .innerJoin("offers","orders.offer_id","offers.id")
        .where("payee_id", userID);
        for (let payeeRecord of payeeRecords) {
          const plusOffsetStartD =
          payeeRecord.start_date.getTime() -
          payeeRecord.start_date.getTimezoneOffset() * 60000;
          payeeRecord.start_date = plusOffsetStartD;
        }
      const recipientRecord = await this.knex("payment_records")
        .select(
          "payment_records.id as payment_record_id",
          "orders.id as order_id",
          "users.email as payee_email",
          "users.first_name as payee_first_name",
          "users.last_name as payee_last_name",
          "payment_records.amount as payment_amount"
        )
        .innerJoin("users", "payment_records.payee_id", "users.id")
        .innerJoin("orders", "payment_records.id", "orders.payment_record_id")
        .where("recipient_id", userID);
      return { payeeRecord: payeeRecords, recipientRecord: recipientRecord };
    } catch (err) {
      console.log(`Payment Record Service Error: ${err.message}`);
      return undefined;
    }
  };

  createPaymentRecord = async (
    payeeID: number,
    recipientID: number,
    paypalID: string,
    amount: number
  ) => {
    try {
      const addData = await this.knex("payment_records")
        .insert([
          {
            paypal_id: paypalID,
            payee_id: payeeID,
            recipient_id: recipientID,
            amount: amount,
          },
        ])
        .returning("*");
      return addData;
    } catch (err) {
      console.log(`Payment Record Service Error: ${err.message}`);
      return undefined;
    }
  };
}
