import bcrypt from 'bcryptjs';

const salt = 10;

export async function hashPW(password: string) {
    const hash = await bcrypt.hash(password, salt)
    return hash
};

export async function checkPW(password: string, hashedPW: string) {
    const check = await bcrypt.compare(password, hashedPW);
    return check;
};
