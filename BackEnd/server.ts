import express from "express";
import AWS from "aws-sdk";
import multer from "multer";
import multerS3 from "multer-s3";
import bodyParser from "body-parser";
import dotenv from "dotenv";
import cors from "cors";
import http from "http";
import socketIO from "socket.io";
import expressSession from "express-session";
import Knex from "knex";
import configs from "./knexfile";
import { UserService } from "./Services/userService";
import { UserController } from "./Controllers/userController";
import { createIsLoggedIn } from "./routeGuard";
import { OrderService } from "./Services/orderService";
import { OrderController } from "./Controllers/orderController";
import { OfferService } from "./Services/offerService";
import { OfferController } from "./Controllers/offerController";
import { RequestService } from "./Services/requestService";
import { RequestController } from "./Controllers/requestController";
import { StreamerReviewsService } from "./Services/streamerReviewsService";
import { DestinationsService } from "./Services/destinationsService";
import { AudienceReviewsService } from "./Services/audienceReviewsService";
import { PaymentRecordsService } from "./Services/paymentRecordsService";
import { LanguageService } from "./Services/languagesService";
import { HashTagService } from "./Services/hashtagService";
import {HashtagController} from "./Controllers/hashtagController";
import { SitesService } from "./Services/siteService";
import { StreamerReviewController } from "./Controllers/streamerReviewController";
import { AudienceReviewsController } from "./Controllers/audienceReviewController";
import { CitiesService } from "./Services/citiesService";
import { BrainTreeController } from "./Controllers/BrainTreeController";
import { CountriesService } from "./Services/countriesService";
import braintree from 'braintree';
import { AgeRangeService } from "./Services/ageRangeService";


// Dotenv setup
dotenv.config();

// Server setup and socketIO setup
const app = express();
const server = new http.Server(app);
export const io = socketIO(server);

// Cross origin resource sharing setup
app.use(cors());

// Multer S3 setup (NEEDS UPDATING!!!!!!!!!!!!!)
const s3 = new AWS.S3({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: "ap-southeast-1",
});

const storage = multerS3({
  s3: s3,
  bucket: "cdn.tecky.hk",
  metadata: (req, file, cb) => {
    cb(null, { fieldName: file.fieldname });
  },
  key: (req, file, cb) => {
    cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`);
  },
});

export const upload = multer({ storage: storage });

// Multer S3 setup (NEEDS UPDATING!!!!!!!!!!!!!)

// For socketIO
const sessionMiddleware = expressSession({
  secret: "There is no secret",
  resave: true,
  saveUninitialized: true,
  cookie: { secure: "auto" },
});

app.use(sessionMiddleware);

io.use((socket, next) => {
  sessionMiddleware(socket.request, socket.request.res, next);
});

// Knex, controller and services
const knex = Knex(configs.development);

// BrainTree setup
const merchantId: any = process.env.BRAINTREE_MERCHANT_ID;
const publicKey: any = process.env.BRAINTREE_PUBLIC_KEY;
const privateKey: any = process.env.BRAINTREE_PRIVATE_KEY;

export const brainTreeGateway = new braintree.BraintreeGateway({  
  environment: braintree.Environment.Sandbox,
  merchantId: merchantId,
  publicKey: publicKey,
  privateKey: privateKey
});

// FrontEnd access to server
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const userService = new UserService(knex);
const offerService = new OfferService(knex);
const requestService = new RequestService(knex);
const streamerReviewsService = new StreamerReviewsService(knex);
const destinationsService = new DestinationsService(knex);
const paymentRecordsService = new PaymentRecordsService(knex);
const audienceReviewsService = new AudienceReviewsService(knex);
const hashtagService = new HashTagService(knex);
const langService = new LanguageService(knex);
const orderService = new OrderService(knex, langService);
const siteService = new SitesService(knex);
const citiesService = new CitiesService(knex);
const countriesService = new CountriesService(knex);
const ageRangeService = new AgeRangeService(knex);
export const isLoggedIn = createIsLoggedIn(userService);
export const userController = new UserController(
  userService,
  streamerReviewsService,
  audienceReviewsService,
  paymentRecordsService,
  offerService,
  requestService,
  orderService,
  langService,
  countriesService,
  citiesService,
  ageRangeService
);
export const offerController = new OfferController(
  offerService,
  destinationsService,
  hashtagService,
  streamerReviewsService,
  audienceReviewsService,
  countriesService,
  citiesService,
  siteService
);
export const requestController = new RequestController(
  requestService,
  hashtagService,
  destinationsService,
  countriesService,
  citiesService,
  siteService
);
export const orderController = new OrderController(
  orderService,
  offerService,
  requestService,
  destinationsService,
  hashtagService,
  paymentRecordsService,
  userService,
  langService
);
export const streamerReviewsController = new StreamerReviewController(
  streamerReviewsService
);
export const audienceReviewsController = new AudienceReviewsController(
  audienceReviewsService
);
export const brainTreeController = new BrainTreeController(paymentRecordsService, orderService, offerService);
export const hashTagController = new HashtagController(
  hashtagService
);

// Routes
import { routes } from "./routes";
app.use("/", routes);

// 24/9 allow access image in database from react by LC
// Hi Marco, need decide using below method or put all in "public folder"
import path from "path";
app.use(
  "/site_journey_pic",
  express.static(path.join(__dirname, "/seeds/seed_sites_pic"))
);

app.use(
  "/site_journey_pic",
  express.static(path.join(__dirname, "/seeds/seed_vacation_pic"))
);

app.use(
  "/cast_profile_pic",
  express.static(path.join(__dirname, "/seeds/seed_profile_pic"))
);
app.use(
  "/cast_sites_pic",
  express.static(path.join(__dirname, "/seeds/seed_sites_pic"))
);
app.use(
  "/cast_vacation_pic",
  express.static(path.join(__dirname, "/seeds/seed_vacation_pic"))
);

const PORT = process.env.PORT || 8080;
server.listen(PORT, () => {
  console.log(`[info] listening to Port: ${PORT}`);
});
